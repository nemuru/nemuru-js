const webpack = require('webpack');
const path = require('path');
const envVars = require('./env');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = (env) => {
  const mode = env.ENV ?? 'test';
  return {
    entry: {
      main: './src/index.ts',
      'load-nemuru': './public/load-nemuru.js',
    },
    output: {
      library: 'Nemuru',
      path: path.resolve(__dirname, 'build'),
      filename: (pathData) => {
        return pathData.chunk.name === 'main' ? envVars[mode].FILENAME : '[name].js';
      },
      chunkFilename: 'nemuru-[name].js',
      clean: true,
    },
    module: {
      rules: [
        {
          test: /\.(js|jsx|ts|tsx)$/,
          exclude: [
            path.resolve(__dirname, 'node_modules'),
            path.resolve(__dirname, 'tests'),
            path.resolve(__dirname, 'widgets'),
          ],
          use: ['babel-loader'],
        },
        {
          test: /\.(css|scss)$/,
          use: ['style-loader', 'css-loader'],
        },
        {
          test: /\.ya?ml$/,
          type: 'json',
          use: 'yaml-loader',
        },
        {
          test: /\.svg$/,
          use: [
            {
              loader: 'svg-url-loader',
              options: {
                limit: 10000,
              },
            },
          ],
        },
      ],
    },
    resolve: {
      extensions: ['.ts', '.tsx', '.js', '.jsx'],
      fallback: {
        path: require.resolve('path-browserify'),
      },
    },
    plugins: [
      // Makes some environment variables available to the JS code.
      new webpack.DefinePlugin({
        ENV: JSON.stringify(envVars[mode].ENV),
        API_URL: JSON.stringify(envVars[mode].API_URL),
        API_TOKEN_URL: JSON.stringify(envVars[mode].API_TOKEN_URL),
        NEMURU_BORROWER_URL: JSON.stringify(envVars[mode].NEMURU_BORROWER_URL),
        CDN_URL: JSON.stringify(envVars[mode].CDN_URL),
        CHECKOUT_HTML_URL: JSON.stringify(envVars[mode].CHECKOUT_HTML_URL),
        LEARN_MORE_HTML_URL: JSON.stringify(envVars[mode].LEARN_MORE_HTML_URL),
        ORDER_MANAGEMENT_HTML_URL: JSON.stringify(envVars[mode].ORDER_MANAGEMENT_HTML_URL),
      }),
      // Generates an `index.html` file with the <script> injected.
      new HtmlWebpackPlugin({
        inject: true,
        template: path.resolve(__dirname, 'public/index.html'),
        favicon: './public/favicon.ico',
        nemuruLibrary: envVars[mode].FILENAME,
        apiUrl: envVars[mode].API_URL,
      }),
    ],
    optimization: {
      splitChunks: {
        cacheGroups: {
          commons: {
            test: /[\\/]node_modules[\\/]/,
            name: 'vendors',
          },
        },
        // cacheGroups: {
        //   commons: {
        //     test: /[\\/]node_modules[\\/]/,
        //     // cacheGroupKey here is `commons` as the key of the cacheGroup
        //     name(module, chunks, cacheGroupKey) {
        //       const moduleFileName = module
        //         .identifier()
        //         .split('/')
        //         .reduceRight((item) => item);
        //       const allChunksNames = chunks.map((item) => item.name).join('~');
        //       return `${cacheGroupKey}-${allChunksNames}-${moduleFileName}`;
        //     },
        //     chunks: 'async',
        //   },
        // },
      },
    },
  };
};
