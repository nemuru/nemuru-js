// load-nemuru.js
// --------------
// This is an auxiliar script used to load nemuru.js library in the browser (by `index.html`),
// as well as providing some utility functions that will be used by html components
// that will call nemuru.js.
//
// This script is included in webpack as an entry-point to ensure it is
// copied to the `/build` directory, so that index.html can call it (automatically injected
// via `html-webpack-plugin`).

// Configuration parameters
// ------------------------

const getURLParameter = (sParam) => {
  const sPageURL = window.location.search.substring(1);
  const sURLVariables = sPageURL.split('&');
  for (let i = 0; i < sURLVariables.length; i++) {
    const sParameterName = sURLVariables[i].split('=');
    if (sParameterName[0] == sParam) {
      return sParameterName[1];
    }
  }
};

const id = getURLParameter('id');

if (id) {
  document.getElementById('checkout_order_id').value = id;
  document.getElementById('order_management_id').value = id;
}

const branding = getURLParameter('branding') ?? 'lending_hub';
const locale = getURLParameter('locale') ?? 'es-ES';
const simulations = document.getElementsByTagName('nemuru-simulation');
for (let i = 0; i < simulations.length; i++) {
  simulations.item(i).setAttribute('style-branding', branding);
}

const apiUrl = document.getElementById('api-url').innerText.replace(/['"]+/g, '');

let username;
let userId;
let password;
if (apiUrl.includes('dev.nemuru.io')) {
  switch (branding) {
    case 'finanpay':
      username = 'finanpay@nemuru.com';
      userId = '4b0763a3-05aa-431f-bb8c-af26f723c161';
      password = '123456';
      break;
    case 'iberia':
      username = 'andre.maia+dev.iberia@nemuru.com';
      userId = '91adff4a-b460-4dde-a4ab-a26ea8b77d9f';
      password = '123456';
      break;
    default:
      username = 'adria.moya+dev.sequra.ecommerce@nemuru.com';
      userId = 'f019db8b-14be-4a70-b53c-fdc8863a7a5c';
      password = '123456';
      break;
  }
}
if (apiUrl.includes('stg.nemuru.io')) {
  switch (branding) {
    // case 'lending_hub':
    //   username = 'calbet.web.api@nemuru.com';
    //   password = 'Rol1KvC2SMIGjq0pvaYP';
    //   userId = 'dbc58515-50a7-482e-8ad3-4fa5e629e7bd';
    //   break;
    // case 'lending_hub':
    //   username = 'comercia.1@nemuru.com';
    //   password = '98u8Bbb7JdgRKb8ugVhSDkHtAmGTgc6u';
    //   userId = '1e7f466b-f96c-4824-b982-1823b1604d71';
    //   break;
    case 'lending_hub':
      username = 'adria.moya+stg.paybylink@nemuru.com';
      password = '123456';
      userId = 'd83cfc95-da94-48c6-8607-27aedfebfc71';
      break;
    case 'finanpay':
      username = 'sipay@nemuru.com';
      password = 'eNb8C3JNDjQmaspWBzaeyt58wYxU8ZRV';
      userId = 'f04700ef-9c48-4522-948b-4d9313e6f0c6';
      break;
    case 'iberia':
      username = 'iberia.web@nemuru.com';
      password = 'BBVvmjtJpsK9757Gk7qzXvfFS6yPLE';
      userId = '494cfa22-8a95-4318-91e2-9d67870ea542';
      break;
    default:
      username = 'ecommerce@nemuru.com';
      userId = '50bfddb1-ee39-46fa-8586-24bf0d3dde58';
      password = '55GaYC6P8Ya75xBhWPB5gAqN';
      break;
  }
}
if (apiUrl.includes('nemuru.com')) {
  switch (branding) {
    default:
      username = 'ecommerce@nemuru.com';
      userId = '50bfddb1-ee39-46fa-8586-24bf0d3dde58';
      password = '55GaYC6P8Ya75xBhWPB5gAqN';
      break;
  }
}
console.log({ apiUrl });
console.log({ username });
console.log({ userId });
console.log({ branding });
console.log({ locale });

// Loading nemuru.js
// -----------------

const nemuruLibrary = document.getElementById('nemuru-library').innerText.replace(/['"]+/g, '');
console.log({ nemuruLibrary });

const params = {
  clientId: userId, // Your merchant ID given by Nemuru.
  scriptUri: nemuruLibrary, // Nemuru Javascript library URI for production, staging or test.
  configuration: {
    currency: 'EUR', // Currency code for chosen locale. Optional, default `EUR`.
    decimalSeparator: '.', // Decimal separator used in currencies formatting. Optional, default `,`.
    thousandSeparator: ',', // Thousand separator used in currencies formatting. Optional, default `.`.
    locale, // Currency code available for chosen locale. Optional, default `EUR`.
  },
  onError: (error) => console.log({ error }), // On error callback.
};

// Option 1
// (function (i, s, o, g, r, a, m) {let n, t; let p = false; n = s.createElement(o); n.type = 'text/javascript'; n.src = g.scriptUri; n.async = true; n.onload = n.onreadystatechange = async function () { if (!p && (!this.readyState || this.readyState == 'complete')) { p = true; Nemuru.init(g)}};t = s.getElementsByTagName(o)[0];t.parentNode.insertBefore(n, t);})( window,  document,"script", params);

// Option 2
const script = document.createElement('script');
script.src = nemuruLibrary;
script.async = true;
script.addEventListener('load', async () => {
  await Nemuru.init(params);
  await calculateConditions();
});
document.body.appendChild(script);

// Login
// -----
// The access_token is necessary when invoking:
// Nemuru.checkout()
// Nemuru.orderManagement()

const getCredentials = async () => {
  const response = await fetch(`${apiUrl}auth/login/`, {
    method: 'POST',
    headers: new Headers({
      'Access-Control-Allow-Origin': '*',
      'Content-Type': 'application/json',
    }),
    body: JSON.stringify({
      username,
      password,
    }),
  });
  return await response.json();
};

getCredentials().then((credentials) => (window.credentials = credentials));

// Calls to nemuru.js
// ------------------

export const calculateConditions = async () => {
  const conditionsReq = {
    conditions: {
      amount: 300,
      product: 'instalments',
      terms: [10, 12],
    },
    onError: (error) => {
      console.log('ON ERROR CALLBACK', error);
    },
  };

  const conditionsRes = await Nemuru.calculateConditions(conditionsReq);

  document.getElementById('conditions-request').innerHTML = JSON.stringify(conditionsReq, undefined, 2);
  document.getElementById('conditions-response').innerHTML = JSON.stringify(conditionsRes, undefined, 2);

  const conditionsReq2 = {
    conditions: {
      amount: 300,
      product: 'split_pay',
      // terms: [3]
    },
    onError: (error) => {
      console.log('ON ERROR CALLBACK', error);
    },
  };

  const conditionsRes2 = await Nemuru.calculateConditions(conditionsReq2);

  document.getElementById('conditions-request-2').innerHTML = JSON.stringify(conditionsReq2, undefined, 2);
  document.getElementById('conditions-response-2').innerHTML = JSON.stringify(conditionsRes2, undefined, 2);

  const conditionsReq3 = { amount: 591 };
  const conditionsRes3 = Nemuru.availableConditions(conditionsReq3);

  document.getElementById('conditions-request-3').innerHTML = JSON.stringify(conditionsReq3, undefined, 2);
  document.getElementById('conditions-response-3').innerHTML = JSON.stringify(conditionsRes3, undefined, 2);

  const conditionsReq4 = { amount: 2600 };
  const conditionsRes4 = Nemuru.availableConditions(conditionsReq4);

  document.getElementById('conditions-request-4').innerHTML = JSON.stringify(conditionsReq4, undefined, 2);
  document.getElementById('conditions-response-4').innerHTML = JSON.stringify(conditionsRes4, undefined, 2);

  const conditionsReq5 = { amount: 591, products: ['split_pay'] };
  const conditionsRes5 = Nemuru.availableConditions(conditionsReq5);

  document.getElementById('conditions-request-5').innerHTML = JSON.stringify(conditionsReq5, undefined, 2);
  document.getElementById('conditions-response-5').innerHTML = JSON.stringify(conditionsRes5, undefined, 2);
};

const loadSimulation = async () => {
  const container = document.getElementById('nemuru-simulation');
  while (container.firstChild) {
    container.removeChild(container.firstChild);
  }

  const amount = document.getElementById('simulation_amount').value;
  const product = document.getElementById('simulation_product').value;
  const variant = document.getElementById('simulation_variant').value;

  const simulation = document.createElement('nemuru-simulation');
  simulation.setAttribute('conditions-amount', parseInt(amount * 100));
  simulation.setAttribute('conditions-product', product);
  simulation.setAttribute('style-color', '#3E6AE1');
  simulation.setAttribute('style-variant', variant);
  simulation.setAttribute('style-branding', branding);
  container.appendChild(simulation);
};

const loadCheckout = async ({ container }) => {
  const orderId = document.getElementById('checkout_order_id').value;
  const expirationTimeInput = document.getElementById('expiration_time_input').value;
  const disableCloseCheck = document.getElementById('disable_close_check').checked;
  const disableFormEditionCheck = document.getElementById('disable_form_edition_check').checked;

  Nemuru.checkout({
    container,
    orderId,
    accessToken: window.credentials.access_token,
    style: {
      color: '#3E6AE1',
      branding: branding,
    },
    behaviour: {
      disableClose: disableCloseCheck,
      expirationTimeInMinutes: expirationTimeInput,
      disableFormEdition: disableFormEditionCheck,
    },
    onStatusChange: (status) => {
      console.log('ON STATUS CHANGE CALLBACK', status);
    },
    onClose: (status) => {
      console.log('ON CLOSE CALLBACK', status);
    },
    onError: (error) => {
      console.log('ON ERROR CALLBACK', error);
    },
  });
};

const loadOrderManagement = async ({ container }) => {
  const orderId = document.getElementById('order_management_id').value;
  console.log('OrderManagement', { orderId, locale, branding });
  Nemuru.orderManagement({
    container,
    orderId,
    accessToken: window.credentials.access_token,
    style: {
      color: '#3E6AE1',
      branding: branding,
    },
    onAction: (action) => {
      console.log('ON ACTION CALLBACK', action);
    },
    onClose: () => {
      console.log('ON CLOSE CALLBACK');
    },
    onError: (error) => {
      console.log('ON ERROR CALLBACK', error);
    },
  });
};

window.loadCheckout = loadCheckout;
window.loadOrderManagement = loadOrderManagement;
window.loadSimulation = loadSimulation;
