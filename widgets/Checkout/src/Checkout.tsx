import { Box } from '@mui/material';
import { getOrderByLoanId, Order, setCredentials, setEnvironment } from '@nemuru-eng/js-api-client';
import React, { useEffect, useRef } from 'react';
import { v4 as uuidv4 } from 'uuid';

import { CheckoutComponentProps } from '../../../src/checkout/Checkout';
import Behaviour from '../../../src/shared/types/Style/Behaviour';
import { Calculator } from './components/Calculator/Calculator';
import { OnboardingDetails } from './components/OnboardingDetails/OnboardingDetails';
import { OnboardingField } from './components/OnboardingDetails/types/OnboardingField';
import { Payment } from './components/Payment/Payment';
import { PhoneVerification } from './components/PhoneVerification/PhoneVerification';
import { CheckoutHeader } from './components/Shared/CheckoutHeader';
import { Countdown } from './components/Shared/Countdown/Countdown';
import { LeaveProcess } from './components/Shared/LeaveProcess/LeaveProcess';
import { Message, MESSAGE_TYPES } from './components/Shared/Message/Message';
import { useCheckoutContext } from './contexts/Checkout/CheckoutContext';
import { useCheckoutProcess } from './hooks/useCheckoutProcess';
import { useDataLayer, CheckoutLocation, DLEvent } from './hooks/useDataLayer';
import { useExpiration } from './hooks/useExpiration';
import './styles.css';
import { ThemeManager } from './styles/ThemeManager';
import { setTranslation, translation } from './translations/Translation';
import { CHECKOUT_STEPS } from './types/CheckoutSteps';
import { Status } from './types/Status';

export const Checkout: React.FC<CheckoutComponentProps> = (props) => {
  const headerRef = useRef(null);

  const {
    env,
    accessToken,
    configuration,
    order: originalOrder,
    loanCalculators,
    amortizationPlans,
    style,
    behaviour: originalBehaviour,
    handleCloseDialog,
    handleStatusChange,
    // handleError,
  } = props;

  const {
    order,
    behaviour,
    lastAction,
    borrowerId,
    checkoutStep,
    updateCheckout,
    setCheckoutStep,
    currentLocation,
    checkoutPreviousStep,
    currentLoanCalculator,
    currentAmortizationPlan,
    disabledFormFields,
  } = useCheckoutContext();

  // const onError = (error: Exception) => {
  //   handleError({ message: error.message, url: error.url, method: error.method, status: error.status });
  // };

  const { setCheckoutStepByAction } = useCheckoutProcess(originalOrder, loanCalculators, amortizationPlans);
  const { pushToDataLayer } = useDataLayer();

  const closeDialog = (status: string, dispatchCallback?: boolean) => {
    pushToDataLayer({ event: DLEvent.WIDGET_CLOSE });
    handleCloseDialog(status, dispatchCallback);
  };

  useEffect(() => {
    setTranslation(configuration.locale, style.branding);
    initCore();

    updateCheckout({
      loanCalculators: loanCalculators?.sort((a, b) => (a.term > b.term ? -1 : 1)),
      amortizationPlans,
      order: originalOrder,
      lastAction: originalOrder.action,
      style,
      behaviour: originalBehaviour,
    });
  }, []);

  const initCore = async () => {
    await setEnvironment(env, configuration);
    await setCredentials({ accessToken, refreshToken: accessToken });
    updateCheckout({ env });
  };

  const pushVisibilityChange = () => {
    if (document.visibilityState === 'visible') {
      pushToDataLayer({ event: DLEvent.WINDOW_VISIBLE, location: currentLocation, orderId: originalOrder.id });
    } else {
      pushToDataLayer({ event: DLEvent.WINDOW_NOT_VISIBLE, location: currentLocation, orderId: originalOrder.id });
    }
  };

  useEffect(() => {
    if (currentLocation) {
      document.addEventListener('visibilitychange', pushVisibilityChange);
      return function cleanup() {
        document.removeEventListener('visibilitychange', pushVisibilityChange);
      };
    }
  }, [currentLocation]);

  useExpiration();

  useEffect(() => {
    if (order) {
      setBorrowerId(order);
      setCheckoutStepByAction(order);
      setDisabledFormFields(behaviour);
      if (
        lastAction &&
        handleStatusChange &&
        Status.createFromAction(order.action).value !== Status.createFromAction(lastAction).value
      ) {
        handleStatusChange(Status.createFromAction(order.action).value);
      }
    }
  }, [order]);

  const setBorrowerId = (order: Order): void => {
    if (borrowerId !== undefined) return;
    let newBorrowerId = uuidv4();
    if (order.customer.borrowerId) {
      newBorrowerId = order.customer.borrowerId;
    }
    updateCheckout({ borrowerId: newBorrowerId });
  };

  const setDisabledFormFields = (behaviour?: Behaviour): void => {
    if (!order || disabledFormFields?.length) return;
    if (behaviour?.disableFormEdition === true) {
      const fieldsToDisable: OnboardingField[] = [];
      for (const customerKey of Object.keys(order.customer)) {
        // @ts-ignore
        if (!!order.customer[customerKey]) {
          fieldsToDisable.push(customerKey as OnboardingField);
        }
      }
      updateCheckout({ disabledFormFields: fieldsToDisable });
    }
  };

  if (!order || !checkoutStep || !translation) return null;

  return (
    <div
      id={'popup-wrapper'}
      className={'popup-wrapper'}
      style={{ background: style.variant.value === 'embedded' ? 'rgba(255, 255, 255, 1)' : '' }}
    >
      <div
        id={'popup-content'}
        className={'popup-content'}
        style={{
          boxShadow: style.variant.value === 'embedded' ? 'rgb(0 0 0 / 10%) 0px 10px 60px 0px' : 'none',
        }}
      >
        <Box sx={{ px: { xs: 4, sm: 5 }, height: '100%' }}>
          <ThemeManager color={style.color} branding={style.branding}>
            <CheckoutHeader headerRef={headerRef} handleCloseDialog={closeDialog} order={order} />
            <Box
              className={'appear-anim step-content'} // @ts-ignore
              height={`calc(100% - ${headerRef?.current?.offsetHeight}px)`}
            >
              <Countdown />
              <LeaveProcess onClose={() => closeDialog(Status.DESISTED)} />
              {checkoutStep === CHECKOUT_STEPS.CALCULATOR && headerRef && (
                <>
                  {currentLoanCalculator ? (
                    <Calculator
                      onSubmit={() => {
                        setCheckoutStep(CHECKOUT_STEPS.ONBOARDING_DETAILS);
                      }}
                    />
                  ) : (
                    <Message
                      messageType={MESSAGE_TYPES.EMPTY_LOAN_CONDITIONS}
                      buttons={[
                        {
                          title: translation.CLOSE,
                          onClick: () => {
                            pushToDataLayer({
                              event: DLEvent.BTN_CLICK,
                              location: CheckoutLocation.EMPTY_LOAN_CONDITIONS,
                              additionalPayload: {
                                type: 'close',
                                label: translation?.CLOSE,
                              },
                            });
                            closeDialog(Status.createFromAction(order.action).value);
                          },
                        },
                      ]}
                    />
                  )}
                </>
              )}
              {checkoutStep === CHECKOUT_STEPS.ONBOARDING_DETAILS && headerRef && (
                <OnboardingDetails
                  onSubmit={() => {
                    setCheckoutStep(CHECKOUT_STEPS.PHONE_VERIFICATION);
                  }}
                />
              )}
              {checkoutStep === CHECKOUT_STEPS.PHONE_VERIFICATION && headerRef && (
                <PhoneVerification
                  onSubmit={() => {
                    setCheckoutStep(CHECKOUT_STEPS.PAYMENT);
                  }}
                />
              )}
              {checkoutStep === CHECKOUT_STEPS.PAYMENT && headerRef && (
                <Payment
                  onSubmit={() => {
                    closeDialog(Status.createFromAction(order.action).value);
                  }}
                />
              )}
              {checkoutStep &&
                [CHECKOUT_STEPS.ERROR, CHECKOUT_STEPS.ERROR_PHONE_VERIFICATION].includes(checkoutStep) &&
                headerRef && (
                  <Message
                    messageType={MESSAGE_TYPES.ERROR}
                    buttons={[
                      {
                        title: translation.RETRY,
                        onClick: async () => {
                          pushToDataLayer({
                            event: DLEvent.BTN_CLICK,
                            location: CheckoutLocation.ERROR,
                            additionalPayload: { type: 'retry', label: translation?.RETRY },
                          });
                          const newOrder = await getOrderByLoanId({ loanId: order?.id });
                          if (newOrder.action !== order?.action) {
                            updateCheckout({ order: newOrder });
                          } else {
                            checkoutPreviousStep && setCheckoutStep(checkoutPreviousStep);
                          }
                        },
                        props: {
                          disablePoweredBy: true,
                        },
                      },
                      {
                        title: translation.CLOSE,
                        onClick: () => {
                          pushToDataLayer({
                            event: DLEvent.BTN_CLICK,
                            location: CheckoutLocation.ERROR,
                            additionalPayload: { type: 'close', label: translation?.CLOSE },
                          });
                          closeDialog(Status.createFromAction(order.action).value);
                        },
                        props: {
                          variant: 'text',
                        },
                      },
                    ]}
                  />
                )}
              {checkoutStep === CHECKOUT_STEPS.DENIED && headerRef && (
                <Message
                  messageType={MESSAGE_TYPES.DENIED}
                  buttons={[
                    {
                      title: translation.CLOSE,
                      onClick: () => {
                        pushToDataLayer({
                          event: DLEvent.BTN_CLICK,
                          location: CheckoutLocation.DENIED,
                          additionalPayload: { type: 'close', label: translation?.CLOSE },
                        });
                        closeDialog(Status.createFromAction(order.action).value);
                      },
                    },
                  ]}
                />
              )}
              {checkoutStep === CHECKOUT_STEPS.EXPIRED && headerRef && (
                <Message
                  messageType={MESSAGE_TYPES.EXPIRED}
                  buttons={[
                    {
                      title: translation.CLOSE,
                      onClick: () => {
                        pushToDataLayer({
                          event: DLEvent.BTN_CLICK,
                          location: CheckoutLocation.EXPIRED,
                          additionalPayload: { type: 'close', label: translation?.CLOSE },
                        });
                        closeDialog(Status.createFromAction(order.action).value);
                      },
                    },
                  ]}
                />
              )}
              {checkoutStep === CHECKOUT_STEPS.CANCELLED && headerRef && (
                <Message
                  messageType={MESSAGE_TYPES.CANCELLED}
                  buttons={[
                    {
                      title: translation.CLOSE,
                      onClick: () => {
                        pushToDataLayer({
                          event: DLEvent.BTN_CLICK,
                          location: CheckoutLocation.CANCELLED,
                          additionalPayload: { type: 'close', label: translation?.CLOSE },
                        });
                        closeDialog(Status.createFromAction(order.action).value);
                      },
                    },
                  ]}
                />
              )}
              {checkoutStep === CHECKOUT_STEPS.CONFIRMED && headerRef && (
                <Message
                  messageType={MESSAGE_TYPES.CONFIRMED}
                  data={{ currentAmortizationPlan }}
                  buttons={[
                    {
                      title: translation.CLOSE,
                      onClick: () => {
                        pushToDataLayer({
                          event: DLEvent.BTN_CLICK,
                          location: CheckoutLocation.CONFIRMED,
                          additionalPayload: { type: 'close', label: translation?.CLOSE },
                        });
                        closeDialog(Status.createFromAction(order.action).value);
                      },
                    },
                  ]}
                />
              )}
              {checkoutStep === CHECKOUT_STEPS.ON_HOLD && headerRef && (
                <Message
                  messageType={MESSAGE_TYPES.ON_HOLD}
                  buttons={[
                    {
                      title: translation.CLOSE,
                      onClick: () => {
                        pushToDataLayer({
                          event: DLEvent.BTN_CLICK,
                          location: CheckoutLocation.ON_HOLD,
                          additionalPayload: { type: 'close', label: translation?.CLOSE },
                        });
                        closeDialog(Status.createFromAction(order.action).value);
                      },
                    },
                  ]}
                />
              )}
              {checkoutStep === CHECKOUT_STEPS.TIMEOUT && headerRef && (
                <Message
                  messageType={MESSAGE_TYPES.TIMEOUT}
                  buttons={[
                    {
                      title: translation.CLOSE,
                      onClick: () => {
                        pushToDataLayer({
                          event: DLEvent.BTN_CLICK,
                          location: CheckoutLocation.TIMEOUT,
                          additionalPayload: { type: 'close', label: translation?.CLOSE },
                        });
                        closeDialog(Status.createFromAction(order.action).value);
                      },
                    },
                  ]}
                />
              )}
              {checkoutStep === CHECKOUT_STEPS.PROCESSING && headerRef && (
                <Message messageType={MESSAGE_TYPES.PROCESSING} />
              )}
            </Box>
          </ThemeManager>
        </Box>
      </div>
    </div>
  );
};

export default Checkout;
