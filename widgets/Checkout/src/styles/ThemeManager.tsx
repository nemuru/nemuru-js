import { Theme } from '@mui/material/styles';
import React from 'react';
import { createTheme, responsiveFontSizes, ThemeProvider } from '@mui/material';

import ThemeOverrides from '../types/ThemeOverrides';
import base from './themes/base';

export const ThemeManager = (props: Props) => {
  let brandingTheme: (Theme & ThemeOverrides) | undefined = undefined;
  try {
    // eslint-disable-next-line @typescript-eslint/no-var-requires
    brandingTheme = require(`./themes/${props.branding.value}.ts`).default;
    // eslint-disable-next-line no-empty
  } catch (e) {}
  const customTheme: Theme & ThemeOverrides = {
    ...base,
    ...brandingTheme,
    palette: { ...base.palette, ...brandingTheme?.palette },
    typography: { ...base.typography, ...brandingTheme?.typography },
    components: { ...base.components, ...brandingTheme?.components },
  };
  if (!customTheme.themeOverridesInputColor) {
    customTheme.palette.primary.main = props.color.value;
    customTheme.palette.primary.dark = props.color.value;
  }
  let theme = createTheme(customTheme);
  theme = responsiveFontSizes(theme, { factor: 1 });
  return <ThemeProvider theme={theme}>{props.children}</ThemeProvider>;
};

interface Props {
  color: { value: string };
  branding: { value: string };
  children: JSX.Element[];
}
