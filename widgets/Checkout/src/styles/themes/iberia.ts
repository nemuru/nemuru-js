import { Theme } from '@mui/material/styles';

import ThemeOverrides from '../../types/ThemeOverrides';

const iberia: Theme & ThemeOverrides = {
  themeOverridesInputColor: true,
  // @ts-ignore
  palette: {
    common: {
      black: '#333333',
      white: '#FFF',
    },
    primary: {
      main: '#D7192D',
      contrastText: '#FFF',
      light: '#D7192D',
      dark: '#A8191A',
    },
    secondary: {
      main: '#D7192D',
      contrastText: '#FFF',
      light: '#A8191A',
      dark: '#A8191A',
    },
    text: {
      primary: '#333333',
      secondary: '#666666',
      disabled: '#666666',
    },
    // @ts-ignore
    grey: {
      A100: '#F8F8F8',
      A200: '#EEEEEE',
      A400: '#CCCCCC',
      A700: '#616161',
    },
  },
  // @ts-ignore
  typography: {
    fontFamily: 'Iberia text, arial-fallback',
    h1: {
      fontStyle: 'normal',
      fontWeight: 'bold',
      fontSize: '32px',
      // lineHeight: '44px',
      '@media (max-width:425px)': {
        fontSize: '30px',
      },
    },
    h2: {
      fontStyle: 'normal',
      fontWeight: 'bold',
      fontSize: '28px',
      // lineHeight: '40px',
      '@media (max-width:425px)': {
        fontSize: '26px',
      },
    },
    h3: {
      fontStyle: 'normal',
      fontWeight: 600,
      fontSize: '24px',
      // lineHeight: '32px',
      '@media (max-width:425px)': {
        fontSize: '22px',
      },
    },
    h4: {
      fontStyle: 'normal',
      fontWeight: 'normal',
      fontSize: '22px',
      fontFamily: 'Iberia headline, arial-fallback',
      // lineHeight: '36px',
      letterSpacing: '0.03em',
      '@media (max-width:425px)': {
        fontSize: '20px',
      },
    },
    h5: {
      fontStyle: 'normal',
      fontWeight: 'normal',
      fontSize: '18px',
      fontFamily: 'Iberia headline, arial-fallback',
      // lineHeight: '26px',
      letterSpacing: '0.02em',
      '@media (max-width:425px)': {
        fontSize: '15px',
      },
    },
    h6: {
      fontStyle: 'normal',
      fontWeight: 600,
      fontSize: '14px',
      // lineHeight: '24px',
      letterSpacing: '0.02em',
    },
    body1: {
      fontStyle: 'normal',
      fontWeight: 400,
      fontSize: '16px',
      // lineHeight: '26px',
      letterSpacing: '0.01em',
      '@media (max-width:425px)': {
        fontSize: '15px',
      },
    },
    body2: {
      fontStyle: 'normal',
      fontWeight: 400,
      fontSize: '15px',
      // lineHeight: '24px',
      '@media (max-width:425px)': {
        fontSize: '14px',
      },
    },
    caption: {
      fontStyle: 'normal',
      fontWeight: 400,
      fontSize: '13px',
      // lineHeight: '20px',
    },
    // fontFamily: '"Open Sans", sans-serif',
  },
  shape: {
    borderRadius: 0,
  },
  components: {
    MuiTypography: {
      styleOverrides: {
        root: {
          color: '#333333',
        },
      },
    },
    MuiCheckbox: {
      styleOverrides: {
        root: {
          textTransform: 'none',
          color: '#666666',
        },
      },
    },
    MuiButton: {
      styleOverrides: {
        root: {
          textTransform: 'none',
          ':hover': {
            backgroundColor: '#F8F8F8',
          },
        },
      },
      variants: [
        {
          props: { variant: 'contained', disabled: false },
          style: {
            color: '#FFF',
            padding: '12px 16px',
          },
        },
        {
          props: { variant: 'contained', disabled: true },
          style: {
            padding: '12px 16px',
          },
        },
        {
          props: { variant: 'text', disabled: false },
          style: {
            color: '#333333',
            textDecoration: 'underline',
            padding: '12px 16px',
            ':hover': {
              textDecoration: 'underline',
            },
          },
        },
        {
          // @ts-ignore
          props: { variant: 'link', color: 'primary' },
          style: ({ theme }) => ({
            color: theme.palette.primary.main,
            fontSize: 'inherit',
            textDecoration: 'underline',
            backgroundColor: 'transparent',
            padding: '0px 2px',
            bottom: '1px',
            borderRadius: '100px',
            ':hover': {
              textDecoration: 'underline',
              backgroundColor: 'transparent',
            },
          }),
        },
      ],
    },
  },
};

export default iberia;
