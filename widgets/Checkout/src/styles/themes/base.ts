import { Theme } from '@mui/material/styles';

import ThemeOverrides from '../../types/ThemeOverrides';

const base: Theme & ThemeOverrides = {
  themeOverridesInputColor: false,
  palette: {
    common: {
      black: '#010007',
      white: '#FFF',
    },
    primary: {
      main: '#3E6AE1',
      contrastText: '#FFF',
      light: '#3E6AE1',
      dark: '#3E6AE1',
    },
    secondary: {
      main: '#010007',
      contrastText: '#FFF',
      light: '#E1E1E5',
      dark: '#010007',
    },
    warning: {
      main: '#FFCA28',
      contrastText: '#FFF',
      light: 'rgba(255, 202, 40, 0.15)',
      dark: 'rgba(255, 202, 40, 1)',
    },
    info: {
      main: '#36a3f7',
      contrastText: '#ffffff',
      light: '#E1F1FD',
      dark: '#E1F1FD',
    },
    success: {
      main: '#4dd970',
      contrastText: '#ffffff',
      light: 'rgba(73,217,106,0.15)',
      dark: 'rgba(73,217,106,0.15)',
    },
    error: {
      main: '#ef5957',
      contrastText: '#ffffff',
      light: 'rgba(239,87,87,0.15)',
      dark: 'rgba(239,87,87,0.15)',
    },
    text: {
      primary: '#010007',
      secondary: '#9B97B3',
      disabled: '#BDBDBD',
      // disabled: '#F0F0F5',
    },
    // @ts-ignore
    grey: {
      A100: '#F8F8F8',
      A200: '#EEEEEE',
      A400: '#BDBDBD',
      A700: '#616161',
    },
  },
  // @ts-ignore
  typography: {
    fontFamily: '"Poppins", sans-serif',
    h1: {
      fontStyle: 'normal',
      fontWeight: 'bold',
      fontSize: '32px',
      // lineHeight: '44px',
      '@media (max-width:425px)': {
        fontSize: '30px',
      },
    },
    h2: {
      fontStyle: 'normal',
      fontWeight: 'bold',
      fontSize: '28px',
      // lineHeight: '40px',
      '@media (max-width:425px)': {
        fontSize: '26px',
      },
    },
    h3: {
      fontStyle: 'normal',
      fontWeight: 600,
      fontSize: '24px',
      // lineHeight: '32px',
      '@media (max-width:425px)': {
        fontSize: '22px',
      },
    },
    h4: {
      fontStyle: 'normal',
      fontWeight: 600,
      fontSize: '22px',
      // lineHeight: '36px',
      letterSpacing: '0.03em',
      '@media (max-width:425px)': {
        fontSize: '20px',
      },
    },
    h5: {
      fontStyle: 'normal',
      fontWeight: 600,
      fontSize: '18px',
      // lineHeight: '26px',
      letterSpacing: '0.02em',
      '@media (max-width:425px)': {
        fontSize: '16px',
      },
    },
    h6: {
      fontStyle: 'normal',
      fontWeight: 600,
      fontSize: '14px',
      // lineHeight: '24px',
      letterSpacing: '0.02em',
    },
    body1: {
      fontStyle: 'normal',
      fontWeight: 400,
      fontSize: '16px',
      // lineHeight: '26px',
      letterSpacing: '0.01em',
      '@media (max-width:425px)': {
        fontSize: '15px',
      },
    },
    body2: {
      fontStyle: 'normal',
      fontWeight: 400,
      fontSize: '15px',
      // lineHeight: '24px',
      '@media (max-width:425px)': {
        fontSize: '14px',
      },
    },
    caption: {
      fontStyle: 'normal',
      fontWeight: 400,
      fontSize: '13px',
      // lineHeight: '20px',
      '@media (max-width:425px)': {
        fontSize: '13px',
      },
    },
    // fontFamily: '"Open Sans", sans-serif',
  },
  shape: {
    borderRadius: 4,
  },
  components: {
    // MuiForm,
    MuiButton: {
      styleOverrides: {
        root: {
          textTransform: 'none',
          ':hover': {
            backgroundColor: '#F8F8F8',
          },
        },
      },
      variants: [
        {
          props: { variant: 'contained', disabled: false },
          style: {
            color: '#FFF',
            padding: '12px 16px',
          },
        },
        {
          props: { variant: 'contained', disabled: true },
          style: {
            padding: '12px 16px',
          },
        },
        {
          props: { variant: 'text', disabled: false },
          style: {
            padding: '12px 16px',
          },
        },
        {
          props: { variant: 'text', disabled: true },
          style: {
            padding: '12px 16px',
          },
        },
        {
          // @ts-ignore
          props: { variant: 'link', color: 'primary' },
          style: ({ theme }) => ({
            color: theme.palette.primary.main,
            fontSize: 'inherit',
            textDecoration: 'underline',
            backgroundColor: 'transparent',
            padding: '0px 2px',
            bottom: '1px',
            borderRadius: '100px',
            ':hover': {
              textDecoration: 'underline',
              backgroundColor: 'transparent',
            },
          }),
        },
      ],
    },
    MuiFormLabel: {
      styleOverrides: {
        root: {
          color: '#010007',
        },
      },
    },
    MuiInputBase: {
      styleOverrides: {
        root: {
          // borderBottom: '2px solid #010007',
          fontSize: '16px',
        },
      },
    },
    MuiInput: {
      styleOverrides: {
        root: {
          ':before': {
            borderBottom: 'none',
          },
          ':after': {
            borderBottom: '1px solid #010007',
          },
          ':hover:not(.Mui-disabled):before': {
            borderBottom: 'none',
          },
          '&.Mui-error:after': { borderBottom: 'none' },
        },
        input: {
          paddingTop: '8px',
          paddingBottom: '16px',
        },
      },
    },
  },
};

export default base;
