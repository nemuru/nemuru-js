import { Action, AmortizationPlanDto, Environment, LoanCalculatorDto, Order, Otp } from '@nemuru-eng/js-api-client';
import React, { createContext, ReactElement, useContext } from 'react';

import Behaviour from '../../../../../src/shared/types/Style/Behaviour';
import Style from '../../../../../src/shared/types/Style/Style';
import { OnboardingField } from '../../components/OnboardingDetails/types/OnboardingField';
import { LEAVE_PROCESS_STEP } from '../../components/Shared/LeaveProcess/LeaveProcess';
import { CheckoutLocation } from '../../hooks/useDataLayer';
import { Customer } from '../../types/Customer';
import { initialState } from './reducer';
import useCheckout from './useCheckout';

export interface CheckoutContextState {
  otp?: Otp;
  env?: Environment;
  order?: Order;
  style?: Style;
  dialCode?: string;
  customer?: Customer;
  sessionId?: string;
  behaviour?: Behaviour;
  lastAction?: Action;
  borrowerId?: string;
  checkoutStep?: string;
  countdownShow: boolean;
  expirationDate?: Date;
  validationCode?: string;
  currentLocation?: CheckoutLocation;
  cancelExpiration: boolean;
  fieldsWithErrors?: OnboardingField[];
  headerCloseEnabled: boolean;
  leaveProcessDialog?: LEAVE_PROCESS_STEP;
  checkoutPreviousStep?: string;
  loanCalculators?: LoanCalculatorDto[];
  amortizationPlans?: AmortizationPlanDto[];
  currentLoanCalculator?: LoanCalculatorDto;
  currentAmortizationPlan?: AmortizationPlanDto;
  disabledFormFields?: OnboardingField[];
  updateCheckout: (checkout: Partial<CheckoutContextState>) => void;
  setValidationCode: (validationCode: string) => void;
  setCheckoutStep: (step: string) => void;
}

const useCheckoutContext = (): CheckoutContextState => {
  return useContext(CheckoutContext);
};

const CheckoutContext = createContext<CheckoutContextState>(initialState);

const CheckoutContextProvider = ({ children }: { children: ReactElement }) => {
  const { state, updateCheckout, setCheckoutStep, setValidationCode } = useCheckout();

  return (
    <>
      <CheckoutContext.Provider
        value={{
          ...state,
          updateCheckout,
          setCheckoutStep,
          setValidationCode,
        }}
      >
        {children}
      </CheckoutContext.Provider>
    </>
  );
};

export { CheckoutContextProvider, useCheckoutContext };
