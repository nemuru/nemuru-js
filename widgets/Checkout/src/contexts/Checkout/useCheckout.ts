import { useReducer } from 'react';

import { actions, initialState, reducer } from './reducer';

const useCheckout = () => {
  const [state, dispatch] = useReducer(reducer, initialState);
  const { updateCheckout, setCheckoutStep, setValidationCode } = actions(dispatch);

  return {
    state,
    setCheckoutStep,
    setValidationCode,
    updateCheckout,
  };
};

export default useCheckout;
