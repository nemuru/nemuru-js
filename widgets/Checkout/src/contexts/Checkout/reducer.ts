import { Dispatch } from 'react';
import { v4 as uuidv4 } from 'uuid';

import { CheckoutContextState } from './CheckoutContext';

const ACTIONS = {
  UPDATE_CONTEXT: 'UPDATE_CONTEXT',
  SET_CHECKOUT_STEP: 'SET_CHECKOUT_STEP',
  SET_VALIDATION_CODE: 'SET_VALIDATION_CODE',
};

interface ActionType {
  type: string;
  payload: unknown;
}

export const initialState: CheckoutContextState = {
  env: undefined,
  otp: undefined,
  style: undefined,
  dialCode: undefined,
  customer: undefined,
  sessionId: uuidv4(),
  borrowerId: undefined,
  checkoutStep: undefined,
  countdownShow: false,
  expirationDate: undefined,
  validationCode: undefined,
  loanCalculators: undefined,
  currentLocation: undefined,
  cancelExpiration: false,
  fieldsWithErrors: undefined,
  headerCloseEnabled: true,
  leaveProcessDialog: undefined,
  amortizationPlans: undefined,
  checkoutPreviousStep: undefined,
  currentLoanCalculator: undefined,
  currentAmortizationPlan: undefined,
  disabledFormFields: undefined,
  updateCheckout: () => {},
  setValidationCode: () => {},
  setCheckoutStep: () => {},
};

export const reducer = (state: typeof initialState, action: ActionType): CheckoutContextState => {
  switch (action.type) {
    case ACTIONS.UPDATE_CONTEXT:
      return {
        ...state,
        ...(action.payload as Partial<CheckoutContextState>),
      };
    case ACTIONS.SET_VALIDATION_CODE:
      return {
        ...state,
        validationCode: action.payload as string,
      };
    case ACTIONS.SET_CHECKOUT_STEP:
      return {
        ...state,
        checkoutStep: action.payload as string,
        checkoutPreviousStep: state.checkoutStep as string,
      };
    default:
      return state;
  }
};

export const actions = (dispatch: Dispatch<ActionType>) => ({
  updateCheckout: (checkout: Partial<CheckoutContextState>) =>
    dispatch({ type: ACTIONS.UPDATE_CONTEXT, payload: checkout }),

  setValidationCode: (validationCode: string) =>
    dispatch({
      type: ACTIONS.SET_VALIDATION_CODE,
      payload: validationCode,
    }),

  setCheckoutStep: (step: string) => {
    dispatch({
      type: ACTIONS.SET_CHECKOUT_STEP,
      payload: step,
    });
  },
});
