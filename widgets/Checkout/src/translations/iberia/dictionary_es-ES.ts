import dicitonary_esES_default from '../default/dictionary_es-ES';

const dictionary_esES: Partial<typeof dicitonary_esES_default> = {
  SPLIT_PAY: 'Divide en tres cuotas',
  INSTALMENTS: 'Pago en cuotas',
  PAYMENT_PLAN_SPLIT_PAY_1: 'Así queda tu plan de pagos:',
  PAYMENT_PLAN_SPLIT_PAY_2: 'meses sin intereses:',
  FINANCING_INFO: 'Detalle de tu compra',
  CONDITIONS_EXAMPLE_1: 'Ejemplo Informativo de compra de un billete por importe de',
  CONDITIONS_EXAMPLE_11: 'del importe del billete y',
  ADDRESS_ERROR: 'Se ha producido un error con los datos de tu dirección de residencia. Por favor, revísalos.',
  EMAIL_AND_PHONE_NUMBER:
    'Necesitamos tu e-mail y teléfono móvil para verificar tu identidad. A continuación te enviaremos un SMS con un código para continuar con el proceso.',
  EMAIL: 'E-mail',
  EMAIL_ERROR: 'El e-mail introducido no es válido',
  CONTACT_INFO_ERROR:
    'Se ha producido un error al validar tu información. Por favor, revisa los datos introducidos y vuelve a intentarlo.',
  INTRODUCE_PAYMENT_CARD: 'Introduce los datos de tu tarjeta para realizar el pago de la primera cuota:',
  EDIT_PHONE_NUMBER: 'Edita tu número de teléfono móvil',
  NEW_OTP: 'Te enviaremos un nuevo código al guardar el número de teléfono móvil nuevo',
  VERIFY_YOUR_PHONE_NUMBER: 'Verifica tu teléfono móvil',
  INTRODUCE_OTP: 'Introduce el código de cinco dígitos que hemos enviado a tu teléfono móvil.',
  OTP_ERROR_RETRY: 'El código no es válido. Por favor, revísalo y vuélvelo a intentar.',
  OTP_ERROR_RETRY_2: 'El código no es válido. Por favor, revísalo o haz click en',
  OTP_ERROR_RETRY_3: 'para recibir un nuevo código.',
  APPLICATION_EXPIRATION_IN: 'Tu solicitud caduca en:',
  ORDER_CONFIRMATION_NOT_COMPLETED:
    'He autorizado la compra con la aplicación de mi banco, pero no la he podido completar',
  DO_NOT_UNDERSTAND_FUTURE_PAYMENTS: 'No entiendo cómo se cobrarán los pagos futuros',
  WAIT_WHILE_VERIFYING: 'Estamos verificando tu número de teléfono móvil. Por favor, espera unos segundos...',
  WAIT_WHILE_PROCESSING: 'Estamos procesando tu solicitud. Por favor, espera unos segundos...',
  PAYMENT_CONFIRMED: '¡Pago realizado!',
  PAYMENT_CONFIRMED_DETAIL:
    'Tu compra ha sido confirmada y el pago de la primera cuota se ha procesado correctamente. Recibirás un e-mail con las condiciones de financiación.',
  OOOPS: 'Lo sentimos',
  DENIED_DETAIL: 'No hemos podido tramitar la operación',
  ANOTHER_OPTION: 'Elige otra opción',
  ENCOURAGE_ANOTHER_METHOD: 'Recuerda que puedes elegir otro método de pago para finalizar tu compra',
  ORDER_CANCELLED: 'Tu pedido se ha cancelado',
  OFF_HOURS_DETAIL:
    'Si has realizado la compra después de las 21h recibirás respuesta a primera hora de la mañana (a las 10h de lunes a sábado).',
  SEND_EMAIL: 'Te enviaremos un e-mail',
  SEND_EMAIL_DETAIL:
    'En breve recibirás un e-mail con la respuesta a tu solicitud. Es posible que nuestro equipo se ponga en contacto contigo para validar algunos datos.',
  UNDERAGE_ERROR: 'La edad no puede ser inferior a 18 años',
  TIMEOUT_TITLE: 'Lo sentimos',
  TIMEOUT_DETAIL: 'La confirmación está tardando un poco más de lo habitual',
  EMPTY_LOAN_CONDITIONS: 'Lo sentimos: no hay opciones de financiación disponibles',
  EMPTY_LOAN_CONDITIONS_DETAIL:
    'No hemos encontrado opciones de financiación disponibles en nuestras entidades financieras.',
};

export default dictionary_esES;
