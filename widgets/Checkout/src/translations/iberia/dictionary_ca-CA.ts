import dicitonary_caCA_default from '../default/dictionary_ca-CA';

const dictionary_caCA: Partial<typeof dicitonary_caCA_default> = {
  CONDITIONS_EXAMPLE_1: 'Exemple Informatiu de compra d\'un bitllet per import de',
  CONDITIONS_EXAMPLE_11: 'de l\'import del bitllet i',
};

export default dictionary_caCA;
