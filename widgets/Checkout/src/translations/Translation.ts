import { Locale } from '@nemuru-eng/js-api-client';
import Branding from '../../../../src/shared/types/Style/Branding';
import dictionary_esES from './default/dictionary_es-ES';

export let translation: typeof dictionary_esES | undefined = undefined;

export const setTranslation = (locale: Locale, branding: Branding) => {
  let defaultTranslation: typeof dictionary_esES;
  let brandingTranslation: Partial<typeof dictionary_esES> | undefined = undefined;
  defaultTranslation = require(`./default/dictionary_${locale}.ts`)?.default;
  try {
    brandingTranslation = require(`./${branding.value}/dictionary_${locale}.ts`)?.default;
  } catch (e) {}
  translation = { ...defaultTranslation, ...(brandingTranslation && brandingTranslation) };
};
