import React from 'react';
import { CheckoutComponentProps } from '../../../src/checkout/Checkout';
import Checkout from './Checkout';
import ErrorBoundary from './components/Shared/ErrorBoundary';
import { CheckoutContextProvider } from './contexts/Checkout/CheckoutContext';
import './styles.css';

const App: React.FC<CheckoutComponentProps> = (props) => {
  return (
    <ErrorBoundary onError={props.handleError}>
      <CheckoutContextProvider>
        <Checkout {...props} />
      </CheckoutContextProvider>
    </ErrorBoundary>
  );
};

export default App;
