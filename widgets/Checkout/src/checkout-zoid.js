import * as zoid from 'zoid/dist/zoid';

const CheckoutZoid = zoid.create({
  tag: 'checkout-component', // This has to be unique per js loaded on the page
  url: `http://localhost:4003/checkout.html`,
  props: {
    env: {
      type: 'string',
      required: true,
    },
    accessToken: {
      type: 'string',
      required: true,
    },
    configuration: {
      type: 'object',
      required: true,
    },
    order: {
      type: 'object',
      required: true,
    },
    loanCalculators: {
      type: 'array',
      required: true,
    },
    style: {
      type: 'object',
      required: true,
    },
    handleStatusChange: {
      type: 'function',
      required: false,
    },
    handleCloseDialog: {
      type: 'function',
      required: true,
    },
  },
});

export default CheckoutZoid;
