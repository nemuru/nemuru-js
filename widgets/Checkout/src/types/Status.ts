import { Action } from '@nemuru-eng/js-api-client';

export class Status {
  static readonly INITIAL = 'initial';
  static readonly APPROVED = 'approved';
  static readonly CONFIRMED = 'confirmed';
  static readonly ON_HOLD = 'on_hold';
  static readonly DENIED = 'denied';
  static readonly CANCELLED = 'cancelled';
  static readonly EXPIRED = 'expired';
  static readonly DESISTED = 'desisted';

  static readonly ALLOWED_VALUES = [
    Status.INITIAL,
    Status.APPROVED,
    Status.CONFIRMED,
    Status.ON_HOLD,
    Status.DENIED,
    Status.CANCELLED,
    Status.EXPIRED,
    Status.DESISTED,
  ];

  static readonly ACTION_MAP = {
    initial: Status.INITIAL,
    require_validate_otp: Status.INITIAL,
    require_payment: Status.INITIAL,
    on_hold: Status.ON_HOLD,
    approved: Status.APPROVED,
    denied: Status.DENIED,
    confirmed: Status.CONFIRMED,
    cancelled: Status.CANCELLED,
    expired: Status.EXPIRED,
    desisted: Status.DESISTED,
  };

  constructor(readonly value: string) {
    this.guard(value);
    this.value = value;
  }

  public static createFromAction(action: Action): Status {
    const status = Status.ACTION_MAP[action];
    return new Status(status);
  }

  private guard(value: string): void {
    // @ts-ignore
    if (!Status.ALLOWED_VALUES.includes(value)) {
      throw new Error(
        `<${
          this.constructor.name
        }> does not allow the value: "${value}". Allowed values are: [${Status.ALLOWED_VALUES.join(', ')}]`,
      );
    }
  }
}
