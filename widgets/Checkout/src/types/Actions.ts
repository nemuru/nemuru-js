import { Action } from '@nemuru-eng/js-api-client';

export const ACTIONS_TO_CLOSE_SCREEN = [
  Action.DENIED,
  Action.DESISTED,
  Action.EXPIRED,
  Action.CANCELLED,
  Action.ON_HOLD,
];

export const ACTIONS_TO_RESOLVE_SCREEN = [...ACTIONS_TO_CLOSE_SCREEN, Action.CONFIRMED];
