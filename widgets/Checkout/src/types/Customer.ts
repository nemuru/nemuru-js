import { DeclaredAddress, PhoneNumber } from '@nemuru-eng/js-api-client';

export interface Customer {
  firstName: string;
  lastName: string;
  secondLastName?: string;
  email?: string;
  phoneNumber?: PhoneNumber;
  documentNumber?: string;
  birthDate?: Date;
  requireAddress: boolean;
  address?: DeclaredAddress;
}
