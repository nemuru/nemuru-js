import { Box, Typography } from '@mui/material';
import { FeePaymentType, patchSetLoanCalculator, Product, Program } from '@nemuru-eng/js-api-client';
import React, { useEffect, useState } from 'react';

import { useCheckoutContext } from '../../contexts/Checkout/CheckoutContext';
import { useDataLayer, CheckoutLocation, DLEvent } from '../../hooks/useDataLayer';
import { translation } from '../../translations/Translation';
import { CHECKOUT_STEPS } from '../../types/CheckoutSteps';
import { AmortizationDetails } from '../Shared/AmortizationDetails';
import { MainButton } from '../Shared/MainButton';
import { PaymentDetails } from '../Shared/PaymentDetails';
import { PaymentPlan } from '../Shared/PaymentPlan/PaymentPlan';
import { TermSelector } from './components/TermSelector/TermSelector';

const location = CheckoutLocation.CALCULATOR;

export const Calculator: React.FC<{ onSubmit: () => void }> = ({ onSubmit }) => {
  const { order, amortizationPlans, currentLoanCalculator, currentAmortizationPlan, updateCheckout, setCheckoutStep } =
    useCheckoutContext();
  const { pushToDataLayer } = useDataLayer();

  const [loading, setLoading] = useState<boolean>(false);

  useEffect(() => {
    pushToDataLayer({ event: DLEvent.PAGE_VIEW, location });
  }, []);

  useEffect(() => {
    if (currentLoanCalculator && amortizationPlans) {
      const amortizationPlan = amortizationPlans.find(
        (amortizationPlan) => amortizationPlan.term === currentLoanCalculator.term,
      );
      if (amortizationPlan) {
        updateCheckout({ currentAmortizationPlan: amortizationPlan });
      }
    }
  }, [currentLoanCalculator]);

  const setLoanCalculatorAndSubmit = async () => {
    setLoading(true);
    pushToDataLayer({
      event: DLEvent.BTN_CLICK,
      location,
      additionalPayload: { type: 'continue', label: translation?.CONTINUE },
    });
    if (order && currentLoanCalculator && order.conditions.term !== currentLoanCalculator.term) {
      try {
        await patchSetLoanCalculator({
          loanId: order.id,
          currency: order.conditions.currency,
          principal: currentLoanCalculator.principalAmount.value,
          setupFeeAmount: currentLoanCalculator.instalmentFeeAmount.value * currentLoanCalculator.term, // FIXME: Only works with type_fixed_amount!
          setupFeeType: (currentLoanCalculator?.setupFeeType as FeePaymentType) ?? FeePaymentType.TYPE_UPFRONT,
          program: currentLoanCalculator.program as Program,
          term: currentLoanCalculator.term,
        });
        onSubmit();
      } catch (err) {
        setCheckoutStep(CHECKOUT_STEPS.ERROR);
      }
    } else {
      onSubmit();
    }
  };

  if (!currentLoanCalculator || !currentAmortizationPlan) return null;

  return (
    <>
      <Box className={'appear-anim'}>
        <Typography variant={'h4'} sx={{ pt: 4, pb: 2 }} color={'secondary.main'}>
          {translation?.PAYMENT_PLAN}
        </Typography>
        <Typography variant={'body2'}>
          {currentLoanCalculator.product === Product.SPLIT_PAY
            ? `${translation?.PAYMENT_PLAN_SPLIT_PAY_1} ${currentLoanCalculator.term} ${translation?.PAYMENT_PLAN_SPLIT_PAY_2}`
            : `${translation?.PAYMENT_PLAN_INSTALMENTS}`}
        </Typography>
        <Box mt={4} />
        <Box display={'flex'} justifyContent={'center'}>
          {currentLoanCalculator.product === Product.SPLIT_PAY ? (
            <PaymentPlan amortizationPlan={currentAmortizationPlan} type={'default'} />
          ) : (
            <TermSelector currentLoanCalculator={currentLoanCalculator} />
          )}
        </Box>
        {currentLoanCalculator.product === Product.INSTALMENTS && (
          <AmortizationDetails amortizationPlan={currentAmortizationPlan} />
        )}
        <Box>
          <Typography variant={'h5'} sx={{ py: 3 }} color={'secondary.main'}>
            {translation?.FINANCING_INFO}
          </Typography>
          <PaymentDetails loanCalculator={currentLoanCalculator} />
        </Box>
      </Box>
      <Box pt={5} className={'appear-anim'}>
        <MainButton label={translation?.CONTINUE} loading={loading} onClick={setLoanCalculatorAndSubmit} />
      </Box>
    </>
  );
};
