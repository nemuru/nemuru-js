import { Box, Typography, useMediaQuery, useTheme } from '@mui/material';
import { LoanCalculatorDto } from '@nemuru-eng/js-api-client';
import React from 'react';

import { useCheckoutContext } from '../../../../contexts/Checkout/CheckoutContext';
import { useDataLayer, CheckoutLocation, DLEvent } from '../../../../hooks/useDataLayer';
import { translation } from '../../../../translations/Translation';

import './TermSelector.css';

export const TermSelector: React.FC<{ currentLoanCalculator: LoanCalculatorDto }> = ({ currentLoanCalculator }) => {
  const { loanCalculators, updateCheckout } = useCheckoutContext();
  const { pushToDataLayer } = useDataLayer();

  const { palette, breakpoints } = useTheme();
  const isScreenXs = useMediaQuery(breakpoints.down(530));

  if (!loanCalculators) return null;

  return (
    <>
      {isScreenXs || loanCalculators.length > 3 ? (
        <Box width={'100%'} display={'flex'} flexDirection={'column'} alignItems={'stretch'}>
          {loanCalculators.map((loanCalculator) => (
            <Box
              key={loanCalculator.term}
              className={'selector-option'}
              px={3}
              height={54}
              borderRadius={'100px'}
              alignItems={'center'}
              justifyContent={'space-between'}
              boxShadow={
                currentLoanCalculator.term === loanCalculator.term
                  ? `0px 0px 0px 2px ${palette.primary.dark} inset`
                  : 'none'
              }
              bgcolor={(theme) =>
                currentLoanCalculator.term === loanCalculator.term ? 'transparent' : theme.palette.grey.A100
              }
              onClick={() => {
                pushToDataLayer({
                  event: DLEvent.LOAN_CALCULATOR_SELECT,
                  location: CheckoutLocation.CALCULATOR,
                  additionalPayload: { term: loanCalculator.term },
                });
                updateCheckout({ currentLoanCalculator: loanCalculator });
              }}
            >
              <Box display={'flex'} alignItems={'center'}>
                <i
                  style={{
                    color: currentLoanCalculator.term === loanCalculator.term ? palette.primary.main : undefined,
                    height: '24px',
                  }}
                  className={
                    currentLoanCalculator.term === loanCalculator.term
                      ? 'material-icons-outlined'
                      : 'material-icons-outlined disabled'
                  }
                >
                  {currentLoanCalculator.term === loanCalculator.term ? 'check_circle' : 'circle'}
                </i>
                <Typography
                  variant={'body2'}
                  sx={{
                    ml: 1,
                  }}
                  color={currentLoanCalculator.term === loanCalculator.term ? 'primary' : 'text.primary'}
                >
                  {`${loanCalculator.term} ${translation?.MONTHS}`}
                </Typography>
              </Box>
              <Box>
                <Typography
                  variant={'body2'}
                  align={'right'}
                  color={currentLoanCalculator.term === loanCalculator.term ? 'primary' : 'text.primary'}
                >
                  <strong>{loanCalculator.instalmentTotalAmount.string}</strong> {translation?.MONTHLY}
                </Typography>
              </Box>
            </Box>
          ))}
        </Box>
      ) : (
        <Box width={'100%'} display={'flex'} justifyContent={'space-between'} flexGrow={1}>
          {loanCalculators.map((loanCalculator) => (
            <Box
              key={loanCalculator.term}
              className={'selector-option'}
              height={100}
              borderRadius={'8px'}
              boxShadow={
                currentLoanCalculator.term === loanCalculator.term
                  ? `0px 0px 0px 2px ${palette.primary.main} inset`
                  : 'none'
              }
              bgcolor={(theme) =>
                currentLoanCalculator.term === loanCalculator.term ? 'transparent' : theme.palette.grey.A100
              }
              onClick={() => {
                pushToDataLayer({
                  event: DLEvent.LOAN_CALCULATOR_SELECT,
                  location: CheckoutLocation.CALCULATOR,
                  additionalPayload: { term: loanCalculator.term },
                });
                updateCheckout({ currentLoanCalculator: loanCalculator });
              }}
            >
              <Typography
                variant={'body2'}
                style={{ position: 'absolute', top: 12, left: 12 }}
                color={currentLoanCalculator.term === loanCalculator.term ? 'primary' : 'text.primary'}
              >
                {`${loanCalculator.term} ${translation?.MONTHS}`}
              </Typography>
              <Box style={{ position: 'absolute', bottom: 12, left: 12 }}>
                <Typography
                  variant={'body1'}
                  sx={{ fontSize: 18 }}
                  color={currentLoanCalculator.term === loanCalculator.term ? 'primary' : 'text.primary'}
                >
                  <strong>{`${loanCalculator.instalmentTotalAmount.string}`}</strong>
                </Typography>
                <Typography
                  variant={'body2'}
                  color={currentLoanCalculator.term === loanCalculator.term ? 'primary' : 'text.primary'}
                >
                  {translation?.MONTHLY}
                </Typography>
              </Box>
              <Box style={{ position: 'absolute', top: 12, right: 12 }}>
                <i
                  className={
                    currentLoanCalculator.term === loanCalculator.term
                      ? 'material-icons-outlined'
                      : 'material-icons-outlined disabled'
                  }
                  style={{
                    color: currentLoanCalculator.term === loanCalculator.term ? palette.primary.main : undefined,
                  }}
                >
                  {currentLoanCalculator.term === loanCalculator.term ? 'check_circle' : 'circle'}
                </i>
              </Box>
            </Box>
          ))}
        </Box>
      )}
    </>
  );
};
