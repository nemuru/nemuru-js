import { Box, Typography } from '@mui/material';
import React from 'react';

import Branding from '../../../../../src/shared/types/Style/Branding';
import { useCheckoutContext } from '../../contexts/Checkout/CheckoutContext';

export const PoweredBy: React.FC = () => {
  const { style } = useCheckoutContext();
  const poweredBy = getPoweredByBranding(style?.branding);
  return (
    <Box pt={1.5} pb={2} width={'100%'} display={'flex'} justifyContent={'center'}>
      {poweredBy}
    </Box>
  );
};

const getPoweredByBranding = (branding?: Branding): JSX.Element => {
  switch (branding?.value) {
    case Branding.IBERIA:
      return (
        <Box display={'flex'} sx={{ opacity: 0.3 }}>
          {poweredByNemuruLogo({ margin: '5px 5px 0 0', height: 16 })}
          <Typography variant={'caption'} color={'text.secondary'}>
            {`for`}
          </Typography>
          <img
            src={'https://cdn.nemuru.com/assets/logos/iberia-cards-grey-logo.svg'}
            style={{ margin: '6px 0 0 4px', height: 16 }}
          />
        </Box>
      );
    default:
      return <Box sx={{ opacity: 0.4 }}>{poweredByNemuruLogo()}</Box>;
  }
};

const poweredByNemuruLogo = (style?: any): JSX.Element => {
  let logoStyle = style;
  if (!logoStyle) {
    logoStyle = { height: 16 };
  }
  return (
    <>
      <img src={'https://cdn.nemuru.com/assets/logos/powered-by-nemuru-bold-logo.svg'} style={logoStyle} />
    </>
  );
};
