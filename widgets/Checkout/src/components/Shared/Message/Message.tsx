import { Box, CircularProgress, Palette, Typography, useTheme } from '@mui/material';
import { AmortizationPlanDto } from '@nemuru-eng/js-api-client';
import React, { useEffect } from 'react';
import { useDataLayer, CheckoutLocation, DLEvent } from '../../../hooks/useDataLayer';
import { translation } from '../../../translations/Translation';
import { MainButton } from '../MainButton';
import { PaymentPlan } from '../PaymentPlan/PaymentPlan';
import './Message.css';

export const MESSAGE_TYPES = {
  VERIFYING_PHONE: 'VERIFYING_PHONE',
  PROCESSING: 'PROCESSING',
  CONFIRMED: 'CONFIRMED',
  ERROR: 'ERROR',
  DENIED: 'DENIED',
  EXPIRED: 'EXPIRED',
  EMPTY_LOAN_CONDITIONS: 'EMPTY_LOAN_CONDITIONS',
  CANCELLED: 'CANCELLED',
  ON_HOLD: 'ON_HOLD',
  TIMEOUT: 'TIMEOUT',
};

const MESSAGE_TYPE_TO_LOCATION = {
  VERIFYING_PHONE: CheckoutLocation.VERIFYING_PHONE,
  PROCESSING: CheckoutLocation.PROCESSING,
  CONFIRMED: CheckoutLocation.CONFIRMED,
  ERROR: CheckoutLocation.ERROR,
  DENIED: CheckoutLocation.DENIED,
  EXPIRED: CheckoutLocation.EXPIRED,
  EMPTY_LOAN_CONDITIONS: CheckoutLocation.EMPTY_LOAN_CONDITIONS,
  CANCELLED: CheckoutLocation.CANCELLED,
  ON_HOLD: CheckoutLocation.ON_HOLD,
  TIMEOUT: CheckoutLocation.TIMEOUT,
};

const ICON_TYPES = (palette: Palette) => ({
  LOADING: <CircularProgress className={'circular-progress'} thickness={3} />,
  CHECK: (
    <Box className={'icon-circle'} sx={{ bgcolor: 'primary.main' }}>
      <i className={'material-icons-outlined'} style={{ color: palette.primary.contrastText }}>
        check
      </i>
    </Box>
  ),
  ERROR: (
    <Box className={'icon-circle'} sx={{ bgcolor: 'secondary.light' }}>
      <i className={'material-icons-outlined'} style={{ color: palette.secondary.contrastText }}>
        warning_amber
      </i>
    </Box>
  ),
  CLOSE: (
    <Box className={'icon-circle'} sx={{ bgcolor: 'secondary.light' }}>
      <i className={'material-icons-outlined'} style={{ color: palette.secondary.contrastText }}>
        close
      </i>
    </Box>
  ),
  TIMER: (
    <Box className={'icon-circle'} sx={{ bgcolor: 'secondary.light' }}>
      <i className={'material-icons-outlined'} style={{ color: palette.secondary.contrastText }}>
        timer
      </i>
    </Box>
  ),
  HOURGLASS: (
    <Box className={'icon-circle'} sx={{ bgcolor: 'secondary.light' }}>
      <i className={'material-icons-outlined'} style={{ color: palette.secondary.contrastText }}>
        hourglass_bottom
      </i>
    </Box>
  ),
});

export const messages = (palette: Palette) => ({
  [MESSAGE_TYPES.VERIFYING_PHONE]: () => ({
    icon: ICON_TYPES(palette).LOADING,
    title: translation?.VERIFYING,
    content: <Typography variant={'body2'}>{translation?.WAIT_WHILE_VERIFYING}</Typography>,
  }),
  [MESSAGE_TYPES.PROCESSING]: () => ({
    icon: ICON_TYPES(palette).LOADING,
    title: translation?.PROCESSING,
    content: <Typography variant={'body2'}>{translation?.WAIT_WHILE_PROCESSING}</Typography>,
  }),
  [MESSAGE_TYPES.CONFIRMED]: (data: { currentAmortizationPlan: AmortizationPlanDto }) => ({
    icon: ICON_TYPES(palette).CHECK,
    title: translation?.PAYMENT_CONFIRMED,
    content: (
      <>
        <Typography variant={'body2'}>{translation?.PAYMENT_CONFIRMED_DETAIL}</Typography>
        <Box mt={5}>
          <PaymentPlan amortizationPlan={data.currentAmortizationPlan} type={'default'} downpaymentPaid={true} />
        </Box>
      </>
    ),
  }),
  [MESSAGE_TYPES.ERROR]: () => ({
    icon: ICON_TYPES(palette).ERROR,
    title: translation?.OOOPS,
    content: <Typography variant={'body2'}>{translation?.INTERNAL_ERROR}</Typography>,
  }),
  [MESSAGE_TYPES.TIMEOUT]: () => ({
    icon: ICON_TYPES(palette).ERROR,
    title: translation?.TIMEOUT_TITLE,
    content: (
      <Box>
        <Typography variant={'body2'}>{translation?.TIMEOUT_DETAIL}</Typography>
        <Box mt={4} display={'flex'}>
          <Box className={'icon-circle-small'} sx={{ bgcolor: 'secondary.light' }}>
            <i className={'material-icons-outlined'} style={{ color: palette.secondary.contrastText }}>
              email
            </i>
          </Box>
          <Box pl={3}>
            <Typography variant={'body2'}>
              <strong>{translation?.SEND_EMAIL}</strong>
            </Typography>
            <Typography variant={'body2'}>{translation?.SEND_EMAIL_DETAIL}</Typography>
          </Box>
        </Box>
      </Box>
    ),
  }),
  [MESSAGE_TYPES.DENIED]: () => ({
    icon: ICON_TYPES(palette).CLOSE,
    title: translation?.SORRY,
    content: (
      <Box>
        <Typography variant={'body2'}>{translation?.DENIED_DETAIL}</Typography>
        <Box mt={4} display={'flex'}>
          <Box className={'icon-circle-small'} sx={{ bgcolor: 'secondary.light' }}>
            <i className={'material-icons-outlined'} style={{ color: palette.secondary.contrastText }}>
              exit_to_app
            </i>
          </Box>
          <Box pl={3}>
            <Typography variant={'body2'}>
              <strong>{translation?.ANOTHER_OPTION}</strong>
            </Typography>
            <Typography variant={'body2'}>{translation?.ENCOURAGE_ANOTHER_METHOD}</Typography>
          </Box>
        </Box>
      </Box>
    ),
  }),
  [MESSAGE_TYPES.EXPIRED]: () => ({
    icon: ICON_TYPES(palette).TIMER,
    title: translation?.ORDER_EXPIRED,
    content: <Typography variant={'body2'}>{translation?.ORDER_EXPIRED_DETAIL}</Typography>,
  }),
  [MESSAGE_TYPES.EMPTY_LOAN_CONDITIONS]: () => ({
    icon: ICON_TYPES(palette).CLOSE,
    title: translation?.EMPTY_LOAN_CONDITIONS,
    content: <Typography variant={'body2'}>{translation?.EMPTY_LOAN_CONDITIONS_DETAIL}</Typography>,
  }),
  [MESSAGE_TYPES.CANCELLED]: () => ({
    icon: ICON_TYPES(palette).CLOSE,
    title: translation?.ORDER_CANCELLED,
    content: undefined,
  }),
  [MESSAGE_TYPES.ON_HOLD]: () => ({
    icon: ICON_TYPES(palette).HOURGLASS,
    title: translation?.ONE_MOMENT,
    content: (
      <Box>
        <Typography variant={'body2'}>{translation?.ON_HOLD_DETAIL}</Typography>
        <Box mt={4} display={'flex'}>
          <Box className={'icon-circle-small'} sx={{ bgcolor: 'secondary.light' }}>
            <i className={'material-icons-outlined'} style={{ color: palette.secondary.contrastText }}>
              watch_later
            </i>
          </Box>
          <Box pl={3}>
            <Typography variant={'body2'}>
              <strong>{translation?.OFF_HOURS}</strong>
            </Typography>
            <Typography variant={'body2'}>{translation?.OFF_HOURS_DETAIL}</Typography>
          </Box>
        </Box>
        <Box mt={4} display={'flex'}>
          <Box className={'icon-circle-small'} sx={{ bgcolor: 'secondary.light' }}>
            <i className={'material-icons-outlined'} style={{ color: palette.secondary.contrastText }}>
              email
            </i>
          </Box>
          <Box pl={3}>
            <Typography variant={'body2'}>
              <strong>{translation?.SEND_EMAIL}</strong>
            </Typography>
            <Typography variant={'body2'}>{translation?.SEND_EMAIL_DETAIL}</Typography>
          </Box>
        </Box>
      </Box>
    ),
  }),
});

export const Message: React.FC<Props> = ({ messageType, data, buttons }) => {
  const { palette } = useTheme();
  const { icon, title, content } = messages(palette)[messageType](data);
  const { pushToDataLayer } = useDataLayer();

  useEffect(() => {
    pushToDataLayer({
      event: DLEvent.PAGE_VIEW,
      location: MESSAGE_TYPE_TO_LOCATION[messageType as keyof typeof MESSAGE_TYPE_TO_LOCATION],
    });
  }, []);

  return (
    <>
      <Box mt={5} className={'appear-anim'}>
        {icon}
        <Typography variant={'h4'} sx={{ pt: 3, pb: 2 }} color={'secondary.main'}>
          {title}
        </Typography>
        {content}
      </Box>
      {buttons && (
        <Box pt={3}>
          {buttons.map((button) => (
            <Box pt={3} key={button.title}>
              <MainButton label={button.title} onClick={button.onClick} {...button?.props} />
            </Box>
          ))}
        </Box>
      )}
    </>
  );
};

type Props = {
  messageType: string;
  data?: any;
  buttons?: { title: string; onClick: () => void; props?: any }[];
};
