import { Box, Typography } from '@mui/material';
import { LoanCalculatorDto } from '@nemuru-eng/js-api-client';
import React from 'react';

import { useCheckoutContext } from '../../contexts/Checkout/CheckoutContext';
import { translation } from '../../translations/Translation';
import { AprAndConditionsExample } from './AprAndConditionsExample';
import { LenderLogo } from './Logo/LenderLogo';

export const PaymentDetails: React.FC<{ loanCalculator: LoanCalculatorDto }> = ({ loanCalculator }) => {
  const { order } = useCheckoutContext();

  if (!loanCalculator || !order) return null;

  return (
    <>
      <Box display={'flex'} justifyContent={'space-between'}>
        <Typography id={'principal-amount-label'} variant={'body2'}>
          {translation?.PURCHASE_AMOUNT}
        </Typography>
        <Typography id={'principal-amount-value'} variant={'body2'} align={'right'}>
          <strong>{loanCalculator?.principalAmount.string}</strong>
        </Typography>
      </Box>
      <Box mt={2} />
      <Box display={'flex'} justifyContent={'space-between'}>
        <Typography
          id={'total-instalment-fee-amount-label'}
          variant={'body2'}
        >{`${translation?.TOTAL_COST} (${loanCalculator?.instalmentFeeAmount.string} ${translation?.PER_INSTALMENT})`}</Typography>
        <Typography id={'total-instalment-fee-amount-value'} variant={'body2'} align={'right'}>
          <strong>{loanCalculator?.totalInstalmentFeeAmount.string}</strong>
        </Typography>
      </Box>
      <Box mt={2} />
      <Box display={'flex'} justifyContent={'space-between'}>
        <Typography id={'total-amount-label'} variant={'body2'}>
          {translation?.TOTAL_TO_PAY}
        </Typography>
        <Typography id={'total-amount-value'} variant={'body2'} align={'right'}>
          <strong>{loanCalculator?.totalAmount.string}</strong>
        </Typography>
      </Box>
      {order.applicationSelected?.lenderFields.lenderRequestId && (
        <>
          <Box mt={2} />
          <Box display={'flex'} justifyContent={'space-between'} alignItems={'center'}>
            <Typography variant={'body2'}>{translation?.SERVICE_OFFERED_BY}</Typography>
            <LenderLogo amount={order.conditions.principal} styleProps={{ height: 28 }} />
          </Box>
        </>
      )}
      <Box mt={3} />
      <AprAndConditionsExample loanCalculator={loanCalculator} />
    </>
  );
};
