import { Box, Collapse, Typography } from '@mui/material';
import { LoanCalculatorDto } from '@nemuru-eng/js-api-client';
import React, { useEffect, useState } from 'react';

import Branding from '../../../../../src/shared/types/Style/Branding';
import { useCheckoutContext } from '../../contexts/Checkout/CheckoutContext';
import { useDataLayer, DLEvent } from '../../hooks/useDataLayer';
import { translation } from '../../translations/Translation';

const ALLOWED_BRANDINGS = [Branding.IBERIA];
const TRANSITION_TIMEOUT = 300;

export const AprAndConditionsExample: React.FC<{ loanCalculator: LoanCalculatorDto }> = ({ loanCalculator }) => {
  const { style, order } = useCheckoutContext();
  const { pushToDataLayer } = useDataLayer();

  if (!style || !order) return null;

  const allowConditionsExample = ALLOWED_BRANDINGS.includes(style.branding.value);

  const [show, setShow] = useState(false);

  useEffect(() => {
    if (show) {
      setTimeout(() => {
        const el = document.getElementById('legal-text');
        if (el) {
          const height = el.offsetHeight;
          const scrollEl = document.getElementById(window.innerWidth > 500 ? 'popup-wrapper' : 'popup-content');
          scrollEl?.scroll({ top: scrollEl.scrollTop + height, left: 0, behavior: 'smooth' });
        }
      }, TRANSITION_TIMEOUT + 10);
    }
  }, [show]);

  return (
    <>
      <Box display={'flex'} flexDirection={'column'}>
        <Box display={'flex'} justifyContent={'center'}>
          <Typography id={'tin-tae'} variant={'caption'} align={'center'} color={'text.secondary'}>
            {`${translation?.TIN}: ${loanCalculator?.annualInterest.string} | ${translation?.TAE}: ${loanCalculator?.apr.string}`}
          </Typography>
          {allowConditionsExample && !order.applicationSelected && (
            <Typography
              variant={'caption'}
              color={'text.secondary'}
              sx={{ ml: 1, textDecoration: 'underline', cursor: 'pointer' }}
              onClick={() => {
                setShow(!show);
                pushToDataLayer({
                  event: DLEvent.BTN_CLICK,
                  additionalPayload: { type: 'conditions_legal_text', label: '+info' },
                });
              }}
            >
              {'+info'}
            </Typography>
          )}
        </Box>
        <Collapse orientation={'vertical'} in={show} timeout={TRANSITION_TIMEOUT}>
          <Box id={'legal-text'} pt={2} display={'flex'}>
            <Typography
              component={'p'}
              variant={'caption'}
              color={'text.secondary'}
              sx={{ fontSize: 11, lineHeight: '14px', textAlign: 'justify' }}
            >
              {`${translation?.CONDITIONS_EXAMPLE_1} ${loanCalculator.principalAmount.string} 
              ${translation?.CONDITIONS_EXAMPLE_2} ${loanCalculator.term} 
              ${translation?.CONDITIONS_EXAMPLE_3} ${loanCalculator.instalmentTotalAmount.string} 
              ${translation?.CONDITIONS_EXAMPLE_4} ${loanCalculator.instalmentAmount.string} 
              ${translation?.CONDITIONS_EXAMPLE_5} ${loanCalculator.instalmentFeeAmount.string} 
              ${translation?.CONDITIONS_EXAMPLE_6} ${loanCalculator.lastInstalmentTotalAmount.string} 
              ${translation?.CONDITIONS_EXAMPLE_7} ${loanCalculator.lastInstalmentAmount.string} 
              ${translation?.CONDITIONS_EXAMPLE_8} ${loanCalculator.lastInstalmentFeeAmount.string} 
              ${translation?.CONDITIONS_EXAMPLE_9} ${loanCalculator.totalAmount.string} 
              ${translation?.CONDITIONS_EXAMPLE_10} ${loanCalculator.principalAmount.string} 
              ${translation?.CONDITIONS_EXAMPLE_11} ${loanCalculator.totalInstalmentFeeAmount.string} 
              ${translation?.CONDITIONS_EXAMPLE_12} ${loanCalculator.annualInterest.string} 
              ${translation?.CONDITIONS_EXAMPLE_13} ${loanCalculator.apr.string}.
              `}
            </Typography>
          </Box>
        </Collapse>
      </Box>
    </>
  );
};
