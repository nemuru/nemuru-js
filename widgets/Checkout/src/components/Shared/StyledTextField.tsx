import { styled, TextField } from '@mui/material';

import Branding from '../../../../../src/shared/types/Style/Branding';
import { useCheckoutContext } from '../../contexts/Checkout/CheckoutContext';

export const StyledTextField = styled(TextField)(({ theme }) => {
  const { style } = useCheckoutContext();

  const baseStyle = {
    '& .MuiInputBase-input': {
      fontSize: theme.typography.body2.fontSize,
      padding: '28px 12px 12px',
    },
    '& label': {
      color: theme.palette.text.secondary,
    },
    '& label.Mui-focused': {
      color: theme.palette.primary.main,
    },
    '& .MuiFilledInput-root': {
      backgroundColor: theme.palette.grey.A100,
      border: 'none',
      borderRadius: '6px',
      '&.Mui-focused': {
        backgroundColor: 'transparent',
        boxShadow: `0px 0px 0px 2px ${theme.palette.primary.main} inset`,
      },
      '& fieldset': {
        borderColor: '#E1E1E5', //FIXME keep as grey.A200 or another one
      },
      '&:hover fieldset': {
        borderColor: '#E1E1E5',
      },
    },
  };

  let brandingStyle = undefined;
  if (style?.branding.value === Branding.IBERIA) {
    brandingStyle = {
      '& label': {
        color: theme.palette.text.secondary,
        textTransform: 'uppercase',
      },
      '& .MuiFilledInput-root': {
        backgroundColor: theme.palette.common.white,
        border: `1px solid ${theme.palette.grey.A400}`,
        borderRadius: theme.shape.borderRadius,
        '&:hover': {
          backgroundColor: theme.palette.common.white,
        },
        '&.Mui-focused': {
          backgroundColor: 'transparent',
          border: `1px solid ${theme.palette.primary.main}`,
        },
        '& fieldset': {
          borderColor: '#E1E1E5',
        },
        '&.Mui-disabled': {
          backgroundColor: 'rgba(0, 0, 0, 0.05)',
        },
      },
    };
  }

  return { ...baseStyle, ...brandingStyle };
});
