import { Select, styled } from '@mui/material';

import Branding from '../../../../../src/shared/types/Style/Branding';
import { useCheckoutContext } from '../../contexts/Checkout/CheckoutContext';

export const StyledSelect = styled(Select)(({ theme }) => {
  const { style } = useCheckoutContext();

  const baseStyle = {
    backgroundColor: theme.palette.grey.A100,
    border: 'none',
    borderRadius: '6px',
    '&.Mui-focused': {
      backgroundColor: 'transparent',
      boxShadow: `0px 0px 0px 2px ${theme.palette.primary.main} inset`,
    },
    '& fieldset': {
      borderColor: '#E1E1E5', //FIXME keep as grey.A200 or another one
    },
    '&:hover fieldset': {
      borderColor: '#E1E1E5',
    },
    '&:before': {
      borderBottom: 'none',
      borderBottomStyle: 'none',
    },
    '&:after': {
      borderBottom: 'none',
      borderBottomStyle: 'none',
    },
    '&:hover': {
      borderBottom: 'none',
      borderBottomStyle: 'none',
    },
    '&:hover:not(.Mui-disabled):before': {
      borderBottom: 'none',
      borderBottomStyle: 'none',
    },
    '&.Mui-disabled:before': {
      borderBottom: 'none',
      borderBottomStyle: 'none',
    },
    '& .MuiSelect-select': {
      borderRadius: '6px !important',
    },
    '& .Mui-disabled': {
      backgroundColor: theme.palette.grey.A100,
    },
    '& .MuiInputBase-input': {
      fontSize: theme.typography.body2.fontSize,
      padding: '28px 12px 11px',
    },
    '& .MuiFilledInput-input.Mui-disabled': {
      color: theme.palette.grey.A400,
      WebkitTextFillColor: theme.palette.grey.A400,
    },
    '& .MuiSvgIcon-root': {
      top: 'calc(50% - 0.3em)',
      '&.Mui-disabled': {
        display: 'none',
      },
    },
  };

  let brandingStyle = undefined;
  if (style?.branding.value === Branding.IBERIA) {
    brandingStyle = {
      backgroundColor: theme.palette.common.white,
      border: `1px solid ${theme.palette.grey.A400}`,
      borderRadius: theme.shape.borderRadius,
      '&.Mui-focused': {
        backgroundColor: 'transparent',
        border: `1px solid ${theme.palette.primary.main} !important`,
      },
      '& .MuiSelect-select': {
        borderRadius: '0px !important',
      },
      '&:hover': {
        backgroundColor: 'transparent',
      },
      '& .MuiFilledInput-input:focus': {
        backgroundColor: 'transparent',
      },
      '& .MuiFilledInput-input.Mui-disabled': {
        color: theme.palette.text.disabled,
        WebkitTextFillColor: theme.palette.text.disabled,
      },
    };
  }

  return { ...baseStyle, ...brandingStyle };
});
