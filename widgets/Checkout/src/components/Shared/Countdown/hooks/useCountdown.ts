import { useEffect, useState } from 'react';

export const useCountdown = () => {
  const [start, setStart] = useState<boolean>(false);
  const [target, setTarget] = useState<Date | undefined>(undefined);
  const [countDown, setCountDown] = useState<number | undefined>(undefined);

  const countDownDate = target && new Date(target).getTime();

  useEffect(() => {
    if (start && countDownDate) {
      const interval = setInterval(() => {
        setCountDown(countDownDate - new Date().getTime());
      }, 1000);

      return () => clearInterval(interval);
    }
  }, [start, countDownDate]);

  const startCountdown = (targetDate: Date) => {
    setTarget(targetDate);
    setStart(true);
  };

  return { countdown: getReturnValues(countDown), startCountdown };
};

const getReturnValues = (countDown?: number): { minutes: number; seconds: number } | undefined => {
  if (!countDown) return;

  const minutes = Math.floor((countDown % (1000 * 60 * 60)) / (1000 * 60));
  const seconds = Math.floor((countDown % (1000 * 60)) / 1000);

  if (seconds < 0) return;

  return { minutes, seconds };
};
