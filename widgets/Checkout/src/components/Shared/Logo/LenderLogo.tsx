import { Money } from '@nemuru-eng/js-api-client';
import React from 'react';

import Branding from '../../../../../../src/shared/types/Style/Branding';
import { useCheckoutContext } from '../../../contexts/Checkout/CheckoutContext';

const AMOUNT_THRESHOLD = 1200;

const QUIX_LOGO = [Branding.IBERIA, Branding.LENDING_HUB];

export const LenderLogo: React.FC<{ amount: Money; styleProps?: any }> = ({ amount, styleProps }) => {
  const { style } = useCheckoutContext();
  if (!style) return null;

  return (
    <img
      id={'lender-logo'}
      src={
        amount.value <= AMOUNT_THRESHOLD && QUIX_LOGO.includes(style.branding.value)
          ? 'https://cdn.nemuru.com/assets/logos/quix-by-sequra-logo.svg'
          : 'https://cdn.nemuru.com/assets/logos/sequra-black-logo.svg'
      }
      style={styleProps}
    />
  );
};
