import { IconButton } from '@mui/material';
import React from 'react';

import './BackButton.css';

export const BackButton: React.FC<{ onClick: () => void; disabled?: boolean }> = ({ onClick, disabled = false }) => {
  return (
    <>
      <IconButton onClick={onClick} className={'back-button'} color={'secondary'} disabled={disabled}>
        <i className={'material-icons-outlined'}>arrow_back</i>
      </IconButton>
    </>
  );
};
