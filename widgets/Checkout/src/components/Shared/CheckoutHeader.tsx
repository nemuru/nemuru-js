import { Box, IconButton, Typography } from '@mui/material';
import { Action, LoanDesistReason, LoanStatus, Order, patchChangeLoanStatus, Product } from '@nemuru-eng/js-api-client';
import React, { MutableRefObject } from 'react';

import { useCheckoutContext } from '../../contexts/Checkout/CheckoutContext';
import { useDataLayer, CheckoutLocation, DLEvent } from '../../hooks/useDataLayer';
import { translation } from '../../translations/Translation';
import { CHECKOUT_STEPS_DIRECT_CLOSE } from '../../types/CheckoutSteps';
import { Status } from '../../types/Status';
import { LEAVE_PROCESS_STEP } from './LeaveProcess/LeaveProcess';

export const CheckoutHeader: React.FC<{
  headerRef: MutableRefObject<null>;
  handleCloseDialog: (status: string, dispatchCallback?: boolean) => void;
  order?: Order;
}> = ({ headerRef, order, handleCloseDialog }) => {
  const { behaviour, checkoutStep, leaveProcessDialog, updateCheckout, headerCloseEnabled } = useCheckoutContext();
  const { pushToDataLayer } = useDataLayer();

  if (!order) return null;

  const onClose = async () => {
    pushToDataLayer({ event: DLEvent.BTN_CLOSE, location: CheckoutLocation.LEAVE_PROCESS });
    if (leaveProcessDialog) {
      await patchChangeLoanStatus({
        loanId: order.id,
        status: LoanStatus.STATUS_DESISTED,
        additional: LoanDesistReason.UNKNOWN,
      });
      updateCheckout({ leaveProcessDialog: LEAVE_PROCESS_STEP.PROCESSING });
      setTimeout(() => handleCloseDialog(Action.DESISTED), 2000);
    } else if (checkoutStep && CHECKOUT_STEPS_DIRECT_CLOSE.includes(checkoutStep)) {
      handleCloseDialog(Status.createFromAction(order.action).value);
    } else {
      updateCheckout({ leaveProcessDialog: LEAVE_PROCESS_STEP.FORM });
    }
  };

  const product = order.conditions.product as keyof typeof productTitle;

  const productTitle = {
    [Product.SPLIT_PAY]: translation?.SPLIT_PAY,
    [Product.INSTALMENTS]: translation?.INSTALMENTS,
  };

  const showHeaderClose = headerCloseEnabled && !behaviour?.disableClose;

  return (
    <>
      <Box ref={headerRef} pt={2} display={'flex'} alignItems={'center'} justifyContent={'space-between'}>
        <Typography variant={'h6'}>{productTitle[product]}</Typography>
        {showHeaderClose && (
          <IconButton id={'close-btn'} onClick={onClose}>
            <i className={'material-icons-outlined'} style={{ height: '24px', width: '24px', color: '#000' }}>
              close
            </i>
          </IconButton>
        )}
      </Box>
    </>
  );
};
