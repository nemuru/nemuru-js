import { Box, Button as MuiButton, ButtonProps as MuiButtonProps, CircularProgress, Typography } from '@mui/material';
import React, { ForwardedRef } from 'react';

import { PoweredBy } from './PoweredBy';

export interface ButtonProps extends Omit<MuiButtonProps, 'size'> {
  label?: string;
  /**
   * The size of the component.
   * @default 'medium'
   */
  size?: 'small' | 'medium' | 'large';
  /**
   * The button displays a loading circular progress.
   * When loading, the button is disabled.
   * @default false
   */
  loading?: boolean;
  /**
   * Disables powered by nemuru message
   * @default false
   */
  disablePoweredBy?: boolean;
}

const CIRCULAR_PROGRESS_HEIGHT = {
  small: 26,
  medium: 28,
  large: 32,
};

export const MainButton = React.forwardRef((props: ButtonProps, ref: ForwardedRef<any>): JSX.Element => {
  const size = props.size !== undefined ? props.size : 'medium';
  const variant = props.variant !== undefined ? props.variant : 'contained';
  const color = props.color !== undefined ? props.color : 'secondary';
  const poweredBy = !(props.disablePoweredBy !== undefined ? props.disablePoweredBy : false);
  const { loading, ...rest } = props;
  return (
    <>
      {loading ? (
        <Box width={'100%'} height={52} display={'flex'} justifyContent={'center'} alignItems={'center'}>
          <CircularProgress id={'loading-btn'} size={CIRCULAR_PROGRESS_HEIGHT[size]} color={'secondary'} />
        </Box>
      ) : (
        <MuiButton
          {...rest}
          fullWidth={true}
          disableElevation={true}
          ref={ref}
          size={size}
          color={color}
          variant={variant}
          disabled={props.disabled}
          style={{ position: 'relative' }}
          sx={{
            '&:hover': {
              backgroundColor: variant === 'text' ? 'grey.A100' : `${color}.dark`,
            },
          }}
        >
          <Typography
            variant={'h5'}
            sx={{
              height: CIRCULAR_PROGRESS_HEIGHT[size],
              color: (theme) =>
                props.disabled
                  ? theme.palette.text.disabled
                  : variant === 'text'
                  ? theme.palette.text.primary
                  : theme.palette.secondary.contrastText,
            }}
          >
            {props.label}
          </Typography>
        </MuiButton>
      )}
      {poweredBy && <PoweredBy />}
    </>
  );
});
