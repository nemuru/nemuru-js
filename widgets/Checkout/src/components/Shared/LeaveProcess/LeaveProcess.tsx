import { Box, CircularProgress, ToggleButton, ToggleButtonGroup, Typography } from '@mui/material';
import { Action, LoanDesistReason, LoanStatus, patchChangeLoanStatus } from '@nemuru-eng/js-api-client';
import React from 'react';

import { useCheckoutContext } from '../../../contexts/Checkout/CheckoutContext';
import { useDataLayer, CheckoutLocation, DLEvent } from '../../../hooks/useDataLayer';
import { translation } from '../../../translations/Translation';
import { MainButton } from '../MainButton';
import './LeaveProcess.css';

export const LeaveProcess: React.FC<{ onClose: () => void }> = ({ onClose }) => {
  const { order, leaveProcessDialog, updateCheckout } = useCheckoutContext();
  const { pushToDataLayer } = useDataLayer();

  const [reason, setReason] = React.useState<string | undefined>(undefined);
  const [loading, setLoading] = React.useState<boolean>(false);

  const handleChange = (event: React.MouseEvent<HTMLElement>, nextView: string) => {
    setReason(nextView);
  };

  const desistLoanAndClose = async () => {
    if (order) {
      setLoading(true);
      await patchChangeLoanStatus({
        loanId: order.id,
        status: LoanStatus.STATUS_DESISTED,
        additional: (reason as LoanDesistReason) ?? LoanDesistReason.UNKNOWN,
      });
      updateCheckout({ leaveProcessDialog: LEAVE_PROCESS_STEP.PROCESSING });
      setLoading(false);
      setTimeout(() => onClose(), 2000);
    }
  };

  if (!leaveProcessDialog || !order) return null;

  const action = (
    Object.keys(DESIST_REASONS_BY_ACTION).includes(order.action) ? order.action : 'default'
  ) as keyof typeof DESIST_REASONS_BY_ACTION;

  const LEAVE_REASONS_MAP = {
    [LoanDesistReason.OTP_NOT_RECEIVED]: translation?.OTP_NOT_RECEIVED,
    [LoanDesistReason.SCA_ERROR]: translation?.SCA_ERROR,
    [LoanDesistReason.ORDER_CONFIRMATION_NOT_COMPLETED]: translation?.ORDER_CONFIRMATION_NOT_COMPLETED,
    [LoanDesistReason.DO_NOT_UNDERSTAND_FUTURE_PAYMENTS]: translation?.DO_NOT_UNDERSTAND_FUTURE_PAYMENTS,
    [LoanDesistReason.NO_FINANCING_WANTED]: translation?.NO_FINANCING_WANTED,
    [LoanDesistReason.NEMURU_IS_EXPENSIVE]: translation?.NEMURU_IS_EXPENSIVE,
  };

  return (
    <>
      {leaveProcessDialog === LEAVE_PROCESS_STEP.FORM && (
        <div className={'wrapper'}>
          <Box className={'appear-anim'}>
            <Typography variant={'h4'} sx={{ pt: 4, pb: 2 }} color={'secondary.main'}>
              {translation?.BEFORE_LEAVING}
            </Typography>
            <Typography variant={'body2'}>{translation?.CHOOSE_DESIST_REASON}</Typography>
            <Box mt={3}>
              <ToggleButtonGroup
                id={'desist-reasons-toggle'}
                orientation="vertical"
                value={reason}
                exclusive
                onChange={handleChange}
                sx={{ minWidth: '100%' }}
              >
                {DESIST_REASONS_BY_ACTION[action].map((key) => (
                  <ToggleButton
                    key={key}
                    value={key}
                    color={'primary'}
                    sx={{
                      padding: '6px',
                      textTransform: 'none',
                      textAlign: 'left',
                      justifyContent: 'left',
                      alignItems: 'center',
                    }}
                  >
                    <Box ml={0.5} mr={1.5} sx={{ borderColor: reason === key ? 'primary.main' : 'text.primary' }}>
                      <i className={'material-icons-outlined'} style={{ fontSize: 20, marginTop: 8 }}>
                        {reason === key ? 'radio_button_checked' : 'radio_button_unchecked'}
                      </i>
                    </Box>
                    <Typography variant={'body2'} color={reason === key ? 'primary.main' : 'text.primary'}>
                      {LEAVE_REASONS_MAP[key as keyof typeof LEAVE_REASONS_MAP]}
                    </Typography>
                  </ToggleButton>
                ))}
              </ToggleButtonGroup>
            </Box>
          </Box>
          <Box pt={3} pb={5}>
            <Box pt={3}>
              <MainButton
                label={translation?.REPLY_AND_CLOSE}
                loading={loading}
                disabled={!reason}
                disablePoweredBy={true}
                onClick={() => {
                  pushToDataLayer({
                    event: DLEvent.BTN_CLICK,
                    location: CheckoutLocation.LEAVE_PROCESS,
                    additionalPayload: {
                      type: 'reply_and_close',
                      label: translation?.REPLY_AND_CLOSE,
                    },
                  });
                  desistLoanAndClose();
                }}
              />
            </Box>
            <Box pt={3}>
              <MainButton
                label={translation?.RETURN_TO_PROCESS}
                disabled={loading}
                variant={'text'}
                disablePoweredBy={true}
                onClick={() => {
                  pushToDataLayer({
                    event: DLEvent.BTN_CLICK,
                    location: CheckoutLocation.LEAVE_PROCESS,
                    additionalPayload: {
                      type: 'return_to_process',
                      label: translation?.RETURN_TO_PROCESS,
                    },
                  });
                  updateCheckout({ leaveProcessDialog: undefined });
                }}
              />
            </Box>
          </Box>
        </div>
      )}
      {leaveProcessDialog === LEAVE_PROCESS_STEP.PROCESSING && (
        <div className={'wrapper'}>
          <Box mt={5} className={'appear-anim'}>
            <CircularProgress className={'circular-progress'} thickness={3} />
            <Typography variant={'h4'} sx={{ pt: 3, pb: 2 }} color={'secondary.main'}>
              {translation?.PROCESSING}
            </Typography>
            <Typography variant={'body2'}>{translation?.WAIT_WHILE_PROCESSING}</Typography>
          </Box>
        </div>
      )}
    </>
  );
};

const DESIST_REASONS_BY_ACTION = {
  [Action.INITIAL]: [
    LoanDesistReason.DO_NOT_UNDERSTAND_FUTURE_PAYMENTS,
    LoanDesistReason.NO_FINANCING_WANTED,
    LoanDesistReason.NEMURU_IS_EXPENSIVE,
  ],
  [Action.REQUIRE_VALIDATE_OTP]: [
    LoanDesistReason.OTP_NOT_RECEIVED,
    LoanDesistReason.DO_NOT_UNDERSTAND_FUTURE_PAYMENTS,
    LoanDesistReason.NO_FINANCING_WANTED,
    LoanDesistReason.NEMURU_IS_EXPENSIVE,
  ],
  default: [
    LoanDesistReason.SCA_ERROR,
    LoanDesistReason.ORDER_CONFIRMATION_NOT_COMPLETED,
    LoanDesistReason.DO_NOT_UNDERSTAND_FUTURE_PAYMENTS,
    LoanDesistReason.NO_FINANCING_WANTED,
    LoanDesistReason.NEMURU_IS_EXPENSIVE,
  ],
};

export enum LEAVE_PROCESS_STEP {
  FORM = 'FORM',
  PROCESSING = 'PROCESSING',
}
