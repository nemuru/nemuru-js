import { Box, Typography, useMediaQuery, useTheme } from '@mui/material';
import { AmortizationPlanDto } from '@nemuru-eng/js-api-client';
import React from 'react';

import './PaymentPlan.css';
import { translation } from '../../../translations/Translation';

export const PaymentPlan: React.FC<Props> = ({ amortizationPlan, type = 'default', downpaymentPaid = false }) => {
  const { breakpoints } = useTheme();
  const isScreenXs = useMediaQuery(breakpoints.down(480));
  const CIRCLE_HEIGHT = isScreenXs ? 140 : 170;
  const ALLOWED_DONUT_TERMS = [6, 10, 12];

  return (
    <>
      {amortizationPlan.term > 3 && type === 'default' && (
        <Box mb={3}>
          <Box display={'flex'} justifyContent={'space-between'}>
            <Box mr={2} className={'donut-chart'} height={CIRCLE_HEIGHT} width={CIRCLE_HEIGHT}>
              {amortizationPlan.instalments.map((instalment, key) => (
                <div
                  key={key}
                  className={'part portion-block'}
                  style={{
                    clip: `rect(0px, ${CIRCLE_HEIGHT}px, ${CIRCLE_HEIGHT}px, ${CIRCLE_HEIGHT / 2}px)`,
                    transform: `rotate(${(360 / amortizationPlan.instalments.length) * key + 1}deg)`,
                  }}
                >
                  <Box
                    sx={{ bgcolor: key === 0 ? 'primary.main' : '#E1E1E5' }}
                    style={{
                      clip: `rect(0px, ${CIRCLE_HEIGHT / 2}px, ${CIRCLE_HEIGHT}px, 0px)`,
                      animationDelay: `${key * (amortizationPlan.term === 12 ? 0.05 : 0.1)}s`,
                    }}
                    className={
                      ALLOWED_DONUT_TERMS.includes(amortizationPlan.term)
                        ? `circle-${amortizationPlan.term}`
                        : 'circle-10'
                    }
                  />
                </div>
              ))}
              <p
                className={'center'}
                style={{
                  width: `${CIRCLE_HEIGHT * 0.8}px`,
                  height: `${CIRCLE_HEIGHT * 0.8}px`,
                }}
              />
            </Box>
            <Box>
              <Box display={'flex'} alignItems={'flex-start'}>
                {downpaymentPaid ? (
                  <Box className={'square'} sx={{ backgroundColor: 'primary.main' }}>
                    <i className={'material-icons-outlined'}>check</i>
                  </Box>
                ) : (
                  <Box className={'square'} sx={{ backgroundColor: 'primary.main' }} />
                )}
                <Box sx={{ ml: '4px' }}>
                  <Typography variant={'body2'}>
                    <strong>{translation?.TODAY}</strong>
                  </Typography>
                  <Typography
                    variant={'body2'}
                  >{`${translation?.PAYMENT} 1 ${translation?.OF} ${amortizationPlan.term}`}</Typography>
                  <Typography variant={'body1'}>
                    <strong>{amortizationPlan.instalments[0].instalmentTotalAmount.string}</strong>
                  </Typography>
                </Box>
              </Box>
              <Box mt={3} display={'flex'} alignItems={'flex-start'}>
                <Box className={'square'} sx={{ backgroundColor: '#E1E1E5' }} />
                <Box sx={{ ml: '4px' }}>
                  <Typography variant={'body2'}>
                    <strong>{`${translation?.IN} ${amortizationPlan.term - 1} ${translation?.TERMS}`}</strong>
                  </Typography>
                  <Typography variant={'body2'}>{translation?.FROM_NEXT_MONTH}</Typography>
                  <Typography variant={'body1'}>
                    <strong>{amortizationPlan.instalments[1].instalmentTotalAmount.string}</strong>
                  </Typography>
                </Box>
              </Box>
            </Box>
          </Box>
        </Box>
      )}
      {type === 'default' && amortizationPlan.term <= 3 && (
        <Box mx={2} position={'relative'} flexGrow={1}>
          {amortizationPlan.instalments.map((instalment, key) => (
            <Box key={key} mb={3} display={'flex'} justifyContent={'space-between'} alignItems={'center'}>
              <Box>
                <Box display={'flex'} alignItems={'center'}>
                  {key === 0 && downpaymentPaid ? (
                    <Box className={'square'} sx={{ backgroundColor: 'primary.main' }}>
                      <i className={'material-icons-outlined'}>check</i>
                    </Box>
                  ) : (
                    <Box className={'square'} sx={{ backgroundColor: '#E1E1E5' }} />
                  )}
                  {key + 1 !== amortizationPlan.instalments.length && <Box className={'vertical-line'} />}
                  <Box>
                    <Typography variant={'body2'} sx={{ mb: '4px' }}>
                      <strong>
                        {instalment.instalmentNumber === 1 ? translation?.TODAY : instalment.instalmentDate.string}
                      </strong>
                    </Typography>
                    <Typography variant={'body2'}>
                      {`${translation?.PAYMENT} ${instalment.instalmentNumber} ${translation?.OF} ${amortizationPlan.term}`}
                    </Typography>
                  </Box>
                </Box>
              </Box>
              <Typography variant={'body1'}>
                <strong>{instalment.instalmentTotalAmount.string}</strong>
              </Typography>
            </Box>
          ))}
        </Box>
      )}
      {type === 'compressed' && (
        <Box position={'relative'}>
          {amortizationPlan.instalments.map((instalment, key) => (
            <Box key={key} display={'flex'} justifyContent={'space-between'} alignItems={'center'}>
              <Box>
                <Box display={'flex'} alignItems={'center'}>
                  {key === 0 && downpaymentPaid ? (
                    <Box className={'square-compressed'} sx={{ backgroundColor: 'primary.main' }}>
                      <i className={'material-icons-outlined'}>check</i>
                    </Box>
                  ) : (
                    <Box className={'square-compressed'} sx={{ backgroundColor: '#E1E1E5' }} />
                  )}
                  {key + 1 !== amortizationPlan.instalments.length && <Box className={'vertical-line-compressed'} />}
                  <Box>
                    <Typography variant={'body2'} component={'span'}>
                      {instalment.instalmentNumber === 1 ? translation?.TODAY : instalment.instalmentDate.string}
                      <Typography variant={'body2'} color={'text.secondary'} component={'span'}>
                        {`  ·  ${translation?.PAYMENT} ${instalment.instalmentNumber} ${translation?.OF} ${amortizationPlan.term}`}
                      </Typography>
                    </Typography>
                  </Box>
                </Box>
              </Box>
              <Typography variant={'body2'}>
                <strong>{instalment.instalmentTotalAmount.string}</strong>
              </Typography>
            </Box>
          ))}
        </Box>
      )}
    </>
  );
};

export default PaymentPlan;

type Props = {
  amortizationPlan: AmortizationPlanDto;
  type: 'default' | 'compressed';
  downpaymentPaid?: boolean;
};
