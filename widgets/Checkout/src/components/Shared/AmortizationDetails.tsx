import { Accordion, AccordionDetails, AccordionSummary, Button } from '@mui/material';
import { AmortizationPlanDto } from '@nemuru-eng/js-api-client';
import React, { useEffect } from 'react';

import { useDataLayer, DLEvent } from '../../hooks/useDataLayer';
import { translation } from '../../translations/Translation';
import { PaymentPlan } from './PaymentPlan/PaymentPlan';

const TRANSITION_TIMEOUT = 300;

export const AmortizationDetails: React.FC<{ amortizationPlan: AmortizationPlanDto }> = ({ amortizationPlan }) => {
  const { pushToDataLayer } = useDataLayer();
  const [expanded, setExpanded] = React.useState<string | false>(false);

  const handleChange = (panel: string) => (event: React.SyntheticEvent, isExpanded: boolean) => {
    pushToDataLayer({
      event: DLEvent.BTN_CLICK,
      additionalPayload: { type: 'payment_details', label: translation?.PAYMENT_DETAILS },
    });
    setExpanded(isExpanded ? panel : false);
  };

  useEffect(() => {
    if (expanded) {
      setTimeout(() => {
        const el = document.getElementById('amortization-details-content');
        if (el) {
          const height = el.offsetHeight / 2;
          const scrollEl = document.getElementById(window.innerWidth > 500 ? 'popup-wrapper' : 'popup-content');
          scrollEl?.scroll({ top: scrollEl.scrollTop + height, left: 0, behavior: 'smooth' });
        }
      }, TRANSITION_TIMEOUT + 10);
    }
  }, [expanded]);

  return (
    <>
      <Accordion
        disableGutters
        variant={'elevation'}
        elevation={0}
        expanded={expanded === 'panel1'}
        onChange={handleChange('panel1')}
        sx={{
          ':before': { backgroundColor: 'transparent' },
        }}
        TransitionProps={{ timeout: TRANSITION_TIMEOUT }}
      >
        <AccordionSummary
          sx={{
            p: 0,
            justifyContent: 'center',
            '& .MuiAccordionSummary-content': { m: 0, flexGrow: 'unset' },
            '& .MuiAccordionSummary-expandIconWrapper': { color: 'text.secondary' },
          }}
        >
          <Button
            disableFocusRipple
            disableTouchRipple
            variant={'text'}
            size={'small'}
            sx={{
              p: '4px 8px',
              color: 'text.secondary',
              border: 'solid 2px',
              borderColor: 'grey.A100',
              textDecoration: 'none',
              borderRadius: (theme) => (theme.shape.borderRadius === 0 ? 0 : 50),
              ':hover': {
                textDecoration: 'none',
              },
            }}
            startIcon={<i className={'material-icons-outlined'}>{expanded ? 'expand_less' : 'expand_more'}</i>}
          >
            {translation?.PAYMENT_DETAILS}
          </Button>
        </AccordionSummary>
        <AccordionDetails
          id={'amortization-details-content'}
          sx={{ p: 0, pb: 3, boxShadow: '0px 15px 20px -10px rgb(0 0 0 / 3%)' }}
        >
          <PaymentPlan amortizationPlan={amortizationPlan} type={'compressed'} />
        </AccordionDetails>
      </Accordion>
    </>
  );
};
