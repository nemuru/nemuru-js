import { Box, FilledTextFieldProps, FormControl, FormHelperText, InputLabel, MenuItem } from '@mui/material';
import { getCountriesAvailable } from '@nemuru-eng/js-api-client';
import React, { ForwardedRef } from 'react';
import { FieldErrors } from 'react-hook-form';

import Branding from '../../../../../src/shared/types/Style/Branding';
import { useCheckoutContext } from '../../contexts/Checkout/CheckoutContext';
import { translation } from '../../translations/Translation';
import { StyledSelect } from './StyledSelect';
import { StyledTextField } from './StyledTextField';

export const PhoneNumberTextField = React.forwardRef((props: Props, ref: ForwardedRef<any>): JSX.Element => {
  const { dialCode, setDialCode, disabledDialCode = false, error, helperText, clearErrors, ...additionalProps } = props;

  const { style } = useCheckoutContext();

  const handleDialogCodeChange = (event: any): void => {
    setDialCode(event.target.value);
    clearErrors();
  };

  const labelBaseStyle = {
    color: 'text.secondary',
  };

  let labelBrandingStyle = undefined;
  if (style?.branding.value === Branding.IBERIA) {
    labelBrandingStyle = {
      textTransform: 'uppercase',
    };
  }

  return (
    <>
      <Box display={'flex'} alignItems={'flex-start'}>
        <FormControl variant="filled" sx={{ width: '150px' }} error={error}>
          <InputLabel sx={{ ...labelBaseStyle, ...labelBrandingStyle }}>{translation?.DIAL_CODE}</InputLabel>
          <StyledSelect
            value={dialCode}
            label={translation?.DIAL_CODE}
            variant={props.variant}
            size={'medium'}
            disabled={disabledDialCode}
            onChange={handleDialogCodeChange}
            MenuProps={{ sx: { zIndex: 9999999999 } }}
            renderValue={(value) => `+${dialCode}`}
            sx={{ mr: 1.5 }}
          >
            {getCountriesAvailable().map((country) => (
              <MenuItem
                key={country.iso2}
                value={country.dialCode}
              >{`${country.name} [+${country.dialCode}]`}</MenuItem>
            ))}
          </StyledSelect>
          {error && (
            <FormHelperText sx={{ minWidth: '300px' }} id={'phone-number-input-helper-text'}>
              {helperText}
            </FormHelperText>
          )}
        </FormControl>
        <StyledTextField
          {...additionalProps}
          ref={ref}
          fullWidth={true}
          size={'medium'}
          variant={props.variant}
          value={props.value}
          error={error}
          InputProps={{
            disableUnderline: true,
            sx: {
              borderRadius: '6px',
            },
          }}
        />
      </Box>
    </>
  );
});

interface Props extends FilledTextFieldProps {
  dialCode: string;
  setDialCode: (dialCode: string) => void;
  disabledDialCode?: boolean;
  errors: FieldErrors<{ phoneNumber?: string }>;
  clearErrors: () => void;
}
