import { OnboardingField } from './OnboardingField';

export interface ErrorFieldsProps {
  fieldsWithErrors?: OnboardingField[];
  removeErrorFields: (errorField: OnboardingField) => void;
}
