export enum OnboardingField {
  firstName = 'firstName',
  lastName = 'lastName',
  secondLastName = 'secondLastName',
  birthDate = 'birthDate',
  phoneNumber = 'phoneNumber',
  email = 'email',
  documentNumber = 'documentNumber',
  streetAddress = 'streetAddress',
  postalCode = 'postalCode',
  city = 'city',
}
