export enum OnboardingStep {
  PERSONAL_INFO = 'PERSONAL_INFO',
  PERSONAL_INFO_AND_PHONE_NUMBER = 'PERSONAL_INFO_AND_PHONE_NUMBER',
  ADDRESS_INFO = 'ADDRESS_INFO',
  CONTACT_INFO = 'CONTACT_INFO',
}
