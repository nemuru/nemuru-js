import {
  Action,
  getOrderByLoanId,
  getProcessResponseByLoanApplicationId,
  InternalErrorException,
  postLoanApplicationInLender,
  postLoanApplicationsByLoanId,
  postSetCustomerDetailsToOrder,
  ProcessError,
} from '@nemuru-eng/js-api-client';
import { useMemo, useState } from 'react';

import { useCheckoutContext } from '../../../contexts/Checkout/CheckoutContext';
import { NavigationStep } from '../../../hooks/useNavigation/types/NavigationStep';
import { ACTIONS_TO_CLOSE_SCREEN } from '../../../types/Actions';
import { CHECKOUT_STEPS } from '../../../types/CheckoutSteps';
import { Customer } from '../../../types/Customer';
import { getLastNames } from '../../../utils/functions';
import { AddressInfo } from '../components/AddressInfo';
import { ContactInfo } from '../components/ContactInfo';
import { PersonalInfo } from '../components/PersonalInfo';
import { PersonalInfoAndPhoneNumber } from '../components/PersonalInfoAndPhoneNumber';
import { OnboardingFieldMap } from '../maps/OnboardingFieldMap';
import { OnboardingFieldsMap } from '../maps/OnboardingFieldsMap';
import { OnboardingField } from '../types/OnboardingField';
import { OnboardingStep } from '../types/OnboardingStep';

export const useOnboardingDetails = (onSubmit?: () => void) => {
  const { order, customer, behaviour, borrowerId, setCheckoutStep, fieldsWithErrors, updateCheckout } =
    useCheckoutContext();

  const [originalCustomer] = useState(customer);

  const onComplete = async (): Promise<void> => {
    if (!order) return;
    await postCustomerDetailsToOrder(customer);
    if (order.applicationSelected === undefined && fieldsWithErrors === undefined) {
      try {
        await postLoanApplicationsByLoanId({ loanId: order.id });
      } catch (err) {}
    }
    if (order.applicationSelected !== undefined || fieldsWithErrors) {
      await processLoanApplicationInLender();
    }
    await pollingAtGetOrderToEnsureLoanApplicationIsSelected();
    if (onSubmit) {
      onSubmit();
    }
  };

  // FIXME: move to another hooks
  const onEditPhoneNumber = async (customer: Customer, onSuccess: () => void) => {
    if (!order?.applicationSelected) return;
    await postCustomerDetailsToOrder(customer);
    try {
      await postLoanApplicationInLender({ loanApplicationId: order.applicationSelected.id });
    } catch (err) {
      if (err instanceof InternalErrorException) {
        if (err.message.includes('Unprocessable Entity')) {
          const processResponse = await getProcessResponseByLoanApplicationId({
            loanApplicationId: order.applicationSelected.id,
          });
          const onboardingFieldsWithErrors = getOnboardingFieldsFromErrors(processResponse.errors).filter(
            (error) => error !== undefined,
          ) as OnboardingField[];
          updateCheckout({ fieldsWithErrors: onboardingFieldsWithErrors });
          return;
        } else {
          setCheckoutStep(CHECKOUT_STEPS.ERROR);
          return;
        }
      }
    }
    const newOrder = await getOrderByLoanId({ loanId: order.id });
    updateCheckout({ order: newOrder, customer: customer });
    onSuccess();
  };

  const postCustomerDetailsToOrder = async (customer?: Customer): Promise<void> => {
    const lastNames = getLastNames(customer?.lastName, customer?.secondLastName);
    if (
      !order ||
      !lastNames ||
      !borrowerId ||
      !customer?.email ||
      !customer?.firstName ||
      !customer?.birthDate ||
      !customer?.phoneNumber ||
      !customer?.documentNumber
    )
      throw new Error();
    // if (fieldsWithErrors) {
    //   // FIXME: Is this necessary??? Possible side effects
    //   updateCheckout({ fieldsWithErrors: undefined });
    // }
    try {
      await postSetCustomerDetailsToOrder({
        loanId: order.id,
        borrowerId,
        onboardingDetailsId: order.customer.id,
        firstName: customer.firstName,
        lastName: lastNames,
        email: customer.email,
        phoneNumber: customer.phoneNumber.value,
        documentNumber: customer.documentNumber,
        birthDate: customer.birthDate,
      });
    } catch (err) {
      setCheckoutStep(CHECKOUT_STEPS.ERROR);
      return;
    }
  };

  const processLoanApplicationInLender = async () => {
    if (order) {
      const newOrder = await getOrderByLoanId({ loanId: order.id });
      if (newOrder.applicationSelected) {
        await postLoanApplicationInLender({ loanApplicationId: newOrder.applicationSelected.id });
      }
    }
  };

  function pollingAtGetOrderToEnsureLoanApplicationIsSelected() {
    let attempts = 0;
    return new Promise(function (resolve, reject) {
      (async function waitForLoanApplicationSelected() {
        if (order) {
          attempts += 1;
          const newOrder = await getOrderByLoanId({ loanId: order.id });
          if (ACTIONS_TO_CLOSE_SCREEN.includes(newOrder.action)) {
            updateCheckout({ order: newOrder });
            return resolve(() => {});
          }
          if (
            newOrder.applicationSelected !== undefined &&
            newOrder.applicationSelected.lenderFields.loanApplicationStatus.state === 'selected' &&
            !newOrder.applicationSelected.lenderFields.lenderRequestId
          ) {
            try {
              const processResponse = await getProcessResponseByLoanApplicationId({
                loanApplicationId: newOrder.applicationSelected.id,
              });
              const onboardingFieldsWithErrors = getOnboardingFieldsFromErrors(processResponse.errors).filter(
                (error) => error !== undefined,
              ) as OnboardingField[];
              updateCheckout({ fieldsWithErrors: onboardingFieldsWithErrors });
              return reject(() => {});
            } catch (err) {
              setCheckoutStep(CHECKOUT_STEPS.ERROR);
              return reject(() => {});
            }
          }
          if (
            newOrder.applicationSelected !== undefined &&
            newOrder.applicationSelected.lenderFields.lenderRequestId &&
            newOrder.action === Action.REQUIRE_VALIDATE_OTP
          ) {
            updateCheckout({ order: newOrder });
            return resolve(() => {});
          }
          if (attempts > 25) {
            setCheckoutStep(CHECKOUT_STEPS.ERROR);
            return reject(() => {});
          }
        }
        setTimeout(await waitForLoanApplicationSelected, 1000);
      })();
    });
  }

  const getOnboardingFieldsFromErrors = (processErrors: ProcessError[]): (OnboardingField | undefined)[] => {
    return processErrors.map((error) => {
      return OnboardingFieldMap.fromErrorField(error.value);
    });
  };

  const personalInfoVisibility = () => {
    const onboardingFields = OnboardingFieldsMap.fromOnboardingStep(OnboardingStep.PERSONAL_INFO);
    if (!originalCustomer) return false;
    if (originalCustomer.email) return false;
    if (fieldsWithErrors?.length && onboardingFields.some((field) => fieldsWithErrors.includes(field))) return true;
    if (fieldsWithErrors?.length && !onboardingFields.some((field) => fieldsWithErrors.includes(field))) return false;
    if (
      originalCustomer.firstName &&
      originalCustomer.lastName &&
      originalCustomer.birthDate &&
      originalCustomer.documentNumber &&
      behaviour?.disableFormEdition
    )
      return false;
    return true;
  };

  const personalInfoAndPhoneNumberVisibility = () => {
    const onboardingFields = OnboardingFieldsMap.fromOnboardingStep(OnboardingStep.PERSONAL_INFO_AND_PHONE_NUMBER);
    if (!originalCustomer) return false;
    if (!originalCustomer.email) return false;
    if (fieldsWithErrors?.length && onboardingFields.some((field) => fieldsWithErrors.includes(field))) return true;
    if (fieldsWithErrors?.length && !onboardingFields.some((field) => fieldsWithErrors.includes(field))) return false;
    if (
      originalCustomer.firstName &&
      originalCustomer.lastName &&
      originalCustomer.birthDate &&
      originalCustomer.phoneNumber &&
      originalCustomer.documentNumber &&
      behaviour?.disableFormEdition
    )
      return false;
    return true;
  };

  const addressInfoVisibility = () => {
    const onboardingFields = OnboardingFieldsMap.fromOnboardingStep(OnboardingStep.ADDRESS_INFO);
    if (!originalCustomer) return false;
    if (!originalCustomer.requireAddress) return false;
    if (fieldsWithErrors?.length && onboardingFields.some((field) => fieldsWithErrors.includes(field))) return true;
    if (fieldsWithErrors?.length && !onboardingFields.some((field) => fieldsWithErrors.includes(field))) return false;
    return true;
  };

  const contactInfoVisibility = () => {
    const onboardingFields = OnboardingFieldsMap.fromOnboardingStep(OnboardingStep.CONTACT_INFO);
    if (!originalCustomer) return false;
    if (originalCustomer.email) return false;
    if (fieldsWithErrors?.length && onboardingFields.some((field) => fieldsWithErrors.includes(field))) return true;
    if (fieldsWithErrors?.length && !onboardingFields.some((field) => fieldsWithErrors.includes(field))) return false;
    return true;
  };

  // Every time there's a fieldsWithErrors change,
  // the updatedAt field will change, so that the useNavigation hook
  // will detect a new array of steps (even if visible steps are the same)
  // and reset the navigation state.
  const updatedAt = useMemo(() => {
    if (fieldsWithErrors) return Date.now();
  }, [fieldsWithErrors]);

  const STEPS: NavigationStep[] = [
    {
      name: OnboardingStep.PERSONAL_INFO,
      component: PersonalInfo,
      visible: personalInfoVisibility(),
      updatedAt,
    },
    {
      name: OnboardingStep.PERSONAL_INFO_AND_PHONE_NUMBER,
      component: PersonalInfoAndPhoneNumber,
      visible: personalInfoAndPhoneNumberVisibility(),
      updatedAt,
    },
    {
      name: OnboardingStep.ADDRESS_INFO,
      component: AddressInfo,
      visible: addressInfoVisibility(),
      updatedAt,
    },
    {
      name: OnboardingStep.CONTACT_INFO,
      component: ContactInfo,
      visible: contactInfoVisibility(),
      updatedAt,
    },
  ];

  const steps = STEPS.filter((step) => step.visible);

  return {
    steps,
    onComplete,
    onEditPhoneNumber,
  };
};
