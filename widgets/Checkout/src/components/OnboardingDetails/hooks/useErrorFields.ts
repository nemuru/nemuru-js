import { useCheckoutContext } from '../../../contexts/Checkout/CheckoutContext';
import { OnboardingField } from '../types/OnboardingField';

export const useErrorFields = () => {
  const { fieldsWithErrors, disabledFormFields, updateCheckout } = useCheckoutContext();

  // useEffect(() => {
  //   if (fieldsWithErrors) {
  //     redirectToStepWithErrors();
  //   }
  // }, [fieldsWithErrors]);

  // const redirectToStepWithErrors = (): void => {
  //   if (!fieldsWithErrors) return;
  //   for (const stepName in OnboardingStep) {
  //     const error = OnboardingFieldsMap.fromOnboardingStep(stepName as OnboardingStep).some((field) =>
  //       fieldsWithErrors.includes(field),
  //     );
  //     if (error && steps.some((step) => step.name === stepName)) {
  //       handleRedirect(stepName);
  //       break;
  //     }
  //   }
  // };

  const removeErrorFields = (errorField: OnboardingField): void => {
    if (!fieldsWithErrors) return;
    const newFieldsWitherrors = fieldsWithErrors.filter((field) => errorField !== field);
    updateCheckout({ fieldsWithErrors: newFieldsWitherrors });
  };

  const fieldIsDisabled = (field: OnboardingField): boolean => {
    if (fieldsWithErrors?.length && fieldsWithErrors.includes(field)) return false;
    if (disabledFormFields?.length && disabledFormFields.includes(field)) return true;
    return false;
  };

  return { fieldsWithErrors, removeErrorFields, fieldIsDisabled };
};
