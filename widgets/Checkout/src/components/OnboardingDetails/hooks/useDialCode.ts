import { useEffect } from 'react';

import Branding from '../../../../../../src/shared/types/Style/Branding';
import { useCheckoutContext } from '../../../contexts/Checkout/CheckoutContext';

export const SPANISH_DIAL_CODE = '34';

export const useDialCode = () => {
  const { dialCode, customer, order, style, updateCheckout } = useCheckoutContext();

  const setDialCode = (dialCode: string) => updateCheckout({ dialCode });

  useEffect(() => {
    if (customer && style && !dialCode) {
      setDialCode(
        style.branding.value === Branding.LENDING_HUB
          ? SPANISH_DIAL_CODE
          : customer?.phoneNumber?.countryCallingCode ?? SPANISH_DIAL_CODE,
      );
    }
  }, [order]);

  return { dialCode, setDialCode };
};
