import React from 'react';

import { useNavigation } from '../../hooks/useNavigation/useNavigation';
import { useDialCode } from './hooks/useDialCode';
import { useOnboardingDetails } from './hooks/useOnboardingDetails';

export const OnboardingDetails: React.FC<{ onSubmit: () => void }> = ({ onSubmit }) => {
  const { steps, onComplete } = useOnboardingDetails(onSubmit);
  const { StepComponent } = useNavigation({ steps, onComplete });
  const { dialCode, setDialCode } = useDialCode();

  if (!dialCode) return null;

  return <StepComponent dialCode={dialCode} setDialCode={setDialCode} />;
};
