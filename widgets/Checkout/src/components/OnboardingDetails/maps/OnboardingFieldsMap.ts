import { OnboardingField } from '../types/OnboardingField';
import { OnboardingStep } from '../types/OnboardingStep';

export class OnboardingFieldsMap {
  static fromOnboardingStep = (onboardingStep: OnboardingStep): OnboardingField[] => {
    switch (onboardingStep) {
      case OnboardingStep.PERSONAL_INFO:
        return [
          OnboardingField.firstName,
          OnboardingField.lastName,
          OnboardingField.secondLastName,
          OnboardingField.birthDate,
          OnboardingField.documentNumber,
        ];
      case OnboardingStep.PERSONAL_INFO_AND_PHONE_NUMBER:
        return [
          OnboardingField.firstName,
          OnboardingField.lastName,
          OnboardingField.secondLastName,
          OnboardingField.birthDate,
          OnboardingField.documentNumber,
          OnboardingField.phoneNumber,
        ];
      case OnboardingStep.ADDRESS_INFO:
        return [OnboardingField.streetAddress, OnboardingField.city, OnboardingField.postalCode];
      case OnboardingStep.CONTACT_INFO:
        return [OnboardingField.email, OnboardingField.phoneNumber];
      default:
        // This case will never be reached,
        // so we can use the never type to avoid
        // returning undefined from the function.
        return onboardingStep as never;
    }
  };
}
