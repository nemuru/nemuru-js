import { ErrorField } from '@nemuru-eng/js-api-client';

import { OnboardingField } from '../types/OnboardingField';

export class OnboardingFieldMap {
  static fromErrorField = (errorField: ErrorField): OnboardingField => {
    switch (errorField) {
      case ErrorField.FIRST_NAME:
        return OnboardingField.firstName;
      case ErrorField.LAST_NAME:
        return OnboardingField.lastName;
      case ErrorField.BIRTH_DATE:
        return OnboardingField.birthDate;
      case ErrorField.PHONE_NUMBER:
        return OnboardingField.phoneNumber;
      case ErrorField.EMAIL:
        return OnboardingField.email;
      case ErrorField.DOCUMENT_NUMBER:
        return OnboardingField.documentNumber;
      case ErrorField.STREET_ADDRESS:
        return OnboardingField.streetAddress;
      case ErrorField.POSTAL_CODE:
        return OnboardingField.postalCode;
      case ErrorField.CITY:
        return OnboardingField.city;
      default:
        // This case will never be reached,
        // so we can use the never type to avoid
        // returning undefined from the function.
        return errorField as never;
    }
  };
}
