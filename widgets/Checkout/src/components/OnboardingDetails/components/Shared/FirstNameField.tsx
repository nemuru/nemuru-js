import React from 'react';
import { FieldErrors } from 'react-hook-form/dist/types/errors';
import { UseFormClearErrors, UseFormRegister } from 'react-hook-form/dist/types/form';

import { useCheckoutContext } from '../../../../contexts/Checkout/CheckoutContext';
import { CheckoutLocation, DLEvent, useDataLayer } from '../../../../hooks/useDataLayer';
import { translation } from '../../../../translations/Translation';
import { StyledTextField } from '../../../Shared/StyledTextField';
import { useErrorFields } from '../../hooks/useErrorFields';
import { OnboardingField } from '../../types/OnboardingField';

interface FormValues {
  firstName?: string;
}

interface FirstNameFieldProps {
  loading: boolean;
  register: UseFormRegister<FormValues>;
  errors: FieldErrors<FormValues>;
  clearErrors: UseFormClearErrors<FormValues>;
  location: CheckoutLocation;
}

export const FirstNameField: React.FC<FirstNameFieldProps> = ({ loading, register, errors, clearErrors, location }) => {
  const { behaviour } = useCheckoutContext();
  const { fieldIsDisabled } = useErrorFields();
  const { pushToDataLayer } = useDataLayer();

  if (!translation) return null;

  return (
    <StyledTextField
      sx={{ mb: 2 }}
      id={'first-name-input'}
      label={translation.FIRSTNAME}
      variant="filled"
      size={'medium'}
      error={!!errors.firstName && !behaviour?.disableFormEdition}
      helperText={!behaviour?.disableFormEdition && errors.firstName?.message}
      disabled={loading || fieldIsDisabled(OnboardingField.firstName)}
      onFocus={() =>
        pushToDataLayer({
          event: DLEvent.INPUT_FOCUS,
          location,
          additionalPayload: { inputField: 'firstName' },
        })
      }
      InputProps={{
        disableUnderline: true,
        sx: {
          borderRadius: '6px',
        },
      }}
      {...register(OnboardingField.firstName as keyof FormValues, {
        maxLength: {
          value: 255,
          message: `${translation.MAX_LENGTH_1} 255 ${translation.MAX_LENGTH_2}`,
        },
        required: {
          value: true,
          message: translation.REQUIRED_FIELD,
        },
        onChange: () => errors && clearErrors(OnboardingField.firstName),
      })}
    />
  );
};
