import React from 'react';
import { FieldErrors } from 'react-hook-form/dist/types/errors';
import { UseFormClearErrors, UseFormRegister } from 'react-hook-form/dist/types/form';

import { useCheckoutContext } from '../../../../contexts/Checkout/CheckoutContext';
import { CheckoutLocation, DLEvent, useDataLayer } from '../../../../hooks/useDataLayer';
import { translation } from '../../../../translations/Translation';
import { getLastNames } from '../../../../utils/functions';
import { validateLastNameByDocumentNumber } from '../../../../utils/validations';
import { StyledTextField } from '../../../Shared/StyledTextField';
import { useErrorFields } from '../../hooks/useErrorFields';
import { OnboardingField } from '../../types/OnboardingField';

interface FormValues {
  lastName?: string;
}

interface LastNameFieldProps {
  loading: boolean;
  secondLastName: string | undefined;
  documentNumber: string | undefined;
  register: UseFormRegister<FormValues>;
  errors: FieldErrors<FormValues>;
  clearErrors: UseFormClearErrors<FormValues>;
  location: CheckoutLocation;
}

export const LastNameField: React.FC<LastNameFieldProps> = ({
  loading,
  secondLastName,
  documentNumber,
  register,
  errors,
  clearErrors,
  location,
}) => {
  const { behaviour } = useCheckoutContext();
  const { fieldIsDisabled } = useErrorFields();
  const { pushToDataLayer } = useDataLayer();

  if (!translation) return null;

  return (
    <StyledTextField
      sx={{ mb: 2 }}
      id={'last-name-input'}
      label={translation.LASTNAME}
      variant="filled"
      size={'medium'}
      error={!!errors.lastName && !behaviour?.disableFormEdition}
      helperText={!behaviour?.disableFormEdition && errors.lastName?.message}
      disabled={loading || fieldIsDisabled(OnboardingField.lastName)}
      onFocus={() =>
        pushToDataLayer({
          event: DLEvent.INPUT_FOCUS,
          location,
          additionalPayload: { inputField: 'lastName' },
        })
      }
      InputProps={{
        disableUnderline: true,
        sx: {
          borderRadius: '6px',
        },
      }}
      {...register(OnboardingField.lastName as keyof FormValues, {
        maxLength: {
          value: 255,
          message: `${translation?.MAX_LENGTH_1} 255 ${translation?.MAX_LENGTH_2}`,
        },
        required: {
          value: true,
          message: translation?.REQUIRED_FIELD,
        },
        onChange: () => errors && clearErrors(OnboardingField.lastName),
        validate: {
          validate: (value: string | undefined) => {
            const lastNames = getLastNames(value, secondLastName);
            return validateLastNameByDocumentNumber(lastNames, documentNumber);
          },
        },
      })}
    />
  );
};
