import React from 'react';
import { FieldErrors } from 'react-hook-form/dist/types/errors';
import { UseFormClearErrors, UseFormRegister } from 'react-hook-form/dist/types/form';

import { useCheckoutContext } from '../../../../contexts/Checkout/CheckoutContext';
import { CheckoutLocation, DLEvent, useDataLayer } from '../../../../hooks/useDataLayer';
import { translation } from '../../../../translations/Translation';
import { validateBirthDate } from '../../../../utils/validations';
import { StyledTextField } from '../../../Shared/StyledTextField';
import { useErrorFields } from '../../hooks/useErrorFields';
import { OnboardingField } from '../../types/OnboardingField';

interface FormValues {
  birthDate?: string;
}

interface BirthDateFieldProps {
  loading: boolean;
  birthDate: string | undefined;
  register: UseFormRegister<FormValues>;
  errors: FieldErrors<FormValues>;
  clearErrors: UseFormClearErrors<FormValues>;
  location: CheckoutLocation;
}

export const BirthDateField: React.FC<BirthDateFieldProps> = ({
  loading,
  birthDate,
  register,
  errors,
  clearErrors,
  location,
}) => {
  const { env } = useCheckoutContext();
  const { fieldIsDisabled } = useErrorFields();
  const { pushToDataLayer } = useDataLayer();

  if (!translation || !env) return null;

  return (
    <StyledTextField
      sx={{ mb: 2 }}
      id={'birth-date-input'}
      label={translation?.BIRTHDATE}
      variant="filled"
      size={'medium'}
      type={'date'}
      value={birthDate}
      error={!!errors.birthDate}
      helperText={errors.birthDate?.message}
      disabled={loading || fieldIsDisabled(OnboardingField.birthDate)}
      onFocus={() =>
        pushToDataLayer({
          event: DLEvent.INPUT_FOCUS,
          location,
          additionalPayload: { inputField: 'birthDate' },
        })
      }
      InputProps={{
        disableUnderline: true,
        sx: {
          borderRadius: '6px',
          color: birthDate && !isNaN(Date.parse(birthDate)) ? 'text.primary' : 'transparent',
          '&.Mui-focused': {
            color: 'text.primary',
          },
        },
      }}
      {...register(OnboardingField.birthDate as keyof FormValues, {
        required: {
          value: true,
          message: translation?.REQUIRED_FIELD,
        },
        onChange: () => errors && clearErrors(OnboardingField.birthDate),
        validate: {
          validate: (value: string | undefined) => validateBirthDate(env, value) || translation?.BIRTHDATE_ERROR,
        },
      })}
    />
  );
};
