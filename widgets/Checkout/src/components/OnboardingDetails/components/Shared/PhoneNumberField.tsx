import React from 'react';
import { FieldErrors } from 'react-hook-form/dist/types/errors';
import { UseFormClearErrors, UseFormRegister } from 'react-hook-form/dist/types/form';

import Branding from '../../../../../../../src/shared/types/Style/Branding';
import { useCheckoutContext } from '../../../../contexts/Checkout/CheckoutContext';
import { CheckoutLocation, DLEvent, useDataLayer } from '../../../../hooks/useDataLayer';
import { translation } from '../../../../translations/Translation';
import { validatePhoneNumber } from '../../../../utils/validations';
import { PhoneNumberTextField } from '../../../Shared/PhoneNumberTextField';
import { useDialCode } from '../../hooks/useDialCode';
import { useErrorFields } from '../../hooks/useErrorFields';
import { OnboardingField } from '../../types/OnboardingField';

interface FormValues {
  phoneNumber?: string;
}

interface PhoneNumberFieldProps {
  loading: boolean;
  phoneNumber: string | undefined;
  register: UseFormRegister<FormValues>;
  errors: FieldErrors<FormValues>;
  clearErrors: UseFormClearErrors<FormValues>;
  location: CheckoutLocation;
}

export const PhoneNumberField: React.FC<PhoneNumberFieldProps> = ({
  loading,
  phoneNumber,
  register,
  errors,
  clearErrors,
  location,
}) => {
  const { style } = useCheckoutContext();
  const { dialCode, setDialCode } = useDialCode();
  const { fieldIsDisabled } = useErrorFields();
  const { pushToDataLayer } = useDataLayer();

  if (!translation || !dialCode) return null;

  return (
    <PhoneNumberTextField
      sx={{ mb: 2 }}
      id={'phone-number-input'}
      dialCode={dialCode}
      setDialCode={setDialCode}
      disabledDialCode={
        style?.branding.value === Branding.LENDING_HUB || loading || fieldIsDisabled(OnboardingField.phoneNumber)
      }
      label={translation?.PHONE_NUMBER}
      errors={errors}
      size={'medium'}
      variant={'filled'}
      error={!!errors.phoneNumber}
      helperText={errors.phoneNumber?.message}
      disabled={loading || fieldIsDisabled(OnboardingField.phoneNumber)}
      clearErrors={clearErrors}
      value={phoneNumber ? phoneNumber.replace(/[^\d]/gm, '') : ''}
      onFocus={() =>
        pushToDataLayer({
          event: DLEvent.INPUT_FOCUS,
          location,
          additionalPayload: { inputField: 'phoneNumber' },
        })
      }
      {...register(OnboardingField.phoneNumber as keyof FormValues, {
        maxLength: {
          value: 16,
          message: `${translation?.MAX_LENGTH_1} 16 ${translation?.MAX_LENGTH_2}`,
        },
        required: {
          value: true,
          message: translation?.REQUIRED_FIELD,
        },
        onChange: () => {
          // if (fieldsWithErrors?.includes(OnboardingField.phoneNumber)) {
          //   removeErrorFields(OnboardingField.phoneNumber);
          // }
          errors && clearErrors(OnboardingField.phoneNumber);
        },
        validate: {
          validate: (value) => validatePhoneNumber(value, dialCode) || translation?.PHONE_NUMBER_ERROR,
        },
      })}
    />
  );
};
