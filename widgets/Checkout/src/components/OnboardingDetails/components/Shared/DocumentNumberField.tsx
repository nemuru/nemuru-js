import React from 'react';
import { FieldErrors } from 'react-hook-form/dist/types/errors';
import { UseFormClearErrors, UseFormRegister } from 'react-hook-form/dist/types/form';

import { CheckoutLocation, DLEvent, useDataLayer } from '../../../../hooks/useDataLayer';
import { translation } from '../../../../translations/Translation';
import { validateDocumentNumber, validateLastNameByDocumentNumber } from '../../../../utils/validations';
import { StyledTextField } from '../../../Shared/StyledTextField';
import { useErrorFields } from '../../hooks/useErrorFields';
import { OnboardingField } from '../../types/OnboardingField';

interface FormValues {
  lastName?: string;
  documentNumber?: string;
}

interface DocumentNumberFieldProps {
  loading: boolean;
  lastName: string | undefined;
  register: UseFormRegister<FormValues>;
  errors: FieldErrors<FormValues>;
  clearErrors: UseFormClearErrors<FormValues>;
  location: CheckoutLocation;
}

export const DocumentNumberField: React.FC<DocumentNumberFieldProps> = ({
  loading,
  lastName,
  register,
  errors,
  clearErrors,
  location,
}) => {
  const { fieldIsDisabled } = useErrorFields();
  const { pushToDataLayer } = useDataLayer();

  if (!translation) return null;

  return (
    <StyledTextField
      sx={{ mb: 2 }}
      id={'document-number-input'}
      label={translation?.DOCUMENT_NUMBER}
      variant="filled"
      size={'medium'}
      error={!!errors.documentNumber}
      helperText={errors.documentNumber?.message}
      disabled={loading || fieldIsDisabled(OnboardingField.documentNumber)}
      onFocus={() =>
        pushToDataLayer({
          event: DLEvent.INPUT_FOCUS,
          location,
          additionalPayload: { inputField: 'documentNumber' },
        })
      }
      InputProps={{
        disableUnderline: true,
        sx: {
          borderRadius: '6px',
        },
      }}
      {...register(OnboardingField.documentNumber as keyof FormValues, {
        required: {
          value: true,
          message: translation?.REQUIRED_FIELD,
        },
        onChange: (e) => {
          if (validateLastNameByDocumentNumber(lastName, e.target.value) === true && errors?.lastName) {
            clearErrors(OnboardingField.lastName);
          }
        },
        validate: {
          validate: (value) => validateDocumentNumber(value) || translation?.DOCUMENT_NUMBER_ERROR,
        },
      })}
    />
  );
};
