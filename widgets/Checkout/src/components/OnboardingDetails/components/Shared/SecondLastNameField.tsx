import React from 'react';
import { FieldErrors } from 'react-hook-form/dist/types/errors';
import { UseFormClearErrors, UseFormRegister } from 'react-hook-form/dist/types/form';

import { CheckoutLocation, DLEvent, useDataLayer } from '../../../../hooks/useDataLayer';
import { translation } from '../../../../translations/Translation';
import { StyledTextField } from '../../../Shared/StyledTextField';
import { useErrorFields } from '../../hooks/useErrorFields';
import { OnboardingField } from '../../types/OnboardingField';

interface FormValues {
  lastName?: string;
  secondLastName?: string;
}

interface SecondLastNameFieldProps {
  loading: boolean;
  register: UseFormRegister<FormValues>;
  errors: FieldErrors<FormValues>;
  clearErrors: UseFormClearErrors<FormValues>;
  location: CheckoutLocation;
}

export const SecondLastNameField: React.FC<SecondLastNameFieldProps> = ({
  loading,
  register,
  errors,
  clearErrors,
  location,
}) => {
  const { fieldIsDisabled } = useErrorFields();
  const { pushToDataLayer } = useDataLayer();

  if (!translation) return null;

  return (
    <StyledTextField
      sx={{ mb: 2 }}
      id={'second-last-name-input'}
      label={translation.SECOND_LASTNAME}
      variant="filled"
      size={'medium'}
      error={!!errors.secondLastName}
      helperText={errors.secondLastName?.message}
      disabled={loading || fieldIsDisabled(OnboardingField.secondLastName)}
      onFocus={() =>
        pushToDataLayer({
          event: DLEvent.INPUT_FOCUS,
          location,
          additionalPayload: { inputField: 'secondLastName' },
        })
      }
      InputProps={{
        disableUnderline: true,
        sx: {
          borderRadius: '6px',
        },
      }}
      {...register(OnboardingField.secondLastName as keyof FormValues, {
        maxLength: {
          value: 255,
          message: `${translation?.MAX_LENGTH_1} 255 ${translation?.MAX_LENGTH_2}`,
        },
        required: {
          value: true,
          message: translation?.REQUIRED_FIELD,
        },
        onChange: () => {
          if (errors?.lastName) clearErrors(OnboardingField.lastName);
        },
      })}
    />
  );
};
