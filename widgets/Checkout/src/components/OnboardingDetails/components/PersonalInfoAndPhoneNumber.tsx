import { Box, Typography } from '@mui/material';
import { PhoneNumber } from '@nemuru-eng/js-api-client';
import React, { useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';

import { useCheckoutContext } from '../../../contexts/Checkout/CheckoutContext';
import { CheckoutLocation, DLEvent, useDataLayer } from '../../../hooks/useDataLayer';
import { StepComponentProps } from '../../../hooks/useNavigation/types/StepComponentProps';
import { translation } from '../../../translations/Translation';
import { ErrorType } from '../../../types/ErrorType';
import { dateToStringAtom } from '../../../utils/functions';
import { MainButton } from '../../Shared/MainButton';
import { useErrorFields } from '../hooks/useErrorFields';
import { OnboardingField } from '../types/OnboardingField';
import { BirthDateField } from './Shared/BirthDateField';
import { DocumentNumberField } from './Shared/DocumentNumberField';
import { FirstNameField } from './Shared/FirstNameField';
import { LastNameField } from './Shared/LastNameField';
import { PhoneNumberField } from './Shared/PhoneNumberField';
import { SecondLastNameField } from './Shared/SecondLastNameField';

const location = CheckoutLocation.PERSONAL_INFO_AND_PHONE_NUMBER;

export const PersonalInfoAndPhoneNumber: React.FC<Props> = ({ dialCode, loading, handleNext }) => {
  const [errorType, setErrorType] = useState<ErrorType | undefined>(undefined);

  const { order, customer, behaviour, updateCheckout } = useCheckoutContext();
  const { fieldsWithErrors } = useErrorFields();
  const { pushToDataLayer } = useDataLayer();

  useEffect(() => {
    pushToDataLayer({ event: DLEvent.PAGE_VIEW, location });
  }, []);

  const formValues: FormValues = {
    [OnboardingField.firstName]: customer?.firstName,
    [OnboardingField.lastName]: customer?.lastName,
    [OnboardingField.secondLastName]: customer?.secondLastName,
    [OnboardingField.documentNumber]: customer?.documentNumber,
    [OnboardingField.birthDate]: dateToStringAtom(customer?.birthDate),
    [OnboardingField.phoneNumber]:
      dialCode === customer?.phoneNumber?.countryCallingCode ? customer?.phoneNumber?.nationalNumber : undefined,
  };

  const {
    getValues,
    watch,
    register,
    setError,
    clearErrors,
    formState: { errors },
    handleSubmit,
  } = useForm<FormValues>({
    defaultValues: formValues,
  });

  const lastName = watch(OnboardingField.lastName) as string | undefined;
  const birthDate = watch(OnboardingField.birthDate) as string | undefined;
  const phoneNumber = watch(OnboardingField.phoneNumber) as string | undefined;
  const documentNumber = watch(OnboardingField.documentNumber) as string | undefined;
  const secondLastName = watch(OnboardingField.secondLastName) as string | undefined;

  const [displaySecondLastName, setDisplaySecondLastName] = useState(false);

  useEffect(() => {
    if (fieldsWithErrors) {
      fieldsWithErrors.forEach((fieldWithError) => {
        if (Object.keys(formValues).includes(fieldWithError)) {
          pushToDataLayer({
            event: DLEvent.INPUT_ERROR,
            location, // @ts-ignore
            additionalPayload: { inputField: fieldWithError, inputValue: getValues(fieldWithError) },
          });
          setError(fieldWithError as keyof FormValues, {
            message: translation?.INVALID_FIELD_ERROR,
          });
        }
      });
    }
    return () => clearErrors();
  }, [fieldsWithErrors]);

  if (errors?.lastName && !displaySecondLastName && behaviour?.disableFormEdition === true) {
    setError(OnboardingField.secondLastName, { message: translation?.REQUIRED_FIELD });
    setDisplaySecondLastName(true);
  }

  const onNext = async (data: FormValues) => {
    if (!order || !dialCode || !data.lastName || !data.firstName || !data.birthDate || !data.documentNumber) return;
    errorType && setErrorType(undefined);
    const newCustomer = {
      ...customer,
      ...{
        lastName: data.lastName?.trim(),
        firstName: data.firstName?.trim(),
        birthDate: new Date(data.birthDate),
        documentNumber: data.documentNumber?.trim(),
        phoneNumber: data.phoneNumber ? new PhoneNumber(`+${dialCode}${data.phoneNumber}`) : undefined,
        requireAddress: customer?.requireAddress ?? false,
      },
      ...(data.secondLastName && { secondLastName: data.secondLastName?.trim() }),
    };
    updateCheckout({ customer: newCustomer });
    handleNext();
  };

  if (!translation) return null;

  return (
    <>
      <Box className={'appear-anim'}>
        <Typography variant={'h4'} sx={{ pt: 4, pb: 2 }} color={'secondary.main'}>
          {translation?.INTRODUCE_YOUR_INFO}
        </Typography>
        <Typography variant={'body2'}>{translation?.INTRODUCE_PERSONAL_INFO}</Typography>
        <Box mt={4} />
        <Box display={'flex'} flexDirection={'column'} justifyContent={'stretch'}>
          <FirstNameField
            loading={loading}
            register={register}
            errors={errors}
            clearErrors={clearErrors}
            location={location}
          />
          <LastNameField
            loading={loading}
            secondLastName={secondLastName}
            documentNumber={documentNumber}
            register={register}
            errors={errors}
            clearErrors={clearErrors}
            location={location}
          />
          {(displaySecondLastName || secondLastName) && (
            <SecondLastNameField
              loading={loading}
              register={register}
              errors={errors}
              clearErrors={clearErrors}
              location={location}
            />
          )}
          <BirthDateField
            loading={loading}
            birthDate={birthDate}
            register={register}
            errors={errors}
            clearErrors={clearErrors}
            location={location}
          />
          <DocumentNumberField
            loading={loading}
            lastName={lastName}
            register={register}
            errors={errors}
            clearErrors={clearErrors}
            location={location}
          />
          <PhoneNumberField
            loading={loading}
            phoneNumber={phoneNumber}
            register={register}
            errors={errors}
            clearErrors={clearErrors}
            location={location}
          />
        </Box>
      </Box>
      <Box pt={3}>
        <MainButton
          label={translation?.CONTINUE}
          loading={loading}
          onClick={() => {
            pushToDataLayer({
              event: DLEvent.BTN_CLICK,
              location,
              additionalPayload: { type: 'continue', label: translation?.CONTINUE },
            });
            handleSubmit(
              (data) => onNext(data),
              (errors) => {
                Object.keys(errors).forEach((errorKey: string) => {
                  const error = errors[errorKey as keyof FormValues];
                  pushToDataLayer({
                    event: DLEvent.INPUT_ERROR,
                    location,
                    additionalPayload: {
                      inputField: errorKey,
                      inputValue: error?.ref?.value,
                      errorMessage: error?.message,
                    },
                  });
                });
              },
            )();
          }}
        />
      </Box>
    </>
  );
};

interface Props extends StepComponentProps {
  dialCode: string;
  setDialCode: (dialCode: string) => void;
}

interface FormValues {
  firstName?: string;
  lastName?: string;
  secondLastName?: string;
  documentNumber?: string;
  birthDate?: string;
  phoneNumber?: string;
}
