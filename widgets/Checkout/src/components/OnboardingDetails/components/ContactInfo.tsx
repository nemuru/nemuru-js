import { Box, Typography } from '@mui/material';
import { PhoneNumber } from '@nemuru-eng/js-api-client';
import React, { useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';

import Branding from '../../../../../../src/shared/types/Style/Branding';
import { useCheckoutContext } from '../../../contexts/Checkout/CheckoutContext';
import { CheckoutLocation, DLEvent, useDataLayer } from '../../../hooks/useDataLayer';
import { StepComponentProps } from '../../../hooks/useNavigation/types/StepComponentProps';
import { translation } from '../../../translations/Translation';
import { ErrorType } from '../../../types/ErrorType';
import { validateEmail, validatePhoneNumber } from '../../../utils/validations';
import { Alert } from '../../Shared/Alert';
import { BackButton } from '../../Shared/BackButton/BackButton';
import { MainButton } from '../../Shared/MainButton';
import { PhoneNumberTextField } from '../../Shared/PhoneNumberTextField';
import { StyledTextField } from '../../Shared/StyledTextField';
import { useErrorFields } from '../hooks/useErrorFields';
import { OnboardingField } from '../types/OnboardingField';

const location = CheckoutLocation.CONTACT_INFO;

export const ContactInfo: React.FC<Props> = ({ dialCode, setDialCode, loading, showPrev, handlePrev, handleNext }) => {
  const [errorType, setErrorType] = useState<ErrorType | undefined>(undefined);
  const { fieldsWithErrors } = useErrorFields();

  const { style, customer, disabledFormFields, updateCheckout } = useCheckoutContext();
  const { pushToDataLayer } = useDataLayer();

  useEffect(() => {
    pushToDataLayer({ event: DLEvent.PAGE_VIEW, location });
  }, []);

  const formValues: FormValues = {
    [OnboardingField.phoneNumber]:
      dialCode === customer?.phoneNumber?.countryCallingCode ? customer?.phoneNumber?.nationalNumber : undefined,
    [OnboardingField.email]: customer?.email,
  };

  const {
    getValues,
    register,
    formState: { errors },
    setError,
    clearErrors,
    handleSubmit,
    watch,
  } = useForm<FormValues>({
    defaultValues: formValues,
  });

  const phoneNumber = watch(OnboardingField.phoneNumber) as string | undefined;

  useEffect(() => {
    if (fieldsWithErrors) {
      fieldsWithErrors.forEach((fieldWithError) => {
        if (Object.keys(formValues).includes(fieldWithError)) {
          pushToDataLayer({
            event: DLEvent.INPUT_ERROR,
            location, // @ts-ignore
            additionalPayload: { inputField: fieldWithError, inputValue: getValues(fieldWithError) },
          });
          setError(fieldWithError as keyof FormValues, { message: translation?.INVALID_FIELD_ERROR });
        }
      });
    }
    return () => clearErrors();
  }, [fieldsWithErrors]);

  useEffect(() => {
    // Cleanup method to store data on unmount
    return () => {
      const data = watch();
      if (!customer?.firstName) return;
      if (!data.email && !data.phoneNumber) return;
      const newCustomer = {
        ...customer,
        ...(data.phoneNumber && { phoneNumber: new PhoneNumber(`+${dialCode}${data.phoneNumber?.trim()}`) }),
        ...(data.email && { email: data.email?.trim() }),
      };
      updateCheckout({ customer: newCustomer });
    };
  }, []);

  const onNext = async (data: FormValues) => {
    if (!dialCode || !data.email || !data.phoneNumber || !customer?.firstName) return;
    if (errorType) setErrorType(undefined);
    const newCustomer = {
      ...customer,
      ...{
        phoneNumber: new PhoneNumber(`+${dialCode}${data.phoneNumber?.trim()}`),
        email: data.email?.trim(),
      },
    };
    updateCheckout({ customer: newCustomer });
    handleNext();
  };

  if (!dialCode || !translation) return null;

  return (
    <>
      <Box className={'appear-anim'}>
        <Box display={'flex'} alignItems={'center'}>
          {showPrev && (
            <Box mt={2} mr={2}>
              <BackButton
                disabled={loading}
                onClick={() => {
                  pushToDataLayer({
                    event: DLEvent.BTN_BACK,
                    location,
                  });
                  handlePrev();
                }}
              />
            </Box>
          )}
          <Typography variant={'h4'} sx={{ pt: 4, pb: 2 }} color={'secondary.main'}>
            {translation?.INTRODUCE_YOUR_INFO}
          </Typography>
        </Box>
        <Typography variant={'body2'}>{translation?.EMAIL_AND_PHONE_NUMBER}</Typography>
        <Box mt={4} />
        <Box display={'flex'} flexDirection={'column'} justifyContent={'stretch'}>
          <PhoneNumberTextField
            sx={{ mb: 2 }}
            id={'phone-number-input'}
            dialCode={dialCode}
            setDialCode={setDialCode}
            disabledDialCode={
              loading ||
              (disabledFormFields?.includes(OnboardingField.documentNumber) ??
                style?.branding.value === Branding.LENDING_HUB ??
                false)
            }
            label={translation?.PHONE_NUMBER}
            errors={errors}
            size={'medium'}
            variant={'filled'}
            error={!!errors.phoneNumber}
            helperText={errors.phoneNumber?.message}
            disabled={loading || (disabledFormFields?.includes(OnboardingField.phoneNumber) ?? false)}
            onFocus={() =>
              pushToDataLayer({
                event: DLEvent.INPUT_FOCUS,
                location,
                additionalPayload: { inputField: 'phoneNumber' },
              })
            }
            clearErrors={clearErrors}
            value={phoneNumber ? phoneNumber.replace(/[^\d]/gm, '') : ''}
            {...register(OnboardingField.phoneNumber as keyof FormValues, {
              maxLength: {
                value: 16,
                message: `${translation?.MAX_LENGTH_1} 16 ${translation?.MAX_LENGTH_2}`,
              },
              required: {
                value: true,
                message: translation?.REQUIRED_FIELD,
              },
              onChange: () => errors && clearErrors(),
              validate: {
                validate: (value) => validatePhoneNumber(value, dialCode) || translation?.PHONE_NUMBER_ERROR,
              },
            })}
          />
          <StyledTextField
            sx={{ mb: 2 }}
            id={'email-input'}
            label={translation?.EMAIL}
            fullWidth={true}
            variant="filled"
            size={'medium'}
            error={!!errors.email}
            helperText={errors.email?.message}
            onFocus={() =>
              pushToDataLayer({
                event: DLEvent.INPUT_FOCUS,
                location,
                additionalPayload: { inputField: 'email' },
              })
            }
            InputProps={{
              disableUnderline: true,
              sx: {
                borderRadius: '6px',
              },
            }}
            disabled={loading || (disabledFormFields?.includes(OnboardingField.email) ?? false)}
            {...register(OnboardingField.email as keyof FormValues, {
              maxLength: {
                value: 64,
                message: `${translation?.MAX_LENGTH_1} 64 ${translation?.MAX_LENGTH_2}`,
              },
              required: {
                value: true,
                message: translation?.REQUIRED_FIELD,
              },
              onChange: () => errors && clearErrors(),
              validate: {
                validate: (value) => validateEmail(value) || translation?.EMAIL_ERROR,
              },
            })}
          />
        </Box>
      </Box>
      <Box pt={3}>
        {errorType && (
          <Box pb={3}>
            <Alert variant={'error'} icon={true}>
              {translation.CONTACT_INFO_ERROR}
            </Alert>
          </Box>
        )}
        <MainButton
          label={translation?.CONTINUE}
          loading={loading}
          onClick={() => {
            pushToDataLayer({
              event: DLEvent.BTN_CLICK,
              location,
              additionalPayload: { type: 'continue', label: translation?.CONTINUE },
            });
            handleSubmit(
              (data) => onNext(data),
              (errors) => {
                Object.keys(errors).forEach((errorKey: string) => {
                  const error = errors[errorKey as keyof FormValues];
                  pushToDataLayer({
                    event: DLEvent.INPUT_ERROR,
                    location,
                    additionalPayload: {
                      inputField: errorKey,
                      inputValue: error?.ref?.value,
                      errorMessage: error?.message,
                    },
                  });
                });
              },
            )();
          }}
        />
      </Box>
    </>
  );
};

interface Props extends StepComponentProps {
  dialCode: string;
  setDialCode: (dialCode: string) => void;
}

interface FormValues {
  phoneNumber?: string;
  email?: string;
}
