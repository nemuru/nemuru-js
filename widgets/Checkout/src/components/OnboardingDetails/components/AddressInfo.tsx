import { Box, Typography } from '@mui/material';
import {
  BadRequestException,
  ConflictException,
  isValidPostalCode,
  patchSetAddressToOnlineOnboardingDetails,
} from '@nemuru-eng/js-api-client';
import React, { useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';

import { useCheckoutContext } from '../../../contexts/Checkout/CheckoutContext';
import { CheckoutLocation, DLEvent, useDataLayer } from '../../../hooks/useDataLayer';
import { StepComponentProps } from '../../../hooks/useNavigation/types/StepComponentProps';
import { translation } from '../../../translations/Translation';
import { CHECKOUT_STEPS } from '../../../types/CheckoutSteps';
import { ErrorType } from '../../../types/ErrorType';
import { Alert } from '../../Shared/Alert';
import { BackButton } from '../../Shared/BackButton/BackButton';
import { MainButton } from '../../Shared/MainButton';
import { StyledTextField } from '../../Shared/StyledTextField';
import { useErrorFields } from '../hooks/useErrorFields';
import { OnboardingField } from '../types/OnboardingField';

const location = CheckoutLocation.ADDRESS_INFO;

export const AddressInfo: React.FC<StepComponentProps> = ({
  loading,
  setLoading,
  showPrev,
  handlePrev,
  handleNext,
}) => {
  const { order, customer, updateCheckout, setCheckoutStep } = useCheckoutContext();
  const { fieldsWithErrors } = useErrorFields();
  const { pushToDataLayer } = useDataLayer();

  const [errorType, setErrorType] = useState<ErrorType | undefined>(undefined);

  useEffect(() => {
    pushToDataLayer({ event: DLEvent.PAGE_VIEW, location });
  }, []);

  const formValues: FormValues = {
    [OnboardingField.streetAddress]: customer?.address?.streetAddress,
    [OnboardingField.city]: customer?.address?.city,
    [OnboardingField.postalCode]: customer?.address?.postalCode,
  };

  const {
    watch,
    getValues,
    register,
    formState: { errors },
    clearErrors,
    setError,
    handleSubmit,
  } = useForm<FormValues>({
    defaultValues: formValues,
  });

  useEffect(() => {
    // Cleanup method to store data on unmount
    return () => {
      const data = watch();
      if (!customer || !data.streetAddress || !data.city || !data.postalCode) return;
      const newCustomer = {
        ...customer,
        ...{
          address: {
            ...customer.address,
            streetAddress: data.streetAddress.trim(),
            city: data.city.trim(),
            country: 'ESP', //FIXME
            postalCode: data.postalCode.trim(),
          },
        },
      };
      updateCheckout({ customer: newCustomer });
    };
  }, []);

  useEffect(() => {
    if (fieldsWithErrors) {
      fieldsWithErrors.forEach((fieldWithError) => {
        if (Object.keys(formValues).includes(fieldWithError)) {
          pushToDataLayer({
            event: DLEvent.INPUT_ERROR,
            location, // @ts-ignore
            additionalPayload: { inputField: fieldWithError, inputValue: getValues(fieldWithError) },
          });
          setError(fieldWithError as 'streetAddress' | 'postalCode' | 'city', {
            message: translation?.INVALID_FIELD_ERROR,
          });
        }
      });
    }
    return () => clearErrors();
  }, [fieldsWithErrors]);

  const onNext = async (data: FormValues): Promise<void> => {
    if (!data.streetAddress || !data.city || !data.postalCode) return;
    if (errorType) setErrorType(undefined);

    if (order && customer) {
      setLoading(true);
      const newCustomer = {
        ...customer,
        ...{
          address: {
            ...customer.address,
            streetAddress: data.streetAddress?.trim(),
            city: data.city?.trim(),
            country: 'ESP', //FIXME
            postalCode: data.postalCode?.trim(),
          },
        },
      };
      updateCheckout({ customer: newCustomer });
      try {
        await patchSetAddressToOnlineOnboardingDetails({
          onboardingDetailsId: order.customer.id,
          streetAddress: data.streetAddress?.trim(),
          streetAddress2: undefined,
          city: data.city?.trim(),
          country: 'ESP', //FIXME: hardcoding Spain
          postalCode: data.postalCode?.trim(),
        });
        handleNext();
      } catch (err) {
        if (err instanceof BadRequestException) {
          setErrorType(ErrorType.BAD_REQUEST);
        } else if (err instanceof ConflictException) {
          setErrorType(ErrorType.CONFLICT);
        } else {
          setCheckoutStep(CHECKOUT_STEPS.ERROR);
        }
      }
      setLoading(false);
    }
  };

  const validatePostalCode = (postalCode?: string): boolean => {
    if (!postalCode) return false;
    return isValidPostalCode(postalCode.trim());
  };

  if (!translation) return null;

  return (
    <>
      <Box className={'appear-anim'}>
        <Box display={'flex'} alignItems={'center'}>
          {showPrev && (
            <Box mt={2} mr={2}>
              <BackButton
                disabled={loading}
                onClick={() => {
                  pushToDataLayer({
                    event: DLEvent.BTN_BACK,
                    location,
                  });
                  handlePrev();
                }}
              />
            </Box>
          )}
          <Typography variant={'h4'} sx={{ pt: 4, pb: 2 }} color={'secondary.main'}>
            {translation?.INTRODUCE_YOUR_INFO}
          </Typography>
        </Box>
        <Typography variant={'body2'}>{translation?.INTRODUCE_RESIDENCE_ADDRESS}</Typography>
        <Box mt={4} />
        <Box display={'flex'} flexDirection={'column'} justifyContent={'stretch'}>
          <StyledTextField
            sx={{ mb: 3 }}
            id={'street-address-input'}
            label={translation?.ADDRESS}
            disabled={loading}
            variant={'filled'}
            size={'medium'}
            error={!!errors.streetAddress}
            helperText={errors.streetAddress?.message}
            onFocus={() =>
              pushToDataLayer({
                event: DLEvent.INPUT_FOCUS,
                location,
                additionalPayload: { inputField: 'streetAddress' },
              })
            }
            InputProps={{
              disableUnderline: true,
              sx: {
                borderRadius: '6px',
              },
            }}
            {...register(OnboardingField.streetAddress as keyof FormValues, {
              required: {
                value: true,
                message: translation?.REQUIRED_FIELD,
              },
              onChange: () => {
                // if (fieldsWithErrors?.includes(OnboardingField.streetAddress)) {
                //   removeErrorFields(OnboardingField.streetAddress);
                // }
                errors && clearErrors();
              },
            })}
          />
          <StyledTextField
            sx={{ mb: 3 }}
            id={'city-input'}
            label={translation?.CITY}
            disabled={loading}
            variant="filled"
            size={'medium'}
            error={!!errors.city}
            helperText={errors.city?.message}
            onFocus={() =>
              pushToDataLayer({
                event: DLEvent.INPUT_FOCUS,
                location,
                additionalPayload: { inputField: 'city' },
              })
            }
            InputProps={{
              disableUnderline: true,
              sx: {
                borderRadius: '6px',
              },
            }}
            {...register(OnboardingField.city as keyof FormValues, {
              required: {
                value: true,
                message: translation?.REQUIRED_FIELD,
              },
              onChange: () => {
                // if (fieldsWithErrors?.includes(OnboardingField.city)) {
                //   removeErrorFields(OnboardingField.city);
                // }
                errors && clearErrors();
              },
            })}
          />
          <StyledTextField
            sx={{ mb: 3 }}
            id={'postal-code-input'}
            label={translation?.POSTAL_CODE}
            disabled={loading}
            variant="filled"
            size={'medium'}
            error={!!errors.postalCode}
            helperText={errors.postalCode?.message}
            onFocus={() =>
              pushToDataLayer({
                event: DLEvent.INPUT_FOCUS,
                location,
                additionalPayload: { inputField: 'postalCode' },
              })
            }
            InputProps={{
              disableUnderline: true,
              sx: {
                borderRadius: '6px',
              },
            }}
            {...register(OnboardingField.postalCode as keyof FormValues, {
              required: {
                value: true,
                message: translation?.REQUIRED_FIELD,
              },
              onChange: () => {
                // if (fieldsWithErrors?.includes(OnboardingField.postalCode)) {
                //   removeErrorFields(OnboardingField.postalCode);
                // }
                errors && clearErrors();
              },
              validate: {
                validate: (value) => validatePostalCode(value) || translation?.INVALID_POSTAL_CODE,
              },
            })}
          />
        </Box>
      </Box>
      <Box pt={3}>
        {errorType && (
          <Box pb={3}>
            <Alert variant={'error'} icon={true}>
              {translation?.ADDRESS_ERROR}
            </Alert>
          </Box>
        )}
        <MainButton
          label={translation?.CONTINUE}
          loading={loading}
          onClick={() => {
            pushToDataLayer({
              event: DLEvent.BTN_CLICK,
              location,
              additionalPayload: { type: 'continue', label: translation?.CONTINUE },
            });
            handleSubmit(
              (data) => onNext(data),
              (errors) => {
                Object.keys(errors).forEach((errorKey: string) => {
                  const error = errors[errorKey as keyof FormValues];
                  pushToDataLayer({
                    event: DLEvent.INPUT_ERROR,
                    location,
                    additionalPayload: {
                      inputField: errorKey,
                      inputValue: error?.ref?.value,
                      errorMessage: error?.message,
                    },
                  });
                });
              },
            )();
          }}
        />
      </Box>
    </>
  );
};

interface FormValues {
  streetAddress?: string;
  city?: string;
  postalCode?: string;
}
