import { Box, Paper, Slide, Typography } from '@mui/material';
import { PhoneNumber } from '@nemuru-eng/js-api-client/';
import React, { useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';

import Branding from '../../../../../../src/shared/types/Style/Branding';
import { useCheckoutContext } from '../../../contexts/Checkout/CheckoutContext';
import { useDataLayer, CheckoutLocation, DLEvent } from '../../../hooks/useDataLayer';
import { translation } from '../../../translations/Translation';
import { validatePhoneNumber } from '../../../utils/validations';
import { useDialCode } from '../../OnboardingDetails/hooks/useDialCode';
import { useOnboardingDetails } from '../../OnboardingDetails/hooks/useOnboardingDetails';
import { OnboardingField } from '../../OnboardingDetails/types/OnboardingField';
import { MainButton } from '../../Shared/MainButton';
import { PhoneNumberTextField } from '../../Shared/PhoneNumberTextField';

const location = CheckoutLocation.PHONE_NUMBER_EDITION;

export const PhoneNumberEdition: React.FC<Props> = ({ dialogOpen, onCloseDialog }) => {
  const [loading, setLoading] = useState(false);

  const { style, order, customer, fieldsWithErrors, borrowerId } = useCheckoutContext();
  const { onEditPhoneNumber } = useOnboardingDetails();
  const { dialCode, setDialCode } = useDialCode();
  const { pushToDataLayer } = useDataLayer();

  const formValues: FormValues = {
    [OnboardingField.phoneNumber]: customer?.phoneNumber?.nationalNumber,
  };

  const {
    reset,
    register,
    setError,
    clearErrors,
    formState: { errors },
    handleSubmit,
    watch,
  } = useForm<FormValues>({
    defaultValues: formValues,
  });

  const phoneNumber = watch(OnboardingField.phoneNumber) as string | undefined;
  const phoneNumberChanged = phoneNumber !== customer?.phoneNumber?.nationalNumber;

  useEffect(() => {
    if (fieldsWithErrors) {
      fieldsWithErrors.forEach((fieldWithError) => {
        if (Object.keys(formValues).includes(fieldWithError)) {
          setError(fieldWithError as keyof FormValues, {
            message: translation?.INVALID_FIELD_ERROR,
          });
        }
      });
    }
    return () => clearErrors();
  }, [fieldsWithErrors]);

  const onSave = async (data: { phoneNumber?: string }) => {
    if (
      !order ||
      !borrowerId ||
      !customer?.email ||
      !customer?.lastName ||
      !customer?.firstName ||
      !customer?.birthDate ||
      !customer?.phoneNumber ||
      !customer?.documentNumber ||
      !order.applicationSelected
    )
      return;
    setLoading(true);
    const newCustomer = {
      ...customer,
      ...{ phoneNumber: new PhoneNumber(`+${dialCode}${data.phoneNumber}`) },
    };
    await onEditPhoneNumber(newCustomer, () => {
      onCloseDialog(true);
    });
    setLoading(false);
  };

  if (!translation || !dialCode) return null;

  return (
    <>
      <Slide direction="up" in={dialogOpen} mountOnEnter unmountOnExit>
        <Paper
          elevation={0}
          sx={{
            position: 'absolute',
            bottom: 0,
            left: { xs: -32, sm: -40 },
            right: { xs: -32, sm: -40 },
            zIndex: 1,
            boxShadow: '0px -20px 20px -10px rgb(0 0 0 / 6%)',
          }}
        >
          <Box pt={3} pb={4} sx={{ px: { xs: 4, sm: 5 } }}>
            <Typography variant={'h4'} sx={{ py: 2 }} color={'secondary.main'}>
              {translation?.EDIT_PHONE_NUMBER}
            </Typography>
            <Typography variant={'body2'}>{translation?.NEW_OTP}</Typography>
            <Box mt={3} />
            <Box display={'flex'} flexDirection={'column'} justifyContent={'stretch'}>
              <PhoneNumberTextField
                sx={{ mb: 1, minHeight: 102 }}
                dialCode={dialCode}
                setDialCode={setDialCode}
                disabledDialCode={style?.branding.value === Branding.LENDING_HUB}
                errors={errors}
                label={translation?.PHONE_NUMBER}
                size={'medium'}
                variant={'filled'}
                error={!!errors.phoneNumber}
                helperText={errors.phoneNumber?.message}
                clearErrors={clearErrors}
                value={phoneNumber ? phoneNumber.replace(/[^\d]/gm, '') : ''}
                {...register(OnboardingField.phoneNumber, {
                  required: {
                    value: true,
                    message: translation?.REQUIRED_FIELD,
                  },
                  onChange: () => errors && clearErrors(),
                  validate: {
                    validate: (value) => validatePhoneNumber(value, dialCode) || translation?.PHONE_NUMBER_ERROR,
                  },
                })}
              />
            </Box>
            <MainButton
              label={translation?.SAVE}
              loading={loading}
              disabled={!phoneNumberChanged}
              disablePoweredBy={true}
              onClick={() => {
                pushToDataLayer({
                  event: DLEvent.BTN_CLICK,
                  location,
                  additionalPayload: { type: 'save', label: translation?.SAVE },
                });
                handleSubmit(
                  (data) => onSave(data),
                  (errors) => {
                    Object.keys(errors).forEach((errorKey: string) => {
                      const error = errors[errorKey as keyof FormValues];
                      pushToDataLayer({
                        event: DLEvent.INPUT_ERROR,
                        location,
                        additionalPayload: {
                          inputField: errorKey,
                          inputValue: error?.ref?.value,
                          errorMessage: error?.message,
                        },
                      });
                    });
                  },
                )();
              }}
            />
            <Box mt={2} />
            <MainButton
              label={translation?.CANCEL}
              variant={'text'}
              onClick={() => {
                reset();
                clearErrors();
                onCloseDialog(false);
                pushToDataLayer({
                  event: DLEvent.BTN_CLICK,
                  location,
                  additionalPayload: { type: 'cancel', label: translation?.CANCEL },
                });
              }}
              disablePoweredBy={true}
            />
          </Box>
        </Paper>
      </Slide>
    </>
  );
};

type Props = {
  dialogOpen: boolean;
  onCloseDialog: (phoneNumberEdited: boolean) => void;
};

type FormValues = {
  phoneNumber?: string;
};
