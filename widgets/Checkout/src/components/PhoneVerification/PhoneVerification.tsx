import { Box, Button, CircularProgress, Typography, useTheme } from '@mui/material';
import {
  Action,
  ConflictException,
  getOrderByLoanId,
  InternalErrorException,
  Order,
  OtpStatus,
  patchResendOtpByLoanId,
  patchSetLastCode,
  postLoanApplicationInLender,
} from '@nemuru-eng/js-api-client';
import React, { useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';

import { useCheckoutContext } from '../../contexts/Checkout/CheckoutContext';
import { useDataLayer, CheckoutLocation, DLEvent } from '../../hooks/useDataLayer';
import { translation } from '../../translations/Translation';
import { CHECKOUT_STEPS } from '../../types/CheckoutSteps';
import { OnboardingField } from '../OnboardingDetails/types/OnboardingField';
import { Alert } from '../Shared/Alert';
import { MainButton } from '../Shared/MainButton';
import { Message, MESSAGE_TYPES } from '../Shared/Message/Message';
import { StyledTextField } from '../Shared/StyledTextField';
import { PhoneNumberEdition } from './components/PhoneNumberEdition';

const OTP_LIMIT = 4;
const OTP_LENGTH = 5;

const location = CheckoutLocation.PHONE_VERIFICATION;

export const PhoneVerification: React.FC<{ onSubmit: () => void }> = ({ onSubmit }) => {
  const [step, setStep] = useState<string>(STEPS.VALIDATION_CODE);
  const [otpError, setOtpError] = useState<boolean>(false);
  const [resendingOtp, setResendingOtp] = useState<{ loading: boolean; success?: boolean }>({ loading: false });
  const [editingPhoneNumber, setEditingPhoneNumber] = useState<boolean>(false);
  const [resolvedOrder, setResolvedOrder] = useState<Order | undefined>(undefined);
  const [otpLimitReached, setOtpLimitReached] = useState<boolean | undefined>(undefined);
  const [resetErrors, setResetErrors] = useState<boolean>(false);

  const { otp, order, customer, updateCheckout, setCheckoutStep, setValidationCode, disabledFormFields } =
    useCheckoutContext();
  const { pushToDataLayer } = useDataLayer();

  useEffect(() => {
    pushToDataLayer({ event: DLEvent.PAGE_VIEW, location });
  }, []);

  const { palette } = useTheme();

  const {
    reset,
    register,
    clearErrors,
    formState: { errors },
    handleSubmit,
    watch,
  } = useForm<FormValues>({
    defaultValues: {
      validationCode: undefined,
    },
  });

  const code = watch('validationCode');

  useEffect(() => {
    if (order) {
      updateCheckout({ otp: order?.otp });
      setOtpLimitReached(order.otp ? order.otp.sentCodes.length >= OTP_LIMIT : undefined);
    }
  }, [order]);

  useEffect(() => {
    if (otpError && code && !resetErrors) {
      setResetErrors(true);
    }
    if (otpError && resetErrors) {
      clearErrors();
      setOtpError(false);
      setResetErrors(false);
    }
  }, [otpError, code]);

  const maskCode = (code?: string): string | undefined => {
    if (!code) return;
    code = code.replace(/[^\d]/gm, '');
    return Array.from(code.substring(0, OTP_LENGTH)).join(' · ');
  };

  const onValidateOtp = (code?: string): boolean => {
    if (!code) return false;
    code = code.replace(/[^\d]/gm, '')?.substring(0, OTP_LENGTH);
    return code.length === 5;
  };

  const onResendOtp = async () => {
    pushToDataLayer({
      event: DLEvent.OTP_RESEND,
      location,
      additionalPayload: { sentCodes: order?.otp?.sentCodes.length },
    });
    if (order) {
      setResendingOtp({ loading: true });
      try {
        await patchResendOtpByLoanId({ loanId: order.id });
        const newOrder = await getOrderByLoanId({ loanId: order.id });
        updateCheckout({ order: newOrder });
        setResendingOtp({ loading: false, success: true });
        otpError && setOtpError(false);
        reset({ validationCode: undefined });
        clearErrors();
      } catch (err) {
        setResendingOtp({ loading: false, success: false });
      }
    }
  };

  const onNext = async (data: { validationCode?: string }) => {
    if (data.validationCode && otp && order) {
      if (otpError) {
        setOtpError(false);
      }
      const code = data.validationCode.replace(/[^\d]/gm, '');
      setValidationCode(code);
      setStep(STEPS.VERIFYING_PHONE);
      await patchLastCode(otp.id, code);
      try {
        await processLoanApplication();
        await ensureLoanApplicationIsSelected();
        // eslint-disable-next-line no-empty
      } catch (err) {}
    }
  };

  const patchLastCode = async (otpId: string, code: string) => {
    await patchSetLastCode({
      otpId,
      code,
      date: new Date(),
    });
  };

  const processLoanApplication = async () => {
    if (order?.applicationSelected) {
      try {
        await postLoanApplicationInLender({ loanApplicationId: order.applicationSelected.id });
        return;
      } catch (err) {
        if (err instanceof ConflictException || err instanceof InternalErrorException) {
          const newOrder = await getOrderByLoanId({ loanId: order.id });
          if (newOrder.otp?.status === OtpStatus.REJECTED) {
            setStep(STEPS.VALIDATION_CODE);
            setOtpError(true);
            pushToDataLayer({
              event: DLEvent.OTP_REJECTED,
              location,
              additionalPayload: { sentCodes: newOrder.otp.sentCodes.length },
            });
            throw err;
          }
        } else {
          setCheckoutStep(CHECKOUT_STEPS.ERROR_PHONE_VERIFICATION);
          throw err;
        }
      }
    }
  };

  function ensureLoanApplicationIsSelected() {
    let attempts = 0;
    return new Promise(function (resolve, reject) {
      (async function waitForLoanApplicationSelected() {
        if (order) {
          attempts += 1;
          const newOrder = await getOrderByLoanId({ loanId: order.id });
          if (
            (newOrder.applicationSelected !== undefined && newOrder.action === Action.REQUIRE_PAYMENT) ||
            newOrder.action === Action.DENIED
          ) {
            return resolve(setResolvedOrder(newOrder));
          }
          if (attempts > 20) return reject();
        }
        setTimeout(await waitForLoanApplicationSelected, 1500);
      })();
    });
  }

  useEffect(() => {
    if (resolvedOrder) {
      updateCheckout({ order: resolvedOrder });
      switch (resolvedOrder.action) {
        case Action.REQUIRE_PAYMENT:
          onSubmit();
          break;
        case Action.DENIED:
          setCheckoutStep(CHECKOUT_STEPS.DENIED);
          break;
      }
    }
  }, [resolvedOrder]);

  if (step === STEPS.VERIFYING_PHONE) {
    return <Message messageType={MESSAGE_TYPES.VERIFYING_PHONE} />;
  }

  if (!customer || !customer.phoneNumber || !translation) return null;

  return (
    <>
      <Box className={'appear-anim'} sx={{ opacity: editingPhoneNumber ? 0.2 : 1 }}>
        <Typography variant={'h4'} sx={{ pt: 4, pb: 2 }} color={'secondary.main'}>
          {translation?.VERIFY_YOUR_PHONE_NUMBER}
        </Typography>
        <Typography variant={'body2'}>{translation?.INTRODUCE_OTP}</Typography>
        <Typography variant={'h5'} sx={{ mt: 3 }}>
          <strong>{`+${customer.phoneNumber.countryCallingCode} ${customer.phoneNumber.nationalNumber}`}</strong>
        </Typography>
        <Typography variant={'body2'} sx={{ mt: 3 }}>
          {translation?.OTP_EXPIRATION}
        </Typography>
        <Box mt={4} />
        <Box display={'flex'} flexDirection={'column'} justifyContent={'stretch'}>
          <StyledTextField
            sx={{ mb: 3 }}
            id={'validation-code-input'}
            label={translation?.OTP}
            placeholder={'1 · 2 · 3 · 4 · 5'}
            value={code ? maskCode(code) : ''}
            variant="filled"
            size={'medium'}
            disabled={editingPhoneNumber}
            onFocus={() =>
              pushToDataLayer({
                event: DLEvent.INPUT_FOCUS,
                location,
                additionalPayload: { inputField: 'validationCode' },
              })
            }
            error={!!errors.validationCode}
            helperText={errors.validationCode?.message}
            InputProps={{
              disableUnderline: true,
              sx: {
                borderRadius: '6px',
              },
            }}
            {...register('validationCode', {
              required: {
                value: true,
                message: translation?.REQUIRED_FIELD,
              },
              validate: {
                validate: (value) => onValidateOtp(value) || translation?.OTP_LENGTH_ERROR,
              },
            })}
          />
          {otpLimitReached && <Typography variant={'body2'}>{translation?.INTRODUCE_ANY_OTP_SENT}</Typography>}
          {!otpLimitReached && !resendingOtp.loading && (resendingOtp.success === undefined || !resendingOtp.success) && (
            <Typography variant={'body2'}>
              {translation?.OTP_NOT_RECEIVED_QUESTION} {/*// @ts-ignore*/}
              <Button variant={'link'} color={'primary'} onClick={() => onResendOtp()}>
                {translation?.OTP_RESEND}
              </Button>
            </Typography>
          )}
          {!otpLimitReached && resendingOtp.loading && (
            <Typography variant={'body2'} style={{ display: 'flex', alignItems: 'center' }}>
              <CircularProgress size={24} sx={{ mr: 1 }} />
              {`${translation?.PROCESSING}`}
            </Typography>
          )}
          {!otpLimitReached && !resendingOtp.loading && resendingOtp.success === true && (
            <Typography variant={'body2'} style={{ display: 'flex', alignItems: 'center' }}>
              <i className={'material-icons-outlined'} style={{ marginRight: 8, color: palette.primary.main }}>
                check
              </i>
              {translation?.OTP_SENT}
            </Typography>
          )}
        </Box>
      </Box>
      <Box pt={3}>
        {otpError && (
          <Box pb={3}>
            <Alert variant={'error'} icon={true}>
              {otpLimitReached || resendingOtp.success === true ? (
                translation?.OTP_ERROR_RETRY
              ) : (
                <>
                  {`${translation?.OTP_ERROR_RETRY_2} `}
                  <span style={{ textDecoration: 'underline' }}>{translation?.OTP_RESEND}</span>
                  {` ${translation?.OTP_ERROR_RETRY_3}`}
                </>
              )}
            </Alert>
          </Box>
        )}
        <MainButton
          label={translation?.CONTINUE}
          disablePoweredBy={!disabledFormFields?.includes(OnboardingField.phoneNumber)}
          onClick={() => {
            pushToDataLayer({
              event: DLEvent.BTN_CLICK,
              location,
              additionalPayload: { type: 'continue', label: translation?.CONTINUE },
            });
            handleSubmit(
              (data) => onNext(data),
              (errors) => {
                Object.keys(errors).forEach((errorKey: string) => {
                  const error = errors[errorKey as keyof FormValues];
                  pushToDataLayer({
                    event: DLEvent.INPUT_ERROR,
                    location,
                    additionalPayload: {
                      inputField: errorKey,
                      inputValue: error?.ref?.value,
                      errorMessage: error?.message,
                    },
                  });
                });
              },
            )();
          }}
        />
        <Box mt={2} />
        {!disabledFormFields?.includes(OnboardingField.phoneNumber) && (
          <MainButton
            label={translation?.EDIT_PHONE_NUMBER_ACTION}
            variant={'text'}
            disabled={otpLimitReached}
            onClick={() => {
              setEditingPhoneNumber(true);
              pushToDataLayer({
                event: DLEvent.PAGE_VIEW,
                location: CheckoutLocation.PHONE_NUMBER_EDITION,
              });
            }}
          />
        )}
      </Box>
      <PhoneNumberEdition
        dialogOpen={editingPhoneNumber}
        onCloseDialog={(phoneNumberEdited: boolean) => {
          phoneNumberEdited && setResendingOtp({ loading: false, success: undefined });
          setEditingPhoneNumber(false);
        }}
      />
    </>
  );
};

const STEPS = {
  VALIDATION_CODE: 'VALIDATION_CODE',
  VERIFYING_PHONE: 'VERIFYING_PHONE',
};

type FormValues = {
  validationCode?: string;
};
