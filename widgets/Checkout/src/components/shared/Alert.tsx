import { Box, Typography, useTheme } from '@mui/material';
import React from 'react';

const ALERT_TYPE = {
  error: {
    textColor: ['error', 'main'],
    bgcolor: ['error', 'light'],
    icon: 'info',
  },
  info: {
    textColor: ['info', 'main'],
    bgcolor: ['info', 'light'],
    icon: 'info',
  },
  warning: {
    textColor: ['warning', 'main'],
    bgcolor: ['warning', 'light'],
    icon: 'info',
  },
};

export const Alert: React.FC<{
  variant: 'error' | 'info' | 'warning';
  icon: boolean;
  children: JSX.Element | string;
}> = ({ variant, icon = true, children }) => {
  const { palette } = useTheme();
  return (
    <>
      <Box
        p={2}
        borderRadius={2}
        display={'flex'}
        alignItems={'flex-start'}
        sx={{ bgcolor: ALERT_TYPE[variant].bgcolor.join('.') }}
      >
        {icon && (
          <Box mr={1}>
            <i
              className={'material-icons-outlined'} /*// @ts-ignore*/
              style={{ color: palette[ALERT_TYPE[variant].textColor[0]][ALERT_TYPE[variant].textColor[1]] }}
            >
              {ALERT_TYPE[variant].icon}
            </i>
          </Box>
        )}
        <Typography variant={'body2'} sx={{ color: ALERT_TYPE[variant].textColor.join('.') }}>
          {children}
        </Typography>
      </Box>
    </>
  );
};
