import { Box, Paper, Typography } from '@mui/material';
import React, { useEffect, useState } from 'react';

import { useCheckoutContext } from '../../../contexts/Checkout/CheckoutContext';
import { useDataLayer, DLEvent } from '../../../hooks/useDataLayer';
import { ALLOWED_ACTIONS_FOR_EXPIRATION } from '../../../hooks/useExpiration';
import { translation } from '../../../translations/Translation';

import './Countdown.css';
import { useCountdown } from './hooks/useCountdown';

export const Countdown: React.FC = () => {
  const { order, countdownShow, expirationDate } = useCheckoutContext();
  const { countdown, startCountdown } = useCountdown();
  const { pushToDataLayer } = useDataLayer();

  const [show, setShow] = useState(false);

  useEffect(() => {
    if (countdownShow && expirationDate && order) {
      if (ALLOWED_ACTIONS_FOR_EXPIRATION.includes(order.action)) {
        startCountdown(expirationDate);
        setShow(true);
        pushToDataLayer({
          event: DLEvent.COUNTDOWN_SHOW,
        });
      }
    }
    if (!countdownShow) {
      setShow(false);
    }
  }, [countdownShow]);

  if (!show || !countdown) return null;

  return (
    <Box className={'countdown-alert'}>
      <Paper elevation={0} sx={{ px: 2, py: 1, bgcolor: 'grey.700' }} className={'appear-anim countdown-paper'}>
        <Box sx={{ display: { xs: 'none', sm: 'block' }, height: 24, color: 'common.white' }}>
          <i className={'material-icons-outlined'} style={{ fontSize: 24 }}>
            timer
          </i>
        </Box>
        <Typography variant={'body2'} sx={{ ml: { xs: 0, sm: 1 }, mr: 1, color: 'common.white' }}>
          {translation?.APPLICATION_EXPIRATION_IN}
          <Typography
            variant={'body2'}
            component={'span'}
            sx={{ display: 'inline-flex', width: 40, ml: '4px', color: 'common.white' }}
          >
            {`${String(countdown.minutes).padStart(2, '0')}:${String(countdown.seconds).padStart(2, '0')}`}
          </Typography>
        </Typography>
      </Paper>
    </Box>
  );
};
