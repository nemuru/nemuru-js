import { Box, Checkbox, Typography } from '@mui/material';
import { AmortizationPlanDto, LoanCalculatorDto, Url, UrlType } from '@nemuru-eng/js-api-client';
import React, { useEffect, useState } from 'react';
import { useDataLayer, CheckoutLocation, DLEvent } from '../../../hooks/useDataLayer';
import { translation } from '../../../translations/Translation';
import { AmortizationDetails } from '../../Shared/AmortizationDetails';
import { MainButton } from '../../Shared/MainButton';
import { PaymentDetails } from '../../Shared/PaymentDetails';

const PaymentSummary: React.FC<Props> = ({
  signatureUrls,
  currentLoanCalculator,
  currentAmortizationPlan,
  paymentMethodReady,
  onSubmit,
}) => {
  const { pushToDataLayer } = useDataLayer();

  useEffect(() => {
    pushToDataLayer({ event: DLEvent.PAGE_VIEW, location: CheckoutLocation.PAYMENT_SUMMARY });
  }, []);

  const [checked, setChecked] = useState(false);

  const handleChange = (): void => {
    setChecked(!checked);
  };

  return (
    <>
      <Box className={'appear-anim'}>
        <Typography variant={'h4'} sx={{ pt: 4, pb: 2 }} color={'secondary.main'}>
          {translation?.SUMMARY}
        </Typography>
        <Box mt={2} />
        <PaymentDetails loanCalculator={currentLoanCalculator} />
        {currentAmortizationPlan && (
          <Box mt={2}>
            <AmortizationDetails amortizationPlan={currentAmortizationPlan} />
          </Box>
        )}
      </Box>
      <Box pt={5} className={'appear-anim'}>
        <Box display={'flex'} alignItems={'flex-start'} sx={{ mb: 2 }}>
          <Checkbox checked={checked} onChange={handleChange} sx={{ p: 0, mr: 1 }} />
          <Typography variant={'caption'} sx={{ mt: '2px' }}>
            {`${translation?.SEQURA_CONDITIONS_1} `}
            <a
              href={signatureUrls.find((url) => url.type === UrlType.PRIVACY_POLICY)?.url}
              style={{ color: 'inherit' }}
              onClick={() =>
                pushToDataLayer({
                  event: DLEvent.A_CLICK,
                  location: CheckoutLocation.PAYMENT_SUMMARY,
                  additionalPayload: { type: 'privacy_policy', label: translation?.SEQURA_CONDITIONS_2 },
                })
              }
              target={'_blank'}
            >
              {translation?.SEQURA_CONDITIONS_2}
            </a>
            {'.'}
          </Typography>
        </Box>
        <Box pb={3}>
          <Typography variant={'caption'}>
            {`${translation?.SEQURA_CONDITIONS_3} `}
            <a
              href={signatureUrls.find((url) => url.type === UrlType.INE)?.url}
              style={{ color: 'inherit' }}
              onClick={() =>
                pushToDataLayer({
                  event: DLEvent.A_CLICK,
                  location: CheckoutLocation.PAYMENT_SUMMARY,
                  additionalPayload: { type: 'ine', label: translation?.SEQURA_CONDITIONS_4 },
                })
              }
              target={'_blank'}
            >
              {translation?.SEQURA_CONDITIONS_4}
            </a>
            {`${translation?.SEQURA_CONDITIONS_5} `}
            <a
              href={signatureUrls.find((url) => url.type === UrlType.CONTRACT)?.url}
              style={{ color: 'inherit' }}
              onClick={() =>
                pushToDataLayer({
                  event: DLEvent.A_CLICK,
                  location: CheckoutLocation.PAYMENT_SUMMARY,
                  additionalPayload: { type: 'contract', label: translation?.SEQURA_CONDITIONS_6 },
                })
              }
              target={'_blank'}
            >
              {translation?.SEQURA_CONDITIONS_6}
            </a>
            {` ${translation?.SEQURA_CONDITIONS_7}`}
          </Typography>
        </Box>
        <MainButton label={translation?.CONTINUE} onClick={onSubmit} disabled={!paymentMethodReady || !checked} />
      </Box>
    </>
  );
};

export default PaymentSummary;

type Props = {
  signatureUrls: Url[];
  currentLoanCalculator: LoanCalculatorDto;
  currentAmortizationPlan: AmortizationPlanDto;
  paymentMethodReady: boolean;
  onSubmit: () => void;
};
