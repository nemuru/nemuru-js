// @ts-ignore
import SequraPCI from '@sequra/mufasa-js-sdk';
import { Box, Typography } from '@mui/material';
import {
  Action,
  AmortizationPlanDto,
  getOrderByLoanId,
  LoanCalculatorDto,
  Order,
  Url,
} from '@nemuru-eng/js-api-client';
import React, { useEffect, useState } from 'react';

import Branding from '../../../../../../../src/shared/types/Style/Branding';
import { useCheckoutContext } from '../../../../contexts/Checkout/CheckoutContext';
import { useDataLayer, CheckoutLocation, DLEvent } from '../../../../hooks/useDataLayer';
import { translation } from '../../../../translations/Translation';
import { ACTIONS_TO_RESOLVE_SCREEN } from '../../../../types/Actions';
import { CHECKOUT_STEPS } from '../../../../types/CheckoutSteps';
import { BackButton } from '../../../Shared/BackButton/BackButton';
import { LenderLogo } from '../../../Shared/Logo/LenderLogo';
import { MainButton } from '../../../Shared/MainButton';
import { Message, MESSAGE_TYPES } from '../../../Shared/Message/Message';
import './PaymentMethod.css';

const PaymentMethod: React.FC<Props> = ({
  paymentUrl,
  currentLoanCalculator,
  currentAmortizationPlan,
  onSubmit,
  onPrev,
}) => {
  const { order, style, setCheckoutStep, updateCheckout } = useCheckoutContext();
  const { pushToDataLayer } = useDataLayer();

  const [step, setStep] = useState<string>(STEPS.PAYMENT_METHOD);
  const [cardDataFulfilled, setCardDataFulfilled] = useState<boolean>(false);
  const [paymentForm, setPaymentForm] = useState(undefined);
  const [resolvedOrder, setResolvedOrder] = useState<Order | undefined>(undefined);

  useEffect(() => {
    let checkoutLocation: CheckoutLocation | undefined;
    switch (step) {
      case STEPS.PAYMENT_METHOD:
        checkoutLocation = CheckoutLocation.PAYMENT_METHOD;
        break;
      case STEPS.PROCESSING_PAYMENT:
        checkoutLocation = CheckoutLocation.PAYMENT_METHOD_PROCESSING;
        break;
    }
    if (!checkoutLocation) return;
    pushToDataLayer({ event: DLEvent.PAGE_VIEW, location: checkoutLocation });
  }, [step]);

  useEffect(() => {
    if (paymentUrl) {
      const pciPaymentForm = SequraPCI.paymentForm({
        url: paymentUrl.url,
        onCardDataFulfilled: () => {
          // console.log('onCardDataFulfilled');
          pushToDataLayer({
            event: DLEvent.SEQURA_CALLBACK,
            location: CheckoutLocation.PAYMENT_METHOD,
            additionalPayload: { callback: 'onCardDataFulfilled' },
          });
          setCardDataFulfilled(true);
        },
        onFormErrors: () => {
          // console.log('onFormErrors');
          pushToDataLayer({
            event: DLEvent.SEQURA_CALLBACK,
            location: CheckoutLocation.PAYMENT_METHOD,
            additionalPayload: { callback: 'onFormErrors' },
          });
          setCardDataFulfilled(false);
        },
        onFormSubmitted: () => {
          // console.log('onFormSubmitted');
          pushToDataLayer({
            event: DLEvent.SEQURA_CALLBACK,
            location: CheckoutLocation.PAYMENT_METHOD,
            additionalPayload: { callback: 'onFormSubmitted' },
          });
          // setStep(STEPS.PROCESSING_PAYMENT);
        },
        onScaRequired: () => {
          // console.log('onScaRequired');
          pushToDataLayer({
            event: DLEvent.SEQURA_CALLBACK,
            location: CheckoutLocation.PAYMENT_METHOD,
            additionalPayload: { callback: 'onScaRequired' },
          });
        },
        onScaLoaded: () => {
          // console.log('onScaLoaded');
          pushToDataLayer({
            event: DLEvent.SEQURA_CALLBACK,
            location: CheckoutLocation.PAYMENT_METHOD,
            additionalPayload: { callback: 'onScaLoaded' },
          });
        },
        onScaClosed: () => {
          // console.log('onScaClosed');
          updateCheckout({ headerCloseEnabled: true });
          pushToDataLayer({
            event: DLEvent.SEQURA_CALLBACK,
            location: CheckoutLocation.PAYMENT_METHOD,
            additionalPayload: { callback: 'onScaClosed' },
          });
          setStep(STEPS.PAYMENT_METHOD);
        },
        onPaymentFailed: () => {
          // console.log('onPaymentFailed');
          updateCheckout({ headerCloseEnabled: true });
          pushToDataLayer({
            event: DLEvent.SEQURA_CALLBACK,
            location: CheckoutLocation.PAYMENT_METHOD,
            additionalPayload: { callback: 'onPaymentFailed' },
          });
          setStep(STEPS.PAYMENT_METHOD);
        },
        onPaymentSuccessful: async () => {
          // console.log('onPaymentSuccessful');
          pushToDataLayer({
            event: DLEvent.SEQURA_CALLBACK,
            location: CheckoutLocation.PAYMENT_METHOD,
            additionalPayload: { callback: 'onPaymentSuccessful' },
          });
          try {
            updateCheckout({ cancelExpiration: true });
            await ensureOrderStatusHasChanged();
          } catch (err) {
            setCheckoutStep(CHECKOUT_STEPS.TIMEOUT);
          }
        },
      }).mount('card_form');
      setPaymentForm(pciPaymentForm);
    }
  }, [paymentUrl]);

  function ensureOrderStatusHasChanged() {
    let attempts = 0;
    return new Promise(function (resolve, reject) {
      (async function waitForLoanApplicationSelected() {
        if (order) {
          attempts += 1;
          try {
            const newOrder = await getOrderByLoanId({ loanId: order.id });
            if (ACTIONS_TO_RESOLVE_SCREEN.includes(newOrder.action)) {
              return resolve(setResolvedOrder(newOrder));
            }
            if (attempts > 60) return reject();
          } catch (err) {
            setCheckoutStep(CHECKOUT_STEPS.ERROR);
          }
        }
        setTimeout(await waitForLoanApplicationSelected, 1500);
      })();
    });
  }

  const onPay = () => {
    pushToDataLayer({
      event: DLEvent.BTN_CLICK,
      location: CheckoutLocation.PAYMENT_METHOD,
      additionalPayload: { type: 'continue', label: translation?.CONTINUE },
    });
    // @ts-ignore
    paymentForm.submitForm();
    updateCheckout({ headerCloseEnabled: false });
    setStep(STEPS.PROCESSING_PAYMENT);
  };

  useEffect(() => {
    if (resolvedOrder) {
      updateCheckout({ headerCloseEnabled: true });
      switch (resolvedOrder.action) {
        case Action.CONFIRMED:
          setCheckoutStep(CHECKOUT_STEPS.CONFIRMED);
          break;
        case Action.CANCELLED:
          setCheckoutStep(CHECKOUT_STEPS.CANCELLED);
          break;
        case Action.DENIED:
          setCheckoutStep(CHECKOUT_STEPS.DENIED);
          break;
        case Action.EXPIRED:
          setCheckoutStep(CHECKOUT_STEPS.EXPIRED);
          break;
        case Action.ON_HOLD:
          setCheckoutStep(CHECKOUT_STEPS.ON_HOLD);
          break;
      }
      updateCheckout({ order: resolvedOrder });
    }
  }, [resolvedOrder]);

  if (!translation || !order) return null;

  if (step === STEPS.PAYMENT_SUCCESSFUL) {
    return (
      <Message
        messageType={MESSAGE_TYPES.CONFIRMED}
        data={{ currentAmortizationPlan }}
        buttons={[
          {
            title: translation?.CLOSE,
            onClick: () => {
              pushToDataLayer({
                event: DLEvent.BTN_CLICK,
                location: CheckoutLocation.CONFIRMED,
                additionalPayload: { type: 'close', label: translation?.CLOSE },
              });
              onSubmit();
            },
          },
        ]}
      />
    );
  }

  return (
    <>
      {step === STEPS.PROCESSING_PAYMENT && (
        <div className={'overlay-blur'}>
          <Message messageType={MESSAGE_TYPES.PROCESSING} />
        </div>
      )}
      <Box className={'appear-anim'}>
        <Box display={'flex'} alignItems={'center'} mb={2}>
          <Box mt={2}>
            <BackButton
              onClick={() => {
                pushToDataLayer({
                  event: DLEvent.BTN_BACK,
                  location: CheckoutLocation.PAYMENT_METHOD,
                });
                onPrev();
              }}
            />
          </Box>
          <Typography variant={'h4'} sx={{ pt: 4, pb: 2, ml: 2 }} color={'secondary.main'}>
            {translation?.PAYMENT_CARD}
          </Typography>
        </Box>
        <Typography variant={'body2'}>{translation?.INTRODUCE_PAYMENT_CARD}</Typography>
        {style?.branding.value !== Branding.NEMURU ? (
          <Box sx={{ mt: { xs: 3, sm: 5 } }} display={'flex'} flexDirection={'column'} alignItems={'center'}>
            <Typography variant={'caption'} align={'center'}>
              {translation?.SERVICE_OFFERED_BY}
            </Typography>
            <LenderLogo amount={order.conditions.principal} styleProps={{ height: 40, marginTop: 16 }} />
          </Box>
        ) : (
          <Box height={24} />
        )}
        <Box sx={{ mt: { xs: 3, sm: 4 } }}>
          <div id={'card_form'} style={{ height: 280, overflow: 'hidden' }}></div>
        </Box>
      </Box>
      <Box>
        <MainButton
          label={`${translation?.TO_PAY} ${currentLoanCalculator.downpaymentTotalAmount.string}`}
          onClick={onPay}
          disabled={!cardDataFulfilled}
        />
      </Box>
    </>
  );
};

export default PaymentMethod;

type Props = {
  paymentUrl: Url;
  currentLoanCalculator: LoanCalculatorDto;
  currentAmortizationPlan: AmortizationPlanDto;
  onSubmit: () => void;
  onPrev: () => void;
};

const STEPS = {
  PAYMENT_METHOD: 'PAYMENT_METHOD',
  PROCESSING_PAYMENT: 'PROCESSING_PAYMENT',
  PAYMENT_SUCCESSFUL: 'PAYMENT_SUCCESSFUL',
};
