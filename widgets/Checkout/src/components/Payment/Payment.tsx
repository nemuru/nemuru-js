import { getPaymentUrlByLoanId, getSignatureUrlsByLoanId, Url } from '@nemuru-eng/js-api-client';
import React, { useEffect, useState } from 'react';
import { useCheckoutContext } from '../../contexts/Checkout/CheckoutContext';
import { CHECKOUT_STEPS } from '../../types/CheckoutSteps';
import { Message, MESSAGE_TYPES } from '../Shared/Message/Message';
import PaymentMethod from './components/PaymentMethod/PaymentMethod';
import PaymentSummary from './components/PaymentSummary';

export const Payment: React.FC<{ onSubmit: () => void }> = ({ onSubmit }) => {
  const [step, setStep] = useState<string>(STEPS.PAYMENT_PLAN);
  const [paymentUrl, setPaymentUrl] = useState<Url | undefined>(undefined);
  const [signatureUrls, setSignatureUrls] = useState<Url[] | undefined>(undefined);

  const { order, setCheckoutStep, currentLoanCalculator, currentAmortizationPlan } = useCheckoutContext();

  useEffect(() => {
    getPaymentUrl();
    getSignatureUrls();
  }, []);

  const getPaymentUrl = async () => {
    if (order) {
      try {
        const url = await getPaymentUrlByLoanId({ loanId: order.id });
        setPaymentUrl(url);
      } catch (err) {
        setCheckoutStep(CHECKOUT_STEPS.ERROR);
      }
    }
  };

  const getSignatureUrls = async () => {
    if (order) {
      try {
        const urls = await getSignatureUrlsByLoanId({ loanId: order.id });
        setSignatureUrls(urls);
      } catch (err) {
        setCheckoutStep(CHECKOUT_STEPS.ERROR);
      }
    }
  };

  if (!currentLoanCalculator || !currentAmortizationPlan || !signatureUrls)
    return <Message messageType={MESSAGE_TYPES.PROCESSING} />;

  return (
    <>
      {step === STEPS.PAYMENT_PLAN && (
        <PaymentSummary
          signatureUrls={signatureUrls}
          currentLoanCalculator={currentLoanCalculator}
          currentAmortizationPlan={currentAmortizationPlan}
          paymentMethodReady={paymentUrl !== undefined && signatureUrls !== undefined}
          onSubmit={() => {
            setStep(STEPS.PAYMENT_METHOD); // FIXME - Control paymentUrl is present
          }}
        />
      )}
      {step === STEPS.PAYMENT_METHOD && paymentUrl && signatureUrls && (
        <PaymentMethod
          paymentUrl={paymentUrl}
          currentLoanCalculator={currentLoanCalculator}
          currentAmortizationPlan={currentAmortizationPlan}
          onPrev={() => {
            setStep(STEPS.PAYMENT_PLAN);
          }}
          onSubmit={onSubmit}
        />
      )}
    </>
  );
};

const STEPS = {
  PAYMENT_PLAN: 'PAYMENT_PLAN',
  PAYMENT_METHOD: 'PAYMENT_METHOD',
};
