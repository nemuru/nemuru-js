export interface NavigationStep {
  name: string;
  component: React.FunctionComponent<any>;
  visible: boolean;
  updatedAt?: number;
}
