export interface StepComponentProps {
  showPrev: boolean;
  handlePrev: () => void;
  handleNext: () => void;
  loading: boolean;
  setLoading: (loading: boolean) => void;
}
