import { useEffect, useMemo, useState } from 'react';

import { NavigationStep } from './types/NavigationStep';

export const useNavigation = ({ steps, onComplete }: { steps: NavigationStep[]; onComplete: () => Promise<void> }) => {
  const [currentIndex, setCurrentIndex] = useState<number>(0);
  const [navigationSteps, setNavigationSteps] = useState<NavigationStep[]>(steps);
  const [completed, setCompleted] = useState(false);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    // Handle steps changes and resetting to first step
    // Comparisson between current navigationSteps and incoming steps based on
    // all steps' name and updated at values. If different, resetNavigation
    // is triggered
    const stepsString = JSON.stringify(
      steps.map((step) => ({
        name: step.name,
        updatedAt: step.updatedAt,
      })),
    );
    const navigationStepsString = JSON.stringify(
      navigationSteps.map((step) => ({
        name: step.name,
        updatedAt: step.updatedAt,
      })),
    );
    if (stepsString !== navigationStepsString) {
      resetNavigation();
    }
  }, [steps]);

  useEffect(() => {
    if (completed) {
      handleOnComplete();
    }
  }, [completed]);

  const handleOnComplete = async () => {
    setLoading(true);
    try {
      await onComplete();
    } catch (err) {
      setCompleted(false);
    }
    setLoading(false);
  };

  const resetNavigation = () => {
    if (loading) setLoading(false);
    if (completed) setCompleted(false);
    setNavigationSteps(steps);
    setCurrentIndex(0);
  };

  // const handleRedirect = (stepName: string) => {
  //   setLoading(false);
  //   setCompleted(false);
  //   setCurrentIndex(navigationSteps.findIndex((step) => step.name === stepName));
  // };

  const handleNext = () => {
    if (currentIndex === navigationSteps.length - 1) {
      setCompleted(true);
      return;
    }
    setCurrentIndex((prevIndex) => {
      return prevIndex + 1;
    });
  };

  const handlePrev = () => {
    setCurrentIndex((prevIndex) => prevIndex - 1);
  };

  const showPrev = useMemo(() => currentIndex - 1 >= 0 && currentIndex - 1 < navigationSteps.length, [currentIndex]);

  const StepComponent = navigationSteps[currentIndex].component;
  StepComponent.defaultProps = {
    showPrev: showPrev,
    handlePrev: handlePrev,
    handleNext: handleNext,
    loading: loading,
    setLoading: setLoading,
  };

  return { StepComponent };
};
