import { Action, AmortizationPlanDto, LoanCalculatorDto, Order, PhoneNumber } from '@nemuru-eng/js-api-client';
import { useEffect } from 'react';

import { useCheckoutContext } from '../contexts/Checkout/CheckoutContext';
import { CHECKOUT_STEPS } from '../types/CheckoutSteps';

export const useCheckoutProcess = (
  order: Order,
  loanCalculators: LoanCalculatorDto[],
  amortizationPlans: AmortizationPlanDto[],
) => {
  const { updateCheckout, setCheckoutStep } = useCheckoutContext();

  // Initial context set state and call to determine first checkout step
  useEffect(() => {
    if (order) {
      updateCheckout({
        customer: {
          firstName: order.customer.firstName,
          lastName: order.customer.lastName,
          email: order.customer.email,
          phoneNumber: order.customer.phoneNumber ? new PhoneNumber(order.customer.phoneNumber) : undefined,
          documentNumber: order.customer.documentNumber,
          birthDate: order.customer.birthDate,
          requireAddress: order.customer.requireAddress,
        },
      });
      let originalLoanCalculator = loanCalculators.find(
        (loanCalculator) => loanCalculator.term === order.conditions.term,
      );
      if (originalLoanCalculator === undefined && loanCalculators.length > 0) {
        originalLoanCalculator = loanCalculators[loanCalculators.length - 1];
      }
      const originalAmortizationPlan = amortizationPlans.find(
        (amortizationPlan) => originalLoanCalculator && amortizationPlan.term === originalLoanCalculator.term,
      );
      updateCheckout({
        ...(originalLoanCalculator && { currentLoanCalculator: originalLoanCalculator }),
        ...(originalAmortizationPlan && { currentAmortizationPlan: originalAmortizationPlan }),
      });
      setCheckoutStepByAction(order);
    }
  }, []);

  const setCheckoutStepByAction = (order: Order): void => {
    let checkoutStep: string | undefined;

    if (order.action === Action.DENIED) {
      checkoutStep = CHECKOUT_STEPS.DENIED;
    } else if (order.action === Action.CANCELLED || order.action === Action.DESISTED) {
      checkoutStep = CHECKOUT_STEPS.CANCELLED;
    } else if (order.action === Action.EXPIRED) {
      checkoutStep = CHECKOUT_STEPS.EXPIRED;
    } else if (order.action === Action.CONFIRMED) {
      checkoutStep = CHECKOUT_STEPS.CONFIRMED;
    } else if (order.action === Action.ON_HOLD) {
      checkoutStep = CHECKOUT_STEPS.ON_HOLD;
    } else if (order.action === Action.APPROVED) {
      checkoutStep = CHECKOUT_STEPS.PROCESSING;
    } else if (
      order.applicationSelected &&
      order.applicationSelected.lenderFields.lenderRequestId &&
      order.action === Action.REQUIRE_VALIDATE_OTP
    ) {
      checkoutStep = CHECKOUT_STEPS.PHONE_VERIFICATION;
    } else if (
      order.applicationSelected &&
      order.applicationSelected.lenderFields.lenderRequestId &&
      order.action === Action.REQUIRE_PAYMENT
    ) {
      checkoutStep = CHECKOUT_STEPS.PAYMENT;
    } else {
      checkoutStep = CHECKOUT_STEPS.CALCULATOR;
    }

    if (checkoutStep) setCheckoutStep(checkoutStep);
  };

  return {
    setCheckoutStepByAction,
  };
};
