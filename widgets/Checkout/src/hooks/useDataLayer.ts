import { useCheckoutContext } from '../contexts/Checkout/CheckoutContext';

declare global {
  interface Window {
    dataLayer?: object[];
  }
}

export enum DLEvent {
  PAGE_VIEW = 'page.view',
  BTN_CLICK = 'button.click',
  BTN_BACK = 'button.back',
  BTN_CLOSE = 'button.close',
  A_CLICK = 'a.click',
  WINDOW_VISIBLE = 'window.visible',
  WINDOW_NOT_VISIBLE = 'window.notVisible',
  WIDGET_CLOSE = 'widget.close',
  INPUT_FOCUS = 'input.focus',
  INPUT_ERROR = 'input.error',
  OTP_RESEND = 'opt.resend',
  OTP_REJECTED = 'otp.rejected',
  COUNTDOWN_SHOW = 'countdown.show',
  LOAN_CALCULATOR_SELECT = 'calculator.select',
  SEQURA_CALLBACK = 'sequra.callback',
}

export enum CheckoutLocation {
  CALCULATOR = 'calculator',
  PERSONAL_INFO = 'onboardingDetails.PersonalInfo',
  PERSONAL_INFO_AND_PHONE_NUMBER = 'onboardingDetails.PersonalInfoAndPhoneNumber',
  ADDRESS_INFO = 'onboardingDetails.AddressInfo',
  CONTACT_INFO = 'onboardingDetails.ContactInfo',
  PHONE_VERIFICATION = 'phoneVerification.PhoneVerification',
  PHONE_NUMBER_EDITION = 'phoneVerification.PhoneNumberEdition',
  VERIFYING_PHONE = 'phoneVerification.VerifyingPhone',
  PAYMENT_SUMMARY = 'payment.PaymentSummary',
  PAYMENT_METHOD = 'payment.PaymentMethod',
  PAYMENT_METHOD_PROCESSING = 'payment.PaymentMethodProcessing',
  LEAVE_PROCESS = 'leaveProcess',
  CONFIRMED = 'confirmed',
  ERROR = 'error',
  DENIED = 'denied',
  EXPIRED = 'expired',
  CANCELLED = 'cancelled',
  ON_HOLD = 'onHold',
  TIMEOUT = 'timeout',
  PROCESSING = 'processing',
  EMPTY_LOAN_CONDITIONS = 'emptyLoanConditions',
}

interface DLObject {
  event: string;
  payload: {
    event: DLEvent;
    timestamp: number;
    location?: CheckoutLocation;
    sessionId?: string;
    loanId?: string;
    additionalPayload?: string;
  };
}

export const useDataLayer = () => {
  const { order, sessionId, updateCheckout, currentLocation } = useCheckoutContext();

  const pushToDataLayer = ({
    event,
    location = undefined,
    additionalPayload = undefined,
    orderId = undefined,
  }: {
    event: DLEvent;
    location?: CheckoutLocation;
    additionalPayload?: object;
    orderId?: string;
  }): void => {
    if (location && location !== currentLocation) updateCheckout({ currentLocation: location });
    if (!location && currentLocation) location = currentLocation;

    const dataLayerObject: DLObject = {
      event: 'dataLayerEvent',
      payload: {
        event,
        timestamp: new Date().getTime(),
        location,
        sessionId,
        loanId: order?.id ?? orderId,
        additionalPayload: additionalPayload ? JSON.stringify(additionalPayload) : undefined,
      },
    };
    window.dataLayer = window.dataLayer || [];
    window.dataLayer.push(dataLayerObject);
  };
  return { pushToDataLayer };
};
