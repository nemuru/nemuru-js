import { Action, getOrderByLoanId, LoanStatus, Order, patchChangeLoanStatus } from '@nemuru-eng/js-api-client';
import { useEffect, useState } from 'react';

import Branding from '../../../../src/shared/types/Style/Branding';
import { useCheckoutContext } from '../contexts/Checkout/CheckoutContext';
import { CHECKOUT_STEPS } from '../types/CheckoutSteps';

export const useExpiration = () => {
  const [callbackSet, setCallbackSet] = useState(false);
  const [doExpire, setDoExpire] = useState(false);
  const [doCountdown, setDoCountdown] = useState(false);

  const { order, style, behaviour, cancelExpiration, updateCheckout, setCheckoutStep } = useCheckoutContext();

  useEffect(() => {
    if (order && style && !callbackSet) {
      setExpirationCallback(order, style.branding, behaviour?.expirationTimeInMinutes);
    } else {
      cancelExpirationIfNeeded();
    }
  }, [order]);

  const setExpirationCallback = (order: Order, branding: Branding, expirationTime?: number): void => {
    if (ALLOWED_ACTIONS_FOR_EXPIRATION.includes(order.action)) {
      const expirationDate = expirationAt(order, branding, expirationTime);
      const interval = expirationDate.getTime() - new Date().getTime();
      setCallbackSet(true);
      updateCheckout({ expirationDate: expirationDate });
      if (interval <= 0) {
        setDoExpire(true);
        return;
      }
      const MAX_TIMEOUT_VALUE = 2000000000;
      if (interval <= MAX_TIMEOUT_VALUE) {
        setTimeout(() => {
          setDoExpire(true);
        }, interval);
        setTimeout(() => {
          setDoCountdown(true);
          updateCheckout({ countdownShow: true });
        }, interval - COUNTDOWN_SHOW_SECONDS_LEFT * 1000);
      }
    }
  };

  const expirationAt = (order: Order, branding: Branding, expirationTime?: number): Date => {
    const key = (
      Object.keys(EXPIRATION_TIME_IN_MINUTES).includes(branding.value) ? branding.value : 'default'
    ) as keyof typeof EXPIRATION_TIME_IN_MINUTES;
    const expirationMinutes = expirationTime || EXPIRATION_TIME_IN_MINUTES[key];
    return new Date(new Date(order.createdAt).getTime() + expirationMinutes * 60 * 1000);
  };

  const cancelExpirationIfNeeded = () => {
    if (order && !ALLOWED_ACTIONS_FOR_EXPIRATION.includes(order.action)) {
      !cancelExpiration && updateCheckout({ cancelExpiration: true });
    }
  };

  useEffect(() => {
    if (doCountdown && !cancelExpiration) {
      updateCheckout({ countdownShow: true });
    }
  }, [doCountdown]);

  useEffect(() => {
    if (doExpire) {
      expirationCallback();
    }
  }, [doExpire]);

  const expirationCallback = async () => {
    if (!cancelExpiration && order) {
      await patchChangeLoanStatus({
        loanId: order.id,
        status: LoanStatus.STATUS_EXPIRED,
      });
      const newOrder = await getOrderByLoanId({ loanId: order.id });
      updateCheckout({ order: newOrder });
      setCheckoutStep(CHECKOUT_STEPS.EXPIRED);
    }
  };

  useEffect(() => {
    if (cancelExpiration) {
      updateCheckout({ countdownShow: false });
    }
  }, [cancelExpiration]);
};

const EXPIRATION_TIME_IN_MINUTES = {
  [Branding.LENDING_HUB]: 15,
  default: 120,
};

const COUNTDOWN_SHOW_SECONDS_LEFT = 60;

export const ALLOWED_ACTIONS_FOR_EXPIRATION = [Action.INITIAL, Action.REQUIRE_VALIDATE_OTP, Action.REQUIRE_PAYMENT];
