import 'regenerator-runtime/runtime';
import React from 'react';
import ReactDOM from 'react-dom';
// noinspection ES6UnusedImports
import CheckoutZoid from './checkout-zoid'; // Do not delete! Needed for two-way communication
import App from './App';

ReactDOM.render(<App {...window.xprops} />, document.getElementById('root'));
