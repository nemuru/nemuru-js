export const dateToStringAtom = (date: Date | undefined): string | undefined => {
  if (!date) return undefined;
  const offset = date.getTimezoneOffset();
  return new Date(date.getTime() - offset * 60 * 1000).toISOString().split('T')[0];
};

export const getAgeFromBirthDate = (birthDate: Date): number => {
  const today = new Date();
  let age = today.getFullYear() - birthDate.getFullYear();
  const m = today.getMonth() - birthDate.getMonth();
  if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
    age--;
  }
  return age;
};

export const getLastNames = (lastName?: string, secondLastName?: string): string | undefined => {
  let lastNames: string | undefined = undefined;
  if (lastName && secondLastName) lastNames = `${lastName} ${secondLastName}`;
  else if (lastName) lastNames = lastName;
  else if (secondLastName) lastNames = secondLastName;
  return lastNames;
};
