import {
  Environment,
  isValidDNI,
  isValidDocumentNumber,
  isValidEmail,
  isValidMobilePhoneNumber,
  isValidNIE,
} from '@nemuru-eng/js-api-client';

import { translation } from '../translations/Translation';
import { getAgeFromBirthDate } from './functions';

export const validateDocumentNumber = (documentNumber?: string): boolean | undefined => {
  if (!documentNumber) return;
  return isValidDocumentNumber(documentNumber);
};

export const validateLastNameByDocumentNumber = (
  lastName?: string,
  documentNumber?: string,
): boolean | string | undefined => {
  if (!lastName || !documentNumber) return;
  const lastNameTrimmed = lastName.replace(/\s+/g, ' ').trim();
  if (isValidDNI(documentNumber)) {
    return lastNameTrimmed.split(' ').length >= 2 ? true : translation?.TWO_SURNAMES_ERROR;
  }
  if (isValidNIE(documentNumber)) {
    return lastNameTrimmed.split(' ').length >= 1 ? true : translation?.SURNAME_ERROR;
  }
};

export const validatePhoneNumber = (phoneNumber?: string, dialCode?: string): boolean => {
  if (!phoneNumber || !dialCode) return false;
  return isValidMobilePhoneNumber(`+${dialCode}${phoneNumber}`);
};

export const validateEmail = (email?: string): boolean => {
  if (!email) return false;
  return isValidEmail(email);
};

export const validateBirthDate = (env: Environment, date?: string): boolean | string | undefined => {
  if (!date) return;
  if (isNaN(Date.parse(date))) return;
  const birthDate = new Date(date);
  const age = getAgeFromBirthDate(birthDate);

  const MIN_AGE = env === Environment.PROD ? 18 : 16;
  const MAX_AGE = 99;

  if (age < MIN_AGE && age >= 0) {
    return translation?.UNDERAGE_ERROR;
  }
  if (age > MAX_AGE || age < 0) {
    return translation?.DATE_ERROR;
  }
  return true;
};
