import {
  AmortizationPlanDto,
  Environment,
  getAmortizationPlans,
  getLoanCalculators,
  getOrderByLoanId,
  getPricingsByAgentId,
  LoanCalculatorDto,
  login,
  proposalsConstructor,
  setCredentials,
  setEnvironment,
} from '@nemuru-eng/js-api-client';
import 'regenerator-runtime/runtime';

const getURLParameter = (sParam: string) => {
  const sPageURL = window.location.search.substring(1);
  const sURLVariables = sPageURL.split('&');
  for (let i = 0; i < sURLVariables.length; i++) {
    const sParameterName = sURLVariables[i].split('=');
    if (sParameterName[0] == sParam) {
      return sParameterName[1];
    }
  }
};

const env = (getURLParameter('env') ?? 'stg') as 'dev' | 'stg';
const loanId = getURLParameter('id') ?? '92206174-7227-4731-ae36-fe84a0b6416d';
const locale = getURLParameter('locale') ?? 'es-ES';
const branding = getURLParameter('branding') ?? 'finanpay';

const TOKEN_EXPIRATION = 7200; //2 hours

let username = 'adria.moya+dev.sequra.ecommerce@nemuru.com';
let password = '123456';
if (env === 'stg') {
  username = 'comercia.1@nemuru.com';
  password = '98u8Bbb7JdgRKb8ugVhSDkHtAmGTgc6u';
}

console.log('[nemuru]', { env, loanId, locale, branding });

const init = async () => {
  await setEnvironment(env as Environment);
  const credentials = await login({ username, password });
  if (credentials) {
    await setCredentials({ accessToken: credentials.accessToken, refreshToken: credentials.refreshToken });
  }
  const order = await getOrderByLoanId({ loanId });
  const pricings = await getPricingsByAgentId({ agentId: order.agentId });
  const pricing = pricings.find((pricing) => pricing.product === order.conditions.product);
  let loanCalculators: LoanCalculatorDto[] = [];
  let amortizationPlans: AmortizationPlanDto[] = [];
  if (pricing) {
    // const terms = [pricing.rangePeriodMin];
    const terms = [3, 10, 12];
    const proposals = proposalsConstructor({
      principal: order.conditions.principal,
      terms: terms,
      pricings: pricings,
      product: order.conditions.product,
    });
    loanCalculators = getLoanCalculators({ proposals });
    amortizationPlans = await getAmortizationPlans({
      proposals,
    });
  }

  // @ts-ignore
  localStorage.setItem('ready', JSON.stringify(true));
  localStorage.setItem('env', JSON.stringify(env));
  localStorage.setItem('branding', JSON.stringify(branding));
  localStorage.setItem('locale', JSON.stringify(locale));
  localStorage.setItem('accessToken', JSON.stringify(credentials?.accessToken));
  localStorage.setItem('loan', JSON.stringify(order));
  localStorage.setItem('loanCalculators', JSON.stringify(loanCalculators));
  localStorage.setItem('amortizationPlans', JSON.stringify(amortizationPlans));
  localStorage.setItem('expiration', JSON.stringify(new Date(new Date().getTime() + TOKEN_EXPIRATION * 1000)));
};

const localStorageFilled =
  localStorage.getItem('env') &&
  localStorage.getItem('branding') &&
  localStorage.getItem('loan') &&
  localStorage.getItem('locale') &&
  localStorage.getItem('accessToken') &&
  localStorage.getItem('loanCalculators') &&
  localStorage.getItem('amortizationPlans');
!localStorageFilled && console.log('[nemuru] Local storage empty');

const lsEnv = localStorage.getItem('env');
const lsLoan = localStorage.getItem('loan');
const lsLocale = localStorage.getItem('locale');
const lsBranding = localStorage.getItem('branding');
const lsExpiration = localStorage.getItem('expiration');

const expired = lsExpiration && new Date() > new Date(JSON.parse(lsExpiration));
expired && console.log('[nemuru] Access token expired');

const paramsChanged =
  (lsEnv && env !== JSON.parse(lsEnv)) ||
  (lsLoan && loanId !== JSON.parse(lsLoan).id) ||
  (lsLocale && JSON.parse(lsLocale) !== locale) ||
  (lsBranding && JSON.parse(lsBranding) !== branding);
paramsChanged && console.log('[nemuru] URL params changed');

if (expired || paramsChanged || !localStorageFilled) {
  localStorage.clear();
  init();
}
