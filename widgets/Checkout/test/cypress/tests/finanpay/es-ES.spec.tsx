import React from 'react';
import { Environment, Locale } from '@nemuru-eng/js-api-client';

import './commands';
import { CheckoutComponentProps } from '../../../../../../src/checkout/Checkout';
import App from '../../../../src/App';
import { mount } from '../../shared/scripts/mount';
import { TestHelper } from '../../shared/scripts/testHelper';
import Branding from '../../../../../../src/shared/types/Style/Branding';

//FIXME: This will be an environment variable
const ENV = Environment.STG;
const BRANDING = Branding.FINANPAY;
const LOCALE = Locale.ES_ES;
const orderId = '32fbb8ed-45c6-4b0a-b63b-ca37af2754ef';

describe('Checkout process ends with confirmed', () => {
  let props: CheckoutComponentProps;
  const helper = new TestHelper(ENV, BRANDING, LOCALE);

  before(async () => {
    cy.intercept({ method: 'GET', url: `**/order/${orderId}/` }, (req) => {
      req.reply({
        statusCode: 200,
        fixture: 'lending_hub/fixtures/getOrderByLoanIdInitial.json',
      });
    });

    cy.intercept(
      { method: 'GET', url: `**/pricings/50bfddb1-ee39-46fa-8586-24bf0d3dde58/pricings.stg.json` },
      (req) => {
        req.reply({
          statusCode: 200,
          fixture: 'lending_hub/fixtures/getPricingsByAgentId.json',
        });
      },
    );

    props = await helper.createCheckoutProps(orderId);
  });

  beforeEach(() => {
    cy.viewport(472, 839);
  });

  it('Checkout - mounts', () => {
    mount(<App {...props} />);
  });

  it('Payment - displays sequra logo below 1200 eur', () => {
    mount(<App {...props} />);
    cy.navigateToPayment(orderId);

    cy.get('img[id="lender-logo"]')
      .should('have.attr', 'src')
      .should('include', 'https://cdn.nemuru.com/assets/logos/sequra-black-logo.svg');
  });
});
