// @ts-ignore
Cypress.Commands.add('navigateToOtp', (orderId: string) => {
  cy.get('button').contains('Continuar').click();

  cy.intercept({ method: 'POST', url: '**/v2/order/customer/' }, (req) => {
    req.reply({ statusCode: 201 });
  }).as('postCustomerDetailsByLoanId');

  cy.intercept({ method: 'POST', url: `**/loan_application/loan/${orderId}/` }, (req) => {
    req.reply({ statusCode: 201 });
  }).as('postLoanApplications');

  cy.intercept({ method: 'GET', url: `**/v2/order/${orderId}/` }, (req) => {
    req.reply({
      statusCode: 200,
      fixture: 'lending_hub/fixtures/getOrderByLoanIdRequireOtp.json',
    });
  }).as('getOrderByLoanId');

  cy.get('button').contains('Continuar').click();

  cy.wait(['@postCustomerDetailsByLoanId', '@postLoanApplications', '@getOrderByLoanId']).wait(200);
});

Cypress.Commands.add('navigateToOtpFullCodes', (orderId: string) => {
  cy.get('button').contains('Continuar').click();

  cy.intercept({ method: 'POST', url: '**/v2/order/customer/' }, (req) => {
    req.reply({ statusCode: 201 });
  }).as('postCustomerDetailsByLoanId');

  cy.intercept({ method: 'POST', url: `**/loan_application/loan/${orderId}/` }, (req) => {
    req.reply({ statusCode: 201 });
  }).as('postLoanApplications');

  cy.intercept({ method: 'GET', url: `**/v2/order/${orderId}/` }, (req) => {
    req.reply({
      statusCode: 200,
      fixture: 'lending_hub/fixtures/getOrderByLoanIdRequireOtpFullCodes.json',
    });
  }).as('getOrderByLoanId');

  cy.get('button').contains('Continuar').click();

  cy.wait(['@postCustomerDetailsByLoanId', '@postLoanApplications', '@getOrderByLoanId']).wait(200);
});

Cypress.Commands.add('navigateToPayment', (orderId: string) => {
  cy.navigateToOtp(orderId);

  cy.get('input').type('23349');

  cy.intercept({ method: 'PATCH', url: '**/api/compliance/otp/' }, (req) => {
    req.reply({ statusCode: 204 });
  }).as('patchSetLastCode');

  cy.intercept({ method: 'POST', url: '**/api/financial/lender/process_loan_application' }, (req) => {
    req.reply({ statusCode: 201 });
  }).as('postLoanApplicationInLender');

  cy.intercept({ method: 'GET', url: `**/v2/order/${orderId}/` }, (req) => {
    req.reply({
      statusCode: 200,
      fixture: 'lending_hub/fixtures/getOrderByLoanIdRequirePayment.json',
    });
  }).as('getOrderByLoanId');

  cy.intercept({ method: 'GET', url: `**/financial/loan_application/payment/${orderId}/` }, (req) => {
    req.reply({
      statusCode: 200,
      fixture: 'lending_hub/fixtures/getPaymentUrlByLoanId.json',
    });
  }).as('getPaymentUrlByLoanId');

  cy.intercept({ method: 'GET', url: `**/financial/loan_application/signature/${orderId}/` }, (req) => {
    req.reply({
      statusCode: 200,
      fixture: 'lending_hub/fixtures/getSignatureUrlsByLoanId.json',
    });
  }).as('getSignatureUrlsByLoanId');

  cy.get('button').contains('Continuar').click();

  cy.wait([
    '@patchSetLastCode',
    '@postLoanApplicationInLender',
    '@getOrderByLoanId',
    '@getPaymentUrlByLoanId',
    '@getSignatureUrlsByLoanId',
  ]).wait(200);
});

Cypress.Commands.add('navigateToDenied', (orderId: string) => {
  cy.navigateToOtp(orderId);

  cy.get('input').type('23349');

  cy.intercept({ method: 'PATCH', url: '**/api/compliance/otp/' }, (req) => {
    req.reply({ statusCode: 204 });
  }).as('patchSetLastCode');

  cy.intercept({ method: 'POST', url: '**/api/financial/lender/process_loan_application' }, (req) => {
    req.reply({ statusCode: 201 });
  }).as('postLoanApplicationInLender');

  cy.intercept({ method: 'GET', url: `**/v2/order/${orderId}/` }, (req) => {
    req.reply({
      statusCode: 200,
      fixture: 'lending_hub/fixtures/getOrderByLoanIdDenied.json',
    });
  }).as('getOrderByLoanId');

  cy.get('button').contains('Continuar').click();

  cy.wait(['@patchSetLastCode', '@postLoanApplicationInLender', '@getOrderByLoanId']).wait(200);
});

Cypress.Commands.add('navigateToExpired', (orderId: string) => {
  cy.get('button').contains('Continuar').click();
  //
  cy.intercept({ method: 'POST', url: '**/v2/order/customer/' }, (req) => {
    req.reply({ statusCode: 201 });
  }).as('postCustomerDetailsByLoanId');

  cy.intercept({ method: 'POST', url: `**/loan_application/loan/${orderId}/` }, (req) => {
    req.reply({ statusCode: 201 });
  }).as('postLoanApplications');

  cy.intercept({ method: 'GET', url: `**/v2/order/${orderId}/` }, (req) => {
    req.reply({
      statusCode: 200,
      fixture: 'lending_hub/fixtures/getOrderByLoanIdExpired.json',
    });
  }).as('getOrderByLoanId');

  cy.get('button').contains('Continuar').click();

  cy.wait(['@postCustomerDetailsByLoanId', '@postLoanApplications', '@getOrderByLoanId']).wait(200);
});

Cypress.Commands.add('navigateToDesisted', (orderId: string) => {
  cy.get('button').contains('Continuar').click();
  //
  cy.intercept({ method: 'POST', url: '**/v2/order/customer/' }, (req) => {
    req.reply({ statusCode: 201 });
  }).as('postCustomerDetailsByLoanId');

  cy.intercept({ method: 'POST', url: `**/loan_application/loan/${orderId}/` }, (req) => {
    req.reply({ statusCode: 201 });
  }).as('postLoanApplications');

  cy.intercept({ method: 'GET', url: `**/v2/order/${orderId}/` }, (req) => {
    req.reply({
      statusCode: 200,
      fixture: 'lending_hub/fixtures/getOrderByLoanIdDesisted.json',
    });
  }).as('getOrderByLoanId');

  cy.get('button').contains('Continuar').click();

  cy.wait(['@postCustomerDetailsByLoanId', '@postLoanApplications', '@getOrderByLoanId']).wait(200);
});

declare namespace Cypress {
  interface Chainable {
    navigateToOtp(orderId: string): Chainable<void>;
    navigateToOtpFullCodes(orderId: string): Chainable<void>;
    navigateToPayment(orderId: string): Chainable<void>;
    navigateToDenied(orderId: string): Chainable<void>;
    navigateToExpired(orderId: string): Chainable<void>;
    navigateToDesisted(orderId: string): Chainable<void>;
  }
}
