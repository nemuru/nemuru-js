import React from 'react';
import { Environment, Locale } from '@nemuru-eng/js-api-client';

import './commands';
import { CheckoutComponentProps } from '../../../../../../src/checkout/Checkout';
import App from '../../../../src/App';
import { mount } from '../../shared/scripts/mount';
import { TestHelper } from '../../shared/scripts/testHelper';
import Branding from '../../../../../../src/shared/types/Style/Branding';
import { replaceNbsp } from '../../shared/scripts/utils';

//FIXME: This will be an environment variable
const ENV = Environment.STG;
const BRANDING = Branding.LENDING_HUB;
const LOCALE = Locale.ES_ES;
const orderId = '32fbb8ed-45c6-4b0a-b63b-ca37af2754ef';

describe('Checkout process ends with confirmed', () => {
  let props: CheckoutComponentProps;
  const helper = new TestHelper(ENV, BRANDING, LOCALE);

  before(async () => {
    cy.intercept({ method: 'GET', url: `**/order/${orderId}/` }, (req) => {
      req.reply({
        statusCode: 200,
        fixture: 'lending_hub/fixtures/getOrderByLoanIdInitial.json',
      });
    });

    cy.intercept(
      { method: 'GET', url: `**/pricings/50bfddb1-ee39-46fa-8586-24bf0d3dde58/pricings.stg.json` },
      (req) => {
        req.reply({
          statusCode: 200,
          fixture: 'lending_hub/fixtures/getPricingsByAgentId.json',
        });
      },
    );

    props = await helper.createCheckoutProps(orderId);
  });

  beforeEach(() => {
    cy.viewport(472, 839);
  });

  it('Checkout - mounts', () => {
    mount(<App {...props} />);
  });

  it('Calculator - display', () => {
    mount(<App {...props} />);

    cy.contains('Paga a plazos');
    cy.contains('Plan de pagos');
    cy.contains('Detalle de la compra');
    cy.contains('Selecciona cómo quieres fraccionar tu compra:');
    cy.get('#principal-amount-label').contains('Importe de la compra');
    cy.get('#principal-amount-value').contains(replaceNbsp('224,41 €'));
    cy.get('#total-instalment-fee-amount-label').contains(`Coste total (${replaceNbsp('2,50 €')} por cuota)`);
    cy.get('#total-instalment-fee-amount-value').contains(replaceNbsp('30,00 €'));
    cy.get('#total-amount-label').contains('Total a pagar');
    cy.get('#total-amount-value').contains(replaceNbsp('254,41 €'));
    cy.get('#tin-tae').contains('TIN').contains(replaceNbsp('0,00 %')).contains('TAE').contains(replaceNbsp('32,43 %'));

    cy.get('button').filter(`:contains("Continuar")`);
  });

  it('Calculator - interaction', () => {
    mount(<App {...props} />);

    cy.get('div').should('have.class', 'selector-option').contains(replaceNbsp('24,94 € al mes')).click();

    cy.get('#principal-amount-value').contains(replaceNbsp('224,41 €'));
    cy.get('#total-instalment-fee-amount-label').contains(`Coste total (${replaceNbsp('2,50 €')} por cuota)`);
    cy.get('#total-instalment-fee-amount-value').contains(replaceNbsp('25,00 €'));
    cy.get('#total-amount-value').contains(replaceNbsp('249,41 €'));
    cy.get('#tin-tae').contains(replaceNbsp('0,00 %')).contains(replaceNbsp('33,38 %'));

    cy.get('div').should('have.class', 'selector-option').contains(replaceNbsp('77,30 € al mes')).click();

    cy.get('#principal-amount-value').contains(replaceNbsp('224,41 €'));
    cy.get('#total-instalment-fee-amount-label').contains(`Coste total (${replaceNbsp('2,50 €')} por cuota)`);
    cy.get('#total-instalment-fee-amount-value').contains(replaceNbsp('7,50 €'));
    cy.get('#total-amount-value').contains(replaceNbsp('231,91 €'));
    cy.get('#tin-tae').contains(replaceNbsp('0,00 %')).contains(replaceNbsp('49,02 %'));
  });

  it('Calculator - sends processPatchSetLoanCalculator with expected request body', () => {
    mount(<App {...props} />);

    cy.get('div').contains(replaceNbsp('77,30 € al mes')).click();

    cy.intercept({ method: 'PATCH', url: '**/api/financial/loan/' }, (req) => {
      expect(req.body.op).to.equal('processPatchSetLoanCalculator');
      expect(req.body.id).to.equal('32fbb8ed-45c6-4b0a-b63b-ca37af2754ef');
      expect(req.body.principal_currency).to.equal('EUR');
      expect(req.body.principal_amount).to.equal(224.41);
      expect(req.body.setup_fee_currency).to.equal('EUR');
      expect(req.body.setup_fee_amount).to.equal(7.5);
      expect(req.body.setup_fee_type).to.equal('type_fixed_amount');
      expect(req.body.term_period).to.equal(3);
      expect(req.body.term_type).to.equal('type_period');
      expect(req.body.term_interval).to.equal('P1M');
      expect(req.body.loan_program_name).to.equal('free_program');
      expect(req.body.loan_program_merchant_discount_percentage).to.equal(0);
      req.reply({ statusCode: 204 });
    });

    cy.get('button').filter(`:contains("Continuar")`).click();
  });

  it('OnboardingDetails - display', () => {
    mount(<App {...props} />);
    cy.get('button').filter(`:contains("Continuar")`).click();

    cy.contains('Introduce tus datos');
    cy.contains('Completa tu información personal:');

    cy.get('label').contains('Nombre');
    cy.get('#first-name-input').should('contain.value', 'Alba').should('not.have.class', 'Mui-disabled');

    cy.get('label').contains('Apellidos');
    cy.get('#last-name-input').should('contain.value', 'Martinez Laguna').should('not.have.class', 'Mui-disabled');

    cy.get('label').contains('Fecha de nacimiento');
    cy.get('#birth-date-input').should('contain.value', '1982-11-03').should('not.have.class', 'Mui-disabled');

    cy.get('label').contains('DNI/NIE');
    cy.get('#document-number-input').should('contain.value', '55593310X').should('not.have.class', 'Mui-disabled');

    cy.get('label').contains('Prefijo');
    cy.get('label').contains('Prefijo').siblings('div').as('prefixInput');
    cy.get('@prefixInput')
      .should('have.class', 'Mui-disabled')
      .children('input')
      .should('contain.value', '34')
      .should('be.disabled');

    cy.get('label').contains('Número de teléfono móvil');
    cy.get('label').contains('Número de teléfono móvil').siblings('div').as('phoneNumberInput');
    cy.get('@phoneNumberInput')
      .should('not.have.class', 'Mui-disabled')
      .children('input')
      .should('contain.value', '659723349')
      .should('not.be.disabled');
  });

  it('OnboardingDetails - two surnames are required when document number is DNI', () => {
    mount(<App {...props} />);
    cy.get('button').filter(`:contains("Continuar")`).click();

    cy.get('#last-name-input').clear().type('Martinez');
    cy.get('button').filter(`:contains("Continuar")`).click();
    cy.get('#last-name-input').parent().should('have.class', 'Mui-error');
    cy.get('#last-name-input-helper-text').should('contain.text', 'Por favor, introduce 2 apellidos');
  });

  it('OnboardingDetails - age must be larger than 18 years', () => {
    mount(<App {...props} />);
    cy.get('button').filter(`:contains("Continuar")`).click();

    cy.get('#birth-date-input').clear().type('2015-01-01');
    cy.get('button').filter(`:contains("Continuar")`).click();
    cy.get('#birth-date-input').parent().should('have.class', 'Mui-error');
    cy.get('#birth-date-input-helper-text').should('contain.text', 'La edad no puede ser inferior a 18 años');
  });

  it('OnboardingDetails - sends postCustomerDetailsByLoanId with expected request body', () => {
    mount(<App {...props} />);
    cy.get('button').filter(`:contains("Continuar")`).click();

    cy.get('#first-name-input').clear().type('Jaime');
    cy.get('#last-name-input').clear().type('Martín Espinosa');
    cy.get('#birth-date-input').clear().type('1987-02-13');
    cy.get('#document-number-input').clear().type('97427900S');

    cy.intercept({ method: 'POST', url: '**/v2/order/customer/' }, (req) => {
      expect(req.body.first_name).to.equal('Jaime');
      expect(req.body.last_name).to.equal('Martín Espinosa');
      expect(req.body.birth_date).to.equal('1987-02-13T00:00:00+00:00');
      expect(req.body.document_number).to.equal('97427900S');
      req.reply({ statusCode: 201 });
    }).as('postCustomerDetailsByLoanId');

    cy.intercept({ method: 'POST', url: `**/loan_application/loan/${orderId}/` }, (req) => {
      req.reply({ statusCode: 201 });
    }).as('postLoanApplications');

    cy.intercept({ method: 'GET', url: `**/v2/order/${orderId}/` }, (req) => {
      req.reply({
        statusCode: 200,
        fixture: 'lending_hub/fixtures/getOrderByLoanIdRequireOtp.json',
      });
    }).as('getOrderByLoanId');

    cy.get('button').filter(`:contains("Continuar")`).click();

    cy.wait(['@postCustomerDetailsByLoanId', '@postLoanApplications', '@getOrderByLoanId']);
  });

  it('PhoneVerification - display', () => {
    mount(<App {...props} />);
    cy.navigateToOtp(orderId);

    cy.contains('Verifica tu móvil');
    cy.contains('Introduce la clave de 5 dígitos enviada por SMS al:');
    cy.contains('+34 659723349');
    cy.contains('Por seguridad, este código caducará en 10 minutos.');
    cy.contains('¿No has recibido el SMS?');
    cy.get('button').contains('Volver a enviar');
    cy.get('button').filter(`:contains("Continuar")`);
    cy.get('button').contains('Editar número de teléfono');
  });

  it('PhoneVerification - no validation code introduced error', () => {
    mount(<App {...props} />);
    cy.navigateToOtp(orderId);

    cy.get('button').filter(`:contains("Continuar")`).click();
    cy.get('#validation-code-input').parent().should('have.class', 'Mui-error');
    cy.get('#validation-code-input-helper-text').should('contain.text', 'Campo requerido');
  });

  it('PhoneVerification - resend new otp', () => {
    mount(<App {...props} />);
    cy.navigateToOtp(orderId);

    cy.intercept({ method: 'PATCH', url: '**/financial/loan_application/' }, (req) => {
      req.reply({ statusCode: 204 });
    });

    cy.get('button').contains('Volver a enviar').click();
    cy.contains('SMS enviado');
    cy.contains('Volver a enviar').should('not.exist');
  });

  it('PhoneVerification - no resend otp when all codes are sent', () => {
    mount(<App {...props} />);
    cy.navigateToOtpFullCodes(orderId);

    cy.intercept({ method: 'PATCH', url: '**/financial/loan_application/' }, (req) => {
      req.reply({ statusCode: 204 });
    });

    cy.contains('Volver a enviar').should('not.exist');
    cy.contains('Por favor, introduce alguno de los códigos enviados.');
    cy.get('button').filter(`:contains("Editar número de teléfono")`).should('have.class', 'Mui-disabled');
  });

  it('Payment - display', () => {
    mount(<App {...props} />);
    cy.navigateToPayment(orderId);

    cy.contains('Resumen');
    cy.contains('Importe de la compra');
    cy.get('#principal-amount-label').contains('Importe de la compra');
    cy.get('#principal-amount-value').contains(replaceNbsp('224,41 €'));
    cy.get('#total-instalment-fee-amount-label').contains(`Coste total (${replaceNbsp('2,50 €')} por cuota)`);
    cy.get('#total-instalment-fee-amount-value').contains(replaceNbsp('30,00 €'));
    cy.get('#total-amount-label').contains('Total a pagar');
    cy.get('#total-amount-value').contains(replaceNbsp('254,41 €'));
    cy.get('#tin-tae').contains('TIN').contains(replaceNbsp('0,00 %')).contains('TAE').contains(replaceNbsp('32,43 %'));

    cy.contains('Servicio ofrecido por');
    cy.contains('Entiendo y acepto la política de privacidad.');
    cy.contains(
      'Al continuar, confirmo que me han hecho y acepto la oferta de crédito, que he leído y acepto las condiciones de contratación y que los datos que he dado a la tienda se usarán para finalizar esta solicitud.',
    );

    cy.get('button').filter(`:contains("Continuar")`).should('have.class', 'Mui-disabled');
    cy.get('input[type="checkbox"]').check();
    cy.get('button').filter(`:contains("Continuar")`).should('not.have.class', 'Mui-disabled');
    cy.get('button').filter(`:contains("Continuar")`).click();

    cy.wait(1000);

    cy.contains('Introduce los datos de tu tarjeta para realizar el pago');
    cy.contains('Servicio ofrecido por');

    cy.get('button').filter(`:contains("Pagar 21,20\u00a0€")`).should('have.class', 'Mui-disabled');
  });

  it('Payment - interaction with card form', () => {
    mount(<App {...props} />);
    cy.navigateToPayment(orderId);
    cy.get('input[type="checkbox"]').check();
    cy.get('button').filter(`:contains("Continuar")`).click();
    cy.wait(1000);

    cy.get('button').filter(`:contains("Pagar 21,20\u00a0€")`).as('payButton');

    cy.get('@payButton').should('have.class', 'Mui-disabled');

    // // The following only works in Google Chrome
    //
    // cy.get('iframe[id="mufasa-iframe"]').then(($iframe) => {
    //   const $body = $iframe.contents().find('body');
    //   cy.wrap($body).contains('Número de tarjeta');
    //   cy.wrap($body).contains('Caduca');
    //   cy.wrap($body).contains('CVV/CVC');
    //   cy.wrap($body).find('input[id="cc-number"]').type('4716773077339777');
    //   cy.wrap($body).find('input[id="cc-exp"]').type('1230');
    //   cy.wrap($body).find('input[id="cc-csc"]').type('123');
    // });
    //
    // cy.get('@payButton').should('not.have.class', 'Mui-disabled');
  });

  it('Denied - display', () => {
    mount(<App {...props} />);
    cy.navigateToDenied(orderId);

    cy.contains('Lo sentimos');
    cy.contains('No hemos podido tramitar la operación...');
    cy.contains('Otra opción');
    cy.contains('Te animamos a que elijas otro método de pago para finalizar tu compra');

    cy.get('button').contains('Cerrar');
  });
  //
  it('Denied - click main button fires onClose with status: denied', () => {
    let statusOnClose: string | undefined = undefined;
    const onClose = (status: string) => (statusOnClose = status);

    mount(<App {...props} handleCloseDialog={onClose} />);
    cy.navigateToDenied(orderId);

    cy.get('button')
      .contains('Cerrar')
      .click()
      .then(() => {
        expect(statusOnClose).to.be.equal('denied');
      });
  });

  it('Denied - click close button fires onClose with status: denied', () => {
    let statusOnClose: string | undefined = undefined;
    const onClose = (status: string) => (statusOnClose = status);

    mount(<App {...props} handleCloseDialog={onClose} />);
    cy.navigateToDenied(orderId);

    cy.get('#close-btn')
      .click()
      .then(() => {
        expect(statusOnClose).to.be.equal('denied');
      });
  });

  it('Expired - displays', () => {
    mount(<App {...props} />);
    cy.navigateToExpired(orderId);

    cy.contains('Tu pedido ha caducado');
    cy.contains('El tiempo límite para finalizar la compra ha terminado.');
    cy.get('button').filter(':contains("Cerrar")').should('not.have.class', 'Mui-disabled').should('not.be.disabled');
  });

  it('Expired - click main button fires onClose with status: expired', () => {
    let statusOnClose: string | undefined = undefined;
    const onClose = (status: string) => (statusOnClose = status);

    mount(<App {...props} handleCloseDialog={onClose} />);
    cy.navigateToExpired(orderId);

    cy.get('button')
      .filter(':contains("Cerrar")')
      .click()
      .then(() => {
        expect(statusOnClose).to.be.equal('expired');
      });
  });

  it('Expired - click close button fires onClose with status: expired', () => {
    let statusOnClose: string | undefined = undefined;
    const onClose = (status: string) => (statusOnClose = status);

    mount(<App {...props} handleCloseDialog={onClose} />);
    cy.navigateToExpired(orderId);

    cy.get('#close-btn')
      .click()
      .then(() => {
        expect(statusOnClose).to.be.equal('expired');
      });
  });

  it('LeaveProcess - display', () => {
    mount(<App {...props} />);
    cy.get('#close-btn').click();

    cy.contains('Antes de marchar...');
    cy.contains('Lamentamos que te vayas. ¿Podrías indicarnos la razón?');
    cy.contains('No entiendo cómo se cobrarán los pagos futuros');
    cy.contains('No me interesa este método de pago');
    cy.contains('El coste de la financiación es elevado');

    cy.get('button').filter(`:contains("Responder y cerrar")`).should('have.class', 'Mui-disabled');
    cy.get('button').filter(`:contains("Responder y cerrar")`).should('be.disabled');
    cy.get('button').filter(`:contains("Volver al proceso")`);

    cy.get('#desist-reasons-toggle')
      .children()
      .each(($el) => {
        cy.wrap($el).click().should('have.class', 'Mui-selected');
      });

    cy.get('button')
      .filter(`:contains("Responder y cerrar")`)
      .should('not.be.disabled')
      .should('not.have.class', 'Mui-disabled');
  });

  it('LeaveProcess - sends patchChangeLoanStatus with expected request body', () => {
    mount(<App {...props} />);

    cy.get('#close-btn').click();

    cy.intercept({ method: 'PATCH', url: '**/api/financial/loan/' }, (req) => {
      expect(req.body.additional).to.equal('nemuru_is_expensive');
      expect(req.body.id).to.equal(orderId);
      expect(req.body.op).to.equal('processPatchChangeLoanStatus');
      expect(req.body.status).to.equal('status_desisted');
      req.reply({ statusCode: 204 });
    }).as('patchChangeLoanStatus');

    cy.get('button').filter(`:contains("El coste de la financiación es elevado")`).click();

    cy.get('button').filter(`:contains("Responder y cerrar")`).click();

    cy.wait('@patchChangeLoanStatus');
  });

  it('LeaveProcess - click main button fires onClose with status: desisted', () => {
    let statusOnClose: string | undefined = undefined;
    const onClose = (status: string) => (statusOnClose = status);

    mount(<App {...props} handleCloseDialog={onClose} />);

    cy.get('#close-btn').click();

    cy.intercept({ method: 'PATCH', url: '**/api/financial/loan/' }, (req) => {
      req.reply({ statusCode: 204 });
    });

    cy.get('button').filter(`:contains("El coste de la financiación es elevado")`).click();

    cy.get('button').filter(`:contains("Responder y cerrar")`).click();

    cy.wait(2500).then(() => {
      expect(statusOnClose).to.be.equal('desisted');
    });
  });

  it('Desisted - displays', () => {
    mount(<App {...props} />);
    cy.navigateToDesisted(orderId);

    cy.contains('Tu pedido ha sido cancelado');
    cy.get('button').filter(':contains("Cerrar")').should('not.have.class', 'Mui-disabled').should('not.be.disabled');
  });

  it('Desisted - click main button fires onClose with status: desisted', () => {
    let statusOnClose: string | undefined = undefined;
    const onClose = (status: string) => (statusOnClose = status);

    mount(<App {...props} handleCloseDialog={onClose} />);
    cy.navigateToDesisted(orderId);

    cy.get('button')
      .filter(':contains("Cerrar")')
      .click()
      .then(() => {
        expect(statusOnClose).to.be.equal('desisted');
      });
  });

  it('Desisted - click close button fires onClose with status: desisted', () => {
    let statusOnClose: string | undefined = undefined;
    const onClose = (status: string) => (statusOnClose = status);

    mount(<App {...props} handleCloseDialog={onClose} />);
    cy.navigateToDesisted(orderId);

    cy.get('#close-btn')
      .click()
      .then(() => {
        expect(statusOnClose).to.be.equal('desisted');
      });
  });
});
