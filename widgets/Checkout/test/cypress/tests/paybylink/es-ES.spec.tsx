import { Environment, Locale } from '@nemuru-eng/js-api-client';
import React from 'react';
import { CheckoutComponentProps } from '../../../../../../src/checkout/Checkout';
import Branding from '../../../../../../src/shared/types/Style/Branding';
import App from '../../../../src/App';
import { mount } from '../../shared/scripts/mount';
import { TestHelper } from '../../shared/scripts/testHelper';

import './commands';

//FIXME: This will be an environment variable
const ENV = Environment.STG;
const BRANDING = Branding.LENDING_HUB;
const LOCALE = Locale.ES_ES;
const orderId = '32fbb8ed-45c6-4b0a-b63b-ca37af2754ef';
const loanApplicationId = '45e69331-59d8-40fe-8c42-7a12080a2da2';

describe('Checkout e2e suite for Pay by link with disableClose and disableFormEdition', () => {
  let props: CheckoutComponentProps;
  const helper = new TestHelper(ENV, BRANDING, LOCALE);

  before(async () => {
    cy.intercept({ method: 'GET', url: `**/order/${orderId}/` }, (req) => {
      req.reply({
        statusCode: 200,
        fixture: 'paybylink/fixtures/getOrderByLoanIdInitial.json',
      });
    });

    cy.intercept(
      { method: 'GET', url: '**/pricings/50bfddb1-ee39-46fa-8586-24bf0d3dde58/pricings.stg.json' },
      (req) => {
        req.reply({
          statusCode: 200,
          fixture: 'paybylink/fixtures/getPricingsByAgentId.json',
        });
      },
    );

    props = await helper.createCheckoutPropsWithDisabledFormEditionAndClose(orderId);
  });

  beforeEach(() => {
    cy.viewport(472, 839);
    cy.wrap('Continuar').as('continueBtn');
  });

  it('Checkout - mounts', () => {
    mount(<App {...props} />);
  });

  it('Checkout - disable close', () => {
    mount(<App {...props} />);
    cy.get('.close-btn').should('not.exist');
  });

  it('OnboardingDetails - display', () => {
    mount(<App {...props} />);
    cy.getContinueBtn().click();

    cy.contains('Introduce tus datos');
    cy.contains('Indica tu dirección de residencia completa:');

    cy.get('label').contains('Dirección');
    cy.get('label').contains('Ciudad');
    cy.get('label').contains('Código postal');
    cy.get('.back-button').should('not.exist');
  });

  it('OnboardingDetails - sends requests with expected body', () => {
    mount(<App {...props} />);
    cy.getContinueBtn().click();

    cy.get('#street-address-input').type('Muntaner 90');
    cy.get('#city-input').type('Barcelona');
    cy.get('#postal-code-input').type('08011');

    cy.intercept({ method: 'PATCH', url: '**/api/financial/onboarding/' }, (req) => {
      expect(req.body.op).to.be.equal('processPatchSetAddress');
      expect(req.body.id).to.be.equal('7a2a7d2a-7414-4fba-9ca6-22f63a81d6b5');
      expect(req.body.street_address).to.be.equal('Muntaner 90');
      expect(req.body.city).to.be.equal('Barcelona');
      expect(req.body.country).to.be.equal('ESP');
      expect(req.body.postal_code).to.be.equal('08011');
      req.reply({ statusCode: 204, delay: 400 });
    }).as('patchSetAddress');

    cy.intercept({ method: 'POST', url: '**/v2/order/customer/' }, (req) => {
      expect(req.body.birth_date).to.equal('1982-11-03T00:00:00+00:00');
      expect(req.body.document_number).to.equal('55593310X');
      expect(req.body.email).to.equal('alba.martinez@gmail.com');
      expect(req.body.first_name).to.equal('Alba');
      expect(req.body.id).to.equal('32fbb8ed-45c6-4b0a-b63b-ca37af2754ef');
      expect(req.body.last_name).to.equal('Martinez');
      expect(req.body.onboarding_details_id).to.equal('7a2a7d2a-7414-4fba-9ca6-22f63a81d6b5');
      expect(req.body.phone_number).to.equal('+34659723349');
      req.reply({ statusCode: 201, delay: 400 });
    }).as('postCustomerDetailsByLoanId');

    cy.intercept({ method: 'POST', url: `**/loan_application/loan/${orderId}/` }, (req) => {
      req.reply({ statusCode: 201, delay: 400 });
    }).as('postLoanApplications');

    cy.intercept({ method: 'GET', url: `**/v2/order/${orderId}/` }, (req) => {
      req.reply({
        statusCode: 200,
        fixture: 'paybylink/fixtures/getOrderByLoanIdRequireOtp.json',
        delay: 400,
      });
    }).as('getOrderByLoanId');

    cy.getContinueBtn().click();

    cy.get('#loading-btn').should('exist');
    cy.get('#street-address-input').parent().should('have.class', 'Mui-disabled');
    cy.get('#city-input').parent().should('have.class', 'Mui-disabled');
    cy.get('#postal-code-input').parent().should('have.class', 'Mui-disabled');

    cy.wait(['@patchSetAddress', '@postCustomerDetailsByLoanId', '@postLoanApplications', '@getOrderByLoanId']);
  });

  it('OnboardingDetails - postal code error', () => {
    mount(<App {...props} />);
    cy.getContinueBtn().click();

    cy.get('#street-address-input').type('Muntaner 20');
    cy.get('#city-input').type('Barcelona');
    cy.get('#postal-code-input').type('0900022');

    cy.getContinueBtn().click();

    cy.get('#postal-code-input').parent().should('have.class', 'Mui-error');
    cy.get('#postal-code-input-helper-text').should('contain.text', 'El código postal introducido no es válido');
  });

  it('OnboardingDetails - street address error', () => {
    mount(<App {...props} />);
    cy.getContinueBtn().click();

    cy.get('#street-address-input').type('Muntaner');
    cy.get('#city-input').type('Barcelona');
    cy.get('#postal-code-input').type('08011');

    cy.intercept({ method: 'PATCH', url: '**/api/financial/onboarding/' }, (req) => {
      expect(req.body.op).to.be.equal('processPatchSetAddress');
      expect(req.body.id).to.be.equal('7a2a7d2a-7414-4fba-9ca6-22f63a81d6b5');
      expect(req.body.street_address).to.be.equal('Muntaner');
      expect(req.body.city).to.be.equal('Barcelona');
      expect(req.body.country).to.be.equal('ESP');
      expect(req.body.postal_code).to.be.equal('08011');
      req.reply({ statusCode: 204 });
    }).as('patchSetAddress');

    cy.intercept({ method: 'POST', url: '**/v2/order/customer/' }, (req) => {
      expect(req.body.birth_date).to.equal('1982-11-03T00:00:00+00:00');
      expect(req.body.document_number).to.equal('55593310X');
      expect(req.body.email).to.equal('alba.martinez@gmail.com');
      expect(req.body.first_name).to.equal('Alba');
      expect(req.body.id).to.equal('32fbb8ed-45c6-4b0a-b63b-ca37af2754ef');
      expect(req.body.last_name).to.equal('Martinez');
      expect(req.body.onboarding_details_id).to.equal('7a2a7d2a-7414-4fba-9ca6-22f63a81d6b5');
      expect(req.body.phone_number).to.equal('+34659723349');
      req.reply({ statusCode: 201, delay: 400 });
    }).as('postCustomerDetailsByLoanId');

    cy.intercept({ method: 'POST', url: `**/loan_application/loan/${orderId}/` }, (req) => {
      req.reply({ statusCode: 201, delay: 400 });
    }).as('postLoanApplications');

    cy.intercept({ method: 'GET', url: `**/v2/order/${orderId}/` }, (req) => {
      req.reply({
        statusCode: 200,
        delay: 400,
        fixture: 'paybylink/fixtures/getOrderByLoanIdError.json',
      });
    }).as('getOrderByLoanId');

    cy.intercept({ method: 'GET', url: `**/loan_application/${loanApplicationId}/error/` }, (req) => {
      req.reply({
        statusCode: 200,
        delay: 400,
        fixture: 'paybylink/fixtures/getProcessResponseByLoanApplicationIdError.json',
      });
    }).as('getProcessResponseByLoanApplicationIdError');

    cy.getContinueBtn().click();

    cy.get('#loading-btn').should('exist');
    cy.get('#street-address-input').parent().should('have.class', 'Mui-disabled');
    cy.get('#city-input').parent().should('have.class', 'Mui-disabled');
    cy.get('#postal-code-input').parent().should('have.class', 'Mui-disabled');

    cy.wait([
      '@patchSetAddress',
      '@postCustomerDetailsByLoanId',
      '@postLoanApplications',
      '@getOrderByLoanId',
      '@getProcessResponseByLoanApplicationIdError',
    ]);

    cy.get('#street-address-input').parent().should('have.class', 'Mui-error');
    cy.get('#street-address-input-helper-text').should('contain.text', 'Por favor, revisa este campo');

    cy.get('#loading-btn').should('not.exist');
    cy.getContinueBtn().should('not.have.class', 'Mui-disabled');
  });

  it('OnboardingDetails - last name and street address error', () => {
    mount(<App {...props} />);
    cy.getContinueBtn().click();

    cy.get('#street-address-input').type('Muntaner');
    cy.get('#city-input').type('Barcelona');
    cy.get('#postal-code-input').type('08011');

    cy.intercept({ method: 'PATCH', url: '**/api/financial/onboarding/' }, (req) => {
      req.reply({ statusCode: 204 });
    }).as('patchSetAddress');

    cy.intercept({ method: 'POST', url: '**/v2/order/customer/' }, (req) => {
      req.reply({ statusCode: 201, delay: 400 });
    }).as('postCustomerDetailsByLoanId');

    cy.intercept({ method: 'POST', url: `**/loan_application/loan/${orderId}/` }, (req) => {
      req.reply({ statusCode: 201, delay: 400 });
    }).as('postLoanApplications');

    cy.intercept({ method: 'GET', url: `**/v2/order/${orderId}/` }, (req) => {
      req.reply({
        statusCode: 200,
        delay: 400,
        fixture: 'paybylink/fixtures/getOrderByLoanIdError.json',
      });
    }).as('getOrderByLoanIdError');

    cy.intercept({ method: 'GET', url: `**/loan_application/${loanApplicationId}/error/` }, (req) => {
      req.reply({
        statusCode: 200,
        delay: 400,
        fixture: 'paybylink/fixtures/getProcessResponseByLoanApplicationIdErrors.json',
      });
    }).as('getProcessResponseByLoanApplicationIdErrors');

    cy.getContinueBtn().click();

    cy.get('#loading-btn').should('exist');
    cy.get('#street-address-input').parent().should('have.class', 'Mui-disabled');
    cy.get('#city-input').parent().should('have.class', 'Mui-disabled');
    cy.get('#postal-code-input').parent().should('have.class', 'Mui-disabled');

    cy.wait([
      '@patchSetAddress',
      '@postCustomerDetailsByLoanId',
      '@postLoanApplications',
      '@getOrderByLoanIdError',
      '@getProcessResponseByLoanApplicationIdErrors',
    ]);

    cy.get('#second-last-name-input').parent().should('have.class', 'Mui-error');
    cy.get('#second-last-name-input-helper-text').should('contain.text', 'Campo requerido');

    cy.get('#loading-btn').should('not.exist');
    cy.getContinueBtn().should('not.have.class', 'Mui-disabled');

    cy.get('#second-last-name-input').type('Pinilla');

    cy.getContinueBtn().click();

    cy.get('#street-address-input').parent().should('have.class', 'Mui-error');
    cy.get('#street-address-input-helper-text').should('contain.text', 'Por favor, revisa este campo');

    cy.intercept({ method: 'POST', url: '**/lender/process_loan_application' }, (req) => {
      req.reply({ statusCode: 201, delay: 400 });
    }).as('postLoanApplicationInLender');

    cy.intercept({ method: 'GET', url: `**/loan_application/${loanApplicationId}/error/` }, (req) => {
      req.reply({
        statusCode: 200,
        delay: 400,
        fixture: 'paybylink/fixtures/getProcessResponseByLoanApplicationIdError.json',
      });
    }).as('getProcessResponseByLoanApplicationIdError');

    cy.getContinueBtn().click();

    cy.get('#loading-btn').should('exist');
    cy.get('#street-address-input').parent().should('have.class', 'Mui-disabled');
    cy.get('#city-input').parent().should('have.class', 'Mui-disabled');
    cy.get('#postal-code-input').parent().should('have.class', 'Mui-disabled');

    cy.wait([
      '@patchSetAddress',
      '@postCustomerDetailsByLoanId',
      '@postLoanApplicationInLender',
      '@getOrderByLoanIdError',
      '@getProcessResponseByLoanApplicationIdError',
    ]);

    cy.get('#street-address-input').parent().should('have.class', 'Mui-error');
    cy.get('#street-address-input-helper-text').should('contain.text', 'Por favor, revisa este campo');

    cy.get('#loading-btn').should('not.exist');
    cy.getContinueBtn().should('not.have.class', 'Mui-disabled');

    cy.get('#street-address-input').type(' 90');

    cy.intercept({ method: 'GET', url: `**/v2/order/${orderId}/` }, (req) => {
      req.reply({
        statusCode: 200,
        delay: 400,
        fixture: 'paybylink/fixtures/getOrderByLoanIdRequireOtp.json',
      });
    }).as('getOrderByLoanId');

    cy.getContinueBtn().click();

    cy.wait(['@patchSetAddress', '@postCustomerDetailsByLoanId', '@postLoanApplicationInLender', '@getOrderByLoanId']);

    cy.contains('Verifica tu móvil');
  });

  it('PhoneVerification - display', () => {
    mount(<App {...props} />);
    cy.navigateToOtp(orderId);

    cy.contains('Verifica tu móvil');
    cy.contains('Introduce la clave de 5 dígitos enviada por SMS al:');
    cy.contains('+34 659723349');
    cy.contains('Por seguridad, este código caducará en 10 minutos.');
    cy.contains('¿No has recibido el SMS?');
    cy.get('button').contains('Volver a enviar');
    cy.getContinueBtn();
    cy.get('button').contains('Editar número de teléfono').should('not.exist');
  });
});
