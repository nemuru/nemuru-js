// @ts-ignore
Cypress.Commands.add('getContinueBtn', () => {
  return cy.get(`@continueBtn`).then((btn) => cy.get('button').filter(`:contains(${btn})`));
});

// @ts-ignore
Cypress.Commands.add('getBackBtn', () => {
  return cy.get('.back-button');
});

Cypress.Commands.add('navigateToOtp', (orderId: string) => {
  cy.getContinueBtn().click();

  cy.get('#street-address-input').type('Muntaner 90');
  cy.get('#city-input').type('Barcelona');
  cy.get('#postal-code-input').type('08011');

  cy.intercept({ method: 'PATCH', url: '**/api/financial/onboarding/' }, (req) => {
    req.reply({ statusCode: 204, delay: 400 });
  }).as('patchSetAddress');

  cy.intercept({ method: 'POST', url: '**/v2/order/customer/' }, (req) => {
    req.reply({ statusCode: 201 });
  }).as('postCustomerDetailsByLoanId');

  cy.intercept({ method: 'POST', url: `**/loan_application/loan/${orderId}/` }, (req) => {
    req.reply({ statusCode: 201 });
  }).as('postLoanApplications');

  cy.intercept({ method: 'GET', url: `**/v2/order/${orderId}/` }, (req) => {
    req.reply({
      statusCode: 200,
      fixture: 'paybylink/fixtures/getOrderByLoanIdRequireOtp.json',
    });
  }).as('getOrderByLoanId');

  cy.getContinueBtn().click();

  cy.get('#loading-btn').should('exist');
  cy.get('#street-address-input').parent().should('have.class', 'Mui-disabled');
  cy.get('#city-input').parent().should('have.class', 'Mui-disabled');
  cy.get('#postal-code-input').parent().should('have.class', 'Mui-disabled');

  cy.wait(['@patchSetAddress', '@postCustomerDetailsByLoanId', '@postLoanApplications', '@getOrderByLoanId']).wait(200);
});

declare namespace Cypress {
  interface Chainable {
    getContinueBtn(): Chainable<void>;
    getBackBtn(): Chainable<void>;
    navigateToOtp(orderId: string): Chainable<void>;
  }
}
