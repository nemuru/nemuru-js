import React from 'react';
import { Environment, Locale } from '@nemuru-eng/js-api-client';

import './commands';
import { CheckoutComponentProps } from '../../../../../../src/checkout/Checkout';
import App from '../../../../src/App';
import { mount } from '../../shared/scripts/mount';
import { TestHelper } from '../../shared/scripts/testHelper';
import Branding from '../../../../../../src/shared/types/Style/Branding';
import { replaceNbsp } from '../../shared/scripts/utils';

//FIXME: This will be an environment variable
const ENV = Environment.STG;
const BRANDING = Branding.IBERIA;
const LOCALE = Locale.CA_CA;
const orderId = '32fbb8ed-45c6-4b0a-b63b-ca37af2754ef';

describe('Checkout e2e suite for Iberia branding, instalments and ca-CA locale', () => {
  let props: CheckoutComponentProps;
  const helper = new TestHelper(ENV, BRANDING, LOCALE);

  before(async () => {
    cy.intercept({ method: 'GET', url: `**/order/${orderId}/` }, (req) => {
      req.reply({
        statusCode: 200,
        fixture: 'iberia/fixtures/getOrderByLoanIdInitial.json',
      });
    });

    cy.intercept(
      { method: 'GET', url: `**/pricings/50bfddb1-ee39-46fa-8586-24bf0d3dde58/pricings.stg.json` },
      (req) => {
        req.reply({
          statusCode: 200,
          fixture: 'iberia/fixtures/getPricingsByAgentId.json',
        });
      },
    );

    props = await helper.createCheckoutPropsWithDisabledFormEdition(orderId);
  });
  beforeEach(() => {
    cy.viewport(472, 839);
    cy.wrap('Continuar').as('continueBtn');
  });

  it('Checkout - mounts', () => {
    mount(<App {...props} />);
  });

  it('Calculator - display', () => {
    mount(<App {...props} />);

    cy.contains('Paga en quotes');
    cy.contains('Pla de pagament');
    cy.contains('Selecciona com vols fraccionar la teva compra:');
    cy.contains('Dades de la compra');
    cy.get('#principal-amount-label').contains('Import de la compra');
    cy.get('#principal-amount-value').contains(replaceNbsp('224,41 €'));
    cy.get('#total-instalment-fee-amount-label').contains(`Cost total (${replaceNbsp('2,50 €')} per quota)`);
    cy.get('#total-instalment-fee-amount-value').contains(replaceNbsp('30,00 €'));
    cy.get('#total-amount-label').contains('Total a pagar');
    cy.get('#total-amount-value').contains(replaceNbsp('254,41 €'));
    cy.get('#tin-tae').contains('TIN').contains(replaceNbsp('0,00 %')).contains('TAE').contains(replaceNbsp('32,43 %'));

    cy.getContinueBtn();
  });

  it('Calculator - conditions example legal text', () => {
    mount(<App {...props} />);

    cy.contains(
      "Exemple Informatiu de compra d'un bitllet per import de 224,41 € (import de la compra) finançada a 12 mesos: les quotes mensuals serien de 21,20 € (que inclou 18,70 € de principal i 2,50 € de cost fix) i una última quota amb un import de 21,21 € (que inclou 18,71 € de principal i 2,50 € de cost fix). L'import total del crèdit seria de 254,41 € (que inclou 224,41 € de l'import del bitllet i 30,00 € per costos fixes del finançament). TIN al 0,00 % i TAE 32,43 %.",
    ).should('not.be.visible');
    cy.get('span').contains('+info').click();
    cy.contains(
      "Exemple Informatiu de compra d'un bitllet per import de 224,41 € (import de la compra) finançada a 12 mesos: les quotes mensuals serien de 21,20 € (que inclou 18,70 € de principal i 2,50 € de cost fix) i una última quota amb un import de 21,21 € (que inclou 18,71 € de principal i 2,50 € de cost fix). L'import total del crèdit seria de 254,41 € (que inclou 224,41 € de l'import del bitllet i 30,00 € per costos fixes del finançament). TIN al 0,00 % i TAE 32,43 %.",
    ).should('be.visible');
  });

  it('OnboardingDetails - display', () => {
    mount(<App {...props} />);
    cy.getContinueBtn().click();

    cy.contains('Introdueix les teves dades');
    cy.contains('Completa la teva informació personal:');

    cy.get('label').contains('Nom');
    cy.get('#first-name-input')
      .should('contain.value', 'Alba')
      .should('have.class', 'Mui-disabled')
      .should('be.disabled');

    cy.get('label').contains('Cognoms');
    cy.get('#last-name-input')
      .should('contain.value', 'Martinez')
      .should('have.class', 'Mui-disabled')
      .should('be.disabled');

    cy.get('label').contains('Data de naixement');
    cy.get('#birth-date-input')
      .should('contain.value', '')
      .should('not.have.class', 'Mui-disabled')
      .should('not.be.disabled');

    cy.get('label').contains('DNI/NIE');
    cy.get('#document-number-input')
      .should('contain.value', '')
      .should('not.have.class', 'Mui-disabled')
      .should('not.be.disabled');

    cy.get('label').contains('Prefix');
    cy.get('label').contains('Prefix').siblings('div').as('prefixInput');
    cy.get('@prefixInput')
      .should('have.class', 'Mui-disabled')
      .children('input')
      .should('contain.value', '34')
      .should('be.disabled');

    cy.get('label').contains('Número de telèfon mòbil');
    cy.get('label').contains('Número de telèfon mòbil').siblings('div').as('phoneNumberInput');
    cy.get('@phoneNumberInput')
      .should('have.class', 'Mui-disabled')
      .children('input')
      .should('contain.value', '659723349')
      .should('be.disabled');

    cy.get('#birth-date-input').type('1982-11-03');
    cy.get('#document-number-input').type('Y7528119A');

    cy.getContinueBtn().click();

    cy.contains('Introdueix les teves dades');
    cy.contains('Indica la teva adreça de residència:');

    cy.get('label').contains('Direcció');
    cy.get('#street-address-input')
      .should('contain.value', '')
      .should('not.have.class', 'Mui-disabled')
      .should('not.be.disabled');

    cy.get('label').contains('Ciutat');
    cy.get('#city-input')
      .should('contain.value', '')
      .should('not.have.class', 'Mui-disabled')
      .should('not.be.disabled');

    cy.get('label').contains('Codi postal');
    cy.get('#postal-code-input')
      .should('contain.value', '')
      .should('not.have.class', 'Mui-disabled')
      .should('not.be.disabled');

    cy.getContinueBtn();
  });

  it('PhoneVerification - display', () => {
    mount(<App {...props} />);
    cy.navigateToOtp(orderId);

    cy.contains('Verifica el teu mòbil');
    cy.contains('Introdueix el codi de 5 dígits enviat per SMS al:');
    cy.contains('+34 659723349');
    cy.contains('Per seguretat, aquest codi caducarà en 10 minuts.');
    cy.contains('No has rebut el SMS?');
    cy.get('button').contains('Tornar a enviar');
    cy.getContinueBtn();
    cy.get('button').contains('Editar el número de telèfon mòbil').should('not.exist');
  });

  it('Payment - display', () => {
    mount(<App {...props} />);
    cy.navigateToPayment(orderId);

    cy.contains('Resum');
    cy.get('#principal-amount-label').contains('Import de la compra');
    cy.get('#principal-amount-value').contains(replaceNbsp('224,41 €'));
    cy.get('#total-instalment-fee-amount-label').contains(`Cost total (${replaceNbsp('2,50 €')} per quota)`);
    cy.get('#total-instalment-fee-amount-value').contains(replaceNbsp('30,00 €'));
    cy.get('#total-amount-label').contains('Total a pagar');
    cy.get('#total-amount-value').contains(replaceNbsp('254,41 €'));
    cy.contains('Servei ofert per');
    cy.get('#tin-tae').contains('TIN');
    cy.get('#tin-tae').contains(replaceNbsp('0,00 %'));
    cy.get('#tin-tae').contains('TAE');
    cy.get('#tin-tae').contains(replaceNbsp('32,43 %'));
    cy.get('span').contains('+info').should('not.exist');

    cy.contains('Entenc i accepto la política de privacitat.');
    cy.contains(
      "En continuar, confirmo que m'han fet i accepto l' oferta de crèdit, que he llegit i accepto les condicions de contractació i que les dades que he donat a la botiga es faran servir per finalitzar aquesta sol·licitud.",
    );

    cy.getContinueBtn().should('have.class', 'Mui-disabled');
    cy.get('input[type="checkbox"]').check();
    cy.getContinueBtn().should('not.have.class', 'Mui-disabled');
    cy.getContinueBtn().click();

    cy.wait(1000);

    cy.contains('Targeta de pagament');
    cy.contains('Introdueix les dades de la teva targeta per fer el pagament de la primera quota');
    cy.contains('Servei ofert per');

    cy.get('button').filter(`:contains("Pagar 21,20\u00a0€")`).should('have.class', 'Mui-disabled');
  });

  it('Denied - display', () => {
    mount(<App {...props} />);
    cy.navigateToDenied(orderId);

    cy.contains('Ho sentim');
    cy.contains("No hem pogut tramitar l'operació...");
    cy.contains('Una altra opció');
    cy.contains("T'animem que triïs un altre mètode de pagament per finalitzar la teva compra");

    cy.get('button').contains('Tancar');
  });

  it('Expired - displays', () => {
    mount(<App {...props} />);
    cy.navigateToExpired(orderId);

    cy.contains('La teva sol·licitud ha caducat');
    cy.contains("El temps límit per completar la compra s'ha acabat.");
    cy.get('button').filter(':contains("Tancar")').should('not.have.class', 'Mui-disabled').should('not.be.disabled');
  });

  it('LeaveProcess - display', () => {
    mount(<App {...props} />);
    cy.get('#close-btn').click();

    cy.contains('Abans de marxar...');
    cy.contains("Lamentem que te'n vagis. Podries indicar-nos la raó?");
    cy.contains('No entenc com es cobraran els pagaments futurs');
    cy.contains("No m'interessa aquest mètode de pagament");
    cy.contains('El cost del finançament és elevat');

    cy.get('button').filter(`:contains("Respondre i tancar")`).should('have.class', 'Mui-disabled');
    cy.get('button').filter(`:contains("Respondre i tancar")`).should('be.disabled');
    cy.get('button').filter(`:contains("Tornar al procés")`);

    cy.get('#desist-reasons-toggle')
      .children()
      .each(($el) => {
        cy.wrap($el).click().should('have.class', 'Mui-selected');
      });

    cy.get('button')
      .filter(`:contains("Respondre i tancar")`)
      .should('not.be.disabled')
      .should('not.have.class', 'Mui-disabled');
  });

  it('Desisted - displays', () => {
    mount(<App {...props} />);
    cy.navigateToDesisted(orderId);

    cy.contains('La teva sol·licitud ha sigut cancel·lada');
    cy.get('button').filter(':contains("Tancar")').should('not.have.class', 'Mui-disabled').should('not.be.disabled');
  });
});
