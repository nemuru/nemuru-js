import React from 'react';
import { Environment, Locale } from '@nemuru-eng/js-api-client';

import './commands';
import { CheckoutComponentProps } from '../../../../../../src/checkout/Checkout';
import App from '../../../../src/App';
import { mount } from '../../shared/scripts/mount';
import { TestHelper } from '../../shared/scripts/testHelper';
import Branding from '../../../../../../src/shared/types/Style/Branding';
import { replaceNbsp } from '../../shared/scripts/utils';

//FIXME: This will be an environment variable
const ENV = Environment.STG;
const BRANDING = Branding.IBERIA;
const LOCALE = Locale.EN_US;
const orderId = '32fbb8ed-45c6-4b0a-b63b-ca37af2754ef';

describe('Checkout e2e suite for Iberia branding, instalments and en-US locale', () => {
  let props: CheckoutComponentProps;
  const helper = new TestHelper(ENV, BRANDING, LOCALE);

  before(async () => {
    cy.intercept({ method: 'GET', url: `**/order/${orderId}/` }, (req) => {
      req.reply({
        statusCode: 200,
        fixture: 'iberia/fixtures/getOrderByLoanIdInitial.json',
      });
    });

    cy.intercept(
      { method: 'GET', url: `**/pricings/50bfddb1-ee39-46fa-8586-24bf0d3dde58/pricings.stg.json` },
      (req) => {
        req.reply({
          statusCode: 200,
          fixture: 'iberia/fixtures/getPricingsByAgentId.json',
        });
      },
    );

    props = await helper.createCheckoutPropsWithDisabledFormEdition(orderId);
  });
  beforeEach(() => {
    cy.viewport(472, 839);
    cy.wrap('Continue').as('continueBtn');
  });

  it('Checkout - mounts', () => {
    mount(<App {...props} />);
  });

  it('Calculator - display', () => {
    mount(<App {...props} />);

    cy.contains('Instalments');
    cy.contains('Payment plan');
    cy.contains('Select how you want to split your purchase:');
    cy.contains('Order details');
    cy.get('#principal-amount-label').contains('Purchase amount');
    cy.get('#principal-amount-value').contains(replaceNbsp('224,41 €'));
    cy.get('#total-instalment-fee-amount-label').contains(`Total cost (${replaceNbsp('2,50 €')} per instalment)`);
    cy.get('#total-instalment-fee-amount-value').contains(replaceNbsp('30,00 €'));
    cy.get('#total-amount-label').contains('Total to pay');
    cy.get('#total-amount-value').contains(replaceNbsp('254,41 €'));
    cy.get('#tin-tae')
      .contains('Interest')
      .contains(replaceNbsp('0,00 %'))
      .contains('APR')
      .contains(replaceNbsp('32,43 %'));

    cy.getContinueBtn();
  });

  it('Calculator - conditions example legal text', () => {
    mount(<App {...props} />);

    cy.contains(
      'Informative example of the purchase of a ticket for an amount of 224,41 € (purchase amount) financed over 12 months: the monthly instalment amount would be 21,20 € (which includes 18,70 € in principal and 2,50 € in fixed fees) and the last instalment amount would be 21,21 € (which includes 18,71 € in principal and 2,50 € in fixed fees). The total amount of the credit would be 254,41 € (which includes 224,41 € for the amount of the ticket and 30,00 € in fixed financing fees). An annual interest of 0,00 % and an APR of 32,43 %.',
    ).should('not.be.visible');
    cy.get('span').contains('+info').click();
    cy.contains(
      'Informative example of the purchase of a ticket for an amount of 224,41 € (purchase amount) financed over 12 months: the monthly instalment amount would be 21,20 € (which includes 18,70 € in principal and 2,50 € in fixed fees) and the last instalment amount would be 21,21 € (which includes 18,71 € in principal and 2,50 € in fixed fees). The total amount of the credit would be 254,41 € (which includes 224,41 € for the amount of the ticket and 30,00 € in fixed financing fees). An annual interest of 0,00 % and an APR of 32,43 %.',
    ).should('be.visible');
  });

  it('OnboardingDetails - display', () => {
    mount(<App {...props} />);
    cy.getContinueBtn().click();

    cy.contains('Personal information');
    cy.contains('Enter your personal information:');

    cy.get('label').contains('First name');
    cy.get('#first-name-input')
      .should('contain.value', 'Alba')
      .should('have.class', 'Mui-disabled')
      .should('be.disabled');

    cy.get('label').contains('Last name');
    cy.get('#last-name-input')
      .should('contain.value', 'Martinez')
      .should('have.class', 'Mui-disabled')
      .should('be.disabled');

    cy.get('label').contains('Date of birth');
    cy.get('#birth-date-input')
      .should('contain.value', '')
      .should('not.have.class', 'Mui-disabled')
      .should('not.be.disabled');

    cy.get('label').contains('National identification number');
    cy.get('#document-number-input')
      .should('contain.value', '')
      .should('not.have.class', 'Mui-disabled')
      .should('not.be.disabled');

    cy.get('label').contains('Code');
    cy.get('label').contains('Code').siblings('div').as('prefixInput');
    cy.get('@prefixInput')
      .should('have.class', 'Mui-disabled')
      .children('input')
      .should('contain.value', '34')
      .should('be.disabled');

    cy.get('label').contains('Mobile phone number');
    cy.get('label').contains('Mobile phone number').siblings('div').as('phoneNumberInput');
    cy.get('@phoneNumberInput')
      .should('have.class', 'Mui-disabled')
      .children('input')
      .should('contain.value', '659723349')
      .should('be.disabled');

    cy.get('#birth-date-input').type('1982-11-03');
    cy.get('#document-number-input').type('Y7528119A');

    cy.getContinueBtn().click();

    cy.contains('Personal information');
    cy.contains('Introduce your residence address:');

    cy.get('label').contains('Address');
    cy.get('#street-address-input')
      .should('contain.value', '')
      .should('not.have.class', 'Mui-disabled')
      .should('not.be.disabled');

    cy.get('label').contains('City');
    cy.get('#city-input')
      .should('contain.value', '')
      .should('not.have.class', 'Mui-disabled')
      .should('not.be.disabled');

    cy.get('label').contains('Postal code');
    cy.get('#postal-code-input')
      .should('contain.value', '')
      .should('not.have.class', 'Mui-disabled')
      .should('not.be.disabled');

    cy.getContinueBtn();
  });

  it('PhoneVerification - display', () => {
    mount(<App {...props} />);
    cy.navigateToOtp(orderId);

    cy.contains('Verify your mobile phone');
    cy.contains('Enter the 5-digit code sent by SMS to:');
    cy.contains('+34 659723349');
    cy.contains('For security, this code will expire in 10 minutes.');
    cy.contains("Haven't received the SMS?");
    cy.get('button').contains('Resend');
    cy.getContinueBtn();
    cy.get('button').contains('Edit mobile phone number').should('not.exist');
  });

  it('Payment - display', () => {
    mount(<App {...props} />);
    cy.navigateToPayment(orderId);

    cy.contains('Summary');
    cy.get('#principal-amount-label').contains('Purchase amount');
    cy.get('#principal-amount-value').contains(replaceNbsp('224,41 €'));
    cy.get('#total-instalment-fee-amount-label').contains(`Total cost (${replaceNbsp('2,50 €')} per instalment)`);
    cy.get('#total-instalment-fee-amount-value').contains(replaceNbsp('30,00 €'));
    cy.get('#total-amount-label').contains('Total to pay');
    cy.get('#total-amount-value').contains(replaceNbsp('254,41 €'));
    cy.contains('Service offered by');
    cy.get('#tin-tae').contains('Interest');
    cy.get('#tin-tae').contains(replaceNbsp('0,00 %'));
    cy.get('#tin-tae').contains('APR');
    cy.get('#tin-tae').contains(replaceNbsp('32,43 %'));
    cy.get('span').contains('+info').should('not.exist');

    cy.contains('I understand and accept the privacy policy.');
    cy.contains(
      'By continuing, I confirm that I have been made and I accept the credit offer, that I have read and accept the contracting conditions and that the information that I gave to the store will be used to complete this application.',
    );

    cy.getContinueBtn().should('have.class', 'Mui-disabled');
    cy.get('input[type="checkbox"]').check();
    cy.getContinueBtn().should('not.have.class', 'Mui-disabled');
    cy.getContinueBtn().click();

    cy.wait(1000);

    cy.contains('Payment card');
    cy.contains('Enter your card details to make the payment');
    cy.contains('Service offered by');

    cy.get('button').filter(`:contains("Pay 21,20\u00a0€")`).should('have.class', 'Mui-disabled');
  });

  it('Denied - display', () => {
    mount(<App {...props} />);
    cy.navigateToDenied(orderId);

    cy.contains('We are sorry');
    cy.contains('We were unable to process your application...');
    cy.contains('Another option');
    cy.contains('We encourage you to choose another payment method to complete your purchase');

    cy.get('button').contains('Close');
  });

  it('Expired - displays', () => {
    mount(<App {...props} />);
    cy.navigateToExpired(orderId);

    cy.contains('Your order has expired');
    cy.contains('The allowed time to complete the purchase has expired.');
    cy.get('button').filter(':contains("Close")').should('not.have.class', 'Mui-disabled').should('not.be.disabled');
  });

  it('LeaveProcess - display', () => {
    mount(<App {...props} />);
    cy.get('#close-btn').click();

    cy.contains('Before leaving...');
    cy.contains('We are sorry to see you go. Could you tell us the reason?');
    cy.contains("I don't understand how future payments will be collected");
    cy.contains('I am not interested in this payment method');
    cy.contains('The cost of financing is high');

    cy.get('button').filter(`:contains("Reply and close")`).should('have.class', 'Mui-disabled');
    cy.get('button').filter(`:contains("Reply and close")`).should('be.disabled');
    cy.get('button').filter(`:contains("Return")`);

    cy.get('#desist-reasons-toggle')
      .children()
      .each(($el) => {
        cy.wrap($el).click().should('have.class', 'Mui-selected');
      });

    cy.get('button')
      .filter(`:contains("Reply and close")`)
      .should('not.be.disabled')
      .should('not.have.class', 'Mui-disabled');
  });

  it('Desisted - displays', () => {
    mount(<App {...props} />);
    cy.navigateToDesisted(orderId);

    cy.contains('Your order is cancelled');
    cy.get('button').filter(':contains("Close")').should('not.have.class', 'Mui-disabled').should('not.be.disabled');
  });
});
