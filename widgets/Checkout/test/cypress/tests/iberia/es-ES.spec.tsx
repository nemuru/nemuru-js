import React from 'react';
import { Environment, Locale } from '@nemuru-eng/js-api-client';

import './commands';
import { CheckoutComponentProps } from '../../../../../../src/checkout/Checkout';
import App from '../../../../src/App';
import { mount } from '../../shared/scripts/mount';
import { TestHelper } from '../../shared/scripts/testHelper';
import Branding from '../../../../../../src/shared/types/Style/Branding';
import { replaceNbsp } from '../../shared/scripts/utils';

//FIXME: This will be an environment variable
const ENV = Environment.STG;
const BRANDING = Branding.IBERIA;
const LOCALE = Locale.ES_ES;
const orderId = '32fbb8ed-45c6-4b0a-b63b-ca37af2754ef';
const loanApplicationId = '45e69331-59d8-40fe-8c42-7a12080a2da2';

describe('Checkout e2e suite for Iberia branding, instalments and es-ES locale', () => {
  let props: CheckoutComponentProps;
  const helper = new TestHelper(ENV, BRANDING, LOCALE);

  before(async () => {
    cy.intercept({ method: 'GET', url: `**/order/${orderId}/` }, (req) => {
      req.reply({
        statusCode: 200,
        fixture: 'iberia/fixtures/getOrderByLoanIdInitial.json',
      });
    });

    cy.intercept({ method: 'GET', url: `**/pricings/50bfddb1-ee39-46fa-8586-24bf0d3dde58/pricings**` }, (req) => {
      req.reply({
        statusCode: 200,
        fixture: 'iberia/fixtures/getPricingsByAgentId.json',
      });
    });

    props = await helper.createCheckoutPropsWithDisabledFormEdition(orderId);
  });

  beforeEach(() => {
    cy.viewport(472, 839);
    cy.wrap('Continuar').as('continueBtn');
  });

  it('Checkout - mounts', () => {
    mount(<App {...props} />);
  });

  it('Calculator - display', () => {
    mount(<App {...props} />);

    cy.contains('Pago en cuotas');
    cy.contains('Plan de pagos');
    cy.contains('Selecciona cómo quieres fraccionar tu compra:');
    cy.contains('Detalle de tu compra');
    cy.get('#principal-amount-label').contains('Importe de la compra');
    cy.get('#principal-amount-value').contains(replaceNbsp('224,41 €'));
    cy.get('#total-instalment-fee-amount-label').contains(`Coste total (${replaceNbsp('2,50 €')} por cuota)`);
    cy.get('#total-instalment-fee-amount-value').contains(replaceNbsp('30,00 €'));
    cy.get('#total-amount-label').contains('Total a pagar');
    cy.get('#total-amount-value').contains(replaceNbsp('254,41 €'));
    cy.get('#tin-tae').contains('TIN').contains(replaceNbsp('0,00 %')).contains('TAE').contains(replaceNbsp('32,43 %'));

    cy.getContinueBtn();
  });

  it('Calculator - conditions example legal text', () => {
    mount(<App {...props} />);

    cy.contains(
      'Ejemplo Informativo de compra de un billete por importe de 224,41 € (importe de la compra) financiado a 12 meses: las cuotas mensuales serían de 21,20 € (que incluye 18,70 € de principal y 2,50 € de coste fijo) y con la última cuota con un importe de 21,21 € (que incluye 18,71 € de principal y 2,50 € de coste fijo). El importe total del crédito sería 254,41 € (que incluye 224,41 € del importe del billete y 30,00 € por costes fijos de la financiación). TIN al 0,00 % y TAE 32,43 %.',
    ).should('not.be.visible');
    cy.get('span').contains('+info').click();
    cy.contains(
      'Ejemplo Informativo de compra de un billete por importe de 224,41 € (importe de la compra) financiado a 12 meses: las cuotas mensuales serían de 21,20 € (que incluye 18,70 € de principal y 2,50 € de coste fijo) y con la última cuota con un importe de 21,21 € (que incluye 18,71 € de principal y 2,50 € de coste fijo). El importe total del crédito sería 254,41 € (que incluye 224,41 € del importe del billete y 30,00 € por costes fijos de la financiación). TIN al 0,00 % y TAE 32,43 %.',
    ).should('be.visible');
  });

  it('Calculator - interaction', () => {
    mount(<App {...props} />);

    cy.get('div').should('have.class', 'selector-option').contains(replaceNbsp('27,43 € al mes')).click();

    cy.get('#principal-amount-value').contains(replaceNbsp('224,41 €'));
    cy.get('#total-instalment-fee-amount-label').contains(`Coste total (${replaceNbsp('2,50 €')} por cuota)`);
    cy.get('#total-instalment-fee-amount-value').contains(replaceNbsp('22,50 €'));
    cy.get('#total-amount-value').contains(replaceNbsp('246,91 €'));
    cy.get('#tin-tae').contains(replaceNbsp('0,00 %'));
    cy.get('#tin-tae').contains(replaceNbsp('33,99 %'));

    cy.get('div').should('have.class', 'selector-option').contains(replaceNbsp('39,90 € al mes')).click();

    cy.get('#principal-amount-value').contains(replaceNbsp('224,41 €'));
    cy.get('#total-instalment-fee-amount-label').contains(`Coste total (${replaceNbsp('2,50 €')} por cuota)`);
    cy.get('#total-instalment-fee-amount-value').contains(replaceNbsp('15,00 €'));
    cy.get('#total-amount-value').contains(replaceNbsp('239,41 €'));
    cy.get('#tin-tae').contains(replaceNbsp('0,00 %'));
    cy.get('#tin-tae').contains(replaceNbsp('37,08 %'));

    cy.get('div').should('have.class', 'selector-option').contains(replaceNbsp('77,30 € al mes')).click();

    cy.get('#principal-amount-value').contains(replaceNbsp('224,41 €'));
    cy.get('#total-instalment-fee-amount-label').contains(`Coste total (${replaceNbsp('2,50 €')} por cuota)`);
    cy.get('#total-instalment-fee-amount-value').contains(replaceNbsp('7,50 €'));
    cy.get('#total-amount-value').contains(replaceNbsp('231,91 €'));
    cy.get('#tin-tae').contains(replaceNbsp('0,00 %'));
    cy.get('#tin-tae').contains(replaceNbsp('49,02 %'));
  });

  it('Calculator - sends processPatchSetLoanCalculator with expected request body', () => {
    mount(<App {...props} />);

    cy.get('div').contains(replaceNbsp('77,30 € al mes')).click();

    cy.intercept({ method: 'PATCH', url: '**/api/financial/loan/' }, (req) => {
      expect(req.body.op).to.equal('processPatchSetLoanCalculator');
      expect(req.body.id).to.equal('32fbb8ed-45c6-4b0a-b63b-ca37af2754ef');
      expect(req.body.principal_currency).to.equal('EUR');
      expect(req.body.principal_amount).to.equal(224.41);
      expect(req.body.setup_fee_currency).to.equal('EUR');
      expect(req.body.setup_fee_amount).to.equal(7.5);
      expect(req.body.setup_fee_type).to.equal('type_fixed_amount');
      expect(req.body.term_period).to.equal(3);
      expect(req.body.term_type).to.equal('type_period');
      expect(req.body.term_interval).to.equal('P1M');
      expect(req.body.loan_program_name).to.equal('free_program');
      expect(req.body.loan_program_merchant_discount_percentage).to.equal(0);
      req.reply({ statusCode: 204 });
    });

    cy.getContinueBtn().click();
  });

  it('OnboardingDetails - display', () => {
    mount(<App {...props} />);
    cy.getContinueBtn().click();

    cy.contains('Introduce tus datos');
    cy.contains('Completa tu información personal:');

    cy.get('label').contains('Nombre');
    cy.get('#first-name-input')
      .should('contain.value', 'Alba')
      .should('have.class', 'Mui-disabled')
      .should('be.disabled');

    cy.get('label').contains('Apellidos');
    cy.get('#last-name-input')
      .should('contain.value', 'Martinez')
      .should('have.class', 'Mui-disabled')
      .should('be.disabled');

    cy.get('label').contains('Fecha de nacimiento');
    cy.get('#birth-date-input')
      .should('contain.value', '')
      .should('not.have.class', 'Mui-disabled')
      .should('not.be.disabled');

    cy.get('label').contains('DNI/NIE');
    cy.get('#document-number-input')
      .should('contain.value', '')
      .should('not.have.class', 'Mui-disabled')
      .should('not.be.disabled');

    cy.get('label').contains('Prefijo');
    cy.get('label').contains('Prefijo').siblings('div').as('prefixInput');
    cy.get('@prefixInput')
      .should('have.class', 'Mui-disabled')
      .children('input')
      .should('contain.value', '34')
      .should('be.disabled');

    cy.get('label').contains('Número de teléfono móvil');
    cy.get('label').contains('Número de teléfono móvil').siblings('div').as('phoneNumberInput');
    cy.get('@phoneNumberInput')
      .should('have.class', 'Mui-disabled')
      .children('input')
      .should('contain.value', '659723349')
      .should('be.disabled');

    cy.get('#birth-date-input').type('1982-11-03');
    cy.get('#document-number-input').type('Y7528119A');

    cy.getContinueBtn().click();

    cy.contains('Introduce tus datos');
    cy.contains('Indica tu dirección de residencia completa:');

    cy.get('label').contains('Dirección');
    cy.get('#street-address-input')
      .should('contain.value', '')
      .should('not.have.class', 'Mui-disabled')
      .should('not.be.disabled');

    cy.get('label').contains('Ciudad');
    cy.get('#city-input')
      .should('contain.value', '')
      .should('not.have.class', 'Mui-disabled')
      .should('not.be.disabled');

    cy.get('label').contains('Código postal');
    cy.get('#postal-code-input')
      .should('contain.value', '')
      .should('not.have.class', 'Mui-disabled')
      .should('not.be.disabled');

    cy.getContinueBtn();
  });

  it('OnboardingDetails - required fields error', () => {
    mount(<App {...props} />);
    cy.getContinueBtn().click();

    cy.getContinueBtn().click();
    cy.get('#birth-date-input').parent().should('have.class', 'Mui-error');
    cy.get('#birth-date-input-helper-text').should('contain.text', 'Campo requerido');
    cy.get('#document-number-input').parent().should('have.class', 'Mui-error');
    cy.get('#document-number-input-helper-text').should('contain.text', 'Campo requerido');
  });

  it('OnboardingDetails - two surnames are required when document number is DNI', () => {
    mount(<App {...props} />);
    cy.getContinueBtn().click();

    cy.get('#birth-date-input').type('1982-11-03');
    cy.get('#document-number-input').type('55593310X');

    cy.getContinueBtn().click();
    cy.get('#second-last-name-input').parent().parent().children('label').should('contain.text', 'Segundo apellido');
    cy.get('#second-last-name-input').parent().should('have.class', 'Mui-error');
    cy.get('#second-last-name-input-helper-text').should('contain.text', 'Campo requerido');
    cy.get('#second-last-name-input').type('Laguna');

    cy.getContinueBtn().click();

    cy.get('#street-address-input').type('Muntaner 90');
    cy.get('#city-input').type('Barcelona');
    cy.get('#postal-code-input').type('08011');

    cy.intercept({ method: 'PATCH', url: '**/api/financial/onboarding/' }, (req) => {
      expect(req.body.op).to.be.equal('processPatchSetAddress');
      expect(req.body.id).to.be.equal('7a2a7d2a-7414-4fba-9ca6-22f63a81d6b5');
      expect(req.body.street_address).to.be.equal('Muntaner 90');
      expect(req.body.city).to.be.equal('Barcelona');
      expect(req.body.country).to.be.equal('ESP');
      expect(req.body.postal_code).to.be.equal('08011');
      req.reply({ statusCode: 204 });
    }).as('patchSetAddress');

    cy.intercept({ method: 'POST', url: '**/v2/order/customer/' }, (req) => {
      expect(req.body.birth_date).to.be.equal('1982-11-03T00:00:00+00:00');
      expect(req.body.document_number).to.be.equal('55593310X');
      expect(req.body.email).to.be.equal('alba.martinez@gmail.com');
      expect(req.body.first_name).to.be.equal('Alba');
      expect(req.body.id).to.be.equal('32fbb8ed-45c6-4b0a-b63b-ca37af2754ef');
      expect(req.body.last_name).to.be.equal('Martinez Laguna');
      expect(req.body.onboarding_details_id).to.be.equal('7a2a7d2a-7414-4fba-9ca6-22f63a81d6b5');
      expect(req.body.phone_number).to.be.equal('+34659723349');
      req.reply({ statusCode: 201 });
    }).as('postCustomerDetailsByLoanId');

    cy.intercept({ method: 'POST', url: `**/loan_application/loan/${orderId}/` }, (req) => {
      req.reply({ statusCode: 201 });
    }).as('postLoanApplications');

    cy.intercept({ method: 'GET', url: `**/v2/order/${orderId}/` }, (req) => {
      req.reply({
        statusCode: 200,
        fixture: 'iberia/fixtures/getOrderByLoanIdRequireOtp.json',
      });
    }).as('getOrderByLoanId');

    cy.getContinueBtn().click();

    cy.wait(['@patchSetAddress', '@postCustomerDetailsByLoanId', '@postLoanApplications', '@getOrderByLoanId']);
  });

  it('OnboardingDetails - one surnames is required when document number is NIE', () => {
    mount(<App {...props} />);
    cy.getContinueBtn().click();

    cy.get('#birth-date-input').type('1982-11-03');
    cy.get('#document-number-input').type('Y7528119A');

    cy.getContinueBtn().click();

    cy.get('#street-address-input').type('Muntaner 90');
    cy.get('#city-input').type('Barcelona');
    cy.get('#postal-code-input').type('08011');

    cy.intercept({ method: 'PATCH', url: '**/api/financial/onboarding/' }, (req) => {
      req.reply({ statusCode: 204 });
    }).as('patchSetAddress');

    cy.intercept({ method: 'POST', url: '**/v2/order/customer/' }, (req) => {
      expect(req.body.birth_date).to.equal('1982-11-03T00:00:00+00:00');
      expect(req.body.document_number).to.equal('Y7528119A');
      expect(req.body.email).to.equal('alba.martinez@gmail.com');
      expect(req.body.first_name).to.equal('Alba');
      expect(req.body.id).to.equal('32fbb8ed-45c6-4b0a-b63b-ca37af2754ef');
      expect(req.body.last_name).to.equal('Martinez');
      expect(req.body.onboarding_details_id).to.equal('7a2a7d2a-7414-4fba-9ca6-22f63a81d6b5');
      expect(req.body.phone_number).to.equal('+34659723349');
      req.reply({ statusCode: 201 });
    }).as('postCustomerDetailsByLoanId');

    cy.intercept({ method: 'POST', url: `**/loan_application/loan/${orderId}/` }, (req) => {
      req.reply({ statusCode: 201 });
    }).as('postLoanApplications');

    cy.intercept({ method: 'GET', url: `**/v2/order/${orderId}/` }, (req) => {
      req.reply({
        statusCode: 200,
        fixture: 'iberia/fixtures/getOrderByLoanIdRequireOtp.json',
      });
    }).as('getOrderByLoanId');

    cy.getContinueBtn().click();

    cy.wait(['@patchSetAddress', '@postCustomerDetailsByLoanId', '@postLoanApplications', '@getOrderByLoanId']);
  });

  it('OnboardingDetails - age must be larger than 18 years', () => {
    mount(<App {...props} />);
    cy.getContinueBtn().click();

    const year = new Date().getFullYear() - 10;
    cy.get('#birth-date-input').clear().type(`${year}-01-01`);
    cy.getContinueBtn().click();
    cy.get('#birth-date-input').parent().should('have.class', 'Mui-error');
    cy.get('#birth-date-input-helper-text').should('contain.text', 'La edad no puede ser inferior a 18 años');
  });

  it('OnboardingDetails - age between 16 and 18 years is allowed in dev/stg', () => {
    mount(<App {...props} />);
    cy.getContinueBtn().click();

    const year = new Date().getFullYear() - 17;
    cy.get('#birth-date-input').clear().type(`${year}-01-01`);
    cy.getContinueBtn().click();
    cy.get('#birth-date-input').parent().should('not.have.class', 'Mui-error');
  });

  it('OnboardingDetails - age between 16 and 18 years is not allowed in prod', () => {
    mount(<App {...{ ...props, env: Environment.PROD }} />);
    cy.getContinueBtn().click();

    const year = new Date().getFullYear() - 17;
    cy.get('#birth-date-input').clear().type(`${year}-01-01`);
    cy.getContinueBtn().click();
    cy.get('#birth-date-input').parent().should('have.class', 'Mui-error');
    cy.get('#birth-date-input-helper-text').should('contain.text', 'La edad no puede ser inferior a 18 años');
  });

  it('OnboardingDetails - keep address values when going back', () => {
    mount(<App {...props} />);
    cy.getContinueBtn().click();

    cy.get('#birth-date-input').type('1989-01-23');
    cy.get('#document-number-input').type('Y7528119A');

    cy.getContinueBtn().click();

    cy.get('#street-address-input').type('Muntaner');
    cy.get('#city-input').type('Barcelona');
    cy.get('#postal-code-input').type('08011');

    cy.getBackBtn().should('exist').click();

    cy.get('#birth-date-input').should('have.value', '1989-01-23');
    cy.get('#document-number-input').should('have.value', 'Y7528119A');

    cy.getContinueBtn().click();

    cy.get('#street-address-input').should('have.value', 'Muntaner');
    cy.get('#city-input').should('have.value', 'Barcelona');
    cy.get('#postal-code-input').should('have.value', '08011');

    cy.get('#street-address-input').type(' 90');

    cy.getBackBtn().should('exist').click();

    cy.getContinueBtn().click();

    cy.get('#street-address-input').should('have.value', 'Muntaner 90');
    cy.get('#street-address-input').parent().should('not.have.class', 'Mui-error');
  });

  it('OnboardingDetails - street address error', () => {
    mount(<App {...props} />);
    cy.getContinueBtn().click();

    cy.get('#birth-date-input').type('1989-01-23');
    cy.get('#document-number-input').type('Y7528119A');

    cy.getContinueBtn().click();

    cy.get('#street-address-input').type('Muntaner');
    cy.get('#city-input').type('Barcelona');
    cy.get('#postal-code-input').type('08011');

    cy.intercept({ method: 'PATCH', url: '**/api/financial/onboarding/' }, (req) => {
      expect(req.body.op).to.be.equal('processPatchSetAddress');
      expect(req.body.id).to.be.equal('7a2a7d2a-7414-4fba-9ca6-22f63a81d6b5');
      expect(req.body.street_address).to.be.equal('Muntaner');
      expect(req.body.city).to.be.equal('Barcelona');
      expect(req.body.country).to.be.equal('ESP');
      expect(req.body.postal_code).to.be.equal('08011');
      req.reply({ statusCode: 204 });
    }).as('patchSetAddress');

    cy.intercept({ method: 'POST', url: '**/v2/order/customer/' }, (req) => {
      expect(req.body.birth_date).to.be.equal('1989-01-23T00:00:00+00:00');
      expect(req.body.document_number).to.be.equal('Y7528119A');
      expect(req.body.email).to.be.equal('alba.martinez@gmail.com');
      expect(req.body.first_name).to.be.equal('Alba');
      expect(req.body.id).to.be.equal('32fbb8ed-45c6-4b0a-b63b-ca37af2754ef');
      expect(req.body.last_name).to.be.equal('Martinez');
      expect(req.body.onboarding_details_id).to.be.equal('7a2a7d2a-7414-4fba-9ca6-22f63a81d6b5');
      expect(req.body.phone_number).to.be.equal('+34659723349');
      req.reply({ statusCode: 201, delay: 400 });
    }).as('postCustomerDetailsByLoanId');

    cy.intercept({ method: 'POST', url: `**/loan_application/loan/${orderId}/` }, (req) => {
      req.reply({ statusCode: 201 });
    }).as('postLoanApplications');

    cy.intercept({ method: 'GET', url: `**/v2/order/${orderId}/` }, (req) => {
      req.reply({
        statusCode: 200,
        delay: 400,
        fixture: 'iberia/fixtures/getOrderByLoanIdError.json',
      });
    }).as('getOrderByLoanId');

    cy.intercept({ method: 'GET', url: `**/loan_application/${loanApplicationId}/error/` }, (req) => {
      req.reply({
        statusCode: 200,
        delay: 400,
        fixture: 'iberia/fixtures/getProcessResponseByLoanApplicationIdError.json',
      });
    }).as('getProcessResponseByLoanApplicationIdError');

    cy.getContinueBtn().click();

    cy.get('#loading-btn').should('exist');
    cy.get('#street-address-input').parent().should('have.class', 'Mui-disabled');
    cy.get('#city-input').parent().should('have.class', 'Mui-disabled');
    cy.get('#postal-code-input').parent().should('have.class', 'Mui-disabled');

    cy.wait([
      '@patchSetAddress',
      '@postCustomerDetailsByLoanId',
      '@postLoanApplications',
      '@getOrderByLoanId',
      '@getProcessResponseByLoanApplicationIdError',
    ]);

    cy.get('#loading-btn').should('not.exist');
    cy.getContinueBtn().should('not.have.class', 'Mui-disabled');
    cy.getBackBtn().should('not.exist');

    cy.get('#street-address-input').should('have.value', 'Muntaner');
    cy.get('#city-input').should('have.value', 'Barcelona');
    cy.get('#postal-code-input').should('have.value', '08011');

    cy.get('#street-address-input').parent().should('have.class', 'Mui-error');
    cy.get('#street-address-input-helper-text').should('contain.text', 'Por favor, revisa este campo');

    cy.get('#street-address-input').type(' 90');
    cy.get('#street-address-input').parent().should('not.have.class', 'Mui-error');

    cy.intercept({ method: 'PATCH', url: '**/api/financial/onboarding/' }, (req) => {
      expect(req.body.op).to.be.equal('processPatchSetAddress');
      expect(req.body.id).to.be.equal('7a2a7d2a-7414-4fba-9ca6-22f63a81d6b5');
      expect(req.body.street_address).to.be.equal('Muntaner 90');
      expect(req.body.city).to.be.equal('Barcelona');
      expect(req.body.country).to.be.equal('ESP');
      expect(req.body.postal_code).to.be.equal('08011');
      req.reply({ statusCode: 204 });
    }).as('patchSetAddressFixed');

    cy.intercept({ method: 'POST', url: '**/lender/process_loan_application' }, (req) => {
      req.reply({ statusCode: 201, delay: 400 });
    }).as('postLoanApplicationInLender');

    cy.intercept({ method: 'GET', url: `**/v2/order/${orderId}/` }, (req) => {
      req.reply({
        statusCode: 200,
        delay: 400,
        fixture: 'iberia/fixtures/getOrderByLoanIdRequireOtp.json',
      });
    }).as('getOrderByLoanId');

    cy.getContinueBtn().click();

    cy.wait([
      '@patchSetAddressFixed',
      '@postCustomerDetailsByLoanId',
      '@postLoanApplicationInLender',
      '@getOrderByLoanId',
    ]);

    cy.contains('Verifica tu teléfono móvil');
  });

  it('OnboardingDetails - street address and phone number error', () => {
    mount(<App {...props} />);
    cy.getContinueBtn().click();

    cy.get('#birth-date-input').type('1989-01-23');
    cy.get('#document-number-input').type('Y7528119A');

    cy.getContinueBtn().click();

    cy.get('#street-address-input').type('Muntaner');
    cy.get('#city-input').type('Barcelona');
    cy.get('#postal-code-input').type('08011');

    cy.intercept({ method: 'PATCH', url: '**/api/financial/onboarding/' }, (req) => {
      expect(req.body.op).to.be.equal('processPatchSetAddress');
      expect(req.body.id).to.be.equal('7a2a7d2a-7414-4fba-9ca6-22f63a81d6b5');
      expect(req.body.street_address).to.be.equal('Muntaner');
      expect(req.body.city).to.be.equal('Barcelona');
      expect(req.body.country).to.be.equal('ESP');
      expect(req.body.postal_code).to.be.equal('08011');
      req.reply({ statusCode: 204 });
    }).as('patchSetAddress');

    cy.intercept({ method: 'POST', url: '**/v2/order/customer/' }, (req) => {
      expect(req.body.birth_date).to.be.equal('1989-01-23T00:00:00+00:00');
      expect(req.body.document_number).to.be.equal('Y7528119A');
      expect(req.body.email).to.be.equal('alba.martinez@gmail.com');
      expect(req.body.first_name).to.be.equal('Alba');
      expect(req.body.id).to.be.equal('32fbb8ed-45c6-4b0a-b63b-ca37af2754ef');
      expect(req.body.last_name).to.be.equal('Martinez');
      expect(req.body.onboarding_details_id).to.be.equal('7a2a7d2a-7414-4fba-9ca6-22f63a81d6b5');
      expect(req.body.phone_number).to.be.equal('+34659723349');
      req.reply({ statusCode: 201, delay: 400 });
    }).as('postCustomerDetailsByLoanId');

    cy.intercept({ method: 'POST', url: `**/loan_application/loan/${orderId}/` }, (req) => {
      req.reply({ statusCode: 201 });
    }).as('postLoanApplications');

    cy.intercept({ method: 'GET', url: `**/v2/order/${orderId}/` }, (req) => {
      req.reply({
        statusCode: 200,
        delay: 400,
        fixture: 'iberia/fixtures/getOrderByLoanIdError.json',
      });
    }).as('getOrderByLoanIdError');

    cy.intercept({ method: 'GET', url: `**/loan_application/${loanApplicationId}/error/` }, (req) => {
      req.reply({
        statusCode: 200,
        delay: 400,
        fixture: 'iberia/fixtures/getProcessResponseByLoanApplicationIdErrors.json',
      });
    }).as('getProcessResponseByLoanApplicationIdErrors');

    cy.getContinueBtn().click();

    cy.get('#loading-btn').should('exist');
    cy.get('#street-address-input').parent().should('have.class', 'Mui-disabled');
    cy.get('#city-input').parent().should('have.class', 'Mui-disabled');
    cy.get('#postal-code-input').parent().should('have.class', 'Mui-disabled');

    cy.wait([
      '@patchSetAddress',
      '@postCustomerDetailsByLoanId',
      '@postLoanApplications',
      '@getOrderByLoanIdError',
      '@getProcessResponseByLoanApplicationIdErrors',
    ]);

    cy.get('#birth-date-input').should('have.value', '1989-01-23');
    cy.get('#document-number-input').should('have.value', 'Y7528119A');
    cy.get('#phone-number-input').should('have.value', '659723349');

    cy.get('#phone-number-input').parent().should('have.class', 'Mui-error');
    cy.get('#phone-number-input-helper-text').should('contain.text', 'Por favor, revisa este campo');

    cy.get('#phone-number-input').clear().type('659723348');

    cy.get('#loading-btn').should('not.exist');
    cy.getBackBtn().should('not.exist');
    cy.getContinueBtn().should('not.have.class', 'Mui-disabled');

    cy.getContinueBtn().click();

    cy.get('#street-address-input').should('have.value', 'Muntaner');
    cy.get('#city-input').should('have.value', 'Barcelona');
    cy.get('#postal-code-input').should('have.value', '08011');

    cy.get('#street-address-input').parent().should('have.class', 'Mui-error');
    cy.get('#street-address-input-helper-text').should('contain.text', 'Por favor, revisa este campo');

    cy.get('#street-address-input').type(' 90');
    cy.get('#street-address-input').parent().should('not.have.class', 'Mui-error');

    cy.getBackBtn().should('exist').click();

    cy.get('#phone-number-input').should('have.value', '659723348');
    cy.get('#phone-number-input').parent().should('have.class', 'Mui-error');
    cy.get('#phone-number-input-helper-text').should('contain.text', 'Por favor, revisa este campo');

    cy.getContinueBtn().click();

    cy.get('#street-address-input').should('have.value', 'Muntaner 90');
    cy.get('#street-address-input').parent().should('have.class', 'Mui-error');

    cy.intercept({ method: 'PATCH', url: '**/api/financial/onboarding/' }, (req) => {
      expect(req.body.op).to.be.equal('processPatchSetAddress');
      expect(req.body.id).to.be.equal('7a2a7d2a-7414-4fba-9ca6-22f63a81d6b5');
      expect(req.body.street_address).to.be.equal('Muntaner 90');
      expect(req.body.city).to.be.equal('Barcelona');
      expect(req.body.country).to.be.equal('ESP');
      expect(req.body.postal_code).to.be.equal('08011');
      req.reply({ statusCode: 204 });
    }).as('patchSetAddressFixed');

    cy.intercept({ method: 'POST', url: '**/v2/order/customer/' }, (req) => {
      expect(req.body.birth_date).to.be.equal('1989-01-23T00:00:00+00:00');
      expect(req.body.document_number).to.be.equal('Y7528119A');
      expect(req.body.email).to.be.equal('alba.martinez@gmail.com');
      expect(req.body.first_name).to.be.equal('Alba');
      expect(req.body.id).to.be.equal('32fbb8ed-45c6-4b0a-b63b-ca37af2754ef');
      expect(req.body.last_name).to.be.equal('Martinez');
      expect(req.body.onboarding_details_id).to.be.equal('7a2a7d2a-7414-4fba-9ca6-22f63a81d6b5');
      expect(req.body.phone_number).to.be.equal('+34659723348');
      req.reply({ statusCode: 201, delay: 400 });
    }).as('postCustomerDetailsByLoanIdFixed');

    cy.intercept({ method: 'POST', url: '**/lender/process_loan_application' }, (req) => {
      req.reply({ statusCode: 201, delay: 400 });
    }).as('postLoanApplicationInLender');

    cy.intercept({ method: 'GET', url: `**/v2/order/${orderId}/` }, (req) => {
      req.reply({
        statusCode: 200,
        delay: 400,
        fixture: 'iberia/fixtures/getOrderByLoanIdRequireOtp.json',
      });
    }).as('getOrderByLoanId');

    cy.getContinueBtn().click();

    cy.wait([
      '@patchSetAddressFixed',
      '@postCustomerDetailsByLoanIdFixed',
      '@postLoanApplicationInLender',
      '@getOrderByLoanId',
    ]);

    cy.contains('Verifica tu teléfono móvil');
  });

  it('OnboardingDetails - sends postCustomerDetailsByLoanId with expected request body', () => {
    mount(<App {...props} />);
    cy.getContinueBtn().click();

    cy.get('#birth-date-input').type('1989-01-23');
    cy.get('#document-number-input').type('Y7528119A');

    cy.getContinueBtn().click();

    cy.get('#street-address-input').type('Muntaner 90');
    cy.get('#city-input').type('Barcelona');
    cy.get('#postal-code-input').type('08011');

    cy.intercept({ method: 'PATCH', url: '**/api/financial/onboarding/' }, (req) => {
      req.reply({ statusCode: 204 });
    }).as('patchSetAddress');

    cy.intercept({ method: 'POST', url: '**/v2/order/customer/' }, (req) => {
      expect(req.body.birth_date).to.be.equal('1989-01-23T00:00:00+00:00');
      expect(req.body.document_number).to.be.equal('Y7528119A');
      expect(req.body.email).to.be.equal('alba.martinez@gmail.com');
      expect(req.body.first_name).to.be.equal('Alba');
      expect(req.body.id).to.be.equal('32fbb8ed-45c6-4b0a-b63b-ca37af2754ef');
      expect(req.body.last_name).to.be.equal('Martinez');
      expect(req.body.onboarding_details_id).to.be.equal('7a2a7d2a-7414-4fba-9ca6-22f63a81d6b5');
      expect(req.body.phone_number).to.be.equal('+34659723349');
      req.reply({ statusCode: 201 });
    }).as('postCustomerDetailsByLoanId');

    cy.intercept({ method: 'POST', url: `**/loan_application/loan/${orderId}/` }, (req) => {
      req.reply({ statusCode: 201 });
    }).as('postLoanApplications');

    cy.intercept({ method: 'GET', url: `**/v2/order/${orderId}/` }, (req) => {
      req.reply({
        statusCode: 200,
        fixture: 'iberia/fixtures/getOrderByLoanIdRequireOtp.json',
      });
    }).as('getOrderByLoanId');

    cy.getContinueBtn().click();

    cy.wait(['@patchSetAddress', '@postCustomerDetailsByLoanId', '@postLoanApplications', '@getOrderByLoanId']).wait(
      200,
    );
  });

  it('PhoneVerification - display', () => {
    mount(<App {...props} />);
    cy.navigateToOtp(orderId);

    cy.contains('Verifica tu teléfono móvil');
    cy.contains('Introduce el código de cinco dígitos que hemos enviado a tu teléfono móvil.');
    cy.contains('+34 659723349');
    cy.contains('Por seguridad, este código caducará en 10 minutos.');
    cy.contains('¿No has recibido el SMS?');
    cy.get('button').contains('Volver a enviar');
    cy.getContinueBtn();
    cy.get('button').contains('Editar número de teléfono').should('not.exist');
  });

  it('PhoneVerification - no validation code introduced error', () => {
    mount(<App {...props} />);
    cy.navigateToOtp(orderId);

    cy.getContinueBtn().click();
    cy.get('#validation-code-input').parent().should('have.class', 'Mui-error');
    cy.get('#validation-code-input-helper-text').should('contain.text', 'Campo requerido');
  });

  it('PhoneVerification - resend new otp', () => {
    mount(<App {...props} />);
    cy.navigateToOtp(orderId);

    cy.intercept({ method: 'PATCH', url: '**/financial/loan_application/' }, (req) => {
      req.reply({ statusCode: 204 });
    });

    cy.get('button').contains('Volver a enviar').click();
    cy.contains('SMS enviado');
    cy.contains('Volver a enviar').should('not.exist');
  });

  it('PhoneVerification - no resend otp when all codes are sent', () => {
    mount(<App {...props} />);
    cy.navigateToOtpFullCodes(orderId);

    cy.intercept({ method: 'PATCH', url: '**/financial/loan_application/' }, (req) => {
      req.reply({ statusCode: 204 });
    });

    cy.contains('Volver a enviar').should('not.exist');
    cy.contains('Por favor, introduce alguno de los códigos enviados.');
  });

  it('Payment - display', () => {
    mount(<App {...props} />);
    cy.navigateToPayment(orderId);

    cy.contains('Resumen');
    cy.contains('Importe de la compra');
    cy.get('#principal-amount-label').contains('Importe de la compra');
    cy.get('#principal-amount-value').contains(replaceNbsp('224,41 €'));
    cy.get('#total-instalment-fee-amount-label').contains(`Coste total (${replaceNbsp('2,50 €')} por cuota)`);
    cy.get('#total-instalment-fee-amount-value').contains(replaceNbsp('30,00 €'));
    cy.get('#total-amount-label').contains('Total a pagar');
    cy.get('#total-amount-value').contains(replaceNbsp('254,41 €'));
    cy.get('#tin-tae').contains('TIN');
    cy.get('#tin-tae').contains(replaceNbsp('0,00 %'));
    cy.get('#tin-tae').contains('TAE');
    cy.get('#tin-tae').contains(replaceNbsp('32,43 %'));
    cy.get('span').contains('+info').should('not.exist');

    cy.contains('Servicio ofrecido por');
    cy.contains('Entiendo y acepto la política de privacidad.');
    cy.contains(
      'Al continuar, confirmo que me han hecho y acepto la oferta de crédito, que he leído y acepto las condiciones de contratación y que los datos que he dado a la tienda se usarán para finalizar esta solicitud.',
    );

    cy.getContinueBtn().should('have.class', 'Mui-disabled');
    cy.get('input[type="checkbox"]').check();
    cy.getContinueBtn().should('not.have.class', 'Mui-disabled');
    cy.getContinueBtn().click();

    cy.wait(1000);

    cy.contains('Introduce los datos de tu tarjeta para realizar el pago');
    cy.contains('Servicio ofrecido por');

    cy.get('button').filter(`:contains("Pagar 21,20\u00a0€")`).should('have.class', 'Mui-disabled');
  });

  it('Payment - displays quix logo below 1200 eur', () => {
    mount(<App {...props} />);
    cy.navigateToPayment(orderId);

    cy.get('img[id="lender-logo"]')
      .should('have.attr', 'src')
      .should('include', 'https://cdn.nemuru.com/assets/logos/quix-by-sequra-logo.svg');
  });

  it('Payment - interaction with card form', () => {
    mount(<App {...props} />);
    cy.navigateToPayment(orderId);
    cy.get('input[type="checkbox"]').check();
    cy.getContinueBtn().click();
    cy.wait(1000);

    cy.get('button').filter(`:contains("Pagar 21,20\u00a0€")`).as('payButton');

    cy.get('@payButton').should('have.class', 'Mui-disabled');

    // // The following only works in Google Chrome
    //
    // cy.get('iframe[id="mufasa-iframe"]').then(($iframe) => {
    //   const $body = $iframe.contents().find('body');
    //   cy.wrap($body).contains('Número de tarjeta');
    //   cy.wrap($body).contains('Caduca');
    //   cy.wrap($body).contains('CVV/CVC');
    //   cy.wrap($body).find('input[id="cc-number"]').type('4716773077339777');
    //   cy.wrap($body).find('input[id="cc-exp"]').type('1230');
    //   cy.wrap($body).find('input[id="cc-csc"]').type('123');
    // });
    //
    // cy.get('@payButton').should('not.have.class', 'Mui-disabled');
  });

  it('Denied - display', () => {
    mount(<App {...props} />);
    cy.navigateToDenied(orderId);

    cy.contains('Lo sentimos');
    cy.contains('No hemos podido tramitar la operación');
    cy.contains('Elige otra opción');
    cy.contains('Recuerda que puedes elegir otro método de pago para finalizar tu compra');

    cy.get('button').contains('Cerrar');
  });
  //
  it('Denied - click main button fires onClose with status: denied', () => {
    let statusOnClose: string | undefined = undefined;
    const onClose = (status: string) => (statusOnClose = status);

    mount(<App {...props} handleCloseDialog={onClose} />);
    cy.navigateToDenied(orderId);

    cy.get('button')
      .contains('Cerrar')
      .click()
      .then(() => {
        expect(statusOnClose).to.be.equal('denied');
      });
  });

  it('Denied - click close button fires onClose with status: denied', () => {
    let statusOnClose: string | undefined = undefined;
    const onClose = (status: string) => (statusOnClose = status);

    mount(<App {...props} handleCloseDialog={onClose} />);
    cy.navigateToDenied(orderId);

    cy.get('#close-btn')
      .click()
      .then(() => {
        expect(statusOnClose).to.be.equal('denied');
      });
  });

  it('Expired - displays', () => {
    mount(<App {...props} />);
    cy.navigateToExpired(orderId);

    cy.contains('Tu pedido ha caducado');
    cy.contains('El tiempo límite para finalizar la compra ha terminado.');
    cy.get('button').filter(':contains("Cerrar")').should('not.have.class', 'Mui-disabled').should('not.be.disabled');
  });

  it('Expired - click main button fires onClose with status: expired', () => {
    let statusOnClose: string | undefined = undefined;
    const onClose = (status: string) => (statusOnClose = status);

    mount(<App {...props} handleCloseDialog={onClose} />);
    cy.navigateToExpired(orderId);

    cy.get('button')
      .filter(':contains("Cerrar")')
      .click()
      .then(() => {
        expect(statusOnClose).to.be.equal('expired');
      });
  });

  it('Expired - click close button fires onClose with status: expired', () => {
    let statusOnClose: string | undefined = undefined;
    const onClose = (status: string) => (statusOnClose = status);

    mount(<App {...props} handleCloseDialog={onClose} />);
    cy.navigateToExpired(orderId);

    cy.get('#close-btn')
      .click()
      .then(() => {
        expect(statusOnClose).to.be.equal('expired');
      });
  });

  it('LeaveProcess - display', () => {
    mount(<App {...props} />);
    cy.get('#close-btn').click();

    cy.contains('Antes de marchar...');
    cy.contains('Lamentamos que te vayas. ¿Podrías indicarnos la razón?');
    cy.contains('No entiendo cómo se cobrarán los pagos futuros');
    cy.contains('No me interesa este método de pago');
    cy.contains('El coste de la financiación es elevado');

    cy.get('button').filter(`:contains("Responder y cerrar")`).should('have.class', 'Mui-disabled');
    cy.get('button').filter(`:contains("Responder y cerrar")`).should('be.disabled');
    cy.get('button').filter(`:contains("Volver al proceso")`);

    cy.get('#desist-reasons-toggle')
      .children()
      .each(($el) => {
        cy.wrap($el).click().should('have.class', 'Mui-selected');
      });

    cy.get('button')
      .filter(`:contains("Responder y cerrar")`)
      .should('not.be.disabled')
      .should('not.have.class', 'Mui-disabled');
  });

  it('LeaveProcess - sends patchChangeLoanStatus with expected request body', () => {
    mount(<App {...props} />);

    cy.get('#close-btn').click();

    cy.intercept({ method: 'PATCH', url: '**/api/financial/loan/' }, (req) => {
      expect(req.body.additional).to.equal('nemuru_is_expensive');
      expect(req.body.id).to.equal(orderId);
      expect(req.body.op).to.equal('processPatchChangeLoanStatus');
      expect(req.body.status).to.equal('status_desisted');
      req.reply({ statusCode: 204 });
    }).as('patchChangeLoanStatus');

    cy.get('button').filter(`:contains("El coste de la financiación es elevado")`).click();

    cy.get('button').filter(`:contains("Responder y cerrar")`).click();

    cy.wait('@patchChangeLoanStatus');
  });

  it('LeaveProcess - click main button fires onClose with status: desisted', () => {
    let statusOnClose: string | undefined = undefined;
    const onClose = (status: string) => (statusOnClose = status);

    mount(<App {...props} handleCloseDialog={onClose} />);

    cy.get('#close-btn').click();

    cy.intercept({ method: 'PATCH', url: '**/api/financial/loan/' }, (req) => {
      req.reply({ statusCode: 204 });
    });

    cy.get('button').filter(`:contains("El coste de la financiación es elevado")`).click();

    cy.get('button').filter(`:contains("Responder y cerrar")`).click();

    cy.wait(2500).then(() => {
      expect(statusOnClose).to.be.equal('desisted');
    });
  });

  it('Desisted - displays', () => {
    mount(<App {...props} />);
    cy.navigateToDesisted(orderId);

    cy.contains('Tu pedido se ha cancelado');
    cy.get('button').filter(':contains("Cerrar")').should('not.have.class', 'Mui-disabled').should('not.be.disabled');
  });

  it('Desisted - click main button fires onClose with status: desisted', () => {
    let statusOnClose: string | undefined = undefined;
    const onClose = (status: string) => (statusOnClose = status);

    mount(<App {...props} handleCloseDialog={onClose} />);
    cy.navigateToDesisted(orderId);

    cy.get('button')
      .filter(':contains("Cerrar")')
      .click()
      .then(() => {
        expect(statusOnClose).to.be.equal('desisted');
      });
  });

  it('Desisted - click close button fires onClose with status: desisted', () => {
    let statusOnClose: string | undefined = undefined;
    const onClose = (status: string) => (statusOnClose = status);

    mount(<App {...props} handleCloseDialog={onClose} />);
    cy.navigateToDesisted(orderId);

    cy.get('#close-btn')
      .click()
      .then(() => {
        expect(statusOnClose).to.be.equal('desisted');
      });
  });
});
