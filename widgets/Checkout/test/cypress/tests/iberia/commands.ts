// @ts-ignore
Cypress.Commands.add('getContinueBtn', () => {
  return cy.get(`@continueBtn`).then((btn) => cy.get('button').filter(`:contains(${btn})`));
});

// @ts-ignore
Cypress.Commands.add('getBackBtn', () => {
  return cy.get('.back-button');
});

Cypress.Commands.add('navigateToOtp', (orderId: string) => {
  cy.getContinueBtn().click();

  cy.get('#birth-date-input').type('1982-11-03');
  cy.get('#document-number-input').type('Y7528119A');

  cy.getContinueBtn().click();

  cy.get('#street-address-input').type('Muntaner 90');
  cy.get('#city-input').type('Barcelona');
  cy.get('#postal-code-input').type('08011');

  cy.intercept({ method: 'PATCH', url: '**/api/financial/onboarding/' }, (req) => {
    req.reply({ statusCode: 204 });
  }).as('patchSetAddress');

  cy.intercept({ method: 'POST', url: '**/v2/order/customer/' }, (req) => {
    req.reply({ statusCode: 201 });
  }).as('postCustomerDetailsByLoanId');

  cy.intercept({ method: 'POST', url: `**/loan_application/loan/${orderId}/` }, (req) => {
    req.reply({ statusCode: 201 });
  }).as('postLoanApplications');

  cy.intercept({ method: 'GET', url: `**/v2/order/${orderId}/` }, (req) => {
    req.reply({
      statusCode: 200,
      fixture: 'iberia/fixtures/getOrderByLoanIdRequireOtp.json',
    });
  }).as('getOrderByLoanId');

  cy.getContinueBtn().click();

  cy.wait(['@patchSetAddress', '@postCustomerDetailsByLoanId', '@postLoanApplications', '@getOrderByLoanId']).wait(200);
});

Cypress.Commands.add('navigateToOtpFullCodes', (orderId: string) => {
  cy.getContinueBtn().click();

  cy.get('#birth-date-input').type('1982-11-03');
  cy.get('#document-number-input').type('Y7528119A');

  cy.getContinueBtn().click();

  cy.get('#street-address-input').type('Muntaner 90');
  cy.get('#city-input').type('Barcelona');
  cy.get('#postal-code-input').type('08011');

  cy.intercept({ method: 'PATCH', url: '**/api/financial/onboarding/' }, (req) => {
    req.reply({ statusCode: 204 });
  }).as('patchSetAddress');

  cy.intercept({ method: 'POST', url: '**/v2/order/customer/' }, (req) => {
    req.reply({ statusCode: 201 });
  }).as('postCustomerDetailsByLoanId');

  cy.intercept({ method: 'POST', url: `**/loan_application/loan/${orderId}/` }, (req) => {
    req.reply({ statusCode: 201 });
  }).as('postLoanApplications');

  cy.intercept({ method: 'GET', url: `**/v2/order/${orderId}/` }, (req) => {
    req.reply({
      statusCode: 200,
      fixture: 'iberia/fixtures/getOrderByLoanIdRequireOtpFullCodes.json',
    });
  }).as('getOrderByLoanId');

  cy.getContinueBtn().click();

  cy.wait(['@patchSetAddress', '@postCustomerDetailsByLoanId', '@postLoanApplications', '@getOrderByLoanId']).wait(200);
});

Cypress.Commands.add('navigateToPayment', (orderId: string) => {
  cy.navigateToOtp(orderId);

  cy.get('input').type('23349');

  cy.intercept({ method: 'PATCH', url: '**/api/compliance/otp/' }, (req) => {
    req.reply({ statusCode: 204 });
  }).as('patchSetLastCode');

  cy.intercept({ method: 'POST', url: '**/api/financial/lender/process_loan_application' }, (req) => {
    req.reply({ statusCode: 201 });
  }).as('postLoanApplicationInLender');

  cy.intercept({ method: 'GET', url: `**/v2/order/${orderId}/` }, (req) => {
    req.reply({
      statusCode: 200,
      fixture: 'iberia/fixtures/getOrderByLoanIdRequirePayment.json',
    });
  }).as('getOrderByLoanId');

  cy.intercept({ method: 'GET', url: `**/financial/loan_application/payment/${orderId}/` }, (req) => {
    req.reply({
      statusCode: 200,
      fixture: 'iberia/fixtures/getPaymentUrlByLoanId.json',
    });
  }).as('getPaymentUrlByLoanId');

  cy.intercept({ method: 'GET', url: `**/financial/loan_application/signature/${orderId}/` }, (req) => {
    req.reply({
      statusCode: 200,
      fixture: 'iberia/fixtures/getSignatureUrlsByLoanId.json',
    });
  }).as('getSignatureUrlsByLoanId');

  cy.getContinueBtn().click();

  cy.wait([
    '@patchSetLastCode',
    '@postLoanApplicationInLender',
    '@getOrderByLoanId',
    '@getPaymentUrlByLoanId',
    '@getSignatureUrlsByLoanId',
  ]).wait(200);
});

Cypress.Commands.add('navigateToDenied', (orderId: string) => {
  cy.navigateToOtp(orderId);

  cy.get('input').type('23349');

  cy.intercept({ method: 'PATCH', url: '**/api/compliance/otp/' }, (req) => {
    req.reply({ statusCode: 204 });
  }).as('patchSetLastCode');

  cy.intercept({ method: 'POST', url: '**/api/financial/lender/process_loan_application' }, (req) => {
    req.reply({ statusCode: 201 });
  }).as('postLoanApplicationInLender');

  cy.intercept({ method: 'GET', url: `**/v2/order/${orderId}/` }, (req) => {
    req.reply({
      statusCode: 200,
      fixture: 'iberia/fixtures/getOrderByLoanIdDenied.json',
    });
  }).as('getOrderByLoanId');

  cy.getContinueBtn().click();

  cy.wait(['@patchSetLastCode', '@postLoanApplicationInLender', '@getOrderByLoanId']).wait(200);
});

Cypress.Commands.add('navigateToExpired', (orderId: string) => {
  cy.getContinueBtn().click();

  cy.get('#birth-date-input').type('1982-11-03');
  cy.get('#document-number-input').type('Y7528119A');

  cy.getContinueBtn().click();

  cy.get('#street-address-input').type('Muntaner 90');
  cy.get('#city-input').type('Barcelona');
  cy.get('#postal-code-input').type('08011');

  cy.intercept({ method: 'PATCH', url: '**/api/financial/onboarding/' }, (req) => {
    req.reply({ statusCode: 204 });
  }).as('patchSetAddress');

  cy.intercept({ method: 'POST', url: '**/v2/order/customer/' }, (req) => {
    req.reply({ statusCode: 201 });
  }).as('postCustomerDetailsByLoanId');

  cy.intercept({ method: 'POST', url: `**/loan_application/loan/${orderId}/` }, (req) => {
    req.reply({ statusCode: 201 });
  }).as('postLoanApplications');

  cy.intercept({ method: 'GET', url: `**/v2/order/${orderId}/` }, (req) => {
    req.reply({
      statusCode: 200,
      fixture: 'iberia/fixtures/getOrderByLoanIdExpired.json',
    });
  }).as('getOrderByLoanId');

  cy.getContinueBtn().click();

  cy.wait(['@patchSetAddress', '@postCustomerDetailsByLoanId', '@postLoanApplications', '@getOrderByLoanId']).wait(200);
});

Cypress.Commands.add('navigateToDesisted', (orderId: string) => {
  cy.getContinueBtn().click();

  cy.get('#birth-date-input').type('1982-11-03');
  cy.get('#document-number-input').type('Y7528119A');

  cy.getContinueBtn().click();

  cy.get('#street-address-input').type('Muntaner 90');
  cy.get('#city-input').type('Barcelona');
  cy.get('#postal-code-input').type('08011');

  cy.intercept({ method: 'PATCH', url: '**/api/financial/onboarding/' }, (req) => {
    req.reply({ statusCode: 204 });
  }).as('patchSetAddress');

  cy.intercept({ method: 'POST', url: '**/v2/order/customer/' }, (req) => {
    req.reply({ statusCode: 201 });
  }).as('postCustomerDetailsByLoanId');

  cy.intercept({ method: 'POST', url: `**/loan_application/loan/${orderId}/` }, (req) => {
    req.reply({ statusCode: 201 });
  }).as('postLoanApplications');

  cy.intercept({ method: 'GET', url: `**/v2/order/${orderId}/` }, (req) => {
    req.reply({
      statusCode: 200,
      fixture: 'iberia/fixtures/getOrderByLoanIdDesisted.json',
    });
  }).as('getOrderByLoanId');

  cy.getContinueBtn().click();

  cy.wait(['@patchSetAddress', '@postCustomerDetailsByLoanId', '@postLoanApplications', '@getOrderByLoanId']).wait(200);
});

declare namespace Cypress {
  interface Chainable {
    getContinueBtn(): Chainable<void>;
    getBackBtn(): Chainable<void>;
    navigateToOtp(orderId: string): Chainable<void>;
    navigateToOtpFullCodes(orderId: string): Chainable<void>;
    navigateToPayment(orderId: string): Chainable<void>;
    navigateToDenied(orderId: string): Chainable<void>;
    navigateToExpired(orderId: string): Chainable<void>;
    navigateToDesisted(orderId: string): Chainable<void>;
  }
}
