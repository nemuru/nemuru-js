import {
  AmortizationPlanDto,
  Configuration,
  Credentials,
  getAmortizationPlans,
  getLoanCalculators,
  getOrderByLoanId,
  getPricingsByAgentId,
  LoanCalculatorDto,
  Locale,
  Money,
  Pricing,
  Product,
  proposalsConstructor,
  setCredentials,
  setEnvironment,
} from '@nemuru-eng/js-api-client';

import { CheckoutComponentProps } from '../../../../../../src/checkout/Checkout';
import { ErrorParams } from '../../../../../../src/shared/types/shared/ErrorParams';
import Branding from '../../../../../../src/shared/types/Style/Branding';
import Color from '../../../../../../src/shared/types/Style/Color';
import Style from '../../../../../../src/shared/types/Style/Style';
import VariantCheckout from '../../../../../../src/shared/types/Style/VariantCheckout';
import dictionary_esES from '../../../../src/translations/default/dictionary_es-ES';
import { setTranslation, translation } from '../../../../src/translations/Translation';

import postOrder from '../fixtures/postOrder.json';
import { AllowedBrandings, AllowedEnvironments, variables } from './variables';

export class TestHelper {
  private readonly username: string;
  private readonly password: string;
  private readonly style: Style;
  private readonly configuration: Configuration;
  private credentials?: Credentials;

  constructor(
    readonly env: AllowedEnvironments,
    readonly branding: AllowedBrandings,
    readonly locale: Locale,
    configuration?: Configuration,
  ) {
    const envVariables = variables[env];

    this.username = envVariables[branding].username;
    this.password = envVariables[branding].password;

    if (!this.username) this.username = envVariables.finanpay.username;
    if (!this.password) this.password = envVariables.finanpay.password;

    this.style = new Style(
      new Color(Color.DEFAULT),
      new VariantCheckout(VariantCheckout.MODAL),
      new Branding(branding),
    );

    const defaultConfiguration = Configuration.createDefault();
    this.configuration =
      configuration ??
      new Configuration({
        currency: defaultConfiguration.currency,
        decimalSeparator: defaultConfiguration.decimalSeparator,
        thousandSeparator: defaultConfiguration.thousandSeparator,
        locale,
      });
  }

  private async init(): Promise<void> {
    setEnvironment(this.env);
    // this.credentials = await login({ username: this.username, password: this.password });
    this.credentials = {
      accessToken: 'PKKpUc5GCQrCIj5SkVyR23pyL1NGKZLE',
      refreshToken: 'Pv7x0DgYV5PM4lgfD0J8bYvzux3s55JV',
    };
    setCredentials({ accessToken: this.credentials.accessToken, refreshToken: this.credentials.refreshToken });
  }

  private async createOrder(order: any): Promise<{ orderId: string }> {
    const response = await fetch(`https://api.${this.env}.nemuru.io/api/v2/order/`, {
      method: 'POST',
      body: JSON.stringify(order),
      headers: new Headers({
        'Content-Type': 'application/json',
        Authorization: `Bearer ${this.credentials?.accessToken}`,
      }),
    });
    if (response.status !== 201) throw new Error(`POST Create order failed with code ${response.status}`);
    const orderId = response.headers.get('location')?.split(':')?.splice(-1)[0];
    if (!orderId) throw new Error('Id not returned from postOrder');
    return { orderId };
  }

  async getTranslation(): Promise<typeof dictionary_esES> {
    setTranslation(this.locale, new Branding(this.branding));
    if (!translation) throw new Error(`No translation found for branding: ${this.branding} and locale: ${this.locale}`);
    return translation;
  }

  private getAllPricingTerms(pricing: Pricing): number[] {
    let pricingTerms: number[] = [];
    pricing.ranges.forEach((range) => {
      for (let i = range.rangePeriodMin; i <= range.rangePeriodMax; i++) {
        pricingTerms.push(i);
      }
    });
    return pricingTerms.filter(TestHelper.onlyUnique);
  }

  private static onlyUnique(value: number, index: number, self: number[]) {
    return self.indexOf(value) === index;
  }

  private getAvailableConditions(
    product: Product,
    amount: Money,
    pricings: Pricing[],
  ): {
    product: string;
    terms: number[];
  }[] {
    if (!pricings) {
      return [];
    }

    const availableConditions: {
      product: string;
      terms: number[];
    }[] = [];

    pricings.forEach((pricing) => {
      if (
        pricing.product === product &&
        pricing.rangePrincipalMin.value <= amount.value &&
        pricing.rangePrincipalMax.value >= amount.value
      ) {
        const productAlreadyAdded = availableConditions.some((condition) => condition.product === pricing.product);
        if (productAlreadyAdded) {
          availableConditions.map((condition) => {
            if (condition.product === pricing.product) {
              const pricingTerms = this.getAllPricingTerms(pricing);
              return {
                ...condition,
                terms: [...condition.terms, ...pricingTerms].filter(TestHelper.onlyUnique),
              };
            } else {
              return condition;
            }
          });
        } else {
          availableConditions.push({
            product: pricing.product,
            terms: this.getAllPricingTerms(pricing),
          });
        }
      }
    });

    return availableConditions;
  }

  async createCheckoutProps(orderId?: string): Promise<CheckoutComponentProps> {
    await this.init();

    if (!this.credentials?.accessToken) throw new Error('No credentials found after init');

    let newOrderId = orderId;
    if (!newOrderId) {
      const { orderId } = await this.createOrder(postOrder);
      newOrderId = orderId;
    }

    const order = await getOrderByLoanId({ loanId: newOrderId });
    const pricings = await getPricingsByAgentId({ agentId: order.agentId });

    const availableConditions = this.getAvailableConditions(
      order.conditions.product,
      order.conditions.principal,
      pricings,
    );
    const terms = availableConditions.find((condition) => condition.product === order.conditions.product)?.terms;

    let loanCalculators: LoanCalculatorDto[] = [];
    let amortizationPlans: AmortizationPlanDto[] = [];
    if (pricings && terms) {
      const proposals = proposalsConstructor({
        principal: order.conditions.principal,
        terms: terms,
        pricings: pricings,
        product: order.conditions.product,
      });
      loanCalculators = getLoanCalculators({ proposals }).sort((a, b) => (a.term > b.term ? -1 : 0));
      amortizationPlans = await getAmortizationPlans({
        proposals,
      });
    }

    return {
      env: this.env,
      accessToken: this.credentials.accessToken,
      configuration: this.configuration,
      order,
      loanCalculators,
      amortizationPlans,
      style: this.style,
      behaviour: {
        expirationTimeInMinutes: 99999999999999,
      },
      handleCloseDialog: (status: string) => {
        console.log(status);
      },
      handleError: (error: ErrorParams) => {
        console.log(error);
      },
    };
  }

  async createCheckoutPropsWithDisabledFormEdition(orderId?: string): Promise<CheckoutComponentProps> {
    const props = await this.createCheckoutProps(orderId);
    return {
      ...props,
      behaviour: {
        ...props.behaviour,
        disableFormEdition: true,
      },
    };
  }

  async createCheckoutPropsWithDisabledFormEditionAndClose(orderId?: string): Promise<CheckoutComponentProps> {
    const props = await this.createCheckoutProps(orderId);
    return {
      ...props,
      behaviour: {
        ...props.behaviour,
        disableFormEdition: true,
        disableClose: true,
      },
    };
  }
}
