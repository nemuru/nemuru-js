import { Environment } from '@nemuru-eng/js-api-client';
import Branding from '../../../../../../src/shared/types/Style/Branding';

export type AllowedEnvironments = Environment.DEV | Environment.STG | Environment.PROD;
export type AllowedBrandings = typeof Branding.LENDING_HUB | typeof Branding.FINANPAY | typeof Branding.IBERIA;

export const variables: Variables = {
  [Environment.DEV]: {
    [Branding.LENDING_HUB]: {
      username: 'adria.moya+dev.sequra.ecommerce@nemuru.com',
      password: '123456',
    },
    [Branding.FINANPAY]: {
      username: 'finanpay@nemuru.com',
      password: '123456',
    },
    [Branding.IBERIA]: {
      username: 'andre.maia+dev.iberia@nemuru.com',
      password: '123456',
    },
  },
  [Environment.STG]: {
    [Branding.LENDING_HUB]: {
      username: 'ecommerce@nemuru.com',
      password: '55GaYC6P8Ya75xBhWPB5gAqN',
    },
    [Branding.FINANPAY]: {
      username: 'sipay@nemuru.com',
      password: 'eNb8C3JNDjQmaspWBzaeyt58wYxU8ZRV',
    },
    [Branding.IBERIA]: {
      username: 'iberia.web@nemuru.com',
      password: 'BBVvmjtJpsK9757Gk7qzXvfFS6yPLE',
    },
  },
  [Environment.PROD]: {
    [Branding.LENDING_HUB]: {
      username: 'ecommerce@nemuru.com',
      password: '55GaYC6P8Ya75xBhWPB5gAqN',
    },
    [Branding.FINANPAY]: {
      username: 'sipay@nemuru.com',
      password: 'eNb8C3JNDjQmaspWBzaeyt58wYxU8ZRV',
    },
    [Branding.IBERIA]: {
      username: 'iberia.web@nemuru.com',
      password: 'BBVvmjtJpsK9757Gk7qzXvfFS6yPLE',
    },
  },
};

type Variables = {
  [key in AllowedEnvironments]: {
    [key in AllowedBrandings]: {
      username: string;
      password: string;
    };
  };
};
