/// <reference types="cypress" />

// Created a wrapper function for cy.mount to avoid type errors
// Specific tsconfig.json for cypress helped out to identify all default cy methods and properties
// but custom commands like "mount" (see support/components.ts) do not propagate types correctly

// @ts-ignore
export const mount = (component: JSX.Element) => cy.mount(component);
