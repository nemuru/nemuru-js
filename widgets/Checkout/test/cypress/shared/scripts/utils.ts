export const replaceNbsp = (text: string) => text.replace(/\u00a0/g, ' ');
