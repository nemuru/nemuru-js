const envVars = {
  test: {
    GTM_ID: 'GTM-K2G2NTD',
  },
  dev: {
    GTM_ID: 'GTM-K2G2NTD',
  },
  stg: {
    GTM_ID: 'GTM-5ZH3S7T',
  },
  prod: {
    GTM_ID: 'GTM-5X5WJ79',
  },
};
module.exports = envVars;
