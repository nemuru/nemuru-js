const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const envVars = require('./env');

module.exports = (env) => {
  const mode = env?.ENV ?? 'dev';
  return {
    mode: 'development',
    entry: {
      main: './src/index.jsx',
      nemuru: './public/nemuru.ts',
    },
    output: {
      path: path.resolve(__dirname, './build'),
    },
    devServer: {
      static: {
        directory: path.join(__dirname, 'public'),
      },
      compress: true,
      // port: 9000,
    },
    module: {
      rules: [
        {
          test: /\.(js|jsx|ts|tsx)$/,
          exclude: /node_modules/,
          use: {
            loader: 'babel-loader',
          },
        },
        {
          test: /\.(css|scss)$/,
          use: ['style-loader', 'css-loader'],
        },
        {
          test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
          use: [
            {
              loader: 'file-loader',
              options: {
                name: '[name].[ext]',
                outputPath: 'fonts/',
              },
            },
          ],
        },
      ],
    },
    resolve: {
      extensions: ['.js', '.jsx', '.ts', '.tsx'],
      fallback: {
        path: require.resolve('path-browserify'),
      },
    },
    plugins: [
      new HtmlWebpackPlugin({
        template: './public/index.html',
        filename: 'index.html',
        inject: false,
        gtmid: envVars[mode].GTM_ID,
      }),
    ],
  };
};
