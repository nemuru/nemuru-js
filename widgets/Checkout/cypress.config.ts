import { defineConfig } from 'cypress';

export default defineConfig({
  fixturesFolder: './test/cypress/tests',
  downloadsFolder: './test/cypress/shared/downloads',
  chromeWebSecurity: false,
  experimentalWebKitSupport: true,
  video: false,
  component: {
    devServer: {
      framework: 'react',
      bundler: 'webpack',
    },
    supportFile: './test/cypress/shared/support/component.ts',
    indexHtmlFile: './test/cypress/shared/support/component-index.html',
    specPattern: './test/cypress/tests/**/*spec.{js,jsx,ts,tsx}',
  },
});
