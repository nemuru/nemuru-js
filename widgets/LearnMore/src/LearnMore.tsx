import { Product } from '@nemuru-eng/js-api-client';
import React, { useEffect, useState } from 'react';
import './styles.css';
import { LearnMoreComponentProps } from '../../../src/LearnMore/LearnMore';
import Content from './components/Content/Content';
import { setTranslation, translation } from './translations/Translation';

const LearnMore: React.FC<LearnMoreComponentProps> = ({
  loanCalculator,
  style,
  availableTerms,
  configuration,
  handleCloseDialog,
}) => {
  const [translationLoaded, setTranslationLoaded] = useState(false);

  useEffect(() => {
    if (!translation) {
      setTranslation(configuration.locale, style.branding);
      setTimeout(() => setTranslationLoaded(true), 20);
    }
  }, [translationLoaded]);

  if (!loanCalculator || !translation) return null;

  const product = loanCalculator.product as keyof typeof productTitle;

  const productTitle = {
    [Product.SPLIT_PAY]: translation.SPLIT_PAY,
    [Product.INSTALMENTS]: translation.INSTALMENTS,
  };

  const productSubtitle = {
    [Product.SPLIT_PAY]: translation.SPLIT_PAY_DETAIL,
    [Product.INSTALMENTS]: translation.INSTALMENTS_DETAIL,
  };

  return (
    <div className={'popup-wrapper'}>
      <div className={'popup-content'}>
        <div style={{ height: '100%', position: 'relative' }}>
          <div style={{ padding: '24px 40px 40px 40px' }}>
            <h6>{productTitle[product]}</h6>
            <h4 style={{ marginTop: '32px', marginBottom: '40px' }}>{productSubtitle[product]}</h4>
            <Content loanCalculator={loanCalculator} style={style} availableTerms={availableTerms} />
          </div>
          <div style={{ position: 'absolute', bottom: '40px', left: '40px', right: '40px' }}>
            <button id={'close-button'} className={'button'} onClick={() => handleCloseDialog()}>
              {translation.CLOSE}
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default LearnMore;
