import React from 'react';
import { translation } from '../../translations/Translation';
import { ContentProps } from './Content';
import LegalText from './LegalText';

const ContentInstalments: React.FC<ContentProps> = ({ loanCalculator, availableTerms }) => {
  if (!translation || !availableTerms) return null;

  const availableTermsStr: string =
    availableTerms.length === 1
      ? `${availableTerms[0]}`
      : `${availableTerms.slice(0, -1).join(', ')} ${translation.OR} ${availableTerms.slice(-1)}`;

  return (
    <>
      <div>
        <ul>
          <li>
            <p>
              <strong>{translation.WHAT_DO_YOU_NEED}</strong>
            </p>
            <p>{translation.WHAT_DO_YOU_NEED_DETAIL}</p>
          </li>
          <li style={{ marginTop: '16px' }}>
            <p>
              <strong>{translation.YOUR_MONEY_RULES}</strong>
            </p>
            <p>{`${translation.YOUR_MONEY_RULES_DETAIL_1} ${availableTermsStr} ${translation.YOUR_MONEY_RULES_DETAIL_2}`}</p>
          </li>
          <li style={{ marginTop: '16px' }}>
            <p>
              <strong>{translation.YOU_DECIDE}</strong>
            </p>
            <p>{translation.YOU_DECIDE_DETAIL}</p>
          </li>
        </ul>
      </div>
      <LegalText loanCalculator={loanCalculator} />
    </>
  );
};

export default ContentInstalments;
