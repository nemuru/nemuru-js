import { LoanCalculatorDto, Product } from '@nemuru-eng/js-api-client';
import React from 'react';
import Style from '../../../../../src/shared/types/Style/Style';
import ContentInstalments from './ContentInstalments';
import ContentSplitPay from './ContentSplitPay';

const Content: React.FC<ContentProps> = ({ loanCalculator, style, availableTerms }) => {
  return (
    <>
      {Product.INSTALMENTS === loanCalculator.product && (
        <ContentInstalments loanCalculator={loanCalculator} style={style} availableTerms={availableTerms} />
      )}
      {Product.SPLIT_PAY === loanCalculator.product && (
        <ContentSplitPay loanCalculator={loanCalculator} style={style} />
      )}
    </>
  );
};

export default Content;

export interface ContentProps {
  loanCalculator: LoanCalculatorDto;
  style: Style;
  availableTerms?: number[];
}
