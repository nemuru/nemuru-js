import { LoanCalculatorDto } from '@nemuru-eng/js-api-client';
import React from 'react';
import { translation } from '../../translations/Translation';

const LegalText: React.FC<{ loanCalculator: LoanCalculatorDto }> = ({ loanCalculator }) => {
  if (!translation) return null;
  return (
    <>
      <p className={'micro'} style={{ marginTop: '40px', marginBottom: '16px' }}>
        {`${translation.TIN}: ${loanCalculator.annualInterest.string} | ${translation.TAE}: ${loanCalculator.apr.string} | ${translation.FINANCED_AMOUNT}: ${loanCalculator.principalAmount.string} | ${translation.QUOTA_AMOUNT}: ${loanCalculator.instalmentTotalAmount.string} | ${translation.LAST_QUOTA_AMOUNT}: ${loanCalculator.lastInstalmentTotalAmount.string} | ${translation.NUMBER_OF_QUOTAS}: ${loanCalculator.term} ${translation.MONTHS} | ${translation.TOTAL_AMOUNT}: ${loanCalculator.totalAmount.string}.`}
      </p>
      <p className={'micro color-grey-dark'} style={{ marginBottom: '100px' }}>
        {translation.SEQURA_OFFERING}
      </p>
    </>
  );
};

export default LegalText;
