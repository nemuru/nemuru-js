import React from 'react';
import { translation } from '../../translations/Translation';
import { ContentProps } from './Content';
import LegalText from './LegalText';

const ContentSplitPay: React.FC<ContentProps> = ({ loanCalculator }) => {
  if (!translation) return null;
  return (
    <>
      <div>
        <ul>
          <li>
            <p>
              <strong>{translation.WHAT_DO_YOU_NEED}</strong>
            </p>
            <p>{translation.WHAT_DO_YOU_NEED_DETAIL}</p>
          </li>
          <li style={{ marginTop: '16px' }}>
            <p>
              <strong>{translation.FREE}</strong>
            </p>
            <p>{translation.FREE_DETAIL}</p>
          </li>
          <li style={{ marginTop: '16px' }}>
            <p>
              <strong>{translation.YOU_DECIDE}</strong>
            </p>
            <p>{translation.YOU_DECIDE_DETAIL}</p>
          </li>
        </ul>
      </div>
      <LegalText loanCalculator={loanCalculator} />
    </>
  );
};

export default ContentSplitPay;
