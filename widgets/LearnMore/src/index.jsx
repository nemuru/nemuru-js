import React from 'react';
import ReactDOM from 'react-dom';
import LearnMore from './LearnMore';
// noinspection ES6UnusedImports
import LearnMoreZoid from './learn-more-zoid'; // Do not delete! Needed for two-way communication

ReactDOM.render(<LearnMore {...window.xprops} />, document.getElementById('root'));
