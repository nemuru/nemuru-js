import * as zoid from 'zoid/dist/zoid';

const LearnMoreZoid = zoid.create({
  tag: 'learn-more-component', // This has to be unique per js loaded on the page
  url: `http://localhost:4002/learn-more.html`,
  props: {
    loanCalculator: {
      type: 'object',
      required: true,
    },
    style: {
      type: 'object',
      required: true,
    },
    availableTerms: {
      type: 'array',
      required: false,
    },
    configuration: {
      type: 'object',
      required: true,
    },
    handleCloseDialog: {
      type: 'function',
      required: true,
    },
  },
});

export default LearnMoreZoid;
