import { dictionary_es_ES_default } from './dictionary_es_ES_default';

export const dictionary_en_US_default: Partial<typeof dictionary_es_ES_default> = {
  SPLIT_PAY: 'Split in 3',
  INSTALMENTS: 'Instalments',
  SPLIT_PAY_DETAIL: 'Split your purchase in 3 months',
  INSTALMENTS_DETAIL: 'Pay comfortably in monthly instalments',
  WHAT_DO_YOU_NEED: 'What do you need?',
  WHAT_DO_YOU_NEED_DETAIL: 'Your document ID, mobile phone and card.',
  YOUR_MONEY_RULES: 'Your money, your rules',
  YOUR_MONEY_RULES_DETAIL_1: 'Split your payment in',
  YOUR_MONEY_RULES_DETAIL_2: 'months with a fixed fee per instalment.',
  YOU_DECIDE: 'You decide',
  YOU_DECIDE_DETAIL:
    'You only pay the first instalment today. You can advance the total or partial payment whenever you want.',
  FREE: 'Free',
  FREE_DETAIL: 'Take your purchase with you, at no additional cost!',
  TIN: 'Interest',
  TAE: 'APR',
  FINANCED_AMOUNT: 'Financed amount',
  QUOTA_AMOUNT: 'Instalment amount',
  LAST_QUOTA_AMOUNT: 'Last instalment amount',
  NUMBER_OF_QUOTAS: 'Nº of instalments',
  MONTHS: 'months',
  TOTAL_AMOUNT: 'Total amount',
  SEQURA_OFFERING:
    'Financing offered by SeQura, which allows you to buy in instalments, without paperwork and with instant approval.',
  CLOSE: 'Close',
  OR: 'or',
};
