import { dictionary_es_ES_default } from './dictionary_es_ES_default';

export const dictionary_ca_CA_default: Partial<typeof dictionary_es_ES_default> = {
  SPLIT_PAY: 'Fracciona en 3',
  INSTALMENTS: 'Paga a plaços',
  SPLIT_PAY_DETAIL: 'Divideix la teva compra en 3 mesos',
  INSTALMENTS_DETAIL: 'Paga poc a poc en quotes mensuals',
  WHAT_DO_YOU_NEED: 'Què necessites?',
  WHAT_DO_YOU_NEED_DETAIL: 'El teu DNI, número de mòbil i la targeta.',
  YOUR_MONEY_RULES: 'Els teus diners, les teves regles',
  YOUR_MONEY_RULES_DETAIL_1: 'Fracciona el teu pagament en',
  YOUR_MONEY_RULES_DETAIL_2: 'mesos amb un cost fixe per quota.',
  YOU_DECIDE: 'Tu decideixes',
  YOU_DECIDE_DETAIL: 'Ara només pagues la primera quota. Podràs avançar el pagament total o parcial quan vulguis.',
  FREE: 'Gratis',
  FREE_DETAIL: "Emporta't la teva compra, sense cap cost addicional!",
  TIN: 'TIN',
  TAE: 'TAE',
  FINANCED_AMOUNT: 'Import finançat',
  QUOTA_AMOUNT: 'Import de les quotes',
  LAST_QUOTA_AMOUNT: 'Import última quota',
  NUMBER_OF_QUOTAS: 'Nº de quotes',
  MONTHS: 'mesos',
  TOTAL_AMOUNT: 'Preu total a plaços i import degut',
  SEQURA_OFFERING:
    'Finançament ofert per SeQura, que et permet comprar en quotes, sense paperassa i amb aprovació instantània.',
  CLOSE: 'Tancar',
  OR: 'o',
};
