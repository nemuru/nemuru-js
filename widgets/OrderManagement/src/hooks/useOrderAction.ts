import { Capture, CaptureStatus, Money, OrderDetails, Refund, RefundStatus } from '@nemuru-eng/js-api-client';
import { useEffect } from 'react';
import { v4 as uuidv4 } from 'uuid';

import { useOrderManagementContext } from '../contexts/OrderManagement/OrderManagementContext';
import OrderAction from '../types/OrderAction';
import OrderLineInAction from '../types/OrderLineInAction';

const useOrderAction = () => {
  const { order, captures, refunds, updateOrderManagement } = useOrderManagementContext();

  useEffect(() => {
    if (order) {
      const actions = setOrderActions(order, captures, refunds);
      const { maxToCapture, maxToRefund } = calculateMaxAmounts(actions);
      updateOrderManagement({ actions, maxToCapture, maxToRefund });
    }
  }, [order, captures, refunds]);

  const setOrderActions = (order: OrderDetails, captures?: Capture[], refunds?: Refund[]): OrderAction[] => {
    const actions: OrderAction[] = [];
    // Refunds
    let refundNumber = 0;
    refunds
      ?.filter((refund) => refund.status === RefundStatus.APPROVED && refund.notified === true)
      .sort((a, b) => (a.createdAt < b.createdAt ? -1 : 1))
      .forEach((refund) => {
        refundNumber += 1;
        actions.push({
          id: refund.id,
          type: 'refund',
          createdAt: refund.createdAt,
          updatedAt: refund.updatedAt,
          amount: refund.amount, // @ts-ignore
          orderLines:
            refund.orderLineIds.length > 0
              ? refund.orderLineIds.map((orderLineId) => {
                  return {
                    ...order.order.orderLines.find((orderLine) => orderLine.id === orderLineId),
                    type: 'refund',
                    status: 'disabled',
                  };
                })
              : createOrderLineForRefundByAmount(refund.amount, refund.description),
          number: refundNumber,
        });
      });
    // Captures
    let captureNumber = 0;
    captures
      ?.filter((capture) => capture.status === CaptureStatus.APPROVED && capture.notified === true)
      .sort((a, b) => (a.createdAt < b.createdAt ? -1 : 1))
      .forEach((capture) => {
        captureNumber += 1;
        actions.push({
          id: capture.id,
          type: 'capture',
          createdAt: capture.createdAt,
          updatedAt: capture.updatedAt,
          amount: capture.amount, // @ts-ignore
          orderLines: capture.orderLineIds.map((orderLineId) => {
            const orderLineRefunded: Refund | undefined = refunds?.find((refund) => {
              if (refund.orderLineIds.find((refundedOrderLineId) => refundedOrderLineId === orderLineId)) {
                return refund;
              }
            });
            return {
              ...order.order.orderLines.find((orderLine) => orderLine.id === orderLineId),
              type: 'capture',
              status: orderLineRefunded ? 'disabled' : 'enabled',
              refundId: orderLineRefunded?.id,
              refundNumber: actions.find((action) => action.id === orderLineRefunded?.id)?.number,
            };
          }),
          number: captureNumber,
        });
      });
    // Pending
    const orderLinesPending: OrderLineInAction[] = [];
    let orderLinesPendingAmount = 0;
    order.order.orderLines.forEach((orderLine) => {
      const orderLineNotPending = actions.some((action) => {
        return action.orderLines.some((orderLineInActions) => orderLineInActions.id === orderLine.id);
      });
      if (!orderLineNotPending) {
        orderLinesPending.push({ ...orderLine, type: 'pending', status: 'enabled' });
        orderLinesPendingAmount += orderLine.totalPriceWithTax.value;
      }
    });
    if (orderLinesPending.length > 0) {
      actions.push({
        id: order.order.id,
        type: 'pending',
        createdAt: order.createdAt,
        updatedAt: order.updatedAt,
        amount: { value: orderLinesPendingAmount, currency: order.totalOrderAmount.currency },
        orderLines: orderLinesPending,
      });
    }

    return actions.sort((a, b) => (a.createdAt < b.createdAt ? -1 : 1));
  };

  const createOrderLineForRefundByAmount = (amount: Money, description?: string) => [
    {
      id: uuidv4(),
      totalDiscount: {
        value: 0,
        currency: amount.currency,
      },
      totalPriceWithTax: amount,
      units: 1,
      article: {
        type: 'refunded_amount',
        name: 'Importe devuelto',
        price: amount,
        description,
      },
      type: 'refund',
      status: 'disabled',
    },
  ];

  const calculateMaxAmounts = (actions: OrderAction[]) => {
    let maxToCapture, maxToRefund;
    if (actions && order) {
      maxToCapture = order.totalOrderAmount.value - order.totalCapturedAmount.value;
      maxToRefund = order.totalCapturedAmount.value - order.totalRefundedAmount.value;
    }
    return { maxToCapture, maxToRefund };
  };

  return {};
};

export default useOrderAction;
