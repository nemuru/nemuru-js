import { useReducer } from 'react';

import { actions, initialState, reducer } from '../contexts/OrderManagement/reducer';

const useOrderManagement = () => {
  const [state, dispatch] = useReducer(reducer, initialState);
  const { updateOrderManagement } = actions(dispatch);

  return {
    state,
    updateOrderManagement,
  };
};

export default useOrderManagement;
