import React from 'react';
import { OrderManagementComponentProps } from '../../../src/orderManagement/OrderManagement';
import ErrorBoundary from './components/Shared/ErrorBoundary';
import { OrderManagementContextProvider } from './contexts/OrderManagement/OrderManagementContext';
import OrderManagement from './OrderManagement';
import './styles.css';

const App: React.FC<OrderManagementComponentProps> = (props) => {
  return (
    <ErrorBoundary onError={props.handleError}>
      <OrderManagementContextProvider>
        <OrderManagement {...props} />
      </OrderManagementContextProvider>
    </ErrorBoundary>
  );
};

export default App;
