import * as zoid from 'zoid/dist/zoid';

const OrderManagementZoid = zoid.create({
  tag: 'order-management-component', // This has to be unique per js loaded on the page
  url: `http://localhost:4004/order-management.html`,
  props: {
    env: {
      type: 'string',
      required: true,
    },
    accessToken: {
      type: 'string',
      required: true,
    },
    configuration: {
      type: 'object',
      required: true,
    },
    orderId: {
      type: 'string',
      required: true,
    },
    style: {
      type: 'object',
      required: true,
    },
    handleCloseDialog: {
      type: 'function',
      required: true,
    },
    handleAction: {
      type: 'function',
      required: false,
    },
  },
});

export default OrderManagementZoid;
