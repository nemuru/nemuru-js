import 'regenerator-runtime/runtime';
import React from 'react';
import ReactDOM from 'react-dom';
// noinspection ES6UnusedImports
import OrderManagementZoid from './order-management-zoid';
import App from './App'; // Do not delete! Needed for two-way communication

ReactDOM.render(<App {...window.xprops} />, document.getElementById('root'));
