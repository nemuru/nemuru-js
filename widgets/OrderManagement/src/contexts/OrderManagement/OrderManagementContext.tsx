import { Capture, OrderDetails, Refund } from '@nemuru-eng/js-api-client';
import React, { createContext, ReactElement, useContext } from 'react';
import useOrderManagement from '../../hooks/useOrderManagement';
import OrderAction from '../../types/OrderAction';
import OrderLineInAction from '../../types/OrderLineInAction';
import { initialState } from './reducer';

export interface OrderManagementContextState {
  step: string;
  env?: 'dev' | 'stg' | 'prod';
  order?: OrderDetails;
  loanId?: string;
  captures?: Capture[];
  refunds?: Refund[];
  actions?: OrderAction[];
  maxToCapture?: number;
  maxToRefund?: number;
  handleAction?: (action: string) => void;
  selectedOrderLines?: OrderLineInAction[];
  selectedActionType?: 'capture' | 'delete' | 'refund' | 'refund_amount';
  updateOrderManagement: (orderManagement: any) => void;
}

const useOrderManagementContext = (): OrderManagementContextState => {
  return useContext(OrderManagementContext);
};

const OrderManagementContext = createContext<OrderManagementContextState>(initialState);

const OrderManagementContextProvider = ({ children }: { children: ReactElement }) => {
  const { state, updateOrderManagement } = useOrderManagement();

  return (
    <>
      <OrderManagementContext.Provider
        value={{
          ...state,
          updateOrderManagement,
        }}
      >
        {children}
      </OrderManagementContext.Provider>
    </>
  );
};

export { OrderManagementContextProvider, useOrderManagementContext };
