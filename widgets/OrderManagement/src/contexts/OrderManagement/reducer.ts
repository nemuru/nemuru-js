import { Dispatch } from 'react';

import { OrderManagementContextState } from './OrderManagementContext';

const ACTIONS = {
  UPDATE_CONTEXT: 'UPDATE_CONTEXT',
};

interface ActionType {
  type: string;
  payload: Partial<OrderManagementContextState>;
}

export const initialState: OrderManagementContextState = {
  step: 'order',
  env: undefined,
  order: undefined,
  loanId: undefined,
  captures: undefined,
  refunds: undefined,
  actions: undefined,
  maxToCapture: undefined,
  maxToRefund: undefined,
  selectedOrderLines: undefined,
  selectedActionType: undefined,
  updateOrderManagement: () => {},
};

export const reducer = (state: typeof initialState, action: ActionType) => {
  switch (action.type) {
    case ACTIONS.UPDATE_CONTEXT:
      return {
        ...state,
        ...action.payload,
      };
    default:
      return state;
  }
};

export const actions = (dispatch: Dispatch<ActionType>) => ({
  updateOrderManagement: (orderManagement: Partial<OrderManagementContextState>) =>
    dispatch({
      type: ACTIONS.UPDATE_CONTEXT,
      payload: orderManagement,
    }),
});
