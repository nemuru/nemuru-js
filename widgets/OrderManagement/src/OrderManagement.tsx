import { Box, CircularProgress, IconButton, Tab, Tabs, Typography, useMediaQuery, useTheme } from '@mui/material';
import {
  Exception,
  getOrderDetailsByLoanId,
  LoanStatusExternal,
  OrderDetails,
  OrderStatus,
  setCredentials,
  setEnvironment,
  UnauthorizedException,
} from '@nemuru-eng/js-api-client';
import React, { useEffect, useRef, useState } from 'react';
import { OrderManagementComponentProps } from '../../../src/orderManagement/OrderManagement';
import OrderActionDialog from './components/OrderTab/OrderActionDialog/OrderActionDialog';
import OrderTab from './components/OrderTab/OrderTab';
import OrderHeader from './components/Shared/OrderHeader';
import TabPanel from './components/Shared/TabPanel';
import SummaryTab from './components/SummaryTab/SummaryTab';
import { useOrderManagementContext } from './contexts/OrderManagement/OrderManagementContext';
import useOrderAction from './hooks/useOrderAction';
import './styles.css';
import ThemeManager from './styles/ThemeManager';

const OrderManagement: React.FC<OrderManagementComponentProps> = (props) => {
  const headerRef = useRef(null);

  const {
    env,
    accessToken,
    configuration,
    orderId: loanId,
    style,
    handleCloseDialog,
    handleAction,
    handleError,
  } = props;

  const onError = (error: Exception) => {
    handleError({ message: error.message, url: error.url, method: error.method, status: error.status });
  };

  const [loading, setLoading] = useState(true);
  const [tabValue, setTabValue] = useState(0);
  const [orderConfirmed, setOrderConfirmed] = useState<boolean | undefined>(undefined);

  const { breakpoints } = useTheme();
  const isScreenXs = useMediaQuery(breakpoints.down(530));

  const tabs = isScreenXs ? ['Acciones', 'Pedido'] : ['Envíos y devoluciones', 'Resumen del pedido'];

  const { step, order, updateOrderManagement } = useOrderManagementContext();

  useOrderAction();

  useEffect(() => {
    initCore();
    updateOrderManagement({ handleAction });
  }, []);

  const initCore = async () => {
    await setEnvironment(env, configuration);
    await setCredentials({ accessToken, refreshToken: accessToken });
    let order: OrderDetails | undefined;
    try {
      order = await getOrderDetailsByLoanId({ loanId });
    } catch (err) {
      if (err instanceof UnauthorizedException) onError(err);
      setOrderConfirmed(false);
      setLoading(false);
      return;
    }
    setOrderConfirmed(
      ALLOWED_LOAN_STATUS_EXTERNAL_VALUES.includes(order.status) && order.order.status === OrderStatus.CONFIRMED,
    );
    updateOrderManagement({
      order,
      captures: order.captures,
      refunds: order.refunds,
      loanId,
    });
    setLoading(false);
  };

  if (!loanId) return null;

  const handleTabChange = (event: React.SyntheticEvent, newValue: number) => {
    setTabValue(newValue);
  };

  return (
    <div className={'popup-wrapper'}>
      <div className={'popup-content'}>
        <Box sx={{ px: { xs: 4, sm: 5 }, height: '100%' }}>
          <ThemeManager color={style.color}>
            <Box ref={headerRef} pt={2} display={'flex'} alignItems={'center'} justifyContent={'space-between'}>
              <Typography variant={'h6'}>Gestor del pedido</Typography>
              <IconButton onClick={() => handleCloseDialog()}>
                <i className={'material-icons-outlined'} style={{ height: '24px', width: '24px', color: '#000' }}>
                  close
                </i>
              </IconButton>
            </Box>
            <Box
              className={'appear-anim'}
              display={'flex'}
              flexDirection={'column'}
              minHeight={'700px'}
              position={'relative'}
              // @ts-ignore
              height={`calc(100% - ${headerRef?.current?.offsetHeight}px)`}
            >
              {loading && (
                <>
                  <Box mt={5} className={'appear-anim'}>
                    <CircularProgress size={80} thickness={3} />
                    <Typography variant={'h4'} sx={{ pt: 4, pb: 2 }} color={'text.primary'}>
                      <strong>Procesando</strong>
                    </Typography>
                    <Typography variant={'body2'}>Espera unos segundos por favor...</Typography>
                  </Box>
                </>
              )}
              {!loading && orderConfirmed === false && (
                <Box display={'flex'} alignItems={'center'} justifyContent={'center'} flexGrow={1}>
                  <Typography variant={'body2'}>El pedido no se encuentra confirmado</Typography>
                </Box>
              )}
              {step === 'order' && order && orderConfirmed === true && (
                <>
                  <OrderHeader />
                  {/*<Divider light sx={{ mt: 3 }} />*/}
                  <Box sx={{ mt: 3, borderBottom: 1, borderColor: 'divider' }}>
                    <Tabs
                      value={tabValue}
                      onChange={handleTabChange}
                      variant={'fullWidth'}
                      sx={{ '& .MuiTabs-indicator': { bgcolor: 'secondary.main' } }}
                    >
                      {tabs.map((value) => (
                        <Tab
                          key={value}
                          label={value}
                          disableRipple
                          sx={{
                            textTransform: 'none',
                            fontWeight: 600,
                            fontSize: (theme) => theme.typography.body2.fontSize,
                            '&.Mui-selected': { color: 'secondary.main' },
                          }}
                        />
                      ))}
                    </Tabs>
                  </Box>
                  <TabPanel value={tabValue} index={0}>
                    <OrderTab />
                  </TabPanel>
                  <TabPanel value={tabValue} index={1}>
                    <SummaryTab />
                  </TabPanel>
                </>
              )}
              {step === 'action' && orderConfirmed === true && <OrderActionDialog />}
            </Box>
          </ThemeManager>
        </Box>
      </div>
    </div>
  );
};

export default OrderManagement;

const ALLOWED_LOAN_STATUS_EXTERNAL_VALUES = [
  LoanStatusExternal.CONFIRMED,
  LoanStatusExternal.CAPTURED,
  LoanStatusExternal.PAID,
  LoanStatusExternal.CANCELLED,
];
