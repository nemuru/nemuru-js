import { OrderLine } from '@nemuru-eng/js-api-client';

interface OrderLineInAction extends OrderLine {
  type: 'pending' | 'capture' | 'refund';
  status: 'enabled' | 'disabled';
  refundId?: string;
  refundNumber?: number;
}

export default OrderLineInAction;
