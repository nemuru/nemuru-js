import { Money } from '@nemuru-eng/js-api-client';

import OrderLineInAction from './OrderLineInAction';

interface OrderAction {
  id: string;
  type: 'pending' | 'capture' | 'refund';
  createdAt: Date;
  updatedAt: Date;
  amount: Money;
  orderLines: OrderLineInAction[];
  number?: number;
}

export default OrderAction;
