import {
  Box,
  Checkbox,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Typography,
} from '@mui/material';
import { Money } from '@nemuru-eng/js-api-client';
import * as React from 'react';

interface EnhancedTableProps {
  dense: boolean;
  columns: Column[];
  selectable: boolean;
  numSelected: number;
  onSelectAllClick: (event: React.ChangeEvent<HTMLInputElement>) => void;
  rowCount: number;
}

function EnhancedTableHead(props: EnhancedTableProps) {
  const { dense, columns, selectable, onSelectAllClick, numSelected, rowCount } = props;

  return (
    <TableHead>
      <TableRow>
        {selectable && (
          <TableCell padding="checkbox">
            <Checkbox
              color="primary"
              indeterminate={numSelected > 0 && numSelected < rowCount}
              checked={rowCount > 0 && numSelected === rowCount}
              onChange={onSelectAllClick}
              inputProps={{
                'aria-label': 'select all desserts',
              }}
            />
          </TableCell>
        )}
        {columns.map((column, key) => (
          <TableCell
            key={column.name}
            align={column.align}
            padding={selectable && key === 0 ? 'none' : 'normal'}
            sx={{ px: selectable && key === 0 ? 0 : dense ? '6px' : '16px' }}
          >
            {column.label}
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
  );
}

const BasicTable: React.FC<Props> = ({
  rows,
  columns,
  selectable,
  dense = false,
  handleSelection,
  isRowSelectable,
  totalAmount,
}) => {
  const [selected, setSelected] = React.useState<readonly string[]>([]);

  const handleSelectAllClick = (event: React.ChangeEvent<HTMLInputElement>) => {
    let newSelected: string[] = [];
    if (event.target.checked) {
      newSelected = rows.filter((row) => isRowSelectable && isRowSelectable(row)).map((n) => n.id);
    }
    setSelected(newSelected);
    handleSelection && handleSelection(newSelected);
  };

  const handleClick = (event: React.MouseEvent<unknown>, name: string) => {
    const selectedIndex = selected.indexOf(name);
    let newSelected: readonly string[] = [];

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, name);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(selected.slice(0, selectedIndex), selected.slice(selectedIndex + 1));
    }

    setSelected(newSelected);
    handleSelection && handleSelection(newSelected);
  };

  const isSelected = (name: string) => selected.indexOf(name) !== -1;

  return (
    <>
      <Box sx={{ width: '100%' }}>
        <TableContainer>
          <Table aria-labelledby="tableTitle" size={dense ? 'small' : 'medium'}>
            <EnhancedTableHead
              dense={dense}
              columns={columns}
              selectable={selectable}
              numSelected={selected.length}
              onSelectAllClick={handleSelectAllClick}
              rowCount={rows.filter((row) => isRowSelectable && isRowSelectable(row)).length}
            />
            <TableBody>
              {rows.map((row, index) => {
                const isItemSelected = isSelected(row.id);
                const labelId = `enhanced-table-checkbox-${index}`;
                return (
                  <TableRow
                    key={labelId}
                    hover={selectable}
                    onClick={(event) => isRowSelectable && isRowSelectable(row) && handleClick(event, row.id)}
                    role={selectable ? 'checkbox' : 'row'}
                    aria-checked={isItemSelected}
                    tabIndex={-1}
                    selected={isItemSelected}
                    sx={{
                      backgroundColor: 'transparent !important',
                    }}
                  >
                    {selectable && (
                      <TableCell padding="checkbox">
                        <Checkbox
                          color="primary"
                          checked={!(isRowSelectable && isRowSelectable(row)) ? false : isItemSelected}
                          inputProps={{
                            'aria-labelledby': labelId,
                          }}
                          disabled={!(isRowSelectable && isRowSelectable(row))}
                        />
                      </TableCell>
                    )}
                    <TableCell
                      component="th"
                      id={labelId}
                      scope="row"
                      padding={selectable ? 'none' : 'normal'}
                      align={columns[0].align}
                      sx={{
                        ...columns[0].cellProps,
                        ...{ px: selectable ? 0 : dense ? '6px' : '16px' },
                      }}
                    >
                      {columns[0].renderCell(row)}
                    </TableCell>
                    {columns.slice(1, columns.length).map((column, key) => (
                      <TableCell
                        key={`${column.name}-${labelId}`}
                        align={column.align}
                        sx={{ ...column.cellProps, ...{ px: dense ? '6px' : '16px' } }}
                      >
                        {column.renderCell(row)}
                      </TableCell>
                    ))}
                  </TableRow>
                );
              })}
            </TableBody>
          </Table>
        </TableContainer>
      </Box>
      {totalAmount && (
        <Box mt={dense ? 1 : 2} width={'100%'} height={50} position={'relative'}>
          <Typography variant={'body2'} sx={{ position: 'absolute', left: dense ? 6 : 16 }}>
            <strong>Total</strong>
          </Typography>
          <Typography variant={'body2'} sx={{ position: 'absolute', right: dense ? 6 : 16 }}>
            <strong>{totalAmount.value.toFixed(2)} €</strong>
          </Typography>
        </Box>
      )}
    </>
  );
};

export default BasicTable;

interface Props {
  rows: any[];
  columns: Column[];
  selectable: boolean;
  isRowSelectable?: (rowData: any) => boolean;
  handleSelection?: (rowData: any) => void;
  totalAmount?: Money;
  dense?: boolean;
}

export interface Column {
  name: string;
  label: JSX.Element;
  align: 'left' | 'right' | 'center';
  renderCell: (rowData: any) => JSX.Element;
  cellProps?: any;
}
