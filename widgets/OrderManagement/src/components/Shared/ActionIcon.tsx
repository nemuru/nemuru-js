import { Avatar } from '@mui/material';
import React from 'react';

const ActionIcon: React.FC<{ type: 'pending' | 'capture' | 'refund'; size: 'small' | 'large' }> = ({ type, size }) => {
  return (
    <>
      <Avatar
        sx={{
          bgcolor: type === 'pending' ? 'warning.main' : type === 'refund' ? 'grey.300' : 'primary.main',
          width: size === 'small' ? 24 : 36,
          height: size === 'small' ? 24 : 36,
          color: type === 'refund' ? 'text.primary' : '#FFF',
        }}
      >
        <i className={'material-icons-outlined'} style={{ fontSize: size === 'small' ? 16 : 20 }}>
          {type === 'refund' ? 'arrow_back' : type === 'capture' ? 'local_shipping' : 'watch_later'}
        </i>
      </Avatar>
    </>
  );
};

export default ActionIcon;
