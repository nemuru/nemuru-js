import { Box, Chip, Typography } from '@mui/material';
import { LoanStatusExternal, OrderDetails } from '@nemuru-eng/js-api-client';
import React from 'react';
import { useOrderManagementContext } from '../../contexts/OrderManagement/OrderManagementContext';

const OrderHeader: React.FC<{}> = () => {
  const { order, actions } = useOrderManagementContext();

  if (!order || !actions) return null;

  const getStatuses = (order: OrderDetails): HeaderOrderStatus[] => {
    if (order.status === LoanStatusExternal.CANCELLED) return [HeaderOrderStatus.CANCELLED];

    const headerOrderStatuses = [];
    const orderFullyCaptured = order.totalOrderAmount.value === order.totalCapturedAmount.value;
    const orderFullyRefunded = order.totalOrderAmount.value === order.totalRefundedAmount.value;
    const orderPaid = order.status === LoanStatusExternal.PAID;

    if (orderFullyRefunded) return [HeaderOrderStatus.CANCELLED];

    if (orderPaid) headerOrderStatuses.push(HeaderOrderStatus.PAID);

    if (order.totalCapturedAmount.value > 0 && !orderPaid) {
      if (orderFullyCaptured) {
        headerOrderStatuses.push(HeaderOrderStatus.FULLY_CAPTURED);
      } else {
        headerOrderStatuses.push(HeaderOrderStatus.PARTIALLY_CAPTURED);
      }
    }

    if (actions.some((action) => action.type === 'pending') && headerOrderStatuses.length === 0)
      headerOrderStatuses.push(HeaderOrderStatus.PENDING_CAPTURE);

    if (order.totalRefundedAmount.value > 0) headerOrderStatuses.push(HeaderOrderStatus.PARTIALLY_REFUNDED);

    return headerOrderStatuses;
  };

  const orderStatuses = getStatuses(order);

  return (
    <>
      <Box mt={2}>
        <Box display={'flex'} alignItems={'flex-start'} justifyContent={'space-between'}>
          <Box>
            <Typography variant={'h6'}>{order.order.merchantOrderReference1}</Typography>
            <Typography variant={'caption'} color={'text.secondary'}>
              Creado el:{' '}
              {order.createdAt.toLocaleDateString('es-ES', {
                day: 'numeric',
                month: 'short',
                year: 'numeric',
                hour: '2-digit',
                minute: '2-digit',
              })}
            </Typography>
          </Box>
          <Typography variant={'h4'} align={'right'} style={{ minWidth: '130px' }}>
            {order.totalOrderAmount.value.toFixed(2)} €
          </Typography>
        </Box>
      </Box>
      <Box mt={1} display={'flex'} flexWrap={'wrap'}>
        {orderStatuses.map((orderStatus, key) => (
          <Chip
            size={'small'}
            key={orderStatus}
            label={orderStatus}
            sx={{ mr: 1, mb: 1 }}
            color={
              STATUS_COLOR[orderStatus] as
                | 'default'
                | 'primary'
                | 'secondary'
                | 'error'
                | 'info'
                | 'success'
                | 'warning'
            }
          />
        ))}
      </Box>
    </>
  );
};

export default OrderHeader;

enum HeaderOrderStatus {
  PENDING_CAPTURE = 'Pendiente enviar',
  PARTIALLY_CAPTURED = 'Parcialmente enviado',
  FULLY_CAPTURED = 'Todo enviado',
  PARTIALLY_REFUNDED = 'Parcialmente devuelto',
  CANCELLED = 'Cancelado',
  PAID = 'Pagado',
}

const STATUS_COLOR = {
  [HeaderOrderStatus.PENDING_CAPTURE]: 'warning',
  [HeaderOrderStatus.PARTIALLY_CAPTURED]: 'primary',
  [HeaderOrderStatus.FULLY_CAPTURED]: 'primary',
  [HeaderOrderStatus.PARTIALLY_REFUNDED]: 'warning',
  [HeaderOrderStatus.CANCELLED]: 'error',
  [HeaderOrderStatus.PAID]: 'primary',
};
