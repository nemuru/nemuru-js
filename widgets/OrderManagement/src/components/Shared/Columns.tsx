import { Box, Chip, Typography } from '@mui/material';
import React from 'react';
import OrderLineInAction from '../../types/OrderLineInAction';
import { Column } from './BasicTable';

export const columns = (hideRefundOnCapture: boolean = false): Column[] => [
  {
    name: 'col1',
    label: (
      <Typography variant={'caption'} color={'text.secondary'}>
        Producto
      </Typography>
    ),
    align: 'left',
    renderCell: (rowData) => {
      const orderLine: OrderLineInAction = rowData;
      // @ts-ignore
      if (orderLine.article.type === 'refunded_amount') {
        return (
          <>
            {orderLine.article.description ? (
              <Box display={'grid'} mt={'4px'}>
                <Typography variant={'caption'}>{orderLine.article.name}</Typography>
                <Typography variant={'caption'} color={'text.secondary'} className={'shorted-cell'}>
                  {orderLine.article.description}
                </Typography>
              </Box>
            ) : (
              <Typography variant={'caption'}>{orderLine.article.name}</Typography>
            )}
          </>
        );
      }
      return (
        <>
          <Box display={'grid'} mt={'4px'}>
            <Typography variant={'caption'} className={'shorted-cell'}>
              {!hideRefundOnCapture && orderLine.refundNumber && (
                <Chip size={'small'} label={'Devuelto'} sx={{ mr: 1, height: '20px' }} />
              )}
              <span
                style={{ textDecoration: !hideRefundOnCapture && orderLine.refundNumber ? 'line-through' : 'none' }}
              >
                {orderLine.article.name}
              </span>
            </Typography>
            <Typography variant={'caption'} color={'text.secondary'}>
              {`${orderLine.units}x ${orderLine.article.price.value.toFixed(2)}€`}
            </Typography>
          </Box>
        </>
      );
    },
    cellProps: {
      py: 0,
    },
  },
  {
    name: 'col2',
    label: (
      <Typography variant={'caption'} color={'text.secondary'}>
        Precio total
      </Typography>
    ),
    align: 'right',
    renderCell: (rowData) => {
      const orderLine: OrderLineInAction = rowData;
      return (
        <>
          <Typography variant={'body2'}>
            {((orderLine.type === 'refund' ? -1 : 1) * orderLine.totalPriceWithTax.value).toFixed(2)} €
          </Typography>
        </>
      );
    },
    cellProps: {
      minWidth: '100px',
    },
  },
];
