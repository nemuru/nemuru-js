import { Button as MuiButton, ButtonProps as MuiButtonProps, CircularProgress, Typography } from '@mui/material';
import React, { ForwardedRef } from 'react';

export interface ButtonProps extends Omit<MuiButtonProps, 'size'> {
  label?: string;
  /**
   * The size of the component.
   * @default 'medium'
   */
  size?: 'small' | 'medium' | 'large';
  /**
   * The button displays a loading circular progress.
   * When loading, the button is disabled.
   * @default false
   */
  loading?: boolean;
}

const CIRCULAR_PROGRESS_HEIGHT = {
  small: 26,
  medium: 26,
  large: 26,
};

export const MainButton = React.forwardRef((props: ButtonProps, ref: ForwardedRef<any>): JSX.Element => {
  const size = props.size !== undefined ? props.size : 'medium';
  const variant = props.variant !== undefined ? props.variant : 'contained';
  const color = props.color !== undefined ? props.color : 'secondary';
  const { loading, ...rest } = props;
  return (
    <>
      <MuiButton
        {...rest}
        fullWidth={true}
        disableElevation={true}
        ref={ref}
        size={size}
        color={color}
        variant={variant}
        disabled={loading === true ? true : props.disabled}
        style={{ position: 'relative' }}
        sx={{
          '&:hover': {
            backgroundColor: variant === 'text' ? 'grey.A100' : `${color}.dark`,
          },
        }}
      >
        {loading ? (
          <CircularProgress size={CIRCULAR_PROGRESS_HEIGHT[size]} color={'secondary'} />
        ) : (
          <Typography
            variant={'h5'}
            sx={{
              height: CIRCULAR_PROGRESS_HEIGHT[size],
              color: (theme) =>
                props.disabled
                  ? theme.palette.text.disabled
                  : variant === 'text'
                  ? theme.palette.text.primary
                  : theme.palette.secondary.contrastText,
            }}
          >
            <strong>{props.label}</strong>
          </Typography>
        )}
      </MuiButton>
    </>
  );
});
