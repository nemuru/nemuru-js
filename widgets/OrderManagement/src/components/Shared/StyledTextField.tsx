import { styled, TextField } from '@mui/material';

export const StyledTextField = styled(TextField)(({ theme }) => ({
  '& .MuiInputBase-input': {
    fontSize: theme.typography.body2.fontSize,
    padding: '28px 12px 12px',
  },
  '& label': {
    color: '#9B97B3',
  },
  '& label.Mui-focused': {
    color: theme.palette.primary.main,
  },
  '& .MuiFilledInput-root': {
    backgroundColor: '#F8F8F8',
    border: 'none',
    '&.Mui-focused': {
      backgroundColor: 'transparent',
      boxShadow: `0px 0px 0px 2px ${theme.palette.primary.main} inset`,
    },
    '& fieldset': {
      borderColor: '#E1E1E5',
    },
    '&:hover fieldset': {
      borderColor: '#E1E1E5',
    },
  },
}));
