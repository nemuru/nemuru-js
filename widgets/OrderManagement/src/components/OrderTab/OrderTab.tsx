import { Box, Divider, Paper } from '@mui/material';
import { Currency } from '@nemuru-eng/js-api-client';
import React from 'react';
import { v4 as uuidv4 } from 'uuid';
import { useOrderManagementContext } from '../../contexts/OrderManagement/OrderManagementContext';
import OrderAction from '../../types/OrderAction';
import OrderLineInAction from '../../types/OrderLineInAction';
import OrderActionAccordion from './OrderActionAccordion';

const OrderTab: React.FC<{}> = () => {
  const { actions } = useOrderManagementContext();

  if (!actions) return null;

  let captureAmount = 0;
  let captureOrderLines: OrderLineInAction[] = [];
  actions
    .filter((action) => action.type === 'capture')
    .map((action) => {
      captureAmount += action.amount.value;
      action.orderLines.map((orderLine) => {
        captureOrderLines.push(orderLine);
      });
    });
  const captureAction: OrderAction = {
    id: uuidv4(),
    type: 'capture',
    createdAt: actions.filter((action) => action.type === 'capture').slice(-1)[0]?.createdAt,
    updatedAt: actions.filter((action) => action.type === 'capture').slice(-1)[0]?.updatedAt,
    amount: { value: captureAmount, currency: Currency.EUR },
    orderLines: captureOrderLines,
  };
  let refundAmount = 0;
  let refundOrderLines: OrderLineInAction[] = [];
  actions
    .filter((action) => action.type === 'refund')
    .map((action) => {
      refundAmount += action.amount.value;
      action.orderLines.map((orderLine) => {
        refundOrderLines.push(orderLine);
      });
    });
  const refundAction: OrderAction = {
    id: uuidv4(),
    type: 'refund',
    createdAt: actions.filter((action) => action.type === 'refund').slice(-1)[0]?.createdAt,
    updatedAt: actions.filter((action) => action.type === 'refund').slice(-1)[0]?.updatedAt,
    amount: { value: refundAmount, currency: Currency.EUR },
    orderLines: refundOrderLines,
  };

  return (
    <>
      <Box mt={4} mb={8} className={'appear-anim'}>
        {actions.some((action) => action.type === 'pending') && (
          <>
            <Paper variant={'outlined'}>
              {actions
                .filter((action) => action.type === 'pending')
                .map((action, key) => (
                  <div key={action.id}>
                    <OrderActionAccordion action={action} />
                  </div>
                ))}
            </Paper>
          </>
        )}

        {actions.some((action) => action.type !== 'pending') && (
          <Paper variant={'outlined'} sx={{ mt: 3 }}>
            {actions.some((action) => action.type === 'capture') && <OrderActionAccordion action={captureAction} />}
            {actions.some((action) => action.type === 'capture') &&
              actions.some((action) => action.type === 'refund') && <Divider light />}
            {actions.some((action) => action.type === 'refund') && <OrderActionAccordion action={refundAction} />}
          </Paper>
        )}
      </Box>
    </>
  );
};

export default OrderTab;
