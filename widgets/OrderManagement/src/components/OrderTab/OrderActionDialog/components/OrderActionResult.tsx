import { Avatar, Box, Typography } from '@mui/material';
import React from 'react';
import { MainButton } from '../../../Shared/MainButton';
import { ORDER_ACTION_INFO_BY_TYPE, ORDER_ACTION_STEPS } from '../config/config';

const OrderActionResult: React.FC<{
  result: string;
  selectedActionType: 'capture' | 'delete' | 'refund' | 'refund_amount';
  handleClose: () => void;
}> = ({ result, selectedActionType, handleClose }) => {
  const ICON_BY_RESULT = {
    [ORDER_ACTION_STEPS.APPROVED]: 'check',
    [ORDER_ACTION_STEPS.REJECTED]: 'highlight_off',
    [ORDER_ACTION_STEPS.ERROR]: 'warning_amber',
  };

  const TITLE_BY_RESULT = {
    [ORDER_ACTION_STEPS.APPROVED]: ORDER_ACTION_INFO_BY_TYPE[selectedActionType].CONFIRMATION_TITLE,
    [ORDER_ACTION_STEPS.REJECTED]: ORDER_ACTION_INFO_BY_TYPE[selectedActionType].REJECTED_TITLE,
    [ORDER_ACTION_STEPS.ERROR]: ORDER_ACTION_INFO_BY_TYPE[selectedActionType].ERROR_TITLE,
  };

  const DESCRIPTION_BY_RESULT = {
    [ORDER_ACTION_STEPS.APPROVED]: ORDER_ACTION_INFO_BY_TYPE[selectedActionType].CONFIRMATION_DESCRIPTION,
    [ORDER_ACTION_STEPS.REJECTED]: ORDER_ACTION_INFO_BY_TYPE[selectedActionType].REJECTED_DESCRIPTION,
    [ORDER_ACTION_STEPS.ERROR]: ORDER_ACTION_INFO_BY_TYPE[selectedActionType].ERROR_DESCRIPTION,
  };

  const BUTTON_BY_RESULT = {
    [ORDER_ACTION_STEPS.APPROVED]: 'Aceptar',
    [ORDER_ACTION_STEPS.REJECTED]: 'Volver',
    [ORDER_ACTION_STEPS.ERROR]: 'Volver',
  };

  return (
    <>
      <Box className={'appear-anim'}>
        <Avatar
          sx={{
            mb: 4,
            bgcolor: result === ORDER_ACTION_STEPS.APPROVED ? 'primary.main' : '#E1E1E5',
            width: 60,
            height: 60,
            color: result === ORDER_ACTION_STEPS.APPROVED ? 'primary.contrastText' : '#FFF',
          }}
        >
          <i className={'material-icons-outlined'} style={{ fontSize: 32 }}>
            {ICON_BY_RESULT[result]}
          </i>
        </Avatar>
        <Typography variant={'h4'} sx={{ mb: 2 }}>
          <strong>{TITLE_BY_RESULT[result]}</strong>
        </Typography>
        {DESCRIPTION_BY_RESULT[result]}
      </Box>
      <MainButton label={BUTTON_BY_RESULT[result]} variant={'contained'} color={'secondary'} onClick={handleClose} />
    </>
  );
};

export default OrderActionResult;
