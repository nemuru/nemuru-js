import { Box, Typography } from '@mui/material';
import {
  getOrderDetailsByLoanId,
  patchDeleteOrderLinesInLender,
  postCapture,
  postRefund,
} from '@nemuru-eng/js-api-client';
import React from 'react';
import { v4 as uuidv4 } from 'uuid';
import { useOrderManagementContext } from '../../../../contexts/OrderManagement/OrderManagementContext';
import BasicTable from '../../../Shared/BasicTable';
import { columns } from '../../../Shared/Columns';
import { MainButton } from '../../../Shared/MainButton';
import { ORDER_ACTION_INFO_BY_TYPE, ORDER_ACTION_STEPS } from '../config/config';

const OrderLineAction: React.FC<Props> = ({
  loading,
  setStep,
  setLoading,
  ensureCaptureIsApproved,
  ensureRefundIsApproved,
  handleClose,
}) => {
  const { order, loanId, selectedOrderLines, selectedActionType, updateOrderManagement } = useOrderManagementContext();

  if (!order || !loanId || !selectedOrderLines || !selectedActionType || selectedActionType === 'refund_amount')
    return null;

  let orderLinesTotalAmount = 0;
  selectedOrderLines.forEach((orderLine) => (orderLinesTotalAmount += orderLine.totalPriceWithTax.value));

  const handleSubmit = async () => {
    if (selectedActionType === 'capture') {
      const captureId = uuidv4();
      setLoading(true);
      try {
        await postCapture({
          id: captureId,
          orderId: order.order.id,
          orderLineIds: selectedOrderLines.map((orderLine) => orderLine.id),
        });
        await ensureCaptureIsApproved(captureId);
      } catch (err) {
        setStep(ORDER_ACTION_STEPS.ERROR);
      }
    }
    if (selectedActionType === 'refund') {
      const refundId = uuidv4();
      setLoading(true);
      try {
        await postRefund({
          id: refundId,
          orderId: order.order.id,
          orderLineIds: selectedOrderLines.map((orderLine) => orderLine.id),
        });
        await ensureRefundIsApproved(refundId);
      } catch (err) {
        setStep(ORDER_ACTION_STEPS.ERROR);
      }
    }
    if (selectedActionType === 'delete') {
      setLoading(true);
      try {
        await patchDeleteOrderLinesInLender({
          orderId: order.order.id,
          orderLineIds: selectedOrderLines.map((orderLine) => orderLine.id),
        });
        const newOrder = await getOrderDetailsByLoanId({ loanId });
        updateOrderManagement({ order: newOrder });
        setStep(ORDER_ACTION_STEPS.APPROVED);
      } catch (err) {
        setStep(ORDER_ACTION_STEPS.ERROR);
      }
    }
    loading && setLoading(false);
  };

  return (
    <>
      <Box className={'appear-anim'} mb={2}>
        <Typography variant={'h4'} sx={{ mb: 2 }}>
          <strong>{ORDER_ACTION_INFO_BY_TYPE[selectedActionType].ACTION_TITLE}</strong>
        </Typography>
        <Box mb={4}>{ORDER_ACTION_INFO_BY_TYPE[selectedActionType].ACTION_DESCRIPTION}</Box>
        <BasicTable
          rows={selectedOrderLines.map((orderLine) => ({
            ...orderLine,
            id: orderLine.id,
          }))}
          columns={columns()}
          selectable={false}
          totalAmount={{
            value: orderLinesTotalAmount,
            currency: selectedOrderLines[0].totalPriceWithTax.currency,
          }}
        />
      </Box>
      <Box>
        <MainButton
          label={`${ORDER_ACTION_INFO_BY_TYPE[selectedActionType].ACTION_BUTTON} ${orderLinesTotalAmount.toFixed(2)} €`}
          variant={'contained'}
          color={'secondary'}
          onClick={handleSubmit}
          loading={loading}
        />
        <Box mt={2} />
        <MainButton label={'Cancelar'} variant={'text'} color={'secondary'} onClick={handleClose} disabled={loading} />
      </Box>
    </>
  );
};

export default OrderLineAction;

interface Props {
  loading: boolean;
  setStep: (step: string) => void;
  setLoading: (loading: boolean) => void;
  ensureCaptureIsApproved: (captureId: string) => void;
  ensureRefundIsApproved: (refundId: string) => void;
  handleClose: () => void;
}
