import { Box, Typography } from '@mui/material';
import { postRefund } from '@nemuru-eng/js-api-client';
import React, { useState } from 'react';
import { v4 as uuidv4 } from 'uuid';
import { useOrderManagementContext } from '../../../../contexts/OrderManagement/OrderManagementContext';
import { MainButton } from '../../../Shared/MainButton';
import { StyledTextField } from '../../../Shared/StyledTextField';
import { ORDER_ACTION_INFO_BY_TYPE, ORDER_ACTION_STEPS } from '../config/config';

const OrderAmountAction: React.FC<Props> = ({ loading, setStep, setLoading, ensureRefundIsApproved, handleClose }) => {
  const { order, actions, maxToRefund, selectedActionType } = useOrderManagementContext();

  const [amountToRefund, setAmountToRefund] = useState<string>('');
  const [refundDescription, setRefundDescription] = useState<string>('');
  const [invalidNumberError, setInvalidNumberError] = useState<boolean>(false);
  const [boundaryNumberError, setBoundaryNumberError] = useState<boolean>(false);

  if (!order || !actions || !selectedActionType || maxToRefund === undefined) return null;

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const value = event.target.value.replace(',', '.');
    if (!value || value === '') {
      setAmountToRefund(value);
      invalidNumberError && setInvalidNumberError(false);
      boundaryNumberError && setBoundaryNumberError(false);
      return;
    }
    const amount = toNumber(value);
    if (!amount) {
      !invalidNumberError && setInvalidNumberError(true);
    } else {
      invalidNumberError && setInvalidNumberError(false);
      handleNumberBoundary(amount);
    }
    setAmountToRefund(value);
  };

  const toNumber = (value: string | undefined): number | undefined => {
    if (value === '') return undefined;
    const result = Number(value);
    return Number.isNaN(result) ? undefined : result;
  };

  const handleNumberBoundary = (value: number) => {
    if (value < 0 || value > maxToRefund) {
      !boundaryNumberError && setBoundaryNumberError(true);
    } else {
      boundaryNumberError && setBoundaryNumberError(false);
    }
  };

  const handleSubmit = async () => {
    const refundId = uuidv4();
    setLoading(true);
    try {
      await postRefund({
        id: refundId,
        orderId: order.order.id,
        orderLineIds: [],
        amount: toNumber(amountToRefund),
        ...(refundDescription !== '' && { description: refundDescription }),
      });
      await ensureRefundIsApproved(refundId);
    } catch (err) {
      setStep(ORDER_ACTION_STEPS.ERROR);
    }
    loading && setLoading(false);
  };

  return (
    <>
      <Box className={'appear-anim'} mb={2}>
        <Typography variant={'h4'} sx={{ mb: 2 }}>
          <strong>{ORDER_ACTION_INFO_BY_TYPE[selectedActionType].ACTION_TITLE}</strong>
        </Typography>
        <Box mb={4}>{`${ORDER_ACTION_INFO_BY_TYPE[selectedActionType].ACTION_DESCRIPTION} ${maxToRefund.toFixed(
          2,
        )} €.`}</Box>
        <StyledTextField
          error={invalidNumberError || boundaryNumberError}
          helperText={
            invalidNumberError
              ? 'Importe inválido'
              : boundaryNumberError
              ? `El importe debe estar comprendido entre ${(0).toFixed(2)} y ${maxToRefund.toFixed(2)} €`
              : ''
          }
          fullWidth
          sx={{ mb: 2 }}
          label={'Importe a devolver'}
          variant="filled"
          size={'medium'}
          InputProps={{
            disableUnderline: true,
            sx: {
              borderRadius: '6px',
            },
          }}
          value={amountToRefund}
          onChange={handleChange}
        />
        <StyledTextField
          fullWidth
          sx={{ mb: 2 }}
          label={'Descripción'}
          variant="filled"
          size={'medium'}
          InputProps={{
            disableUnderline: true,
            sx: {
              borderRadius: '6px',
            },
          }}
          value={refundDescription}
          onChange={(event) => setRefundDescription(event.target.value)}
        />
      </Box>
      <Box>
        <MainButton
          label={`${ORDER_ACTION_INFO_BY_TYPE[selectedActionType].ACTION_BUTTON} ${(
            toNumber(amountToRefund) ?? 0
          ).toFixed(2)} €`}
          color={'secondary'}
          variant={'contained'}
          onClick={handleSubmit}
          loading={loading}
          disabled={amountToRefund === undefined || amountToRefund === '' || invalidNumberError || boundaryNumberError}
        />
        <Box mt={2} />
        <MainButton label={'Cancelar'} color={'secondary'} variant={'text'} onClick={handleClose} disabled={loading} />
      </Box>
    </>
  );
};

export default OrderAmountAction;

interface Props {
  loading: boolean;
  setStep: (step: string) => void;
  setLoading: (loading: boolean) => void;
  ensureRefundIsApproved: (refundId: string) => void;
  handleClose: () => void;
}
