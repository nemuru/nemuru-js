import { Typography } from '@mui/material';
import React from 'react';

export const ORDER_ACTION_STEPS = {
  ACTION: 'ACTION',
  APPROVED: 'APPROVED',
  REJECTED: 'REJECTED',
  ERROR: 'ERROR',
};

const ERROR_TITLE = '¡Oooooops!';
const ERROR_DESCRIPTION =
  'Algo salió mal y no ha sido posible completar tu petición. Por favor, vuélvelo a intentar o ponte en contacto con nosotros.';

export const ORDER_ACTION_INFO_BY_TYPE = {
  capture: {
    ACTION_TITLE: 'Nuevo envío',
    ACTION_DESCRIPTION: (
      <>
        <Typography variant={'body2'}>Vas a enviar los siguientes productos a tu cliente:</Typography>
      </>
    ),
    ACTION_BUTTON: 'Capturar envío de',
    CONFIRMATION_TITLE: '¡Envío confirmado!',
    CONFIRMATION_DESCRIPTION: (
      <>
        <Typography variant={'body2'}>A continuación:</Typography>
        <ul>
          <li>
            <Typography variant={'body2'}>
              Se inculirá el importe de este envío en el siguiente desembolso que recibas.
            </Typography>
          </li>
          <li>
            <Typography variant={'body2'}>
              Se procesará el cobro a tu cliente (en caso de no haber pagado todavía).
            </Typography>
          </li>
        </ul>
      </>
    ),
    REJECTED_TITLE: 'Envío rechazado',
    REJECTED_DESCRIPTION:
      'La solicitud de envío ha sido rechazada por la entidad financiera. Por favor, vuélvelo a intentar o ponte en contacto con nosotros.',
    ERROR_TITLE,
    ERROR_DESCRIPTION,
  },
  refund: {
    ACTION_TITLE: 'Nueva devolución',
    ACTION_DESCRIPTION: (
      <>
        <Typography variant={'body2'}>Esta devolución generará una deducción en tu próximo abono:</Typography>
      </>
    ),
    ACTION_BUTTON: 'Devolver',
    CONFIRMATION_TITLE: '¡Devolución confirmada!',
    CONFIRMATION_DESCRIPTION: (
      <>
        <Typography variant={'body2'}>A continuación:</Typography>
        <ul>
          <li>
            <Typography variant={'body2'}>Se ajustará el plan de pagos de tu cliente.</Typography>
          </li>
          <li>
            <Typography variant={'body2'}>
              Se compensará (deducción) este importe en el siguiente desembolso que recibas.
            </Typography>
          </li>
        </ul>
      </>
    ),
    REJECTED_TITLE: 'Devolución rechazada',
    REJECTED_DESCRIPTION:
      'La solicitud de devolución ha sido rechazada por la entidad financiera. Por favor, vuélvelo a intentar o ponte en contacto con nosotros.',
    ERROR_TITLE,
    ERROR_DESCRIPTION,
  },
  refund_amount: {
    ACTION_TITLE: 'Nueva devolución',
    ACTION_DESCRIPTION: 'Puedes devolver hasta un total de',
    ACTION_BUTTON: 'Devolver',
    CONFIRMATION_TITLE: '¡Devolución confirmada!',
    CONFIRMATION_DESCRIPTION: (
      <>
        <Typography variant={'body2'}>A continuación:</Typography>
        <ul>
          <li>
            <Typography variant={'body2'}>Se ajustará el plan de pagos de tu cliente.</Typography>
          </li>
          <li>
            <Typography variant={'body2'}>
              Se compensará (deducción) este importe en el siguiente desembolso que recibas.
            </Typography>
          </li>
        </ul>
      </>
    ),
    REJECTED_TITLE: 'Devolución rechazada',
    REJECTED_DESCRIPTION:
      'La solicitud de devolución ha sido rechazada por la entidad financiera. Por favor, vuélvelo a intentar o ponte en contacto con nosotros.',
    ERROR_TITLE,
    ERROR_DESCRIPTION,
  },
  delete: {
    ACTION_TITLE: 'Eliminar productos',
    ACTION_DESCRIPTION: (
      <>
        <Typography variant={'body2'}>Los siguientes productos serán eliminados del pedido:</Typography>
      </>
    ),
    ACTION_BUTTON: 'Eliminar',
    CONFIRMATION_TITLE: '¡Cancelación confirmada!',
    CONFIRMATION_DESCRIPTION: (
      <>
        <Typography variant={'body2'}>A continuación:</Typography>
        <ul>
          <li>
            <Typography variant={'body2'}>Se ajustará el plan de pagos de tu cliente.</Typography>
          </li>
          <li>
            <Typography variant={'body2'}>
              Se compensará (deducción) este importe en el siguiente desembolso que recibas.
            </Typography>
          </li>
        </ul>
      </>
    ),
    REJECTED_TITLE: ERROR_TITLE,
    REJECTED_DESCRIPTION: ERROR_DESCRIPTION,
    ERROR_TITLE,
    ERROR_DESCRIPTION,
  },
};
