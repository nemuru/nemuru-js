import { Box } from '@mui/material';
import { CaptureStatus, getOrderDetailsByLoanId, RefundStatus } from '@nemuru-eng/js-api-client';
import React, { useState } from 'react';
import { useOrderManagementContext } from '../../../contexts/OrderManagement/OrderManagementContext';
import OrderActionResult from './components/OrderActionResult';
import OrderAmountAction from './components/OrderAmountAction';
import OrderLineAction from './components/OrderLineAction';
import { ORDER_ACTION_STEPS } from './config/config';

const OrderActionDialog: React.FC = () => {
  const { order, selectedOrderLines, selectedActionType, handleAction, updateOrderManagement } =
    useOrderManagementContext();
  const [step, setStep] = useState<string>(ORDER_ACTION_STEPS.ACTION);
  const [loading, setLoading] = useState(false);

  if (!order || !selectedActionType) return null;

  function ensureCaptureIsApproved(captureId: string) {
    let attempts = 0;
    return new Promise(function (resolve, reject) {
      (async function waitForCaptureToBeProcessed() {
        if (order) {
          attempts += 1;
          const newOrder = await getOrderDetailsByLoanId({ loanId: order.id });
          const newCapture = newOrder.captures.find((capture) => capture.id === captureId);
          if (newCapture?.notified === true) {
            newCapture.status === CaptureStatus.APPROVED;
            switch (newCapture.status) {
              case CaptureStatus.APPROVED:
                return resolve(
                  (() => {
                    updateOrderManagement({ order: newOrder, captures: newOrder.captures });
                    handleAction && handleAction('order_capture_created');
                    setLoading(false);
                    setStep(ORDER_ACTION_STEPS.APPROVED);
                  })(),
                );
              case CaptureStatus.REJECTED:
                return resolve(
                  (() => {
                    setLoading(false);
                    setStep(ORDER_ACTION_STEPS.REJECTED);
                  })(),
                );
            }
          }
          if (attempts > 20) {
            return resolve(
              (() => {
                setLoading(false);
                setStep(ORDER_ACTION_STEPS.ERROR);
              })(),
            );
          }
        }
        setTimeout(await waitForCaptureToBeProcessed, 1000);
      })();
    });
  }

  function ensureRefundIsApproved(refundId: string) {
    let attempts = 0;
    return new Promise(function (resolve, reject) {
      (async function waitForRefundToBeProcessed() {
        if (order) {
          attempts += 1;
          const newOrder = await getOrderDetailsByLoanId({ loanId: order.id });
          const newRefund = newOrder.refunds.find((refund) => refund.id === refundId);
          if (newRefund?.notified === true) {
            switch (newRefund.status) {
              case RefundStatus.APPROVED:
                return resolve(
                  (() => {
                    updateOrderManagement({ order: newOrder, refunds: newOrder.refunds });
                    handleAction && handleAction('order_refund_created');
                    setLoading(false);
                    setStep(ORDER_ACTION_STEPS.APPROVED);
                  })(),
                );
              case RefundStatus.REJECTED:
                return resolve(
                  (() => {
                    setLoading(false);
                    setStep(ORDER_ACTION_STEPS.REJECTED);
                  })(),
                );
            }
          }
          if (attempts > 20) {
            return resolve(
              (() => {
                setLoading(false);
                setStep(ORDER_ACTION_STEPS.ERROR);
              })(),
            );
          }
        }
        setTimeout(await waitForRefundToBeProcessed, 1000);
      })();
    });
  }

  const handleClose = () => {
    updateOrderManagement({ step: 'order' });
  };

  return (
    <>
      <Box
        my={5}
        height={'100%'}
        flexGrow={1}
        display={'flex'}
        flexDirection={'column'}
        justifyContent={'space-between'}
      >
        {step === ORDER_ACTION_STEPS.ACTION && (
          <>
            {selectedOrderLines && selectedActionType !== 'refund_amount' && (
              <OrderLineAction
                setStep={setStep}
                loading={loading}
                setLoading={setLoading}
                ensureCaptureIsApproved={ensureCaptureIsApproved}
                ensureRefundIsApproved={ensureRefundIsApproved}
                handleClose={handleClose}
              />
            )}
            {!selectedOrderLines && selectedActionType === 'refund_amount' && (
              <OrderAmountAction
                setStep={setStep}
                loading={loading}
                setLoading={setLoading}
                ensureRefundIsApproved={ensureRefundIsApproved}
                handleClose={handleClose}
              />
            )}
          </>
        )}
        {step !== ORDER_ACTION_STEPS.ACTION && (
          <OrderActionResult result={step} selectedActionType={selectedActionType} handleClose={handleClose} />
        )}
      </Box>
    </>
  );
};

export default OrderActionDialog;
