import {
  Accordion,
  AccordionActions,
  AccordionDetails,
  AccordionSummary,
  Box,
  Button,
  Typography,
} from '@mui/material';
import React, { useState } from 'react';
import { useOrderManagementContext } from '../../contexts/OrderManagement/OrderManagementContext';
import OrderAction from '../../types/OrderAction';
import OrderLineInAction from '../../types/OrderLineInAction';
import ActionIcon from '../Shared/ActionIcon';
import BasicTable from '../Shared/BasicTable';
import { columns } from '../Shared/Columns';

const OrderActionAccordion: React.FC<{ action: OrderAction }> = ({ action }) => {
  const [expanded, setExpanded] = useState<string | false>(action.type === 'pending' ? 'panel1' : false);
  const [orderLinesSelected, setOrderLinesSelected] = useState<OrderLineInAction[]>([]);

  const { maxToRefund, updateOrderManagement } = useOrderManagementContext();

  const handleChange = (panel: string) => (event: React.SyntheticEvent, isExpanded: boolean) => {
    setExpanded(isExpanded ? panel : false);
  };

  const handleSelection = (orderLineIds: string[]) => {
    const orderLines = action.orderLines.filter((orderLine) => orderLineIds.includes(orderLine.id));
    setOrderLinesSelected(orderLines);
  };

  const rows = action.orderLines.map((orderLine) => ({ ...orderLine, id: orderLine.id }));

  const isRowSelectable = (rowData: OrderLineInAction) => {
    return (
      rowData.status === 'enabled' &&
      !(action.type === 'capture' && maxToRefund !== undefined && rowData.totalPriceWithTax.value > maxToRefund)
    );
  };

  if (maxToRefund === undefined) return null;

  return (
    <>
      <Accordion
        disableGutters
        variant={'elevation'}
        elevation={0}
        expanded={expanded === 'panel1'}
        onChange={handleChange('panel1')}
        sx={{
          borderRadius: 1,
          ':before': { backgroundColor: 'transparent' },
          ':first-of-type': { borderRadius: 1 },
          ':last-of-type': { borderRadius: 1 },
        }}
      >
        <AccordionSummary expandIcon={<i className={'material-icons-outlined'}>expand_more</i>}>
          <Box width={'100%'} display={'flex'} alignItems={'center'} justifyContent={'space-between'}>
            <Box display={'flex'} alignItems={'center'}>
              <Box mr={2}>
                <ActionIcon type={action.type} size={'large'} />
              </Box>
              <Box display={'block'}>
                <Typography sx={{ flexShrink: 0 }} variant={'body2'}>
                  <strong>{EVENT_TYPE_MAP[action.type]}</strong>
                </Typography>
                <Typography sx={{ flexShrink: 0 }} variant={'caption'} color={'text.secondary'}>
                  {action.createdAt.toLocaleDateString('es-ES', {
                    day: 'numeric',
                    month: 'short',
                    year: 'numeric',
                    hour: '2-digit',
                    minute: '2-digit',
                  })}
                </Typography>
              </Box>
            </Box>
            <Typography variant={'body2'} sx={{ mr: 2 }}>
              <strong>{`${((action.type === 'refund' ? -1 : 1) * action.amount.value).toFixed(2)} €`}</strong>
            </Typography>
          </Box>
        </AccordionSummary>
        <AccordionDetails sx={{ pb: 0 }}>
          <div style={{ width: '100%' }}>
            <BasicTable
              rows={rows}
              columns={columns()}
              selectable={action.type !== 'refund' && rows.some((row) => isRowSelectable(row))}
              handleSelection={handleSelection}
              isRowSelectable={isRowSelectable}
              totalAmount={{
                value: (action.type === 'refund' ? -1 : 1) * action.amount.value,
                currency: action.amount.currency,
              }}
            />
          </div>
        </AccordionDetails>
        {action.type !== 'refund' && (
          <AccordionActions sx={{ px: 2, pb: 2 }}>
            {['pending'].includes(action.type) && (
              <Button
                disableElevation
                size={'small'}
                variant={'contained'}
                color={'secondary'}
                disabled={orderLinesSelected.length === 0}
                sx={{ py: '8px', px: '12px' }}
                onClick={() => {
                  updateOrderManagement({
                    step: 'action',
                    selectedOrderLines: orderLinesSelected,
                    selectedActionType: 'capture',
                  });
                }}
              >
                <strong>Enviar productos</strong>
              </Button>
            )}
            {['capture'].includes(action.type) && maxToRefund > 0 && (
              <Button
                disableElevation
                size={'small'}
                variant={'text'}
                color={'secondary'}
                sx={{ py: '8px', px: '12px', backgroundColor: 'rgba(1, 0, 7, 0.04)' }}
                onClick={() => {
                  updateOrderManagement({
                    step: 'action',
                    selectedOrderLines: undefined,
                    selectedActionType: 'refund_amount',
                  });
                }}
              >
                <strong>Devolver importe</strong>
              </Button>
            )}
            {['capture'].includes(action.type) && rows.some((row) => isRowSelectable(row)) && (
              <Button
                disableElevation
                size={'small'}
                variant={'contained'}
                color={'secondary'}
                disabled={orderLinesSelected.length === 0}
                sx={{ py: '8px', px: '12px' }}
                onClick={() => {
                  updateOrderManagement({
                    step: 'action',
                    selectedOrderLines: orderLinesSelected,
                    selectedActionType: 'refund',
                  });
                }}
              >
                <strong>Devolver productos</strong>
              </Button>
            )}
            {['pending'].includes(action.type) && rows.some((row) => isRowSelectable(row)) && (
              <Button
                disableElevation
                size={'small'}
                variant={'contained'}
                color={'secondary'}
                disabled={orderLinesSelected.length === 0}
                sx={{ py: '8px', px: '12px' }}
                onClick={() => {
                  updateOrderManagement({
                    step: 'action',
                    selectedOrderLines: orderLinesSelected,
                    selectedActionType: 'delete',
                  });
                }}
              >
                <strong>Eliminar</strong>
              </Button>
            )}
          </AccordionActions>
        )}
      </Accordion>
    </>
  );
};

export default OrderActionAccordion;

const EVENT_TYPE_MAP = {
  capture: 'Total enviado',
  refund: 'Total devuelto',
  pending: 'Pendiente de envío',
};
