import { Accordion, AccordionDetails, AccordionSummary, Box, Divider, Typography } from '@mui/material';
import React, { useState } from 'react';
import OrderAction from '../../types/OrderAction';
import './SummaryActionsAccordion.css';
import BasicTable from '../Shared/BasicTable';
import { columns } from '../Shared/Columns';

const ActionDetailAccordion: React.FC<{ action: OrderAction }> = ({ action }) => {
  const [expanded, setExpanded] = useState<string | false>(false);

  const handleChange = (panel: string) => (event: React.SyntheticEvent, isExpanded: boolean) => {
    setExpanded(isExpanded ? panel : false);
  };

  return (
    <>
      <Accordion
        disableGutters
        variant={'elevation'}
        elevation={0}
        expanded={expanded === 'panel1'}
        onChange={handleChange('panel1')}
        sx={{
          borderRadius: 1,
          ':before': { backgroundColor: 'transparent' },
          ':first-of-type': { borderRadius: 1 },
          ':last-of-type': { borderRadius: 1 },
        }}
      >
        <AccordionSummary
          sx={{
            m: 0,
            p: 0,
            '& .MuiAccordionSummary-content': { m: 0, alignItems: 'flex-start' },
            display: 'flex',
            justifyContent: 'space-between',
          }}
          // expandIcon={<i className={'material-icons-outlined'}>expand_more</i>}
        >
          <Box display={'grid'} flexGrow={1}>
            <Typography variant={'body2'}>
              <strong>
                {action.type === 'capture'
                  ? `Envío nº ${action.number}`
                  : action.type === 'refund'
                  ? `Devolución nº ${action.number}`
                  : 'Pedido creado'}
              </strong>
            </Typography>
            <Typography variant={'caption'} color={'text.secondary'}>
              {action.createdAt.toLocaleDateString('es-ES', {
                day: 'numeric',
                month: 'short',
                year: 'numeric',
                hour: '2-digit',
                minute: '2-digit',
              })}
            </Typography>
            <Typography variant={'caption'} color={'text.secondary'} sx={{ textDecoration: 'underline' }}>
              Ver detalle
            </Typography>
          </Box>
          <Typography variant={'body2'}>
            <strong>
              {action.type === 'refund'
                ? `  ${(-1 * action.amount.value).toFixed(2)} €`
                : `  ${action.amount.value.toFixed(2)} €`}
            </strong>
          </Typography>
        </AccordionSummary>
        <AccordionDetails sx={{ px: 0, pt: 2, pb: 1 }}>
          <BasicTable
            rows={action.orderLines}
            columns={columns(true)}
            selectable={false}
            dense={true}
            totalAmount={{
              value: (action.type === 'refund' ? -1 : 1) * action.amount.value,
              currency: action.amount.currency,
            }}
          />
        </AccordionDetails>
      </Accordion>
    </>
  );
};

export default ActionDetailAccordion;
