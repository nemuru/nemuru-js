import { Box, Divider, Typography } from '@mui/material';
import React from 'react';
import { useOrderManagementContext } from '../../contexts/OrderManagement/OrderManagementContext';
import SummaryActionsAccordion from './SummaryActionsAccordion';

const SummaryTab: React.FC = () => {
  const { order, actions } = useOrderManagementContext();

  if (!order || !actions) return null;

  let totalCaptured = order.totalCapturedAmount.value;
  let totalRefunded = order.totalRefundedAmount.value;
  let leftToCapture = order.totalOrderAmount.value - totalCaptured;
  let leftToRefund = totalCaptured - totalRefunded;

  return (
    <>
      <Box mt={4} mb={5} sx={{ px: { xs: 0, sm: 2 } }} className={'appear-anim'}>
        <Typography sx={{ flexShrink: 0, mb: 3 }} variant={'h5'}>
          <strong>Resumen</strong>
        </Typography>
        <Box p={2} bgcolor={'#0100070A'} borderRadius={2}>
          <Box display={'flex'} alignItems={'center'} justifyContent={'space-between'}>
            <Typography variant={'body2'}>Total del pedido</Typography>
            <Typography variant={'body2'}>
              <strong>{order.totalOrderAmount.value.toFixed(2)} €</strong>
            </Typography>
          </Box>
          <Box mt={2} />
          <Box display={'flex'} alignItems={'center'} justifyContent={'space-between'}>
            <Typography variant={'body2'}>Pendiente enviar</Typography>
            <Typography variant={'body2'}>{leftToCapture.toFixed(2)} €</Typography>
          </Box>
          <Box mt={2} />
          <Divider light />
          <Box mt={2} />
          <Box display={'flex'} alignItems={'center'} justifyContent={'space-between'}>
            <Typography variant={'body2'}>Total enviado</Typography>
            <Typography variant={'body2'}>
              <strong>{totalCaptured.toFixed(2)} €</strong>
            </Typography>
          </Box>
          <Box mt={2} />
          {/*<Divider light />*/}
          <Box display={'flex'} alignItems={'center'} justifyContent={'space-between'}>
            <Typography variant={'body2'}>Total devuelto</Typography>
            <Typography variant={'body2'}>{(-1 * totalRefunded).toFixed(2)} €</Typography>
          </Box>
          <Box mt={2} />
          <Divider light />
          <Box mt={2} />
          <Box display={'flex'} alignItems={'center'} justifyContent={'space-between'}>
            <Typography variant={'body2'}>Facturado a cliente</Typography>
            <Typography variant={'body2'}>
              <strong>{leftToRefund.toFixed(2)} €</strong>
            </Typography>
          </Box>
        </Box>
        <Box mt={3} />
        {/*<Divider light />*/}
        {actions.filter((action) => action.type !== 'pending').length > 0 && (
          <SummaryActionsAccordion order={order} actions={actions.filter((action) => action.type !== 'pending')} />
        )}
      </Box>
    </>
  );
};

export default SummaryTab;
