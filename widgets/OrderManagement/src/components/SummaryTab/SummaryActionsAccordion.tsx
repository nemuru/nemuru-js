import { Box, Typography } from '@mui/material';
import { OrderDetails } from '@nemuru-eng/js-api-client';
import React from 'react';
import OrderAction from '../../types/OrderAction';
import ActionIcon from '../Shared/ActionIcon';
import ActionDetailAccordion from './ActionDetailAccordion';
import './SummaryActionsAccordion.css';

const SummaryActionsAccordion: React.FC<{ order: OrderDetails; actions: OrderAction[] }> = ({ order, actions }) => {
  const orderAction: OrderAction = {
    id: order.order.id,
    type: 'pending',
    createdAt: order.createdAt,
    updatedAt: order.updatedAt,
    amount: order.totalOrderAmount,
    orderLines: order.order.orderLines.map((orderLine) => ({
      ...orderLine,
      type: 'pending',
      status: 'enabled',
    })),
  };

  return (
    <>
      <Box mt={4}>
        <Typography sx={{ flexShrink: 0 }} variant={'h5'}>
          <strong>Eventos</strong>
        </Typography>
        <Box display={'flex'} mt={3}>
          <Box mr={2} display={'flex'} flexDirection={'column'} alignItems={'center'}>
            <ActionIcon type={'pending'} size={'small'} />
            <Box className={'vertical-line'} />
          </Box>
          <Box pb={3} flexGrow={1}>
            <ActionDetailAccordion action={orderAction} />
          </Box>
        </Box>
        {actions.map((action, key) => (
          <Box key={key} display={'flex'}>
            <Box mr={2} display={'flex'} flexDirection={'column'} alignItems={'center'}>
              <ActionIcon type={action.type} size={'small'} />
              {actions.length - 1 > key && <Box className={'vertical-line'} />}
            </Box>
            <Box pb={3} flexGrow={1}>
              <ActionDetailAccordion action={action} />
            </Box>
          </Box>
        ))}
      </Box>
    </>
  );
};

export default SummaryActionsAccordion;
