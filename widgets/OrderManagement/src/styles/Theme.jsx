const Theme = {
  palette: {
    primary: {
      main: '#4CC6CD',
    },
    secondary: {
      main: '#010007',
      contrastText: '#fff',
    },
    warning: {
      main: '#ffca28',
      contrastText: '#ffffff',
      light: 'rgba(255, 202, 40, 0.15)',
    },
    info: {
      main: '#36a3f7',
      contrastText: '#ffffff',
      light: '#E1F1FD',
    },
    success: {
      main: '#4dd970',
      contrastText: '#ffffff',
      light: 'rgba(73,217,106,0.15)',
    },
    error: {
      main: '#ef5957',
      contrastText: '#ffffff',
      light: 'rgba(239,87,87,0.15)',
    },
    text: {
      primary: '#010007',
      secondary: '#9B97B3',
      disabled: '#F0F0F5',
    },
    grey: {
      [300]: '#EBEBEB',
    },
  },
  typography: {
    fontFamily: '"Poppins", sans-serif',
    h1: {
      fontStyle: 'normal',
      fontWeight: 'bold',
      fontSize: '32px',
      // lineHeight: '44px',
      '@media (max-width:425px)': {
        fontSize: '30px',
      },
    },
    h2: {
      fontStyle: 'normal',
      fontWeight: 'bold',
      fontSize: '28px',
      // lineHeight: '40px',
      '@media (max-width:425px)': {
        fontSize: '26px',
      },
    },
    h3: {
      fontStyle: 'normal',
      fontWeight: 600,
      fontSize: '24px',
      // lineHeight: '32px',
      '@media (max-width:425px)': {
        fontSize: '22px',
      },
    },
    h4: {
      fontStyle: 'normal',
      fontWeight: 600,
      fontSize: '22px',
      // lineHeight: '36px',
      letterSpacing: '0.03em',
      '@media (max-width:425px)': {
        fontSize: '20px',
      },
    },
    h5: {
      fontStyle: 'normal',
      fontWeight: 600,
      fontSize: '18px',
      // lineHeight: '26px',
      letterSpacing: '0.02em',
      '@media (max-width:425px)': {
        fontSize: '16px',
      },
    },
    h6: {
      fontStyle: 'normal',
      fontWeight: 600,
      fontSize: '14px',
      // lineHeight: '24px',
      letterSpacing: '0.02em',
    },
    body1: {
      fontStyle: 'normal',
      fontWeight: 400,
      fontSize: '16px',
      // lineHeight: '26px',
      letterSpacing: '0.01em',
      '@media (max-width:425px)': {
        fontSize: '15px',
      },
    },
    body2: {
      fontStyle: 'normal',
      fontWeight: 400,
      fontSize: '15px',
      // lineHeight: '24px',
      '@media (max-width:425px)': {
        fontSize: '14px',
      },
    },
    caption: {
      fontStyle: 'normal',
      fontWeight: 400,
      fontSize: '13px',
      // lineHeight: '20px',
      '@media (max-width:425px)': {
        fontSize: '13px',
      },
    },
    // fontFamily: '"Open Sans", sans-serif',
  },
  shadows: ['none', '0px 0px 10px rgb(0 0 0 / 6%)', '0px 0px 20px rgb(0 0 0 / 6%)', '0px 0px 30px rgb(0 0 0 / 6%)'],
  components: {
    // MuiForm,
    MuiButton: {
      styleOverrides: {
        root: {
          // ':hover': {
          //   backgroundColor: 'transparent',
          // },
        },
      },
      variants: [
        {
          props: { variant: 'rounded', disabled: false },
          style: {
            textTransform: 'none',
            color: '#010007',
            border: '2px solid #010007',
            padding: '12px 16px',
            borderRadius: '100px',
          },
        },
        {
          props: { variant: 'rounded', disabled: true },
          style: {
            textTransform: 'none',
            color: '#E1E1E5',
            border: '2px solid #E1E1E5',
            padding: '12px 16px',
            borderRadius: '100px',
          },
        },
        {
          props: { variant: 'contained', disabled: false },
          style: {
            textTransform: 'none',
            color: '#FFF',
            padding: '12px 16px',
          },
        },
        {
          props: { variant: 'contained', disabled: true },
          style: {
            textTransform: 'none',
            // color: '#E1E1E5',
            padding: '12px 16px',
          },
        },
        {
          props: { variant: 'outlined', disabled: false },
          style: {
            textTransform: 'none',
            border: '2px solid #010007',
            padding: '12px 16px',
            ':hover': {
              border: '2px solid #010007',
            },
          },
        },
        {
          props: { variant: 'outlined', disabled: true },
          style: {
            textTransform: 'none',
            color: '#E1E1E5',
            border: '2px solid #E1E1E5',
            padding: '12px 16px',
            ':hover': {
              border: '2px solid #E1E1E5',
            },
          },
        },
        {
          props: { variant: 'text', disabled: false },
          style: {
            textTransform: 'none',
            padding: '12px 16px',
          },
        },
        {
          props: { variant: 'text', disabled: true },
          style: {
            textTransform: 'none',
            padding: '12px 16px',
          },
        },
        {
          props: { variant: 'link', color: 'primary' },
          style: ({ theme }) => ({
            color: theme.palette.primary.main,
            fontSize: 'inherit',
            textTransform: 'none',
            textDecoration: 'underline',
            backgroundColor: 'transparent',
            padding: '0px 2px',
            bottom: '1px',
            borderRadius: '100px',
            ':hover': {
              textTransform: 'none',
              textDecoration: 'underline',
              backgroundColor: 'transparent',
            },
          }),
        },
      ],
    },
    MuiFormLabel: {
      styleOverrides: {
        root: {
          color: '#010007',
        },
      },
    },
    MuiInputBase: {
      styleOverrides: {
        root: {
          // borderBottom: '2px solid #010007',
          fontSize: '16px',
        },
      },
    },
    MuiTextField: {
      variants: [
        {
          props: { variant: 'reddit' },
          style: ({ theme }) => ({
            '.MuiFilledInput-root': {
              border: '1px solid #e2e2e1',
              overflow: 'hidden',
              borderRadius: 4,
              backgroundColor: theme.palette.mode === 'light' ? '#fcfcfb' : '#2b2b2b',
              transition: theme.transitions.create(['border-color', 'background-color', 'box-shadow']),
              ':hover': {
                backgroundColor: 'transparent',
              },
              '.Mui-focused': {
                backgroundColor: 'transparent',
                // boxShadow: `${alpha(theme.palette.primary.main, 0.25)} 0 0 0 2px`,
                borderColor: theme.palette.primary.main,
              },
            },
          }),
        },
      ],
    },
    MuiInput: {
      styleOverrides: {
        root: {
          ':before': {
            borderBottom: 'none',
          },
          ':after': {
            borderBottom: '1px solid #010007',
          },
          ':hover:not(.Mui-disabled):before': {
            borderBottom: 'none',
          },
          '&.Mui-error:after': { borderBottom: 'none' },
        },
        input: {
          paddingTop: '8px',
          paddingBottom: '16px',
        },
      },
    },
  },
};

export default Theme;
