import React from "react";
import { createTheme, ThemeProvider } from "@mui/material";
import Theme from "./Theme";

const ThemeManager = (props) => {
  const customTheme = { ...Theme };
  customTheme.palette.primary.main = props.color.value;
  return (
    <ThemeProvider theme={createTheme(customTheme)}>
      {props.children}
    </ThemeProvider>
  );
};

export default ThemeManager;
