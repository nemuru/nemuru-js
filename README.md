# Nemuru.js

### Goal

Build a Javascript SDK with UI components for easily integrate in third-party webpages.

[Nemuru docs](https://docs.dev.nemuru.io) includes detailed guides to implement our Javascript SDK.

### Usage

```shell
# Install dependencies
npm run install-all

# Install specific version of a package in all sub-repos
npm run install-all --package=@nemuru-eng/js-api-client@1.7.8

# Start [dev]
npm run start-dev

# Start [stg]
npm run start-stg

# Start [prod]
npm run start-prod

# Build nemuru.dev.js [dev]
npm run build-dev

# Build nemuru.stg.js [stg]
npm run build-stg

# Build nemuru.prod.js [prod]
npm run build-prod

# Publish to S3 bucket nemuru.dev.js [dev]
npm run deploy-dev

# Publish to S3 bucket nemuru.stg.js [stg]
npm run deploy-stg

# Publish to S3 bucket nemuru.prod.js [prod]
npm run deploy-prod
```

### Links

Setup React + Webpack 5

* https://www.youtube.com/watch?v=TOb1c39m64A

Loading fonts with webpack

* https://chriscourses.com/blog/loading-fonts-webpack

Loading svgs with webpack

* https://www.pluralsight.com/guides/how-to-load-svg-with-react-and-webpack
* https://fonts.google.com/icons

Different builds (prod and dev) with webpack

* https://webpack.js.org/plugins/define-plugin/

Setting up CDN in AWS

* https://www.youtube.com/watch?v=-DDGYzKtNwc

Javascript SDK in a serverless world

* https://www.youtube.com/watch?v=Tkmee1wEeqA

Dynamically load async script with React

* https://rangen.medium.com/dynamically-load-google-scripts-with-react-and-the-useeffect-hook-3700b908e50f

### Typescript

Typescript and PropTypes

* https://blog.logrocket.com/comparing-typescript-and-proptypes-in-react-applications/
* https://www.npmjs.com/package/babel-plugin-typescript-to-proptypes

The `babel-plugin-typescript-to-proptypes` only works with React components (proptypes = props in components), and this
plugin DOES NOT support converting props who's type information is referenced in another file, as Babel has no access to
this information, and it does not run TypeScript's type checker.


