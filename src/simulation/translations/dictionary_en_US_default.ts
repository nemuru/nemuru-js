import { dictionary_es_ES_default } from './dictionary_es_ES_default';

export const dictionary_en_US_default: Partial<typeof dictionary_es_ES_default> = {
  PAY_IN_INSTALMENTS_FROM: 'Pay in instalments starting at',
  PAY_IN_INSTALMENTS_FOR: 'Pay in instalments of',
  MONTHLY: 'per month',
  MONTHLY_IN: 'per month in',
  MONTHLY_IN_2: '/month in',
  SPLIT_IN: 'Split in',
  PAYMENTS_OF: 'payments of',
  INFO: 'info',
  MONTHS: 'months',
  MONTH: 'month',
  COST_INCLUDED: 'Cost included',
  TIN: 'Interest',
  TAE: 'APR',
  FOR: 'For',
};
