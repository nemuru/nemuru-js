import { dictionary_es_ES_default } from './dictionary_es_ES_default';

export const dictionary_ca_CA_default: Partial<typeof dictionary_es_ES_default> = {
  PAY_IN_INSTALMENTS_FROM: 'Paga-ho a plaços des de',
  PAY_IN_INSTALMENTS_FOR: 'Paga-ho a plaços per',
  MONTHLY: 'al mes',
  MONTHLY_IN: 'al mes en',
  MONTHLY_IN_2: '/mes en',
  SPLIT_IN: 'Divideix en',
  PAYMENTS_OF: 'pagaments de',
  INFO: 'info',
  MONTHS: 'mesos',
  MONTH: 'mes',
  COST_INCLUDED: 'Cost inclòs',
  TIN: 'TIN',
  TAE: 'TAE',
  FOR: 'Per',
};
