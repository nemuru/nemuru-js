import { Locale } from '@nemuru-eng/js-api-client';
import Branding from '../../shared/types/Style/Branding';
import { dictionary_ca_CA_default } from './dictionary_ca_CA_default';
import { dictionary_en_US_default } from './dictionary_en_US_default';
import { dictionary_es_ES_default } from './dictionary_es_ES_default';

export let translation: typeof dictionary_es_ES_default | undefined = undefined;

export const setTranslation = (locale: Locale, branding: Branding) => {
  const brandingMapped = BRANDING_MAP[branding.value as keyof typeof BRANDING_MAP] ?? Branding.DEFAULT; // @ts-ignore
  translation = TRANSLATION_MAP[brandingMapped][locale];
};

const BRANDING_MAP = {
  [Branding.IBERIA]: Branding.DEFAULT,
  [Branding.FINANPAY]: Branding.DEFAULT,
  [Branding.LENDING_HUB]: Branding.DEFAULT,
  [Branding.NEMURU]: Branding.DEFAULT,
};

const TRANSLATION_MAP = {
  [Branding.DEFAULT]: {
    [Locale.ES_ES]: dictionary_es_ES_default,
    [Locale.CA_CA]: dictionary_ca_CA_default,
    [Locale.EN_US]: dictionary_en_US_default,
  },
};
