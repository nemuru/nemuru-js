// @ts-ignore
import styles from '!!raw-loader!./styles.css';
import { Configuration, LoanCalculatorDto, Product } from '@nemuru-eng/js-api-client';
import React from 'react';
import ReactDOM from 'react-dom';
import { calculateConditions } from '../calculateConditions';
import * as learnMore from '../learnMore/LearnMore';
import InMemoryStorage from '../shared/storage/InMemoryStorage';
import Branding from '../shared/types/Style/Branding';
import Color from '../shared/types/Style/Color';
import Style from '../shared/types/Style/Style';
import VariantSimulation from '../shared/types/Style/VariantSimulation';
import { filterLoanCalculatorsByBranding } from '../shared/utils/filterLoanCalculatorsByBranding';
import Simulation from './components/Simulation';

// https://gilfink.medium.com/wrapping-react-components-inside-custom-elements-97431d1155bd
// https://www.peerigon.com/en/blog/using-framework-agnostic-web-components-in-your-react-application/

export class SimulationElement extends HTMLElement {
  async connectedCallback() {
    const command = this.getData();
    const props = await this.getProps(command);
    if (!props || !props.loanCalculators?.length) return;
    const font = document.createElement('link');
    font.rel = 'stylesheet';
    font.href = 'https://fonts.googleapis.com/css2?family=Poppins:wght@400;700&display=swap';
    this.appendChild(font);
    this.attachShadow({ mode: 'open' });
    // // Without adding styles (no need to create div + style tags
    // if (this.shadowRoot) {
    //   ReactDOM.render(<Simulation {...props} />, this.shadowRoot);
    // }
    const template = document.createElement('template');
    template.innerHTML = `
        <body>
          <style>
              ${styles}
          </style>
          <div id="root"></div>
        </body>
    `;
    if (this.shadowRoot) {
      this.shadowRoot.appendChild(template.content.cloneNode(true));
      ReactDOM.render(<Simulation {...props} />, this.shadowRoot.getElementById('root'));
    }
  }

  disconnectedCallback() {
    ReactDOM.unmountComponentAtNode(this);
  }

  getData(): SimulationAttributes {
    const amountAttr = this.getAttribute('conditions-amount');
    if (!amountAttr) throw Error('The <amount> attribute is missing');
    const amount = parseFloat(amountAttr) / 100;

    const productAttr = this.getAttribute('conditions-product');
    if (!productAttr) throw Error('The <product> attribute is missing');
    const product = productAttr as Product;

    const color = this.getAttribute('style-color') ?? Color.DEFAULT;
    const variant = this.getAttribute('style-variant') ?? VariantSimulation.TEXT;
    const branding = this.getAttribute('style-branding') ?? undefined;

    const style = new Style(new Color(color), new VariantSimulation(variant), new Branding(branding));

    return {
      amount,
      product,
      style,
    };
  }

  async getProps(attributes: SimulationAttributes): Promise<SimulationProps | undefined> {
    const { style, product } = attributes;

    let loanCalculators = await calculateConditions({
      conditions: {
        amount: attributes.amount,
        product: attributes.product,
      },
      onError: (error) => {
        console.log(error);
      },
    });

    // Filter loanCalculators based on Branding
    if (loanCalculators) loanCalculators = filterLoanCalculatorsByBranding(loanCalculators, attributes.style.branding);

    const availableTerms: number[] | undefined = loanCalculators?.map((loanCalculator) => loanCalculator.term);

    const handleOpenDialog: (currentLoanCalculator: LoanCalculatorDto) => void = (currentLoanCalculator) => {
      learnMore.open(style, currentLoanCalculator, availableTerms);
    };

    const configuration = InMemoryStorage.configuration;

    if (!configuration) return;

    return {
      product,
      configuration,
      loanCalculators,
      style,
      handleOpenDialog,
    };
  }
}

interface SimulationAttributes {
  amount: number;
  product: Product;
  style: Style;
}

export interface SimulationProps {
  product: Product;
  configuration: Configuration;
  loanCalculators: LoanCalculatorDto[] | undefined;
  style: Style;
  handleOpenDialog: (currentLoanCalculator: LoanCalculatorDto) => void;
}
