import { LoanCalculatorDto } from '@nemuru-eng/js-api-client';
import React from 'react';
import Style from '../../shared/types/Style/Style';
import { translation } from '../translations/Translation';
import DetailsCalculator from './DetailsCalculator';
import MoreInfo from './MoreInfo';
import { Product } from './Simulation';

const SelectCalculator: React.FC<Props> = ({
  currentLoanCalculator,
  loanCalculators,
  changeLoanCalculator,
  handleOpenDialog,
  style,
}) => {
  if (!currentLoanCalculator || !translation) return null;
  return (
    <div className={'select-calculator'}>
      <div style={{ display: 'block', marginRight: '16px' }}>
        <div className={'select-calculator'}>
          <p style={{ marginRight: '8px', display: 'contents' }}>
            <span style={{ marginRight: '4px' }}>{translation.PAY_IN_INSTALMENTS_FOR}</span>
            <strong style={{ marginRight: '4px' }}>{currentLoanCalculator?.instalmentTotalAmount.string}</strong>
            <span style={{ marginRight: '6px' }}>{translation.MONTHLY_IN}</span>
          </p>
          <select
            style={{ marginRight: '8px', borderRadius: '6px' }}
            onChange={(event) => changeLoanCalculator(parseInt(event.target.value))}
            value={currentLoanCalculator.term}
          >
            {loanCalculators.map((loanCalculator) => (
              <option key={loanCalculator.term} value={loanCalculator.term}>
                {loanCalculator.term}
              </option>
            ))}
          </select>
          <p style={{ marginRight: '8px' }}>{` ${translation.MONTHS}`}</p>
        </div>
        {currentLoanCalculator.product === Product.INSTALMENTS && (
          <DetailsCalculator currentLoanCalculator={currentLoanCalculator} />
        )}
      </div>
      <MoreInfo onClick={() => handleOpenDialog(currentLoanCalculator)} style={style} />
    </div>
  );
};

export default SelectCalculator;

interface Props {
  currentLoanCalculator: LoanCalculatorDto;
  loanCalculators: LoanCalculatorDto[];
  changeLoanCalculator: (term: number) => {};
  handleOpenDialog: (currentLoanCalculator: LoanCalculatorDto) => {};
  style: Style;
}
