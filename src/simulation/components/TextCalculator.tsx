import { LoanCalculatorDto } from '@nemuru-eng/js-api-client';
import React from 'react';
import Style from '../../shared/types/Style/Style';
import { translation } from '../translations/Translation';
import DetailsCalculator from './DetailsCalculator';
import MoreInfo from './MoreInfo';
import { Product } from './Simulation';

const TextCalculator: React.FC<Props> = ({ currentLoanCalculator, handleOpenDialog, style }) => {
  if (!currentLoanCalculator || !translation) return null;
  return (
    <div className={'text-calculator'}>
      {currentLoanCalculator.product === Product.INSTALMENTS && (
        <div style={{ marginRight: '16px' }}>
          <p>
            {`${translation.PAY_IN_INSTALMENTS_FROM} `}
            <strong>{currentLoanCalculator?.instalmentTotalAmount.string}</strong>
            {` ${translation.MONTHLY}`}
          </p>
          <DetailsCalculator currentLoanCalculator={currentLoanCalculator} />
        </div>
      )}
      {currentLoanCalculator.product === Product.SPLIT_PAY && (
        <p style={{ marginRight: '16px' }}>
          {`${translation.SPLIT_IN} ${currentLoanCalculator.term} ${translation.PAYMENTS_OF} `}
          <strong>{currentLoanCalculator?.instalmentTotalAmount.string}</strong>
          {`  ${translation.MONTHLY}`}
        </p>
      )}
      <MoreInfo onClick={() => handleOpenDialog(currentLoanCalculator)} style={style} />
    </div>
  );
};

export default TextCalculator;

interface Props {
  currentLoanCalculator: LoanCalculatorDto;
  handleOpenDialog: (currentLoanCalculator: LoanCalculatorDto) => {};
  style: Style;
}
