import { LoanCalculatorDto } from '@nemuru-eng/js-api-client';
import React from 'react';
import Style from '../../shared/types/Style/Style';
import { translation } from '../translations/Translation';
import DetailsCalculator from './DetailsCalculator';
import MoreInfo from './MoreInfo';
import { Product } from './Simulation';

const SelectBannerCalculator: React.FC<Props> = ({
  currentLoanCalculator,
  loanCalculators,
  changeLoanCalculator,
  handleOpenDialog,
  style,
}) => {
  const prevLoanCalculator = () => {
    if (!prevDisabled) {
      const prevLoanCalculator = loanCalculators[loanCalculators.indexOf(currentLoanCalculator) - 1];
      changeLoanCalculator(prevLoanCalculator.term);
    }
  };

  const nextLoanCalculator = () => {
    if (!nextDisabled) {
      const nextLoanCalculator = loanCalculators[loanCalculators.indexOf(currentLoanCalculator) + 1];
      changeLoanCalculator(nextLoanCalculator.term);
    }
  };

  const prevDisabled = loanCalculators.indexOf(currentLoanCalculator) === 0;
  const nextDisabled = loanCalculators.indexOf(currentLoanCalculator) === loanCalculators.length - 1;

  if (!currentLoanCalculator || !translation) return null;
  return (
    <div className={'select-banner-calculator'}>
      <div style={{ display: 'block', marginRight: '16px' }}>
        <div
          style={{
            display: 'inline-flex',
            flexWrap: 'wrap',
            alignItems: 'center',
          }}
        >
          <p style={{ display: 'contents' }}>
            <span style={{ marginRight: '4px' }}>{translation.FOR}</span>
            <strong>{currentLoanCalculator?.instalmentTotalAmount.string}</strong>
            <span>{translation.MONTHLY_IN_2}</span>
          </p>
          <div style={{ display: 'flex' }}>
            <button className={'button'} onClick={prevLoanCalculator}>
              <svg
                role="img"
                width={18}
                height={18}
                viewBox={'0 0 22 22'}
                style={{ fill: prevDisabled ? '#ccc' : '#000' }}
              >
                <path d="M14.71,6.71 C14.32,6.32 13.69,6.32 13.3,6.71 L8.71,11.3 C8.32,11.69 8.32,12.32 8.71,12.71 C9.8616019,13.8616019 10.7253033,14.7253033 11.3011043,15.3011043 C11.7453033,15.7453033 12.4116019,16.4116019 13.3,17.3 C13.69,17.69 14.32,17.69 14.71,17.3 C15.1,16.91 15.1,16.28 14.71,15.89 L10.83,12 L14.71,8.12 C15.1,7.73 15.09,7.09 14.71,6.71 Z"></path>
              </svg>
            </button>
            <p>{`${currentLoanCalculator.term} ${translation.MONTHS}`}</p>
            <button className={'button'} onClick={nextLoanCalculator}>
              <svg
                role="img"
                width={18}
                height={18}
                viewBox={'4 0 22 22'}
                style={{ fill: nextDisabled ? '#ccc' : '#000' }}
              >
                <path
                  transform="translate(13.500000, 12.000000) scale(-1, 1) translate(-13.500000, -12.000000)"
                  d="M14.71,6.71 C14.32,6.32 13.69,6.32 13.3,6.71 L8.71,11.3 C8.32,11.69 8.32,12.32 8.71,12.71 C9.8616019,13.8616019 10.7253033,14.7253033 11.3011043,15.3011043 C11.7453033,15.7453033 12.4116019,16.4116019 13.3,17.3 C13.69,17.69 14.32,17.69 14.71,17.3 C15.1,16.91 15.1,16.28 14.71,15.89 L10.83,12 L14.71,8.12 C15.1,7.73 15.09,7.09 14.71,6.71 Z"
                ></path>
              </svg>
            </button>
          </div>
        </div>
        {currentLoanCalculator.product === Product.INSTALMENTS && (
          <DetailsCalculator currentLoanCalculator={currentLoanCalculator} />
        )}
      </div>
      <MoreInfo onClick={() => handleOpenDialog(currentLoanCalculator)} style={style} />
    </div>
  );
};

export default SelectBannerCalculator;

interface Props {
  currentLoanCalculator: LoanCalculatorDto;
  loanCalculators: LoanCalculatorDto[];
  changeLoanCalculator: (term: number) => {};
  handleOpenDialog: (currentLoanCalculator: LoanCalculatorDto) => {};
  style: Style;
}
