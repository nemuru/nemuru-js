import React from 'react';
import SelectBannerCalculator from './SelectBannerCalculator';
import SelectCalculator from './SelectCalculator';
import TextBannerCalculator from './TextBannerCalculator';
import TextCalculator from './TextCalculator';
import { setTranslation } from '../translations/Translation';

// Used React class components to avoid rule of hooks error
// when invoking this component from src/Library/Simulation/Simulation
// web component (React component instantiated from inside a class).
class Simulation extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      currentLoanCalculator: props.loanCalculators
        ? props.loanCalculators[props.loanCalculators.length - 1]
        : undefined,
      translation: setTranslation(props.configuration.locale, props.style.branding.value),
    };
    this.changeLoanCalculator = this.changeLoanCalculator.bind(this);
  }

  changeLoanCalculator(term) {
    const currentLoanCalculator = this.props.loanCalculators?.find((loanCalculator) => loanCalculator.term === term);
    this.setState({ currentLoanCalculator });
  }

  render() {
    const { currentLoanCalculator } = this.state;
    const { product, loanCalculators, style, handleOpenDialog } = this.props;
    if (!loanCalculators) return null;
    return (
      <>
        {[VariantSimulation.TEXT, VariantSimulation.SELECT].includes(style.variant.value) &&
          product === Product.SPLIT_PAY && (
            <TextCalculator
              currentLoanCalculator={currentLoanCalculator}
              handleOpenDialog={handleOpenDialog}
              style={style}
            />
          )}
        {[VariantSimulation.TEXT_BANNER, VariantSimulation.SELECT_BANNER].includes(style.variant.value) &&
          product === Product.SPLIT_PAY && (
            <TextBannerCalculator
              currentLoanCalculator={currentLoanCalculator}
              handleOpenDialog={handleOpenDialog}
              style={style}
            />
          )}
        {[VariantSimulation.TEXT].includes(style.variant.value) && product === Product.INSTALMENTS && (
          <TextCalculator
            currentLoanCalculator={currentLoanCalculator}
            handleOpenDialog={handleOpenDialog}
            style={style}
          />
        )}
        {[VariantSimulation.TEXT_BANNER].includes(style.variant.value) && product === Product.INSTALMENTS && (
          <TextBannerCalculator
            currentLoanCalculator={currentLoanCalculator}
            handleOpenDialog={handleOpenDialog}
            style={style}
          />
        )}
        {[VariantSimulation.SELECT].includes(style.variant.value) && product === Product.INSTALMENTS && (
          <SelectCalculator
            currentLoanCalculator={currentLoanCalculator}
            loanCalculators={loanCalculators}
            changeLoanCalculator={this.changeLoanCalculator}
            handleOpenDialog={handleOpenDialog}
            style={style}
          />
        )}
        {[VariantSimulation.SELECT_BANNER].includes(style.variant.value) && product === Product.INSTALMENTS && (
          <SelectBannerCalculator
            currentLoanCalculator={currentLoanCalculator}
            loanCalculators={loanCalculators}
            changeLoanCalculator={this.changeLoanCalculator}
            handleOpenDialog={handleOpenDialog}
            style={style}
          />
        )}
      </>
    );
  }
}

export const Product = {
  SPLIT_PAY: 'split_pay',
  INSTALMENTS: 'instalments',
};

export const VariantSimulation = {
  TEXT: 'text',
  SELECT: 'select',
  TEXT_BANNER: 'text_banner',
  SELECT_BANNER: 'select_banner',
};

export default Simulation;
