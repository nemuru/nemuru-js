import { LoanCalculatorDto } from '@nemuru-eng/js-api-client';
import React from 'react';
import { translation } from '../translations/Translation';

const DetailsCalculator: React.FC<{ currentLoanCalculator: LoanCalculatorDto }> = ({ currentLoanCalculator }) => {
  if (!currentLoanCalculator || !translation) return null;
  return (
    <p className={'caption'}>
      <span
        style={{ flexWrap: 'nowrap', display: 'inline-block' }}
      >{`${translation.COST_INCLUDED}: ${currentLoanCalculator?.instalmentFeeAmount.string}/${translation.MONTH}`}</span>
      {` | `}
      <span
        style={{ flexWrap: 'nowrap', display: 'inline-block' }}
      >{`${translation.TIN}: ${currentLoanCalculator?.annualInterest.string}`}</span>
      {` | `}
      <span
        style={{
          flexWrap: 'nowrap',
          display: 'inline-block',
        }}
      >{`${translation.TAE}: ${currentLoanCalculator?.apr.string}`}</span>
    </p>
  );
};

export default DetailsCalculator;
