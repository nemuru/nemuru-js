import React from 'react';
import Branding from '../../shared/types/Style/Branding';
import Style from '../../shared/types/Style/Style';
import { translation } from '../translations/Translation';

const MoreInfo: React.FC<{ onClick: () => void; style: Style }> = ({ onClick, style }) => {
  return (
    <div style={{ height: 'fit-content', display: 'flex' }}>
      <a className={'link'} onClick={onClick} style={{ color: style.color.value }}>
        {`+${translation?.INFO}`}
      </a>
      {style?.branding.value === Branding.LENDING_HUB && (
        <img
          src={'https://cdn.nemuru.com/assets/logos/quix-logo.svg'}
          style={{ height: '18px', marginLeft: '6px', alignSelf: 'center' }}
        />
      )}
    </div>
  );
};

export default MoreInfo;
