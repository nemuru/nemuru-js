import { LoanCalculatorDto } from '@nemuru-eng/js-api-client';
import React from 'react';
import Style from '../../shared/types/Style/Style';
import TextCalculator from './TextCalculator';

const TextBannerCalculator: React.FC<Props> = ({ currentLoanCalculator, handleOpenDialog, style }) => {
  return (
    <div className={'text-banner-calculator'}>
      <TextCalculator currentLoanCalculator={currentLoanCalculator} handleOpenDialog={handleOpenDialog} style={style} />
    </div>
  );
};

export default TextBannerCalculator;

interface Props {
  currentLoanCalculator: LoanCalculatorDto;
  handleOpenDialog: (currentLoanCalculator: LoanCalculatorDto) => {};
  style: Style;
}
