import { isString } from '@nemuru-eng/js-api-client';
import React from 'react';
import ReactDOM from 'react-dom';
import ErrorBoundary from '../shared/components/ErrorBoundary/ErrorBoundary';
import InMemoryStorage from '../shared/storage/InMemoryStorage';
import Branding from '../shared/types/Style/Branding';
import Color from '../shared/types/Style/Color';
import Style from '../shared/types/Style/Style';
import VariantCheckout from '../shared/types/Style/VariantCheckout';
import { handleError } from '../shared/utils/handleError';
import Checkout from './Checkout';
import { CheckoutOptions } from './index';

export interface CheckoutOptionsParsed extends Omit<CheckoutOptions, 'style'> {
  style: Style;
}

export const mountCheckout = (options: CheckoutOptions) => {
  if (!InMemoryStorage.userId || !InMemoryStorage.configuration) return;

  let validOptions: CheckoutOptionsParsed | undefined;

  try {
    validOptions = validate(options);
  } catch (err) {
    handleError({ err, onError: options.onError });
  }

  if (!validOptions) return;

  ReactDOM.render(
    <ErrorBoundary onError={validOptions.onError}>
      <Checkout options={validOptions} />
    </ErrorBoundary>,
    document.getElementById(validOptions.container),
  );
};

const validate = (options: CheckoutOptions): CheckoutOptionsParsed => {
  if (!isString(options.container)) {
    throw new Error(`<container> with value '${options.container}' is not a valid string`);
  }

  const style = new Style(
    new Color(options?.style?.color ?? Color.DEFAULT),
    new VariantCheckout(options?.style?.variant ?? VariantCheckout.MODAL),
    new Branding(options?.style?.branding),
  );

  if (options?.onError && typeof options?.onError !== 'function') {
    throw new Error('<OnError> is not a valid function');
  }

  if (options?.onStatusChange && typeof options?.onStatusChange !== 'function') {
    throw new Error('<OnStatusChange> is not a valid function');
  }

  if (options?.onClose && typeof options?.onClose !== 'function') {
    throw new Error('<OnClose> is not a valid function');
  }

  return { ...options, style };
};
