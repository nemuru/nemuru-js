import { ErrorParams } from '../shared/types/shared/ErrorParams';

export interface CheckoutOptions {
  container: string;
  orderId: string;
  accessToken: string;
  style: {
    color?: string;
    branding: string;
    variant?: string;
  };
  behaviour?: {
    disableClose?: boolean;
    disableFormEdition?: boolean;
    expirationTimeInMinutes?: number;
  };
  onStatusChange?: (status: string) => void;
  onClose?: (status: string) => void;
  onError?: (error: ErrorParams) => void;
}

// Static import
// import { mountCheckout } from './mountCheckout';
// export const checkout = async (options: CheckoutOptions) => {
//   mountCheckout(options);
// };

// // Dynamic import
export const checkout = async (options: CheckoutOptions) => {
  const { mountCheckout } = await import(/* webpackChunkName: "mount-checkout" */ './mountCheckout');
  mountCheckout(options);
};
