import {
  AmortizationPlanDto,
  Configuration,
  LoanCalculatorDto,
  Order,
  setCredentials,
  UnknownException,
} from '@nemuru-eng/js-api-client';
import React, { useEffect, useState } from 'react';
import ReactDOM from 'react-dom';
import Spinner from '../shared/components/Spinner/Spinner';
import useOrder from '../shared/hooks/useOrder';
import usePricings from '../shared/hooks/usePricings';
import InMemoryStorage from '../shared/storage/InMemoryStorage';
import { ErrorParams } from '../shared/types/shared/ErrorParams';
import Behaviour from '../shared/types/Style/Behaviour';
import Style from '../shared/types/Style/Style';
import { CheckoutOptionsParsed } from './mountCheckout';

require('./CheckoutWidgetComponent');

const Checkout: React.FC<{ options: CheckoutOptionsParsed }> = ({ options }) => {
  const { userId, configuration } = InMemoryStorage;

  const [loading, setLoading] = useState<boolean>(true);
  const [error, setError] = useState<Error | undefined>(undefined);

  if (error) {
    // Throws error to be caught by the parent ErrorBoundary
    // This is needed because errors throught in event handlers are not caught
    // https://reactjs.org/docs/error-boundaries.html
    throw error;
  }

  let tokenSet = false;

  const setToken = async (accessToken: string) => {
    await setCredentials({ accessToken, refreshToken: accessToken });
    tokenSet = true;
  };

  !tokenSet && setToken(options.accessToken);

  const { order, orderExists, getOrderById } = useOrder();
  const { loanCalculators, amortizationPlans, postLoanCalculator } = usePricings(options.style.branding);

  useEffect(() => {
    getLoan(options.orderId);
  }, []);

  const getLoan = async (orderId: string) => {
    try {
      await getOrderById(orderId);
    } catch (err) {
      if (err instanceof Error) {
        setError(err);
      }
    }
  };

  useEffect(() => {
    if (orderExists && loanCalculators === undefined) {
      doPostLoanCalculator();
    }
  }, [orderExists, loanCalculators]);

  const doPostLoanCalculator = async () => {
    try {
      if (order) {
        await postLoanCalculator(order.conditions.principal, order.conditions.product);
      }
    } catch (err) {
      if (err instanceof Error) {
        setError(err);
      }
    }
    setLoading(false);
  };

  if (!userId || !configuration) return null;

  const close = (status: string, dispatchCallback = true) => {
    if (dispatchCallback && options.onClose) {
      options.onClose(status);
    }
    const currentNode = document.getElementById(options.container);
    if (currentNode) {
      ReactDOM.unmountComponentAtNode(currentNode);
    }
  };

  if (loading) {
    return (
      <div
        style={{
          position: 'fixed',
          height: '100%',
          width: '100%',
          inset: 0,
          overflow: 'hidden',
          zIndex: 2147483647,
        }}
      >
        <Spinner />
      </div>
    );
  }

  if (!order || !loanCalculators?.length || !amortizationPlans) return null;

  return (
    <>
      <CheckoutWrapper
        env={ENV}
        accessToken={options.accessToken}
        configuration={configuration}
        order={order}
        loanCalculators={loanCalculators}
        amortizationPlans={amortizationPlans}
        style={options.style}
        behaviour={options.behaviour}
        handleStatusChange={options.onStatusChange}
        handleCloseDialog={close}
        handleError={(error: ErrorParams) =>
          setError(new UnknownException(error.message, error.method, error.url, error.status))
        }
      />
    </>
  );
};

const CheckoutWrapper: React.FC<CheckoutComponentProps> = (props) => {
  // @ts-ignore
  const CheckoutWidgetComponent = window.CheckoutWidgetComponent;

  if (!CheckoutWidgetComponent || !props) return null;

  return (
    <>
      <div
        style={{
          height: '100%',
          width: '100%',
          position: 'fixed',
          top: 0,
          left: 0,
          right: 0,
          bottom: 0,
          zIndex: 2147483647,
          overflow: 'hidden',
        }}
      >
        <CheckoutWidgetComponent {...props} />
      </div>
    </>
  );
};

export interface CheckoutComponentProps {
  env: typeof ENV;
  accessToken: string;
  configuration: Configuration;
  order: Order;
  loanCalculators: LoanCalculatorDto[];
  amortizationPlans: AmortizationPlanDto[];
  style: Style;
  behaviour?: Behaviour;
  handleStatusChange?: (status: string) => void;
  handleCloseDialog: (status: string, dispatchCallback?: boolean) => void;
  handleError: (error: ErrorParams) => void;
}

export default Checkout;
