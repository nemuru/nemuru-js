import { Configuration, Exception, setCredentials, UnknownException } from '@nemuru-eng/js-api-client';
import React, { useEffect, useState } from 'react';
import ReactDOM from 'react-dom';
import Spinner from '../shared/components/Spinner/Spinner';
import InMemoryStorage from '../shared/storage/InMemoryStorage';
import { ErrorParams } from '../shared/types/shared/ErrorParams';
import Style from '../shared/types/Style/Style';
import { OrderManagementOptionsParsed } from './mountOrderManagement';

require('./OrderManagementWidgetComponent');

const OrderManagement: React.FC<{ options: OrderManagementOptionsParsed }> = ({ options }) => {
  const { userId, configuration } = InMemoryStorage;
  const [error, setError] = useState<Exception | undefined>(undefined);

  if (error) {
    // Throws error to be caught by the parent ErrorBoundary
    // This is needed because errors throught in event handlers are not caught
    // https://reactjs.org/docs/error-boundaries.html
    throw error;
  }

  const [loading, setLoading] = useState<boolean>(true);

  let tokenSet = false;

  const setToken = async (accessToken: string) => {
    await setCredentials({ accessToken, refreshToken: accessToken });
    tokenSet = true;
  };

  !tokenSet && setToken(options.accessToken);

  useEffect(() => {
    setTimeout(() => {
      setLoading(false);
    }, 200);
  }, []);

  if (!userId || !configuration) return null;

  const close = (dispatchCallback = true) => {
    // @ts-ignore
    dispatchCallback && options.onClose();
    const currentNode = document.getElementById(options.container);
    if (currentNode) {
      ReactDOM.unmountComponentAtNode(currentNode);
    }
  };

  if (loading) {
    return (
      <div
        style={{
          position: 'fixed',
          height: '100%',
          width: '100%',
          inset: 0,
          overflow: 'hidden',
          zIndex: 2147483647,
        }}
      >
        <Spinner />
      </div>
    );
  }

  return (
    <>
      <OrderManagementWrapper
        env={ENV}
        accessToken={options.accessToken}
        configuration={configuration}
        orderId={options.orderId}
        style={options.style}
        handleCloseDialog={close}
        handleAction={options.onAction}
        handleError={(error: ErrorParams) =>
          setError(new UnknownException(error.message, error.method, error.url, error.status))
        }
      />
    </>
  );
};

const OrderManagementWrapper: React.FC<OrderManagementComponentProps> = (props) => {
  // @ts-ignore
  const OrderManagementWidgetComponent = window.OrderManagementWidgetComponent;

  if (!OrderManagementWidgetComponent || !props) return null;

  return (
    <>
      <div
        style={{
          height: '100%',
          width: '100%',
          position: 'fixed',
          top: 0,
          left: 0,
          right: 0,
          bottom: 0,
          zIndex: 2147483647,
          overflow: 'hidden',
        }}
      >
        <OrderManagementWidgetComponent {...props} />
      </div>
    </>
  );
};

export interface OrderManagementComponentProps {
  env: typeof ENV;
  accessToken: string;
  configuration: Configuration;
  orderId: string;
  style: Style;
  handleCloseDialog: (dispatchCallback?: boolean) => void;
  handleAction?: (action: string) => void;
  handleError: (error: ErrorParams) => void;
}

export default OrderManagement;
