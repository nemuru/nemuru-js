import { isString } from '@nemuru-eng/js-api-client';
import React from 'react';
import ReactDOM from 'react-dom';
import ErrorBoundary from '../shared/components/ErrorBoundary/ErrorBoundary';
import InMemoryStorage from '../shared/storage/InMemoryStorage';
import Branding from '../shared/types/Style/Branding';
import Color from '../shared/types/Style/Color';
import Style from '../shared/types/Style/Style';
import VariantOrderManagement from '../shared/types/Style/VariantOrderManagement';
import { handleError } from '../shared/utils/handleError';
import { OrderManagementOptions } from './index';
import OrderManagement from './OrderManagement';

export interface OrderManagementOptionsParsed extends Omit<OrderManagementOptions, 'style'> {
  style: Style;
}

export const mountOrderManagement = (options: OrderManagementOptions) => {
  if (!InMemoryStorage.userId || !InMemoryStorage.configuration) return;

  let validOptions: OrderManagementOptionsParsed | undefined;

  try {
    validOptions = validate(options);
  } catch (err) {
    handleError({ err, onError: options.onError });
  }

  if (!validOptions) return;

  ReactDOM.render(
    <ErrorBoundary onError={validOptions.onError}>
      <OrderManagement options={validOptions} />
    </ErrorBoundary>,
    document.getElementById(validOptions.container),
  );
};

const validate = (options: OrderManagementOptions): OrderManagementOptionsParsed => {
  if (!isString(options.container)) {
    throw new Error(`<container> with value '${options.container}' is not a valid string`);
  }

  const style = new Style(
    new Color(options?.style?.color ?? Color.DEFAULT),
    new VariantOrderManagement(VariantOrderManagement.MODAL),
    new Branding(options?.style?.branding),
  );

  if (options?.onError && typeof options?.onError !== 'function') {
    throw new Error('<OnError> is not a valid function');
  }

  if (options?.onAction && typeof options?.onAction !== 'function') {
    throw new Error('<OnAction> is not a valid function');
  }

  if (options?.onClose && typeof options?.onClose !== 'function') {
    throw new Error('<OnClose> is not a valid function');
  }

  return { ...options, style };
};
