import { ErrorParams } from '../shared/types/shared/ErrorParams';

export interface OrderManagementOptions {
  container: string;
  orderId: string;
  accessToken: string;
  style: {
    branding: string;
    color?: string;
  };
  onClose?: () => void;
  onAction?: (action: string) => void;
  onError?: (error: ErrorParams) => void;
}

// Static import
// import { mountOrderManagement } from './mountOrderManagement';
// export const orderManagement = async (options: OrderManagementOptions) => {
//   mountOrderManagement(options);
// };

// Dynamic import
export const orderManagement = async (options: OrderManagementOptions) => {
  const { mountOrderManagement } = await import(
    /* webpackChunkName: "mount-order-management" */ './mountOrderManagement'
  );
  mountOrderManagement(options);
};
