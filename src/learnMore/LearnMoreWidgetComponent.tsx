import React from 'react';
import ReactDOM from 'react-dom';
// @ts-ignore
import * as zoid from 'zoid/dist/zoid.frameworks';

interface LearnMoreWindow extends Window {
  LearnMoreWidgetComponent: any;
}

const w = window as any as LearnMoreWindow;

if (w.LearnMoreWidgetComponent === undefined) {
  w.LearnMoreWidgetComponent = zoid
    .create({
      tag: 'learn-more-component',
      url: LEARN_MORE_HTML_URL,
      dimensions: {
        width: '100%',
        height: `${window.screen.height}px`,
        // height: `${document.body.scrollHeight}px`,
      },
      // autoResize: {
      //   height: true,
      //   width: false,
      // },
      props: {
        loanCalculator: {
          type: 'object',
          required: true,
        },
        style: {
          type: 'object',
          required: true,
        },
        availableTerms: {
          type: 'array',
          required: false,
        },
        configuration: {
          type: 'object',
          required: true,
        },
        handleCloseDialog: {
          type: 'function',
          required: true,
        },
      },
      // @ts-ignore
      containerTemplate: function containerTemplate({ doc, uid, frame, prerenderFrame, event }) {
        let container = doc.createElement('div');
        event.on('zoid-rendered', () => {
          prerenderFrame.classList.remove('zoid-visible');
          prerenderFrame.classList.add('zoid-invisible');
          frame.classList.remove('zoid-invisible');
          frame.classList.add('zoid-visible');
          setTimeout(() => {
            prerenderFrame.style.display = 'none';
          }, 100);
        });
        const style = doc.createElement('style');
        style.innerText = `#${uid} {
            display: inline-block;
            position: relative;
            width: 100%;
            height: 945px;
            height: 100%;
            position: fixed;
            top: 0;
            left: 0;
            bottom: 0;
            right: 0;
            z-index: 9999999;
            overflow: hidden;
        } 
        
        #${uid} > iframe {
            display: inline-block;
            position: absolute;
            width: 100%;
            height: 100%;
            top: 0;
            left: 0;
        }`;
        container.appendChild(style);
        container.id = uid;
        container.appendChild(frame);
        container.appendChild(prerenderFrame);
        return container;
      },
    })
    .driver('react', {
      React: React,
      ReactDOM: ReactDOM,
    });
}
