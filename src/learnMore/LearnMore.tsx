import { Configuration, LoanCalculatorDto } from '@nemuru-eng/js-api-client';
import React from 'react';
import ReactDOM from 'react-dom';
import InMemoryStorage from '../shared/storage/InMemoryStorage';
import Style from '../shared/types/Style/Style';

require('./LearnMoreWidgetComponent');

const LearnMoreWrapper: React.FC<LearnMoreComponentProps> = (props) => {
  // @ts-ignore
  const LearnMoreWidgetComponent = window.LearnMoreWidgetComponent;

  if (!LearnMoreWidgetComponent || !props) return null;

  return (
    <div
      style={{
        height: '100%',
        width: '100%',
        position: 'fixed',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        zIndex: 2147483647,
        overflow: 'hidden',
      }}
    >
      <LearnMoreWidgetComponent {...props} />
    </div>
  );
};

export const open = (style: Style, loanCalculator?: LoanCalculatorDto, availableTerms?: number[]) => {
  const currentNode = document.getElementById('learn-more-nemuru');
  if (currentNode) {
    ReactDOM.unmountComponentAtNode(currentNode);
  } else {
    const node = document.createElement('div');
    node.setAttribute('id', 'learn-more-nemuru');
    document.body.appendChild(node);
  }
  if (!InMemoryStorage.configuration) return;
  ReactDOM.render(
    <LearnMoreWrapper
      loanCalculator={loanCalculator}
      style={style}
      availableTerms={availableTerms}
      configuration={InMemoryStorage.configuration}
      handleCloseDialog={close}
    />,
    document.getElementById('learn-more-nemuru'),
  );
};

export const close = () => {
  const currentNode = document.getElementById('learn-more-nemuru');
  if (currentNode) {
    ReactDOM.unmountComponentAtNode(currentNode);
    currentNode.remove();
  }
};

export interface LearnMoreComponentProps {
  loanCalculator?: LoanCalculatorDto;
  style: Style;
  availableTerms?: number[];
  configuration: Configuration;
  handleCloseDialog: () => void;
}
