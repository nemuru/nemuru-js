import { Pricing, Product } from '@nemuru-eng/js-api-client';

import InMemoryStorage from '../shared/storage/InMemoryStorage';

interface AvailableConditionsOptions {
  amount: number;
  products?: string[];
}

type AvailableConditionsOptionsParsed = AvailableConditionsOptions;

interface AvailableCondition {
  product: string;
  terms: number[];
}

export const availableConditions = (options: AvailableConditionsOptions): AvailableCondition[] | undefined => {
  if (!InMemoryStorage.userId || !InMemoryStorage.configuration) return;

  try {
    const validOptions = validate(options);

    const { pricings } = InMemoryStorage;
    if (!pricings) {
      return [];
    }

    let products = validOptions.products;
    if (!validOptions.products || validOptions.products.length === 0) {
      const uniqueProducts: Product[] = [];
      pricings.forEach((pricing) => {
        if (!uniqueProducts.includes(pricing.product)) {
          uniqueProducts.push(pricing.product);
        }
      });
      products = uniqueProducts;
    }

    const availableConditions: AvailableCondition[] = [];

    pricings.forEach((pricing) => {
      if (
        products?.includes(pricing.product) &&
        pricing.rangePrincipalMin.value <= validOptions.amount &&
        pricing.rangePrincipalMax.value >= validOptions.amount
      ) {
        const productAlreadyAdded = availableConditions.some((condition) => condition.product === pricing.product);
        if (productAlreadyAdded) {
          availableConditions.map((condition) => {
            if (condition.product === pricing.product) {
              const pricingTerms = getAllPricingTerms(pricing);
              return {
                ...condition,
                terms: [...condition.terms, ...pricingTerms].filter(onlyUnique),
              };
            } else {
              return condition;
            }
          });
        } else {
          availableConditions.push({
            product: pricing.product,
            terms: getAllPricingTerms(pricing),
          });
        }
      }
    });
    return availableConditions;
  } catch (err) {}
};

const validate = (options: AvailableConditionsOptions): AvailableConditionsOptionsParsed => {
  return options;
};

const getAllPricingTerms = (pricing: Pricing): number[] => {
  const pricingTerms: number[] = [];
  pricing.ranges.forEach((range) => {
    for (let i = range.rangePeriodMin; i <= range.rangePeriodMax; i++) {
      pricingTerms.push(i);
    }
  });
  return pricingTerms.filter(onlyUnique);
};

const onlyUnique = (value: number, index: number, self: number[]) => {
  return self.indexOf(value) === index;
};
