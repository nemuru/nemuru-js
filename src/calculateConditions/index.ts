import {
  Currency,
  getLoanCalculators,
  LoanCalculatorDto,
  Pricing,
  proposalsConstructor,
} from '@nemuru-eng/js-api-client';

import { availableConditions } from '../availableConditions';
import InMemoryStorage from '../shared/storage/InMemoryStorage';
import { handleError } from '../shared/utils/handleError';

interface CalculateConditionsOptions {
  conditions: {
    amount: number;
    product: string;
    terms?: number[];
  };
  onError?: (error: string) => void;
}

interface CalculateConditionsOptionsParsed extends CalculateConditionsOptions {
  currency: Currency;
}

export const calculateConditions = async (options: CalculateConditionsOptions) => {
  if (!InMemoryStorage.userId || !InMemoryStorage.configuration) return;
  try {
    const validOptions = validate(options, InMemoryStorage.configuration.currency);

    const { pricings } = InMemoryStorage;
    if (!pricings) {
      options.onError && options.onError('No pricings available');
      return;
    }

    let loanCalculators: LoanCalculatorDto[] = [];
    await pricings.reduce(async (prevPricing, pricing) => {
      await prevPricing;
      const calculators = await getLoanCalculatorsByAmountAndPricing({
        amount: validOptions.conditions.amount,
        currency: validOptions.currency,
        product: validOptions.conditions.product,
        terms: validOptions.conditions.terms,
        pricing,
      });
      if (calculators) {
        loanCalculators = [...loanCalculators, ...calculators];
      }
    }, Promise.resolve());

    return loanCalculators;
  } catch (err) {
    handleError({ err, onError: options.onError });
  }
};

const validate = (options: CalculateConditionsOptions, currency: Currency): CalculateConditionsOptionsParsed => {
  return { ...options, currency };
};

const getLoanCalculatorsByAmountAndPricing = ({
  amount,
  currency,
  pricing,
  product,
  terms,
}: {
  amount: number;
  currency: Currency;
  pricing: Pricing;
  product: string;
  terms?: number[];
}): LoanCalculatorDto[] | undefined => {
  if (product !== pricing.product || !InMemoryStorage.configuration) return;

  const conditions = availableConditions({
    amount,
    products: [product],
  });

  if (!conditions) return;

  const availableTerms = conditions.find((condition) => condition.product === product)?.terms;

  let calculateTerms: number[] = [];

  if (!terms && !availableTerms) {
    return [];
  } else if (!terms && availableTerms?.length) {
    calculateTerms = availableTerms;
  } else if (terms?.length && availableTerms?.length) {
    calculateTerms = terms.filter((term) => availableTerms.includes(term));
  }

  try {
    const proposals = proposalsConstructor({
      principal: { value: amount, currency },
      terms: calculateTerms,
      pricings: [pricing],
      product: pricing.product,
    });
    return getLoanCalculators({ proposals });
  } catch (err) {}
  return;
};
