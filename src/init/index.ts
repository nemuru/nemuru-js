import {
  Configuration,
  Currency,
  Environment,
  getPricingsByAgentId,
  isString,
  Locale,
  Separator,
  setEnvironment,
} from '@nemuru-eng/js-api-client';

import InMemoryStorage from '../shared/storage/InMemoryStorage';
import { handleError } from '../shared/utils/handleError';

interface InitOptions {
  clientId: string;
  configuration: {
    currency: string;
    decimalSeparator?: string;
    thousandSeparator?: string;
    locale: string;
  };
  onError?: (error: string) => void;
}

interface InitOptionsParsed extends InitOptions {
  configuration: Configuration;
}

export const init = async (options: InitOptions) => {
  try {
    const validOptions = validation(options);
    try {
      const userId = validOptions.clientId;
      await setEnvironment(ENV as Environment, validOptions.configuration);
      InMemoryStorage.userId = userId;
      InMemoryStorage.configuration = validOptions.configuration;
      InMemoryStorage.pricings = await getPricingsByAgentId({ agentId: userId });
    } catch (err) {
      if (validOptions.onError && err instanceof Error) {
        validOptions.onError(err.message);
      } else {
        throw err;
      }
    }

    const targetNode = document.body;

    const config = { childList: true };

    const callback = (mutationList: MutationRecord[], observer: MutationObserver) => {
      for (const mutation of mutationList) {
        if (mutation.type === 'childList') {
          mutation.addedNodes.forEach((node) => {
            if (node.nodeName.toLowerCase() === 'nemuru-simulation') {
              observer.disconnect();
            }
          });
        }
      }
    };

    const observer = new MutationObserver(callback);

    observer.observe(targetNode, config);

    if (!window.customElements.get('nemuru-simulation')) {
      const currentSimulations = document.getElementsByTagName('nemuru-simulation');

      if (currentSimulations.length > 0) {
        // console.log('nemuru-simulation elements found');
        await setSimulation();
      }

      if (currentSimulations.length === 0) {
        const targetNode = document.body;
        const config = { childList: true, subtree: true };
        const callback = async (mutationList: MutationRecord[], observer: MutationObserver) => {
          for (const mutation of mutationList) {
            if (mutation.type === 'childList') {
              await mutation.addedNodes.forEach(async (node) => {
                if (node.nodeName.toLowerCase() === 'nemuru-simulation') {
                  // console.log('new nemuru-simulation element added');
                  await setSimulation();
                  observer.disconnect();
                }
              });
            }
          }
        };
        const observer = new MutationObserver(callback);
        observer.observe(targetNode, config);
      }
    }
  } catch (err) {
    handleError({ err, onError: options.onError });
  }
};

const validation = (options: InitOptions): InitOptionsParsed => {
  if (!isString(options.clientId)) {
    throw Error('client_id is not valid');
  }
  if (options?.onError && typeof options?.onError !== 'function') {
    throw new Error('<OnError> is not a valid function');
  }
  return {
    ...options,
    configuration: new Configuration({
      currency: options.configuration.currency as Currency,
      decimalSeparator: (options.configuration?.decimalSeparator as Separator) || Separator.COMMA,
      thousandSeparator: (options.configuration?.thousandSeparator as Separator) || Separator.DOT,
      locale: options.configuration.locale as Locale,
    }),
  };
};

const setSimulation = async () => {
  const { SimulationElement } = await import(/* webpackChunkName: "simulation" */ '../simulation/Simulation');
  window.customElements.define('nemuru-simulation', SimulationElement);
};
