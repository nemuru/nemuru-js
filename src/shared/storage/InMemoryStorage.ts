import { Configuration, Pricing } from '@nemuru-eng/js-api-client';

const InMemoryStorage: Props = {
  userId: undefined,
  configuration: undefined,
  pricings: undefined,
};

export default InMemoryStorage;

interface Props {
  userId?: string;
  configuration?: Configuration;
  pricings?: Pricing[];
}
