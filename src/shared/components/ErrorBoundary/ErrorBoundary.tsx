import { Exception } from '@nemuru-eng/js-api-client';
import React, { ErrorInfo } from 'react';

import { ErrorParams } from '../../types/shared/ErrorParams';

// https://reactjs.org/docs/error-boundaries.html

class ErrorBoundary extends React.Component<Props, State> {
  readonly onError?: (error: ErrorParams) => void;

  constructor(props: Props) {
    super(props);
    this.state = { hasError: false };
    this.onError = props.onError;
  }

  static getDerivedStateFromError(error: Error) {
    // Update state so the next render will show the fallback UI.
    return { hasError: true };
  }

  componentDidCatch(error: Exception | Error, errorInfo: ErrorInfo) {
    // You can also log the error to an error reporting service
    if (this.onError) {
      if (error instanceof Exception) {
        this.onError({ message: error.message, url: error.url, method: error.method, status: error.status });
      } else {
        this.onError({ message: error.message });
      }
    }
  }

  componentDidUpdate(prevProps: Props, prevState: State) {
    if (prevState.hasError) {
      this.setState({ hasError: false });
    }
  }

  render() {
    if (this.state.hasError) {
      // You can render any custom fallback UI
      return <></>;
    }
    return this.props.children;
  }
}

export default ErrorBoundary;

interface Props {
  onError?: (error: ErrorParams) => void;
}

interface State {
  hasError: boolean;
}
