import { Exception } from '@nemuru-eng/js-api-client';

import { ErrorParams } from '../types/shared/ErrorParams';

export const handleError = ({ err, onError }: { err: Error | unknown; onError?: (error: ErrorParams) => void }) => {
  if (err instanceof Error) {
    if (onError) {
      if (err instanceof Exception) {
        onError({ message: err.message, url: err.url, method: err.method, status: err.status });
      } else {
        onError({ message: err.message });
      }
    }
    if (!onError) {
      console.error(err);
    }
  } else {
    console.error(err);
  }
};
