import { LoanCalculatorDto } from '@nemuru-eng/js-api-client';

import Branding from '../types/Style/Branding';

export const filterLoanCalculatorsByBranding = (
  loanCalculators: LoanCalculatorDto[],
  branding?: Branding,
): LoanCalculatorDto[] => {
  if (!branding) return loanCalculators;

  const ALLOWED_CONDITIONS_PER_BRANDING = {
    [Branding.LENDING_HUB]: { maxAPR: 0.4, maxPrincipal: 1200 },
  };

  const currentBranding = branding.value as keyof typeof ALLOWED_CONDITIONS_PER_BRANDING;

  const maxAPR = ALLOWED_CONDITIONS_PER_BRANDING[currentBranding]?.maxAPR;
  const maxPrincipal = ALLOWED_CONDITIONS_PER_BRANDING[currentBranding]?.maxPrincipal;
  // const allowedTerms = ALLOWED_CONDITIONS_PER_BRANDING[currentBranding]?.terms;
  // const minPrincipal = ALLOWED_CONDITIONS_PER_BRANDING[currentBranding]?.minPrincipal;

  return loanCalculators
    .filter((loanCalculator) => {
      if (maxAPR !== undefined) {
        if (loanCalculator.apr.value <= maxAPR) return loanCalculator;
      } else {
        return loanCalculator;
      }
    })
    .filter((loanCalculator) => {
      if (maxPrincipal !== undefined) {
        if (loanCalculator.principalAmount.value <= maxPrincipal) return loanCalculator;
      } else {
        return loanCalculator;
      }
    });
  // .filter((loanCalculator) => {
  //   if (allowedTerms !== undefined) {
  //     if (allowedTerms.includes(loanCalculator.term)) return loanCalculator;
  //   } else {
  //     return loanCalculator;
  //   }
  // })
  // .filter((loanCalculator) => {
  //   if (minPrincipal !== undefined) {
  //     if (loanCalculator.principalAmount.value >= minPrincipal) return loanCalculator;
  //   } else {
  //     return loanCalculator;
  //   }
  // })
};
