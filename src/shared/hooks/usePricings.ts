import {
  AmortizationPlanDto,
  getAmortizationPlans,
  getLoanCalculators,
  LoanCalculatorDto,
  Money,
  Product,
  proposalsConstructor,
} from '@nemuru-eng/js-api-client';
import { useState } from 'react';

import { availableConditions } from '../../availableConditions';
// import AvailableConditions from '../../availableConditions';
// import AvailableConditionsCommand from '../../AvailableConditions/AvailableConditionsCommand';
import InMemoryStorage from '../storage/InMemoryStorage';
import Branding from '../types/Style/Branding';
import { filterLoanCalculatorsByBranding } from '../utils/filterLoanCalculatorsByBranding';

const usePricings: (branding?: Branding) => Props = (branding) => {
  const [state, setState] = useState<State>({
    ...initialState,
  });

  const { loanCalculators, amortizationPlans } = state;

  const { pricings, configuration } = InMemoryStorage;

  const postLoanCalculator = async (amount: Money, product: Product) => {
    if (!configuration?.currency) return;

    const conditions = availableConditions({
      amount: amount.value,
      products: [product],
    });

    if (!conditions) throw new Error('No available conditions');

    const terms = conditions.find((condition) => condition.product === product)?.terms;
    // This assummes there's only on pricing per product
    const pricing = pricings?.find((pricing) => pricing.product === product);
    if (pricing && pricings && terms) {
      const proposals = proposalsConstructor({ principal: amount, terms, pricings, product });
      let loanCalculators = getLoanCalculators({ proposals });
      loanCalculators = filterLoanCalculatorsByBranding(loanCalculators, branding);
      if (loanCalculators.length === 0) {
        throw new Error('Loan calculators not available for given conditions');
      }
      const amortizationPlans = await getAmortizationPlans({ proposals });
      setState((s) => ({
        ...s,
        loanCalculators,
        amortizationPlans,
      }));
    }
  };

  const resetUserPricing = () => {
    setState((s) => ({ ...s, ...initialState }));
  };

  return {
    loanCalculators,
    amortizationPlans,
    postLoanCalculator,
    resetUserPricing,
  };
};

export default usePricings;

interface State {
  loanCalculators: LoanCalculatorDto[] | undefined;
  amortizationPlans: AmortizationPlanDto[] | undefined;
}

interface Props {
  loanCalculators?: LoanCalculatorDto[];
  amortizationPlans?: AmortizationPlanDto[];
  postLoanCalculator: (amount: Money, product: Product) => void;
  resetUserPricing: () => void;
}

const initialState = {
  loanCalculators: undefined,
  amortizationPlans: undefined,
};
