import { getOrderByLoanId, Order } from '@nemuru-eng/js-api-client';
import { useState } from 'react';

const useOrder: () => {
  order?: Order;
  orderExists?: boolean;
  getOrderById: (loanId: string) => void;
  resetUseOrder: () => void;
} = () => {
  const [state, setState] = useState<State>({
    ...initialState,
  });

  const { order, orderExists } = state;

  const getOrderById = async (orderId: string) => {
    const newOrder = await getOrderByLoanId({ loanId: orderId });
    setState((s) => ({
      ...s,
      order: newOrder,
      orderExists: true,
    }));
  };

  const resetUseOrder = () => {
    setState((s) => ({ ...s, ...initialState }));
  };

  return { order, orderExists, getOrderById, resetUseOrder };
};

export default useOrder;

interface State {
  order: Order | undefined;
  orderExists: boolean | undefined;
}

const initialState = {
  order: undefined,
  orderExists: undefined,
};
