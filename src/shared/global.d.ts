declare const API_URL: string;
declare const API_TOKEN_URL: string;
declare const ENV: string;
declare const CDN_URL: string;
declare const CHECKOUT_HTML_URL: string;
declare const LEARN_MORE_HTML_URL: string;
declare const ORDER_MANAGEMENT_HTML_URL: string;
