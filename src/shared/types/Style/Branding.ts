import { isString } from '@nemuru-eng/js-api-client';

class Branding {
  static readonly NEMURU = 'nemuru';
  static readonly LENDING_HUB = 'lending_hub';
  static readonly FINANPAY = 'finanpay';
  static readonly IBERIA = 'iberia';
  static readonly DEFAULT = 'default';

  static readonly ALLOWED_VALUES = [
    Branding.NEMURU,
    Branding.DEFAULT,
    Branding.LENDING_HUB,
    Branding.FINANPAY,
    Branding.IBERIA,
  ];

  readonly value: string;

  constructor(value?: string) {
    if (!value) throw new Error(`<${this.constructor.name}> is undefined`);
    this.guardIsValidString(value);
    this.guard(value);
    this.value = value;
  }

  private guardIsValidString(value: string): void {
    if (!isString(value)) {
      throw new Error(`<${this.constructor.name}> with value '${value}' is not a valid string`);
    }
  }

  private guard(value: string): void {
    if (!Branding.ALLOWED_VALUES.includes(value)) {
      throw new Error(`<${this.constructor.name}> does not allow the value: "${value}".`);
    }
  }
}

export default Branding;
