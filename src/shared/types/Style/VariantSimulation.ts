import Variant from './Variant';

class VariantSimulation extends Variant {
  static readonly TEXT = 'text';
  static readonly SELECT = 'select';
  static readonly TEXT_BANNER = 'text_banner';
  static readonly SELECT_BANNER = 'select_banner';

  static readonly ALLOWED_VALUES = [
    VariantSimulation.TEXT,
    VariantSimulation.SELECT,
    VariantSimulation.TEXT_BANNER,
    VariantSimulation.SELECT_BANNER,
  ];

  static readonly ALLOWED_VALUES_FOR_SPLIT_PAY = [VariantSimulation.TEXT, VariantSimulation.SELECT];

  constructor(value: string) {
    super(value);
    this.guard(value);
  }

  private guard(value: string): void {
    if (!VariantSimulation.ALLOWED_VALUES.includes(value)) {
      throw new Error(
        `<${
          this.constructor.name
        }> does not allow the value: "${value}". Allowed values are: [${VariantSimulation.ALLOWED_VALUES.join(', ')}]`,
      );
    }
  }
}

export default VariantSimulation;
