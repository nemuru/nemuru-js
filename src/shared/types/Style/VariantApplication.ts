import Variant from './Variant';

class VariantApplication extends Variant {
  static readonly TEXT = 'text';
  static readonly BUTTON = 'button';
  static readonly HIDDEN = 'hidden';

  static readonly ALLOWED_VALUES = [VariantApplication.TEXT, VariantApplication.BUTTON, VariantApplication.HIDDEN];

  constructor(value: string) {
    super(value);
    this.guard(value);
  }

  private guard(value: string): void {
    if (!VariantApplication.ALLOWED_VALUES.includes(value)) {
      throw new Error(
        `<${
          this.constructor.name
        }> does not allow the value: "${value}". Allowed values are: [${VariantApplication.ALLOWED_VALUES.join(', ')}]`,
      );
    }
  }
}

export default VariantApplication;
