import Branding from './Branding';
import Color from './Color';
import Variant from './Variant';

class Style {
  constructor(readonly color: Color, readonly variant: Variant, readonly branding: Branding) {
    this.color = color;
    this.variant = variant;
    this.branding = branding;
  }
}

export default Style;
