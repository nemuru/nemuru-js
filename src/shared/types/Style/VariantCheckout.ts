import Variant from './Variant';

class VariantCheckout extends Variant {
  static readonly MODAL = 'modal';
  static readonly EMBEDDED = 'embedded';

  static readonly ALLOWED_VALUES = [VariantCheckout.MODAL, VariantCheckout.EMBEDDED];

  constructor(value: string) {
    super(value);
    this.guard(value);
  }

  private guard(value: string): void {
    if (!VariantCheckout.ALLOWED_VALUES.includes(value)) {
      throw new Error(
        `<${
          this.constructor.name
        }> does not allow the value: "${value}". Allowed values are: [${VariantCheckout.ALLOWED_VALUES.join(', ')}]`,
      );
    }
  }
}

export default VariantCheckout;
