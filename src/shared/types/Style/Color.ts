import { isString } from '@nemuru-eng/js-api-client';

class Color {
  static readonly DEFAULT = '#3E6AE1';

  constructor(readonly value: string) {
    this.guardIsValidString(value);
    this.isValidHex(value);
    this.value = value;
  }

  private guardIsValidString(value: string): void {
    if (!isString(value)) {
      throw new Error(`<${this.constructor.name}> with value '${value}' is not a valid string`);
    }
  }

  private isValidHex(color: string): void {
    if (color.substring(0, 1) === '#') color = color.substring(1);

    let isValidHex = false;

    switch (color.length) {
      case 3:
        isValidHex = /^[0-9A-F]{3}$/i.test(color);
        break;
      case 6:
        isValidHex = /^[0-9A-F]{6}$/i.test(color);
        break;
      case 8:
        isValidHex = /^[0-9A-F]{8}$/i.test(color);
        break;
      default:
        break;
    }

    if (!isValidHex) {
      throw new Error(`<${this.constructor.name}> is not a valid hex color`);
    }
  }
}

export default Color;
