import Variant from './Variant';

class VariantOrderManagement extends Variant {
  static readonly MODAL = 'modal';
  static readonly EMBEDDED = 'embedded';

  static readonly ALLOWED_VALUES = [VariantOrderManagement.MODAL, VariantOrderManagement.EMBEDDED];

  constructor(value: string) {
    super(value);
    this.guard(value);
  }

  private guard(value: string): void {
    if (!VariantOrderManagement.ALLOWED_VALUES.includes(value)) {
      throw new Error(
        `<${
          this.constructor.name
        }> does not allow the value: "${value}". Allowed values are: [${VariantOrderManagement.ALLOWED_VALUES.join(
          ', ',
        )}]`,
      );
    }
  }
}

export default VariantOrderManagement;
