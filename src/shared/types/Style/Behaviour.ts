interface Behaviour {
  disableClose?: boolean;
  disableFormEdition?: boolean;
  expirationTimeInMinutes?: number;
}

export default Behaviour;
