import { isString } from '@nemuru-eng/js-api-client';

abstract class Variant {
  constructor(readonly value: string) {
    this.guardIsValidString(value);
    this.value = value;
  }

  private guardIsValidString(value: string): void {
    if (!isString(value)) {
      throw new Error(`<${this.constructor.name}> with value '${value}' is not a valid string`);
    }
  }
}

export default Variant;
