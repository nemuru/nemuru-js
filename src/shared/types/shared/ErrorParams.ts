export interface ErrorParams {
  message: string;
  url?: string;
  method?: string;
  status?: number;
}
