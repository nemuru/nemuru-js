const AWS = require('aws-sdk');
const path = require('path');
const fs = require('fs');

const BUCKET_NAME = 'nemuru-com-cdn';
const BACKUP_DIR = '.backup';

const env = process.env.NODE_ENV;

const directories = {
  './build': '',
  './widgets/Checkout/build': '/widgets/checkout',
  './widgets/LearnMore/build': '/widgets/learn-more',
  './widgets/OrderManagement/build': '/widgets/order-management',
};

// Set credentials
AWS.config.loadFromPath('./.aws/credentials.json');

// @ts-ignore
AWS.config.getCredentials((err) => {
  if (err) console.log(err.stack);
  else {
    console.log('Access key:', AWS.config.credentials.accessKeyId);
    console.log('Secret key:', AWS.config.credentials.secretAccessKey);
  }
});

// Set the Region
// AWS.config.update({ region: 'eu-west-1' });
console.log('Region: ', AWS.config.region);

// Create S3 service object
const s3 = new AWS.S3({ apiVersion: '2006-03-01' });

const backup = async () => {
  console.log('\n====================================');
  console.log('Backing up S3 to local...');
  console.log('====================================');

  const ts = new Date().toISOString();
  const prefix = `assets/${env}/`;
  const params = {
    Bucket: BUCKET_NAME,
    Prefix: prefix,
  };

  s3.listObjectsV2(params, async (err, data) => {
    if (err) {
      console.log(err, err.stack);
      return;
    }
    if (!data.Contents) return;

    for (const content of data.Contents) {
      if (content.Size === 0) continue;
      const readFile = content.Key;
      if (!readFile) continue;

      let directory = readFile.split('/');
      directory.pop();
      const newDirectory = `${BACKUP_DIR}/${env}/${ts}/${directory.join('/')}`;

      if (!fs.existsSync(newDirectory)) {
        console.log('\n-> Creating folder:', newDirectory);
        fs.mkdirSync(newDirectory, { recursive: true });
      }

      const writeFile = `/${BACKUP_DIR}/${env}/${ts}/${readFile}`;
      const readStream = await s3.getObject({ Bucket: BUCKET_NAME, Key: readFile }).createReadStream();
      const writeStream = fs.createWriteStream(path.join(__dirname, writeFile));
      readStream.pipe(writeStream);
      console.log(`\n-> Creating backup from ${BUCKET_NAME}: ${readFile} -> ${writeFile}`);
    }
  });
};

const cacheControlByFile = (readFile) => {
  if (readFile.includes('learn-more')) return 'public,max-age=86400';
  if (readFile.includes('nemuru-vendors.js')) return 'public,max-age=86400';
  if (readFile.includes('order-management')) return 'public,max-age=21600';
  if (readFile.includes('favicon.ico')) return 'public,max-age=604800';
  if (readFile.includes(`nemuru.${env}.js`)) return 'no-cache,no-store,private';
  return 'public,max-age=3600';
  // return 'no-cache,no-store,private';
};

const contentTypeByFile = (readFile) => {
  if (readFile.includes('.html')) return 'text/html';
  if (readFile.includes('.js')) return 'application/javascript';
  return;
};

const uploadFileToS3 = async (readFile, writeFile) => {
  const contentType = contentTypeByFile(readFile);
  const params = {
    Bucket: BUCKET_NAME,
    Key: writeFile,
    Body: Buffer.from(fs.readFileSync(readFile)),
    CacheControl: cacheControlByFile(readFile),
    ...(contentType && { ContentType: contentType }),
  };
  await s3.upload(params, (err, data) => {
    if (err) console.log('-> Upload error', err);
    if (data) console.log(`-> Upload success: ${readFile} -> `, data.Location);
  });
};

const upload = async () => {
  console.log('\n====================================');
  console.log('Uploading files to S3...');
  console.log('====================================');
  for (const directory of Object.keys(directories)) {
    fs.readdir(directory, (err, files) => {
      console.log(`\n${directory}`);
      console.log('====================================');
      files.forEach((file) => {
        const originFile = `${directory}/${file}`;
        // @ts-ignore
        const destinationFile = `assets/${env}${directories[directory]}/${file}`;
        console.log(originFile, '->', destinationFile);
        uploadFileToS3(originFile, destinationFile);
      });
    });
  }
};

const deploy = async () => {
  await backup();
  setTimeout(async () => {
    await upload();
  }, 3000);
};

deploy();
